"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"common/config",
	"common/util/StringsFunctions",
	"common/util/CoreValidator"
], function (App, P, constants, config) {

	App.module("Consumer.Settings.Models", function (Models, App, B, Mn, $, _) {
		Models.EquipmentSettingsPanelModel = B.Model.extend({
			defaults: {
				deviceModel: null,
				modelCollection: null
			}
		});

		Models.SettingsContentRowViewModel = B.Model.extend({
			defaults: {
				type: "readonly",
				value: null,
				collection: null,
				propertyModel: null
			},
			initialize: function (/*data, options*/) {
				this.revert();
				this.listenTo(this.propertyModel, "change:value", this.revert);
			},
			isDirty: function () {
				return  this.get("value") !== this.get("propertyModel").get("value");
			},
			revert: function () {
				this.set("value", this.get("propertyModel").get("value"));
			},
			save: function () {
				if (this.isDirty()) {
					return this.get("propertyModel").setProperty(this.get("value"));
				}
			}
		});

		Models.SettingsContentRowViewCollection = B.Collection.extend({
			model: Models.SettingsContentRowViewModel,
			isDirty: function () {
				return this.some(function (model) {
					return model.isDirty();
				});
			},
			revert: function () {
				this.each(function (model) {
					model.revert();
				});
			},

			/**
			 * Saves all settings that have changed.
			 * @param devices optional list of devices to save properties on.
			 * @returns {*}
			 */
			save: function (devices) {
				var that = this,
						hadLanModeProperty = false,
						settingLanModeToDynamic,
						promises = [],
						networkSettings = [],
                        // Bug_CASBW-22
                        dirtyModels = [];
				if (devices) {
					this.each(function (model) {
						var isLanModeProp = model.get("isNetworkLanModeProperty");
                        var pName = model.get("propertyModel").get("display_name"),
                            deviceProperty = null,
                            promise = null;
                        
						hadLanModeProperty = hadLanModeProperty || isLanModeProp;
						settingLanModeToDynamic = settingLanModeToDynamic || (!!isLanModeProp && model.get("value") === 1);
                        
                        if (model.get("isNetworkSetting")) {
                            networkSettings.push(model);
                        } else if ((!isLanModeProp || model.get("value") === 1) && model.isDirty()) { // either we arent the network lan mode property or we are and we are switching to dynamic
                            _.each(devices, function (device) {
                                deviceProperty = device.get(pName);
                                if (deviceProperty) {
                                    promise = deviceProperty.setProperty(model.get("value"));
                                    promises.push(promise);
                                }
                            });
                        }
					});
				} else {
					this.each(function (model) {
						var promise = model.save();
						if (promise) {
							promises.push(promise);
						}
					});
				}
                
                // Bug_CASBW-22 Start
                dirtyModels = _.filter(this.models, function(model) {
                    return model.isDirty();
                });
                
				return P.all(promises).then(function () {
                    if (hadLanModeProperty) {
						that.trigger("saved:networkSettings", settingLanModeToDynamic ? null : networkSettings, dirtyModels);
                    }
				});
                // Bug_CASBW-22 End
			}
		});

		var createX100RangeArray = function (min, max, increment) {
			var list = [],
				i = min;

			increment = (increment || 1);

			for (i; i <= max; i += increment) {
				list.push({
					value: i * 100,
					text: App.translate("equipment.property.values.numberInCFormatted", {number: i.toString()})
				});
			}
			return list;
		};

		var fullRangeMin = 5,
			fullRangeMax = 45,
			fullRangeIncriment = 0.5;

		Models.SettingsContentFactory = {
			fullTemperatureRange: createX100RangeArray(fullRangeMin, fullRangeMax, fullRangeIncriment),
			lockKeyArray: [
				{
					key: "equipment.property.values.locked",
					value: 1
				},
				{
					key: "equipment.property.values.unlocked",
					value: 0
				}
			],
			temperatureDisplayModeArray: [
				{
					key: "equipment.property.values.fahrenheit",
					value: 1
				},
				{
					key: "equipment.property.values.celsius",
					value: 0
				}
			],
			enabledDisabledArray: [
				{
					key: "equipment.property.values.enabled",
					value: 1
				},
				{
					key: "equipment.property.values.disabled",
					value: 0
				}
			],
			yesNoArray: [
				{
					key: "equipment.property.values.yes",
					value: 1
				},
				{
					key: "equipment.property.values.no",
					value: 0
				}
			],
			onOffArray: [
				{
					key: "equipment.property.values.on",
					value: 1
				},
				{
					key: "equipment.property.values.off",
					value: 0
				}
			],
			systemModeArray: [
				{
					key: "equipment.property.values.off",
					value: 0
				},
				{
					key: "equipment.property.values.auto",
					value: 1
				},
				{
					key: "equipment.property.values.cool",
					value: 3
				},
				{
					key: "equipment.property.values.heat",
					value: 4
				},
				{
					key: "equipment.property.values.emergencyHeating",
					value: 5
				}
			],
           

			_getPropFromDeviceOrDevices: function (deviceObj, propName) {
				if (_.isArray(deviceObj)) {
					return _.reduce(deviceObj, function (acc, device) {
						if (acc) {
							return acc;
						} else if (device.get(propName)) {
							return device.get(propName);
						} else {
							return false;
						}
					}, false);
				} else {
					return deviceObj.get(propName);
				}
			},
            getPropFromDeviceOrDevices: function(deviceObj, propName){
                return this._getPropFromDeviceOrDevices(deviceObj, propName);
            },
            
            createViewCollectionUnValidate: function (deviceModel) {
				//List defined at https://salusinc.atlassian.net/wiki/display/SCS/7+-+Thermostat+Control
				var that = this,
					propertyNames = [
						"LockKey",
						"ShutOffDisplay",
                        "FrostSetpoint_x100",
                        "TimeFormat24Hour"
					];

				var properties = _.compact(_.map(propertyNames, function (propertyName) {
					var prop = that._getPropFromDeviceOrDevices(deviceModel, propertyName);

					if (!prop) {
						return prop;
					}

					return that._createViewModel(prop);

				}));

				return new Models.SettingsContentRowViewCollection(properties);
			},

			createViewCollection: function (deviceModel) {


				//List defined at https://salusinc.atlassian.net/wiki/display/SCS/7+-+Thermostat+Control
				var that = this,
					propertyNames = [
						"LockKey",
                        "ShutOffDisplay",
						"KeyLock",
						"FrostSetpoint_x100",
						"TimeFormat24Hour",
						"TemperatureDisplayMode",
						"HeatingControl",
						"TemperatureOffset",
						"OUTSensorProbe",
						"OUTSensorType",
						"CylinderConnection",
						"CoolingControl",
						"ActuatorType",
						"ValveProtection",
						"MaxHeatSetpoint_x100",
						"MinCoolSetpoint_x100",
						"FloorCoolingMin_x100",
						"FloorHeatingMax_x100",
						"FloorHeatingMin_x100",
						"LEDMode",
						"NetworkLANMode",
						"NetworkLANIP",
						"NetworkLANSubnet",
						"NetworkLANRouterAddr",
						"NetworkPriDNS",
						"NetworkSecDNS",
                        "AllowUnlockFromDevice"
					];

				var properties = _.compact(_.map(propertyNames, function (propertyName) {
					var prop = that._getPropFromDeviceOrDevices(deviceModel, propertyName);

					if (!prop) {
						return prop;
					}

					return that._createViewModel(prop);

				}));

				return new Models.SettingsContentRowViewCollection(properties);
			},

			createSettingsViewCollection: function (deviceModel) {
				var properties = [], p;
				for (p in deviceModel.attributes) {
					if (deviceModel.attributes.hasOwnProperty(p)) {
						var prop = deviceModel.attributes[p];

						if (prop !== null && typeof prop === 'object') {
							if (prop.isLinkedDeviceProperty) {
								var viewModel = this._createViewModel(prop);

								if (viewModel) {
									properties.push(viewModel);
								}
							}
						}
					}
				}

				return new Models.SettingsContentRowViewCollection(properties);
			},

			/**
			 * Definitions at https://salusinc.atlassian.net/wiki/display/SCS/7+-+Thermostat+Control
			 * @param prop
			 * @returns {*}
			 * @private
			 */
			_createViewModel: function (prop) {
				if (!prop) {
					return null;
				}

				var propertyName = prop.get("display_name");
				if (!propertyName || propertyName.endsWith("_d") || propertyName.endsWith("_c")) {
					return null;
				}

				var options = {type: "readonly"};

				if (propertyName === "CoolingSetpoint_x100") {
					var list = Models.SettingsContentFactory.fullTemperatureRange.slice().reverse();
					options = {
						type: "dropdown",
						collection: list
					};
				} else if (propertyName === "HeatingControl") {
					options = {
						type: "dropdown",
						collection: [
							{
								text: App.translate("equipment.property.values.shortCyle"),
                                //text: "PWM",
								value: 0
							},
							{
								text: App.translate("equipment.property.values.midCyle"),
								value: 1
							},
							{
								text: App.translate("equipment.property.values.longCycle"),
								value: 2
							}
						]
					};
				} else if (propertyName === "TemperatureOffset") {
					options = {
						type: "dropdown",
						collection: createX100RangeArray(-3, 3, 0.5)
					};
				} else if (propertyName === "CylinderConnection") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.cylinderNotConnected",
								value: 0
							},
							{
								key: "equipment.property.values.cylinderConnected",
								value: 1
							}
						]
					};
				} else if (propertyName === "OUTSensorProbe") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.sensorNotConnected",
								value: 0
							},
							{
								key: "equipment.property.values.sensorConnected",
								value: 1
							}
						]
					};
				} else if (propertyName === "OUTSensorType") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.sensorAir",
								value: 0
							},
							{
								key: "equipment.property.values.sensorFloor",
								value: 1
							}
						]
					};
				} else if (propertyName === "CoolingControl") {
					options = {
						type: "dropdown",
						collection: [
							{
								text: App.translate("equipment.property.values.midCyle"),
								value: 1
							},
							{
								text: App.translate("equipment.property.values.longCycle"),
								value: 2
							}
						]
					};
				} else if (propertyName === "ActuatorType") {
					options = {
						type: "readonly",
						collection: [
							{
								key: "equipment.property.values.actuatorOpen",
								value: 0
							},
							{
								key: "equipment.property.values.actuatorClosed",
								value: 1
							}
						]
					};
				} else if (propertyName === "ValveProtection") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.enabledDisabledArray
					};
				} else if (propertyName === "FrostSetpoint_x100") {
					options = {
						type: "dropdown",
						collection: createX100RangeArray(5, 17, 0.5)
					};
				} else if (propertyName === "FloorCoolingMax_x100") {
					options = {
						type: "numberX100",
						validationMethod: function () {
							//"This" will be the EquipmentSettingsPanelContentRow object.
							//Range 6 - 45°C

							var value = this.model.get("value");
							return App.validate.intX100Range(value, 6, 45);
						}
					};
				} else if (propertyName === "FloorCoolingMin_x100") {
					options = {
						type: "numberX100",
						validationMethod: function () {
							//"This" will be the EquipmentSettingsPanelContentRow object.
							//Range 6 - 45°C

							var value = this.model.get("value");
							return App.validate.intX100Range(value, 6, 45);
						}
					};
				} else if (propertyName === "FloorHeatingMax_x100") {
					options = {
						type: "numberX100",
						validationMethod: function () {
							//"This" will be the EquipmentSettingsPanelContentRow object.
							//Range 11 - 45°C

							var value = this.model.get("value");
							return App.validate.intX100Range(value, 11, 45);
						}
					};
				} else if (propertyName === "FloorHeatingMin_x100") {
					options = {
						type: "numberX100",
						validationMethod: function () {
							//"This" will be the EquipmentSettingsPanelContentRow object.
							//Range 6 - 40°C

							var value = this.model.get("value");
							return App.validate.intX100Range(value, 6, 40);
						}
					};
				} else if (propertyName === "HeatingSetpoint_x100") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.fullTemperatureRange
					};
				} else if (propertyName === "HoldDuration") {
					options = {
						type: "text",
						validationMethod: function () {
							//"This" will be the EquipmentSettingsPanelContentRow object.
							//0 - 1440, 65535 (0xffff) means Permanent Hold

							var value = this.model.get("value");

							if (value === constants.thermostatHoldDuration) {
								return {
									valid: true
								};
							}

							return App.validate.intRange(value, 0, 1440);
						}
					};
				} else if (propertyName === "LockKey" || propertyName === "KeyLock") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.lockKeyArray
					};
				} else if (propertyName === "MaxHeatSetpoint_x100" ||
					propertyName === "MinCoolSetpoint_x100") {
					options = {
						type: "numberX100",
						step: 0.50,
						validationMethod: function () {
							//"This" will be the EquipmentSettingsPanelContentRow object.
							//Range 5 - 35°C

							var value = this.model.get("value");
							return App.validate.intX100Range(value, config.thermostatMinimumTemp, config.thermostatMaximumTemp);
						}
					};
				} else if (propertyName === "OnOff") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.onOffArray
					};
				} else if (propertyName === "ShutOffDisplay") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.turnOn",
								value: 0
							},
							{
								key: "equipment.property.values.turnOff",
								value: 1
							}
						]
					};
				} else if (propertyName === "SystemMode") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.systemModeArray
					};
				} else if (propertyName === "TemperatureDisplayMode") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.temperatureDisplayModeArray
					};
				} else if (propertyName === "TimeFormat24Hour") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.hour12",
								value: 0
							},
							{
								key: "equipment.property.values.hour24",
								value: 1
							}
						]
					};
				} else if (propertyName === "NetworkLANMode") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.static",
								value: 0
							},
							{
								key: "equipment.property.values.dynamic",
								value: 1
							}
						],
						isNetworkLanModeProperty: true
					};
				} else if (propertyName === "NetworkLANIP" || propertyName === "NetworkLANSubnet" ||
						propertyName === "NetworkLANRouterAddr" || propertyName === "NetworkPriDNS" || propertyName === "NetworkSecDNS") {
					options = {
						type: "text",
						isNetworkSetting: true,
						validationMethod: function () {
							return App.validate.ipAddress(this.model.get("value"));
						}
					};
				}  else if (propertyName === "LEDMode") {
					options = {
						type: "dropdown",
						collection: [
							{
								key: "equipment.property.values.alwaysOn",
								value: 1
							},
							{
								key: "equipment.property.values.offWhenIdle",
								value: 2
							}
						]
					};
				}else if (propertyName === "AllowUnlockFromDevice") {
					options = {
						type: "dropdown",
						collection: Models.SettingsContentFactory.yesNoArray
					};
				} 

				return new Models.SettingsContentRowViewModel(_.extend(options, {
					propertyModel: prop
				}));
			}
		};
	});

	return App.Consumer.Settings.Models;
});