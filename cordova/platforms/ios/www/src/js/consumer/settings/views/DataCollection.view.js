"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions"
], function (App, consumerTemplates) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {

		Views.DataCollectionLayout = Mn.LayoutView.extend({
			template: consumerTemplates["settings/dataCollectionLayout"],
			className: "container",
			regions: {
				"radioOn": ".bb-has-collection-on",
				"radioOff": ".bb-has-collection-off",
				"saveButton": ".bb-save-button"
			},
			ui: {
				infoSection: ".bb-data-collection-info"
			},
			events: {
				"change input[name=hasCollection]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleSaveButtonClicked", "_toggleInfoSectionHide");

				var dataCollectionOff = App.getDataCollectionOff();

				this.registerRegion("radioOn", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: true,
						name: "hasCollection",
						isChecked: !dataCollectionOff,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radioOff", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: false,
						name: "hasCollection",
						isChecked: !!dataCollectionOff,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.saveButtonView = this.registerRegion("saveButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.save",
					classes: "center-relative disabled",
					clickedDelegate: this.handleSaveButtonClicked
				}));
			},

			onRender: function () {
				this._toggleInfoSectionHide(!App.getDataCollectionOff());
			},

			_toggleInfoSectionHide: function (shouldHide) {
				if (!shouldHide) {
					this.ui.infoSection.show();
				} else {
					this.ui.infoSection.hide();
				}
			},

			handleRadioChange: function () {
				var selected = this.$("input[name=hasCollection]").filter(":checked").val() === "false";
				this.saveButtonView.enable();
				this._toggleInfoSectionHide(!selected);
			},
			handleSaveButtonClicked: function () {
				var selected = this.$("input[name=hasCollection]").filter(":checked").val(),
						value = selected === "false", that = this;

				this.saveButtonView.showSpinner();

				this.model.set("dataCollectionOff", value ? 1 : 0);
				this.model.saveDataCollection().then(function (dataCollectionOff) {
					that.saveButtonView.hideSpinner();

					if (!dataCollectionOff) {
						window.location.reload(true);
					}
				}).catch(function () {
					that.saveButtonView.hideSpinner();
				});
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.Settings.Views.DataCollectionLayout;
});