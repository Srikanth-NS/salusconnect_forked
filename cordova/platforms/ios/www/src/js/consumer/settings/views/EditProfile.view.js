"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/SalusModal.view",
	"consumer/settings/views/DeleteUserProfileModal.view",
	"consumer/models/SalusButtonPrimary",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusDropdown.view"
], function (App, config, constants, consumerTemplates,SalusPage, SalusViewMixin,
			 ConsumerMixin, SalusLinkButtonView, SalusModalView) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.EditProfilePage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/profile/editProfile"],
			id: "edit-profile",
			regions: {
				"firstNameRegion": ".bb-first-name",
				"lastNameRegion": ".bb-last-name",
				"emailRegion": ".bb-email",
				"languageRegion": ".bb-language",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button",
				"deleteProfileRegion": ".bb-delete-profile",
				"changePasswordRegion": ".bb-password"
			},
			initialize: function () {
				_.bindAll(this, "handleSubmitClicked", "validateForm", "handleCancelClicked", "handleChangePasswordClick");

				var languagesCollection = new App.Consumer.Models.DropdownItemViewCollection();

                
				this._fillDropdownCollection(this.getLanguageList(), config.languagePrefix, languagesCollection);

				this.firstNameField =  new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.formFields.firstName",
                    required: true,
					value: this.model.get("firstname")
				});

				this.lastNameField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.formFields.lastName",
					required: true,
					value: this.model.get("lastname")
				});

				this.emailField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.formFields.email",
					value: this.model.get("email"),
					readonly: true
				});

				this.languageDropdown = new App.Consumer.Views.SalusDropdownWithValidation({
					collection: languagesCollection,
					validationMethod: this._validateLanguageConfirm,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "languageSelect",
						dropdownLabelKey: "settings.profile.formFields.languageSelect"
					}),
					defaultSelectionValue: this.model.get("language"),
					boundAttribute: "language",
					model: this.model
				});

				this.submitButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "settings.profile.formFields.submitButton",
					classes: "width100",
					clickedDelegate: this.handleSubmitClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});

				this.deleteProfile = new SalusLinkButtonView({
					buttonText: App.translate("settings.profile.deleteProfile"),
					className: "delete-button btn btn-link margin-r-10",
					clickedDelegate: this.handleRemoveClick
				});

				this.changePasswordLink = new SalusLinkButtonView({
					buttonTextKey: "settings.profile.changePassword.title",
					className: "btn btn-link",
					clickedDelegate: this.handleChangePasswordClick
				});
              

				this.validationNeeded.push(this.firstNameField, this.lastNameField);

				this.attachValidationListener();

				this.userMetaPromise = App.salusConnector.getSessionUser().fetchMetaData();
			},
			onRender: function () {
				var that = this;

				this.showSpinner({textKey: constants.spinnerTextKeys.loading}, this.$el.find(".profile-info"));

				this.userMetaPromise.then(function () {
					that.hideSpinner();

					that.firstNameRegion.show(that.firstNameField);
					that.lastNameRegion.show(that.lastNameField);
					that.emailRegion.show(that.emailField);
					that.languageRegion.show(that.languageDropdown);
					that.submitButtonRegion.show(that.submitButton);
					that.cancelButtonRegion.show(that.cancelButton);
					that.deleteProfileRegion.show(that.deleteProfile);
					that.changePasswordRegion.show(that.changePasswordLink);
                    
				});
			},
            getLanguageList: function(){
               var list = config.languages;
               var lan = this.model.get("language");
               if(lan === "da"){
                   list = config.languages_da;
               }
               else if(lan === "nl"){
                   list = config.languages_nl;
               }
                else if(lan === "fr"){
                   list = config.languages_fr;
               }
                else if(lan === "de"){
                   list = config.languages_de;
               }
                else if(lan === "no"){
                   list = config.languages_no;
               }
                else if(lan === "pl"){
                   list = config.languages_pl;
               }
                else if(lan === "ro"){
                   list = config.languages_ro;
               }
                else if(lan === "ru"){
                   list = config.languages_ru;
               }
                else if(lan === "sv"){
                   list = config.languages_sv;
               }
               return list;
               
            },
			_fillDropdownCollection: function (list, prefix, collection) {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});
			},
			validateForm: function () {
				var valid = this.isValid();

				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			saveData: function () {
				var firstName, lastName;

				firstName = this.firstNameRegion.currentView.getValue();
				this.model.set("firstname", firstName);

				lastName = this.lastNameRegion.currentView.getValue();
				this.model.set("lastname", lastName);

				return this.model.persist();
			},
			handleSubmitClicked: function () {
				var that = this;

				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}

				this.submitButton.showSpinner();

				this.saveData().then(function () {
					that.submitButton.hideSpinner();
					var language = that.model.get("language");
					App.changeLanguage(language).catch(function (err) {
						//ingnore lanugage change error. will default to dev/english
						App.error("Language file " + language + " : " + err);
					}).then(function () {
						App.navigate("");
					});
				}).catch(function () {
					that.submitButton.hideSpinner();
				});

			},
			handleCancelClicked: function () {
				window.history.back();
			},
			handleRemoveClick: function (){
				App.modalRegion.show(new SalusModalView({
					contentView: new App.Consumer.Settings.Views.DeleteUserProfileModalView()
				}));
				App.showModal();
			},
			handleChangePasswordClick: function () {
				App.navigate("/settings/profile/changePassword");
			}
            
		}).mixin([SalusPage, ConsumerMixin.FormWithValidation], {
			analyticsSection: "settings",
			analyticsPage: "profile" //TODO: This should be a function so the equipment Id is recorded.
		});
	});


	return App.Consumer.Settings.Views.EditProfilePage;
});

