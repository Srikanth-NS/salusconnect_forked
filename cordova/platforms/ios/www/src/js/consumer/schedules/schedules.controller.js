"use strict";

define([
    "app",
    "common/constants",
    "consumer/consumer.controller",
    "consumer/schedules/views/SchedulePage.view",
    "consumer/schedules/views/presets/SchedulePresets.view"
], function (App, constants, ConsumerController) {
    App.module("Consumer.Schedules", function (Schedules) {
        Schedules.Controller = {
            presets: function () {
                var page = new App.Consumer.Schedules.Views.Presets.SchedulePresetsView();
                ConsumerController.showLayout(page, true, ["devices", "ruleGroups"]);
            },
            //editSchedule: function () { //TODO - replace with Bennett's after merge
            //	require([
            //		"consumer/consumer.controller",
            //		"consumer/schedules/views/EditSchedules.view"
            //	], function (ConsumerController) {
            //		var page = new App.Consumer.Schedules.Views.EditSchedulesView();
            //		ConsumerController.showLayout(page, true, ["devices", "ruleGroups"]);
            //	});
            //},
            thermostatSchedule: function thermostatSchedule(key) {
                var categories = constants.categoryTypes,
                        page = new App.Consumer.Schedules.Views.SchedulePage({deviceCategory: categories.THERMOSTATS, key: key});

                ConsumerController.showLayout(page, true, ["devices", "tscs"]);
            },
            waterHeaterSchedule: function waterHeaterSchedule(key) {
                var categories = constants.categoryTypes,
                        page = new App.Consumer.Schedules.Views.SchedulePage({deviceCategory: categories.WATERHEATERS, key: key});

                ConsumerController.showLayout(page, true, ["devices"]);
            },
            smartPlugSchedule: function smartPlugSchedule(key) {
                var categories = constants.categoryTypes,
                        page = new App.Consumer.Schedules.Views.SchedulePage({deviceCategory: categories.SMARTPLUGS, key: key});

                ConsumerController.showLayout(page, true, ["devices"]);
            },
//			currentDevice:function (key){
//			    var categories = constants.categoryTypes,
//                	page = new App.Consumer.Schedules.Views.SchedulePage({"key": key});
//
//                ConsumerController.showLayout(page, true, ["devices"]);
//			},
            cleanPageEventController: function () {
                var pec = App.Consumer.Schedules.PageEventController;

                if (pec) {
                    pec.off();
                }
            }
        };
    });

    return App.Consumer.Schedules.Controller;
});