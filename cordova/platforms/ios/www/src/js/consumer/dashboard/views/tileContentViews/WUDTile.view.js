"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView"
], function (App, constants, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {
		Views.WUDTile = {};

		// generic used for wud
		Views.WUDTile.FrontView = Mn.ItemView.extend({
			className: "wud-tile",
			template: consumerTemplates["dashboard/tile/equipmentTileFront"],
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}
		}).mixin([TileContentViewMixin]);

		// no back view for these
		Views.WUDTile.BackView = {};
	});

	return App.Consumer.Dashboard.Views.WUDTile;
});
