"use strict";

define([
	"app",
	"consumer/consumer.controller",
	"consumer/newUser/views/NewUserLayout"
], function (App, ConsumerController) {

	App.module("Consumer.NewUser", function (NewUser) {
		NewUser.Controller = {
			"newUser": function () {
					var newUserLayout = new App.Consumer.NewUser.Views.NewUserLayout();

					ConsumerController.showLayout(newUserLayout, false);
			}
		};
	});

	return App.Consumer.NewUser.Controller;
});