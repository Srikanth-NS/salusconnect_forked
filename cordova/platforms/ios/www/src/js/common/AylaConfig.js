"use strict";

define(["app"], function (App) {

	/**
	 * TODO there is a known issue where certain adblock browser extensions
	 * will block calls to ads-dev if they detect ".com/ads".
	 * This will affect dev and prod urls
	 */


	/** note: there seems to be an issue with android and the ec2 url's selfsigned ssl cert on some of the calls.
	 *  for now keep dont move over the salus-web-services.appspot.com calls to ec2 until they have a cert
	 *  affected call seems to be weather.
	 **/
	var AylaConfig = {
		appId: "",
		appSecret: "",	///I hate to put this here
		stdTimeoutMs: 5000, // timeout after 5 seconds
		endpoints: {
			login: "salusconnect.io/users/sign_in.json",
			logout: "salusconnect.io/users/sign_out.json",
			refreshToken: "salusconnect.io/users/refresh_token.json",
			device: {
				list: "salusconnect.io/apiv1/devices.json",
				add: "salusconnect.io/apiv1/devices.json", // param {device: {dsn: dsn}}
				register: "salusconnect.io/apiv1/devices/register.json?regtype=Node&dsn=<%=dsn%>",  // this should be used as a GET and only for the gateway // params are: ip, dsn, regtype {“SameLAN”, “ButtonPush”, “APMode”, “Display”, “Dsn”, “None”(OEM),"Node”(For Gateway)}
				registerButtonPush: "salusconnect.io/apiv1/devices/register.json?regtype=Button-Push", //Temporary: point to ayla directly as they need our public client ip to do registration.
				nodesAndProperties: "salusconnect.io/apiv1/devices/<%=id%>/nodes.json",
				listByNode: "salusconnect.io/apiv1/devices/<%=id%>/registered_nodes.json",
				detail: "salusconnect.io/apiv1/devices/<%=id%>.json",
				save: "salusconnect.io/apiv1/devices/<%=id%>.json",
				remove: "salusconnect.io/apiv1/devices/<%=id%>.json", // delete same as details
				factory_reset: "salusconnect.io/apiv1/devices/<%=id%>/cmds/factory_reset.json", // used as put
				property: {
					list: "salusconnect.io/apiv1/devices/<%=id%>/properties.json",
					fetch: "salusconnect.io/apiv1/properties/<%=id%>.json",
					setDataPoint: "salusconnect.io/apiv1/properties/<%=key%>/datapoints.json",
					getDataPointTimeRange: "salusconnect.io/apiv1/properties/<%=key%>/datapoints.json",
					getAggregate: "salusconnect.io/data/v1/average?dsn=<%=dsn%>&key=<%=prop%>&earliest_time=<%=startDate%>&latest_time=<%=endDate%>&timespan=<%=timespan%>&fmt=json",
					aggregateResults: "salusconnect.io/data/results?ref=<%=ref%>"
				},
				address: "salusconnect.io/apiv1/devices/<%=id%>/addr.json",
				weather: "salusconnect.io/weather?dsn=<%=dsn%>",
				metaData: {
					"fetch": "salusconnect.io/apiv1/dsns/<%=dsn%>/data/metaData.json",
					"save": "salusconnect.io/apiv1/dsns/<%=dsn%>/data/metaData.json", // same as fetch,
					"remove": "salusconnect.io/apiv1/dsns/<%=dsn%>/data/metaData.json",
					"create": "salusconnect.io/apiv1/dsns/<%=dsn%>/data.json"
				},
				metaDataApi: {
					"fetch": "salusconnect.io/apiv1/dsns/<%=dsn%>/data/<%=propKey%>.json",
					"fetchAll": "salusconnect.io/apiv1/dsns/<%=dsn%>/data.json",
					"save": "salusconnect.io/apiv1/dsns/<%=dsn%>/data/<%=propKey%>.json", // same as fetch,
					"delete": "salusconnect.io/apiv1/dsns/<%=dsn%>/data/<%=propKey%>.json", // same as fetch,
					"create": "salusconnect.io/apiv1/dsns/<%=dsn%>/data.json"
				},
				linkedThermostats: {
					"create": "salusconnect.io/linkedDevices/dsns/<%=dsn%>",
					"fetch": "salusconnect.io/linkedDevices/dsns/<%=dsn%>?key=<%=key%>",
					"delete": "salusconnect.io/linkedDevices/dsns/<%=dsn%>?key=<%=key%>",
					"list": "salusconnect.io/linkedDevices/dsns/<%=dsn%>"
				},
				schedules: {
					"list": "salusconnect.io/apiv1/devices/<%=deviceId%>/schedules.json",
					"create": "salusconnect.io/apiv1/devices/<%=device_id%>/schedules.json",
					"update": "salusconnect.io/apiv1/devices/<%=device_id%>/schedules/<%=key%>.json",
					"fetch": "salusconnect.io/apiv1/schedules/<%=key%>.json",
					"delete": "salusconnect.io/apiv1/schedules/<%=key%>.json",
					"actions": "salusconnect.io/apiv1/schedules/<%=schedule_id%>/schedule_actions.json",
					"putActions": "salusconnect.io/apiv1/schedule_actions/<%=key%>.json"
				},
				timeZones: {
					"fetch": "salusconnect.io/apiv1/devices/<%= key %>/time_zones.json",
                    "detail": "salusconnect.io/apiv1/time_zones.json?tz_id=<%=key%>"
				}
			},
			rule: {
				"create": "salusconnect.io/rules/dsns/<%=dsn%>",
				"update": "salusconnect.io/rules/dsns/<%=dsn%>",
				"fetch": "salusconnect.io/rules/dsns/<%=dsn%>?key=<%=key%>",
				"list": "salusconnect.io/rules/dsns/<%=dsn%>",
				"delete": "salusconnect.io/rules/dsns/<%=dsn%>?key=<%=key%>"
			},
			ruleGroups: {
				"create": "salusconnect.io/ruleGroups/dsns/<%=dsn%>",
				"list": "salusconnect.io/ruleGroups/dsns/<%=dsn%>", //same as create
				"fetch": "salusconnect.io/ruleGroups/dsns/<%=dsn%>?key=<%=key%>",
				"deleteAll": "salusconnect.io/ruleGroups/dsns/<%=dsn%>", //same as create
				"delete": "salusconnect.io/ruleGroups/dsns/<%=dsn%>?key=<%=key%>", //same as fetch
				"update": "salusconnect.io/ruleGroups/dsns/<%=dsn%>" //same as create
			},
			aylaTrigger: {
				fetch: "salusconnect.io/apiv1/properties/<%=property_key%>/triggers.json",
				create: "salusconnect.io/apiv1/properties/<%=property_key%>/triggers.json",
				update: "salusconnect.io/apiv1/triggers/<%=key%>.json",
				delete: "salusconnect.io/apiv1/triggers/<%=key%>.json",
				fetchTriggerApps: "salusconnect.io/apiv1/triggers/<%=key%>/trigger_apps.json",
				triggerAppDelete: "salusconnect.io/apiv1/trigger_apps/<%=key%>.json"
			},
			userProfile: {
				fetch: "salusconnect.io/users/get_user_profile.json",
				save: "salusconnect.io/users.json<%= query_string_params %>",
				delete: "salusconnect.io/users.json",
				changePassword: "salusconnect.io/users.json",
                resetPassword: "salusconnect.io/users/password.json",
				dataCollectionOff: {
					"fetch": "salusconnect.io/api/v1/users/data/dataCollectionOff.json",
					"save": "salusconnect.io/api/v1/users/data/dataCollectionOff.json", //same as fetch,
					"create": "salusconnect.io/api/v1/users/data.json"
				},
				metaData: {
					"fetch": "salusconnect.io/api/v1/users/data/metaData.json",
					"save": "salusconnect.io/api/v1/users/data/metaData.json", //same as fetch
					"remove": "salusconnect.io/api/v1/users/data/metaData.json", // same as fetch
					"create": "salusconnect.io/api/v1/users/data.json"
				},
				tileOrder: {
					"fetch": "salusconnect.io/api/v1/users/data/tileOrder.json",
					"save": "salusconnect.io/api/v1/users/data/tileOrder.json", // same as fetch,
					"delete": "salusconnect.io/api/v1/users/data/tileOrder.json", // same as fetch,
					"create": "salusconnect.io/api/v1/users/data.json"
				}
			},
			auth: {
				resendConfirmationToken: "salusconnect.io/users/confirmation.json",
				confirmUserToken: "salusconnect.io/users/confirmation.json",
				mgPassword: "salusconnect.io/users/password.json<%= query_string_params %>"
			},
			groups: {
				list: "salusconnect.io/apiv1/groups.json",
				create: "salusconnect.io/apiv1/groups.json", //same as list
				remove: "salusconnect.io/apiv1/groups/<%=key%>.json",
				load: "salusconnect.io/apiv1/groups/<%=id%>.json", //same as remove
				put: "salusconnect.io/apiv1/groups/<%=key%>.json", //same as load
				addDevice: "salusconnect.io/apiv1/groups/<%=key%>/devices.json",
				removeDevice: "salusconnect.io/apiv1/groups/<%=key%>/devices/<%=device_id%>.json"
			},
			share: {
				createProxy: "salusconnect.io/proxyusers.json",
				list: "salusconnect.io/api/v1/users/shares.json",
				create: "salusconnect.io/api/v1/users/shares.json",
				read: "salusconnect.io/api/v1/users/shares/<%=id%>.json",
				update: "salusconnect.io/api/v1/users/shares/<%=id%>.json",
				delete: "salusconnect.io/api/v1/users/shares/<%=id%>.json",
				batchShare: "salusconnect.io/batchShare.json"
			},
			photo: {
				create: "salusconnect.io/photo",
				update: "salusconnect.io/photo",
				remove: "salusconnect.io/photo"
			},
			log: {
				create: "salusconnect.io/services/collector/event"
			}
		},
		/**
		 * Builds a localized version of an Ayla URL so that the Nginx config can proxy pass to the correct endpoint
		 * @param aylaUrlPart
		 * @returns {*}
		 */
		buildLocalURLForAyla: function (aylaUrlPart) {

			if (aylaUrlPart.toLowerCase().indexOf("http") === 0) {
				return aylaUrlPart;
			}

			var hostName = window.location.hostname,
				host = window.location.origin || (window.location.protocol + "//" + window.location.host); //IE fixes

			if (host === "file://" || hostName.indexOf("localhost") >= 0 || hostName.indexOf("210.75.16.197") >= 0) {
				host = "https:/"; //TODO: config drive this per environment.

				//TODO QUICK FIX
				aylaUrlPart = aylaUrlPart.replace("salusconnect.io/", App.getIsEU() ? "eu.salusconnect.io/" : "us.salusconnect.io/");
			}
			
			// special case for staging to hit itself
			if (hostName.indexOf("salusconnect") >= 0 && aylaUrlPart.indexOf("salusconnect.io/") === 0) {
				host = "";
				aylaUrlPart = aylaUrlPart.replace("salusconnect.io/", "");
			}
            
            if (aylaUrlPart.indexOf("?") >= 0) {
                aylaUrlPart = aylaUrlPart + "&";
            } else {
                 aylaUrlPart = aylaUrlPart + "?";
            }
            aylaUrlPart = aylaUrlPart + "timestamp=" + new Date().getTime();

			return host + "/" + aylaUrlPart;
		},
		/**
		 * Jasmine tests call this method to determine what protocol to expect in an Ayla ajax URL.
		 * In order to get proxy passing to work, we needed to move the setting of "https://" out of the javascript and
		 * into the dev and int nginx configs. During a proxy pass, the server will prepend "https://"
		 * onto any Ayla URL. This means that any Ayla calls made on desktop should hit "http".
		 * @returns {"http", "https"}
		 *
		 */
		getURLProtocolForAylaCalls: function () {
			// TODO add logic here to determine WHEN to set this to http or https (i.e. for testing on mobile, since those calls will be made directly to Ayla)
			return "http";
		}
	};

	return AylaConfig;
});