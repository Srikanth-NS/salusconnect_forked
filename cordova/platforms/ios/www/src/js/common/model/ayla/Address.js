"use strict";

define([
	"app",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked"
], function (App, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B) {
		/**
		 *  This represents a physical device location for the device with id <deviceId>
		 */
		Models.AylaAddress = B.Model.extend({
			idAttribute: "deviceId",
			defaults: {
				deviceId: null,
				city: null,
				country: null,
				state: null,
				street: null,
				zip: null
			},
			initialize: function (/*data, options*/) {
				if (this.get("deviceId")) {
					this.load();
				}
			},

			load: function () {
				var that = this;
				this.fetchPromise = this.fetch({
					isLowPriority: true,
					success: function () {
						that.trigger("fetch:success");
					}
				});

				return this.fetchPromise;
			},

			validate: function (attrs) {
				if (!attrs.deviceId) {
					return "cannot save without a device id";
				}
			},
			getFetchPromise: function () {
				return this.fetchPromise || this.load();
			}
		}).mixin([AylaBackedMixin], {
			fetchUrl: AylaConfig.endpoints.device.address,
			apiWrapperObjectName: "addr",
			apiCreatedAttribute: true, /// always treat as created
			localOnlyProperties: ["deviceId"]
		});
	});

	return App.Models.AylaAddress;
});
