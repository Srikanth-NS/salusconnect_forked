//
//  AylaApiClient.m
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 6/4/12.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaHost.h"
#import "AylaSetupSupport.h"
#import "AylaReachability.h"
#import "AylaReachabilitySupport.h"

typedef NS_ENUM(NSInteger, AylaApiClientDestination) {
    AylaApiClientDestinationUnknown,
    AylaApiClientDestinationUserService,
    AylaApiClientDestinationDeviceService,
    AylaApiClientDestinationNonSecureDeviceService,
    AylaApiClientDestinationTriggerAppService,
    AylaApiClientDestinationLogService,
    AylaApiClientDestinationDevice
};

@interface AylaApiClient ()
@property (nonatomic, assign) AylaApiClientDestination clientDestination;
@end

@implementation AylaApiClient
static NSString * const https = @"https://";
static NSString * const http = @"http://";
static NSString * const serviceDomainUS = @".aylanetworks.com";
static NSString * const serviceDomainCN = @".ayla.com.cn";
@synthesize deviceServicePath = _deviceServicePath;
@synthesize userServicePath = _userServicePath;
@synthesize applicationTriggerServicePath = _applicationTriggerServicePath;
@synthesize connectedDevicePath = _connectedDevicePath;
@synthesize connectNewDevicePath = _connectNewDevicePath;
@synthesize logServicePath = _logServicePath;

static NSInteger deviceSeviceType = -1; static NSString *deviceAuthToken = nil;
static NSInteger deviceServiceLocation = -1;
static NSInteger deviceNonSecureProductionSevice = -1;
static NSInteger deviceNonSecureServiceLocation = -1;
static NSInteger userProductionService = -1; static NSString *userAuthToken = nil;
static NSInteger userServiceLocation = -1;
static NSInteger appTriggerProductionService = -1; static NSString *appTriggerAuthToken = nil;
static NSInteger appTriggerServiceLocation = -1;
static NSInteger logServiceType = -1; static NSString *logAuthToken = nil;
static NSInteger logServiceLocation = -1;

+ (NSString *)buildUrlPathWithAppId:(NSString *)appId andSuffixUrl:(NSString *)suffixUrl isHttps:(BOOL)isHttps
{
    return [NSString stringWithFormat:@"%@%@%@", isHttps?https:http, appId, suffixUrl];
}

+ (NSString *)addLocation:(AylaServiceLocation)location toUrlPath:(NSString *)url
{
    NSString *newUrl = [url mutableCopy];
    if(location == AylaServiceLocationCN) {
        newUrl = [url stringByReplacingOccurrencesOfString:serviceDomainUS withString:serviceDomainCN];
    }
    return newUrl;
}

+ (id)sharedDeviceServiceInstance
{
  static AylaApiClient *__sharedDeviceServiceInstance = nil;;
  if ( (deviceSeviceType != [AylaSystemUtils.serviceType integerValue]) ||
       (deviceServiceLocation != [AylaSystemUtils serviceLocation]) ||
       (gblAuthToken!= nil && ![deviceAuthToken isEqualToString:gblAuthToken]))
  {
        deviceAuthToken = gblAuthToken;
        deviceSeviceType = [AylaSystemUtils.serviceType integerValue];
        deviceServiceLocation = [AylaSystemUtils serviceLocation];
        NSString *deviceServicePath = nil;
        switch(deviceSeviceType){
            case AML_DEVICE_SERVICE:
                if([AylaSystemUtils appId]){
                    deviceServicePath = [AylaApiClient buildUrlPathWithAppId:[AylaSystemUtils appId] andSuffixUrl:GBL_DEVICE_SUFFIX_URL isHttps:YES];
                    break;
                }
                else {
                    saveToLog(@"%@, %@, %@:%@, %@", @"W", @"ApiClient", @"deviceServicePath", @"AML_DEVICE_SERVICE with invalid appId", @"sharedDeviceServiceInstance");
                }
            case AML_STAGING_SERVICE:
                deviceServicePath = GBL_DEVICE_STAGING_URL;
                break;
            case AML_DEVELOPMENT_SERVICE:
                deviceServicePath = [AylaApiClient addLocation:deviceServiceLocation toUrlPath:GBL_DEVICE_DEVELOP_URL];
                break;
            case AML_DEMO_SERVICE:
                deviceServicePath = [AylaApiClient addLocation:deviceServiceLocation toUrlPath:GBL_DEVICE_DEMO_URL];
                break;
            default:
                deviceServicePath = [AylaApiClient addLocation:deviceServiceLocation toUrlPath:GBL_DEVICE_SERVICE_URL];
        }
        __sharedDeviceServiceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:deviceServicePath]];
        __sharedDeviceServiceInstance.clientDestination = AylaApiClientDestinationDeviceService;
        //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"deviceServicePath", __sharedDeviceServiceInstance.baseURL, @"sharedDeviceServiceInstance");
  }
  return __sharedDeviceServiceInstance;
}


+ (id)sharedNonSecureDeviceServiceInstance
{
    static AylaApiClient *__sharedNonSecureDeviceServiceInstance;
    if ((deviceNonSecureProductionSevice != [AylaSystemUtils.serviceType integerValue]) ||
        (deviceNonSecureServiceLocation != [AylaSystemUtils serviceLocation]) ||
        (gblAuthToken!= nil && ![deviceAuthToken isEqualToString:gblAuthToken]))
    {
        deviceNonSecureProductionSevice = [AylaSystemUtils.serviceType integerValue];
        deviceNonSecureServiceLocation = [AylaSystemUtils serviceLocation];
        NSString *deviceServicePath = nil;
        switch(deviceNonSecureProductionSevice){
            case AML_DEVICE_SERVICE:
                if([AylaSystemUtils appId]){
                    deviceServicePath = [AylaApiClient buildUrlPathWithAppId:[AylaSystemUtils appId] andSuffixUrl:GBL_NON_SECURE_DEVICE_SUFFIX_URL isHttps:NO];
                    break;
                }
                else {
                    saveToLog(@"%@, %@, %@:%@, %@", @"W", @"ApiClient", @"deviceServicePath", @"AML_DEVICE_SERVICE with invalid appId", @"sharedNonSecureDeviceServiceInstance");
                }
            case AML_STAGING_SERVICE:
                deviceServicePath = GBL_NON_SECURE_DEVICE_STAGING_URL;
                break;
            case AML_DEVELOPMENT_SERVICE:
                deviceServicePath = [AylaApiClient addLocation:deviceNonSecureServiceLocation toUrlPath:GBL_NON_SECURE_DEVICE_DEVELOP_URL];
                break;
            case AML_DEMO_SERVICE:
                deviceServicePath = [AylaApiClient addLocation:deviceNonSecureServiceLocation toUrlPath:GBL_NON_SECURE_DEVICE_DEMO_URL];
                break;
            default:
                deviceServicePath = [AylaApiClient addLocation:deviceNonSecureServiceLocation toUrlPath:GBL_NON_SECURE_DEVICE_SERVICE_URL];
        }
        __sharedNonSecureDeviceServiceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:deviceServicePath]];
        __sharedNonSecureDeviceServiceInstance.clientDestination = AylaApiClientDestinationNonSecureDeviceService;
        //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"deviceNonSecureServicePath", __sharedNonSecureDeviceServiceInstance.baseURL, @"sharedNonSecureDeviceServiceInstance");
    }
    return __sharedNonSecureDeviceServiceInstance;
}


+ (id)sharedUserServiceInstance
{
  static AylaApiClient *__sharedUserServiceInstance;
  if ( (userProductionService != [AylaSystemUtils.serviceType integerValue]) ||
       (userServiceLocation != [AylaSystemUtils serviceLocation]) ||
      (![userAuthToken isEqualToString:gblAuthToken]))
    {
        userAuthToken = gblAuthToken;
        userProductionService = [AylaSystemUtils.serviceType integerValue];
        userServiceLocation = [AylaSystemUtils serviceLocation];
        NSString *userServicePath = nil;
        switch(userProductionService){
            case AML_STAGING_SERVICE:
                userServicePath = GBL_USER_STAGING_URL;
                break;
            case AML_DEVELOPMENT_SERVICE:
                userServicePath = [AylaApiClient addLocation:userServiceLocation toUrlPath:GBL_USER_DEVELOP_URL];
                break;
            case AML_DEMO_SERVICE:
                userServicePath = [AylaApiClient addLocation:userServiceLocation toUrlPath:GBL_USER_DEMO_URL];
                break;
            default:
                userServicePath = [AylaApiClient addLocation:userServiceLocation toUrlPath:GBL_USER_SERVICE_URL];
        }
        __sharedUserServiceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:userServicePath]];
        __sharedUserServiceInstance.clientDestination = AylaApiClientDestinationUserService;
        //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"userServicePath", __sharedUserServiceInstance.baseURL, @"sharedUserServiceInstance");
  }
  return __sharedUserServiceInstance;
}

+ (id)sharedAppTriggerServiceInstance
{
  static AylaApiClient *__sharedAppTriggerServiceInstance;
  
  if ( (appTriggerProductionService != [AylaSystemUtils.serviceType integerValue]) ||
       (appTriggerServiceLocation != [AylaSystemUtils serviceLocation]) ||
       (![appTriggerAuthToken isEqualToString:gblAuthToken]))
    {
        appTriggerAuthToken = gblAuthToken;
        appTriggerProductionService = [AylaSystemUtils.serviceType integerValue];
        appTriggerServiceLocation = [AylaSystemUtils serviceLocation];
        NSString *appTriggerServicePath = nil;
        switch(appTriggerProductionService){
            case AML_DEVICE_SERVICE:
                if([AylaSystemUtils appId]){
                    appTriggerServicePath = [AylaApiClient buildUrlPathWithAppId:[AylaSystemUtils appId] andSuffixUrl:GBL_APPTRIGGER_SUFFIX_URL isHttps:YES];
                    break;
                }
                else {
                    saveToLog(@"%@, %@, %@:%@, %@", @"W", @"ApiClient", @"appTriggerServicePath", @"AML_DEVICE_SERVICE with invalid appId", @"sharedAppTriggerServiceInstance");
                }
            case AML_STAGING_SERVICE:
                appTriggerServicePath = GBL_APPTRIGGER_STAGING_URL;
                break;
            case AML_DEVELOPMENT_SERVICE:
                appTriggerServicePath = [AylaApiClient addLocation:appTriggerServiceLocation toUrlPath:GBL_APPTRIGGER_DEVELOP_URL];
                break;
            case AML_DEMO_SERVICE:
                appTriggerServicePath = [AylaApiClient addLocation:appTriggerServiceLocation toUrlPath:GBL_APPTRIGGER_DEMO_URL];
                break;
            default:
                appTriggerServicePath = [AylaApiClient addLocation:appTriggerServiceLocation toUrlPath:GBL_APPTRIGGER_SERVICE_URL];
        }
        __sharedAppTriggerServiceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:appTriggerServicePath]];
        __sharedAppTriggerServiceInstance.clientDestination = AylaApiClientDestinationTriggerAppService;
        //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"appTriggerServicePath", __sharedAppTriggerServiceInstance.baseURL, @"sharedAppTriggerServiceInstance");
  }
  return __sharedAppTriggerServiceInstance;
}

// Local module connection
+ (id)sharedConnectedDeviceInstance:(NSString *)lanIp
{
    static AylaApiClient *__sharedConnectedDeviceInstance;
    NSString *basePath = [NSString stringWithFormat:@"%@%@%@", @"http://", lanIp, @"/"];
    //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"basePath", basePath, @"sharedConnectedDeviceInstance");
    if(__sharedConnectedDeviceInstance == nil ||
       [[__sharedConnectedDeviceInstance.baseURL absoluteString] compare:basePath]!= 0){
        __sharedConnectedDeviceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:basePath]];
        __sharedConnectedDeviceInstance.clientDestination = AylaApiClientDestinationDevice;
    }
    return __sharedConnectedDeviceInstance;
}

+ (id)sharedNewDeviceInstance
{
    static AylaApiClient *__sharedNewDeviceInstance = nil;
    NSString *newDevicePath = GBL_MODULE_DEFAULT_WIFI_IPADDR;
    if(__sharedNewDeviceInstance == nil){
        __sharedNewDeviceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:newDevicePath]];
        __sharedNewDeviceInstance.clientDestination = AylaApiClientDestinationDevice;
        saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"connectNewDevicePath", __sharedNewDeviceInstance.baseURL, @"sharedNewDeviceInstance");
    }
    return __sharedNewDeviceInstance;
}

+ (id)sharedLogServiceInstance
{
    static AylaApiClient *__sharedLogServiceInstance = nil;
    if ((logServiceType != [AylaSystemUtils.serviceType integerValue ]) ||
        (logServiceLocation != [AylaSystemUtils serviceLocation]) ||
        (![logAuthToken isEqualToString:gblAuthToken]))
    {
        logAuthToken = gblAuthToken;
        logServiceType = [AylaSystemUtils.serviceType integerValue];
        logServiceLocation = [AylaSystemUtils serviceLocation];
        NSString *logServicePath = nil;
        switch(logServiceType){
            case AML_STAGING_SERVICE:
                logServicePath = GBL_LOG_STAGING_URL;
                break;
            case AML_DEVELOPMENT_SERVICE:
                logServicePath = [AylaApiClient addLocation:logServiceLocation toUrlPath:GBL_LOG_DEVELOP_URL];
                break;
            case AML_DEMO_SERVICE:
                logServicePath = [AylaApiClient addLocation:logServiceLocation toUrlPath:GBL_LOG_DEMO_URL];
                break;
            default:
                logServicePath = [AylaApiClient addLocation:logServiceLocation toUrlPath:GBL_LOG_SERVICE_URL];
        }
        __sharedLogServiceInstance = [[AylaApiClient alloc] initWithBaseURL:[NSURL URLWithString:logServicePath]];
        __sharedLogServiceInstance.clientDestination = AylaApiClientDestinationLogService;
        saveToLog(@"%@, %@, %@:%@, %@", @"I", @"ApiClient", @"logServicePath", __sharedLogServiceInstance.baseURL, @"sharedLogServiceInstance");
    }
    return __sharedLogServiceInstance;
}
                                              
- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self) {
        saveToLog(@"%@, %@, %@", @"I", @"ApiClient", @"initWithBaseURL");
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        
        if(url && gblAuthToken &&
           [url.absoluteString rangeOfString:https].location != NSNotFound ) {
            [self.requestSerializer setValue:[@"auth_token " stringByAppendingString:gblAuthToken] forHTTPHeaderField:@"Authorization"];
        }
        else {
            [self.requestSerializer setValue:[@"auth_token " stringByAppendingString:@"null"] forHTTPHeaderField:@"Authorization"];
        }
    }
    return self;
}

- (NSOperation *)getPath:(NSString *)path
     parameters:(NSDictionary *)parameters
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
   return [self executeRequestMethod:@"GET" path:path parameters:parameters success:success failure:failure];
}

- (NSOperation *)postPath:(NSString *)path
      parameters:(NSDictionary *)parameters
         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    return [self executeRequestMethod:@"POST" path:path parameters:parameters success:success failure:failure];
}

- (NSOperation *)putPath:(NSString *)path
     parameters:(NSDictionary *)parameters
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    return [self executeRequestMethod:@"PUT" path:path parameters:parameters success:success failure:failure];
}

- (NSOperation *)deletePath:(NSString *)path
        parameters:(NSDictionary *)parameters
           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    return [self executeRequestMethod:@"DELETE" path:path parameters:parameters success:success failure:failure];
}

- (NSOperation *)executeRequestMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
                          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    void (^successResp)(AFHTTPRequestOperation *operation, id responseObject)
    = ^(AFHTTPRequestOperation *operation, id responseObject) {
        [self didReceiveOperationResponse:operation statusCode:0 httpCode:operation.response.statusCode];
        success(operation, responseObject);
    };
    
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:method URLString:[[NSURL URLWithString:path relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:successResp failure:failure];
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                      path:(NSString *)path
                                parameters:(NSDictionary *)parameters
{
    NSError *error;
    
    NSMutableURLRequest *urlRequest
    = [self.requestSerializer requestWithMethod:method URLString:[NSString stringWithFormat:@"%@%@",self.baseURL.absoluteString, path, nil] parameters:parameters error:&error];
    
    if(error) {
        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"ApiClient", @"error", [NSNumber numberWithInteger:error.code], @"requestWithMethod");
    }
    
    return urlRequest;
}

- (void)enqueueHTTPRequestOperation:(AFHTTPRequestOperation *)operation {
    [self.operationQueue addOperation:operation];
}

- (void)didReceiveOperationResponse:(AFHTTPRequestOperation *)operation statusCode:(NSInteger)statusCode httpCode:(NSInteger)httpCode
{
    if(self.clientDestination == AylaApiClientDestinationDeviceService) {
        [AylaReachability setConnectivity:AML_REACHABILITY_REACHABLE];
    }
}

@end
