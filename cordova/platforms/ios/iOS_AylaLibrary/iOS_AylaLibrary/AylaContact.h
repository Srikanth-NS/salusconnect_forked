//
//  AylaContact.h
//  iOS_AylaLibrary
//
//  Created by Yipei Wang on 1/13/15.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AylaResponse;
@class AylaError;

@interface AylaContact : NSObject

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phoneCountryCode;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *streetAddress;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong, readonly) NSString *updatedAt;

/**
 *  Create a new contact
 *  @param contact The contact to be created.
 *  @param callParams Not used, set to be nil.
 *  @param success would be called with created contact when request is succeeded.
 *  @param failure would be called with an AylaError object when request is failed.
 */
+ (NSOperation *)createContact:(AylaContact *)contact withParams:(NSDictionary *)callParams
                       success:(void (^)(AylaResponse *resp, AylaContact *createdContact))successBlock
                       failure:(void (^)(AylaError *error))failureBlock;

/**
 *  Get a contact with contact Id
 *  @param contactId Id or requested contact.
 *  @param success would be called with a contact when request is succeeded.
 *  @param failure would be called with an AylaError object when request is failed.
 */
+ (NSOperation *)getWithId:(NSNumber *)contactId
                   success:(void (^)(AylaResponse *resp, AylaContact *contact))successBlock
                   failure:(void (^)(AylaError *error))failureBlock;

/**
 *  Get all contacts with input params
 *  @param callParams Current supported call params: kAylaContactParamFilter - provide a filter for retrieved contacts.
 *  @param success would be called with an array of retrieved contacts when request is succeeded.
 *  @param failure would be called with an AylaError object when request is failed.
 */
+ (NSOperation *)getAllWithParams:(NSDictionary *)callParams
                          success:(void (^)(AylaResponse *resp, NSArray *contacts))successBlock
                          failure:(void (^)(AylaError *error))failureBlock;

/**
 *  Update an existing contact
 *  @param callParams NSDicionary of to-be-updated contact infos.
 *  @param success would be called with updated contact when request is succeeded.
 *  @param failure would be called with an AylaError object when request is failed.
 */
+ (NSOperation *)updateContact:(AylaContact *)contact withParams:(NSDictionary *)callParams
                          success:(void (^)(AylaResponse *resp, AylaContact *updatedContact))successBlock
                          failure:(void (^)(AylaError *error))failureBlock;
/**
 *  Delete a contact
 *  @param contact The contact to be deleted.
 *  @param success would be called when request is succeeded.
 *  @param failure would be called with an AylaError object when request is failed.
 */
+ (NSOperation *)deleteContact:(AylaContact *)contact
                          success:(void (^)(AylaResponse *resp))successBlock
                          failure:(void (^)(AylaError *error))failureBlock;

@end

extern NSString * const kAylaContactParamFirstName;
extern NSString * const kAylaContactParamLastName;
extern NSString * const kAylaContactParamDisplayName;
extern NSString * const kAylaContactParamEmail;
extern NSString * const kAylaContactParamPhoneCountryCode;
extern NSString * const kAylaContactParamPhoneNumber;
extern NSString * const kAylaContactParamStreetAddress;
extern NSString * const kAylaContactParamZipCode;
extern NSString * const kAylaContactParamCountry;
extern NSString * const kAylaContactParamFilter;