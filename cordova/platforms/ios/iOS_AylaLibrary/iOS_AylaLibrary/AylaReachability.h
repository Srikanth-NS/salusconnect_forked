//
//  AylaReachability.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 2/11/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AylaReachability : NSObject

/**
 * This asynchronous method determines reachability status for the current LAN  enabled device and service. Asynchronous results 
   are returned to the registered reachabilityHandle. Handler.
 */
+ (void)determineReachability;

/**
 * This method is used to asynchronously determine reachability to Device. Once new reachability is determined, block would be called with reachability status message.
 */
+ (void)determineDeviceReachabilityWithBlock:(void(^)(int))block;

/**
 * This method is used to asynchronously determine reachability to Service. Once new reachability is determined, block would be called with reachability status message.
 */
+ (void)determineServiceReachabilityWithBlock:(void(^)(int))block;

/**
 * This real-time method returns current reachability status for the current connected LME device.
 */
+ (int)getDeviceReachability;

/**
 * This real-time method returns the current status for service reachability
 */
+ (int)getConnectivity;

/**
 * This real-time method returns a combined reachability status. If either LME device or Ayla Device Service were reachable, this method would return reachability
   status AML_REACHABILITY_REACHABLE.
 */
+ (int)getReachability;

@end
