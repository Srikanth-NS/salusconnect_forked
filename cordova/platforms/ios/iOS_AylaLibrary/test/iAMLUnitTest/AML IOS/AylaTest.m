//
//  AylaTest.m
//  AML IOS
//
//  Created by Yipei Wang on 1/31/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import "AylaTest.h"

//------------------- BEGIN DEVICE SPECIFIC TEST CRITERIA ------------------------------
// Replace with the product dsn of your Standard Demo Kit
NSString *gblAmlProductName = @"AC000W00000XXXX"; //@"AC000W000XXXXXX";

// Replace with the property name you want to test
NSString *gblAmlPropertyName = @"cmd";

//test for creating data point at Lan Mode
NSString *gblBooleanTypePropertyName1 = @"Green_LED"; //boolean type property, set to nil if device doesn't have this type of property
NSString *gblStringTypePropertyName1 = @"cmd"; //string type property, set to nil if device doesn't have this type of property
NSString *gblIntegerTypePropertyName1 = @"input"; //integer type property, set to nil if device doesn't have this type of property
NSString *gblDecimalTypePropertyName1 = @"decimal_in"; //Decimal type property, set to nil if device doesn't have this type of property
NSString *gblStreamTypePropertyName1 = @"stream_down"; //Stream type property, set to nil if device doesn't have this type of property
NSString *gblFloatTypePropertyName1 = nil;
NSString *gblFilePropertyName = @"stream_down"; //file type property, set to nil if device doesn't have this type of property


// Replace with values used by triggers & apps
NSString *gblAmlCountryCode = @"1";
NSString *gblAmlPhoneNumber = @"8005551212"; // sms trigger alerts are sent here
NSString *gblAmlEmailAddress = @"myEmail@myCompany.com"; // email trigger alerts are sent here
//------------------- END DEVICE SPECIFIC TEST CRITERIA ------------------------------


// track success/failure of tests
int gblNumberOfTestSections = 0;
int gblTestSectionsCompleted = 0;
int gblNumberOfTests = 0; // added as tests are executed
int gblTestsPassed = 0;
int gblTestsFailed = 0;
int gblTestTimeout = 30; // seconds to wait for all tests to complete
bool gblLoopTesting = FALSE;

// test objects
AylaDevice *gblAmlTestDevice = nil;
AylaProperty *gblAmlTestProperty = nil;
AylaPropertyTrigger *gblAmlTestPropertyTrigger = nil;
AylaApplicationTrigger *gblAmlTestApplicationTrigger = nil;
AylaDatum *gblAmlTestDatum = nil;
AylaShare *gblAmlTestShare = nil;
NSString *outputMsgs =nil;
NSString *gblTestString = nil;
bool runAllInALoop = false;
AylaProperty *gblAmlTestFileProperty = nil;

@implementation AylaTest

@end

@implementation TestSequencer : NSObject

- (TestSequencer *)initWithTestSuite:(NSArray *)myTestSuite
{
    self = [super init];
    if (self) {
        nextTestIndex = 0;
        testSuite = myTestSuite;
    }
    return self;
}

+ (TestSequencer *)testSequencerWithTestSuite:(NSArray *)myTestSuite
{
    return [[TestSequencer alloc] initWithTestSuite:myTestSuite];
}

- (void)executeNextTest:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    if (nextTestIndex < testSuite.count) {
        void (^test)(void (^)(void), void (^)(void)) = [testSuite objectAtIndex:nextTestIndex++];
        test(successBlock, failureBlock);
    } else {
        successBlock();      // end the call chain
    }
}

- (int)numberOfTests
{
    return testSuite.count;
}

@end
