"use strict";

define([
	"app",
	"moment",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.oneTouchCriteriaItemView"
], function (App, moment, constants, consumerTemplates, SalusView, OTCriteriaItemMixin) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * This file contains a collectionView as well as childViews for displaying rule criteria
		 * These views are found on the Create/Edit OT page and the detail page,
		 * as well as used within the "Undo" menu
		 */

		/**
		 * OneTouchCriteriaCollectionView
		 * This collection view can be used as a container for any type of rule criteria (condition, action, ...)
		 * Specify the type by giving options {type: constants.oneTouchMenuTypes.*}. Also supports readonly as an option
		 *
		 * It manages displaying a specific childView for a specific type of rule criteria, generated using a OT menu or through RuleParser.
		 *
		 */
		Views.OneTouchCriteriaCollectionView = Mn.CollectionView.extend({
			template: false,
			className: "one-touch-criteria-items",
			getChildView: function (model) {
				var ruleType = model.get("type");

				if (ruleType === "tod") {
					return Views.OneTouchCriteriaTimeOfDay;
				} else if (ruleType === "dow") {
					return Views.OneTouchCriteriaDayOfWeek;
				} else if (ruleType === "sms" || ruleType === "email") {
					return Views.OneTouchCriteriaSendNotification;
				} else if (ruleType === "propVal") {
					return Views.OneTouchCriteriaPropVal;
				} else if (ruleType === "propChange") {
					return Views.OneTouchCriteriaPropChange;
				} else if (ruleType === "buttonPress") {
					return Views.OneTouchCriteriaButtonPress;
				} else if (ruleType === "timer") {
					var subtype = model.get("subType");

					if (subtype === "undo") {
						return Views.OneTouchCriteriaUndoTimer;
					} else if (subtype === "propVal") {
						return Views.OneTouchCriteriaTimedPropChange; // TODO; is this needed for propVal
					} else if (subtype === "propChange") {
						return Views.OneTouchCriteriaTimedPropChange;
					}
				}

				return Views.OneTouchCriteriaItemView;
			},
			childEvents: {
				"remove:selection": "onRemoveSelection"
			},
			childViewOptions: function (model, index) {
				return {
					model: model,
					collectionIndex: index,
					type: this.options.type,
					readonly: this.options.readonly
				};
			},
			initialize: function () {
				_.bindAll(this, "onRemoveSelection");

				this.$el.addClass(this.options.type || "");

				var deleteHookBase = "criteriaCollection:";

				this.useDeleteHook = this.options.useDeleteHook;

				if (this.options.deleteHookKey) {
					this.deleteHookKey = deleteHookBase.concat(this.options.deleteHookKey);
				}

				this.listenTo(this.collection, "reRender:collection", this.render);
			},
			/**
			 * remove from the collection
			 */
			onRemoveSelection: function (criteriaItemView) {
				if (!this.useDeleteHook) {
					criteriaItemView.model.destroy();
					criteriaItemView.destroy();
                    this.trigger("itemDelete");
				} else {
					//trigger event
					var deleteHookObj = {
						criteriaView: criteriaItemView
					};

					this.trigger(this.deleteHookKey, deleteHookObj);
				}
			}
		}).mixin([SalusView]);

		/**
		 * Below begins the specific types of childViews
		 * They manage displaying the rule criteria type as the title,
		 * And give some context as to what the rule does as criteria
		 *
		 * They all should utilize the OTCriteriaItem mixin
		 */

		/**
		 * tod
		 */
		Views.OneTouchCriteriaTimeOfDay = Mn.ItemView.extend({
			initialize: function () {
				this.start = this.model.get("startTime");
//				this.end = this.model.get("endTime");

				// when user creates these for first time, they are moment objects. after downloading back, they are an integer
				if (_.isNumber(this.start)) {
					// convert int to string and prepend 0 if needed
					this.start = this._makeTimeStringFull(this.start.toString());
//					this.end = this._makeTimeStringFull(this.end.toString());

					this.start = moment(this.start, "HHmmss", null, true);
//					this.end = moment(this.end, "HHmmss", null, true);
				}

//				this.isSameTime = this.start.isSame(this.end);
			},

			onRender: function () {
				var startTime = this.start.local().format("HH:mm"),
//					endTime = this.end.local().format("HH:mm"),
					todString = App.translate("equipment.oneTouch.criteria.timeOfDay.prefix", {
						val: startTime
					});

//				if (!this.isSameTime) {
//					// append is... to...
//					todString = todString + " " +
//						App.translate("equipment.oneTouch.criteria.timeOfDay.suffix", {
//							val: endTime
//						});
//				}

				this.ui.description.text(todString);
			},

			_makeTimeStringFull: function (timeString) {
				while (timeString.length < 6) {
					timeString = "0" + timeString;
				}

				return timeString;
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * dow
		 */
		Views.OneTouchCriteriaDayOfWeek = Mn.ItemView.extend({
			initialize: function () {
				this.days = this.model.get("dayPattern");
			},
			onRender: function () {
				var daysString,
					daysOfWeekTranslated,
					descriptionString;

				// try to match up days
				if (this.days.length === 2 && _.difference(this.days, [1, 7]).length === 0) {
					// weekend
					daysString = App.translate("equipment.oneTouch.criteria.dayOfWeek.weekends");
				} else if (this.days.length === 5 && _.difference(this.days, [2, 3, 4, 5, 6]).length === 0) {
					// weekday
					daysString = App.translate("equipment.oneTouch.criteria.dayOfWeek.weekdays");
				} else if (this.days.length === 7 && _.difference(this.days, [1, 2, 3, 4, 5, 6, 7]).length === 0) {
					// everyday
					daysString = App.translate("equipment.oneTouch.criteria.dayOfWeek.everyday");
				} else {
					// specific days
					daysOfWeekTranslated = _.map(this.days, function (dowInt) {
						// need -1 to get sunday(1) -> 0
						return App.translate(constants.daysOfWeekAbbr[dowInt - 1]);
					});

					daysString = daysOfWeekTranslated.join(", ");
				}

				descriptionString = App.translate("equipment.oneTouch.criteria.dayOfWeek.prefix", {
					val: daysString
				});

				this.ui.description.text(descriptionString);
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * email || sms
		 */
		Views.OneTouchCriteriaSendNotification = Mn.ItemView.extend({
			initialize: function () {
				this.recipients = this.model.get("recipients");
			},
			onRender: function () {
				var descriptionString,
					recipientsString = "",
					smsArray;

				if (this.ruleType === "email") {
					if (this.recipients) {
						recipientsString = this.recipients.join(", ");

						descriptionString = App.translate("equipment.oneTouch.criteria.sendAlert.email.prefix", {
							val: recipientsString
						});
					} else {
						descriptionString = App.translate("equipment.oneTouch.criteria.sendAlert.email.sendEmail");
					}

				} else if (this.ruleType === "sms") {
					if (this.recipients) {
						smsArray = _.map(this.recipients, function (recipient) {
							return "+" + recipient.countryCode + recipient.phoneNumber;
						});

						recipientsString = smsArray.join(", ");

						descriptionString = App.translate("equipment.oneTouch.criteria.sendAlert.sms.prefix", {
							val: recipientsString
						});
					} else {
						descriptionString = App.translate("equipment.oneTouch.criteria.sendAlert.sms.sendSMS");
					}
				}

				this.ui.description.text(descriptionString);
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * propVal
		 * A condition for when a device property touches the rule criteria
		 */
		Views.OneTouchCriteriaPropVal = Mn.ItemView.extend({
			initialize: function () {
				// try getting the device name
				if (this.device) {
					this.deviceName = this.device.getDisplayName();
				} else {
					this.device = App.salusConnector.getDeviceByEUID(this.model.get("EUID"));
					this.deviceName = this.device ? this.device.getDisplayName() : this.model.get("EUID");
				}

				// hold bools if they're there
				this.propVal = {
					lt: !!this.model.get("lt") || this.model.get("lt") === 0,
					eq: !!this.model.get("eq") || this.model.get("eq") === 0,
					gt: !!this.model.get("gt") || this.model.get("gt") === 0
				};

				this.propName = this.model.get("propName");
			},
			onRender: function () {
				var descriptionString,
					preposition; // is, is less than, is greater than, is between

				if (this.propVal.eq) {
					preposition = App.translate("equipment.oneTouch.criteria.propVal.is", {
						val: this._getValueForDevice(this.model.get("eq"), this.propName)
					});

				} else if (this.propVal.lt) {
					preposition = App.translate("equipment.oneTouch.criteria.propVal.isLessThan", {
						val: this._getValueForDevice(this.model.get("lt"), this.propName)
					});

					if (this.propVal.gt) {
						preposition = App.translate("equipment.oneTouch.criteria.propVal.isBetween", {
							lower_val: this._getValueForDevice(this.model.get("gt"), this.propName), // gt is lower value
							upper_val: this._getValueForDevice(this.model.get("lt"), this.propName)
						});
					}
				} else if (this.propVal.gt) {
					preposition = App.translate("equipment.oneTouch.criteria.propVal.isGreaterThan", {
						val: this._getValueForDevice(this.model.get("gt"), this.propName)
					});
				}

				descriptionString = this.deviceName + " " + preposition; // "smart plug is " || thermostat is between _gt_ and _lt_

				this.ui.description.text(descriptionString);
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * propChange
		 * An action to set a device property
		 */
		Views.OneTouchCriteriaPropChange = Mn.ItemView.extend({
			initialize: function () {
				// try getting the device name
				if (this.device) {
					this.deviceName = this.device.getDisplayName();
				} else {
					this.device = App.salusConnector.getDeviceByEUID(this.model.get("EUID"));
					this.deviceName = this.device ? this.device.getDisplayName() : this.model.get("EUID");
				}

				this.newVal = this.model.get("newVal");
				this.propName = this.model.get("propName");
			},
			onRender: function () {
				var descriptionString,
					modelType = this.device ? this.device.modelType : "";

				if (modelType) {
					if (modelType === constants.modelTypes.SMARTPLUG || modelType === constants.modelTypes.MINISMARTPLUG) {
						descriptionString = App.translate("equipment.oneTouch.criteria.propVal.smartPlug.action", {
							device_name: this.deviceName,
							val: this._getValueForDevice(this.newVal)
						});
					} else if (modelType === constants.modelTypes.THERMOSTAT || modelType === constants.modelTypes.IT600THERMOSTAT) {
						descriptionString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.action", {
							device_name: this.deviceName,
							val: this._getValueForDevice(this.newVal, this.propName)
						});
					} else if (modelType === constants.modelTypes.WATERHEATER) {
						descriptionString = App.translate("equipment.oneTouch.criteria.propVal.waterHeater.action", {
							device_name: this.deviceName,
							val: this._getValueForDevice(this.newVal, this.propName)
						});
					}
				} else {
					// bit of an error case
					descriptionString = App.translate("equipment.oneTouch.criteria.propVal.generic", {
						device_name: this.deviceName,
						val: this.newVal
					});
				}

				this.ui.description.text(descriptionString);
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * buttonPress
		 */
		Views.OneTouchCriteriaButtonPress = Mn.ItemView.extend({
			onRender: function () {
				this.ui.description.text(App.translate("equipment.oneTouch.criteria.buttonPress"));
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * timedPropChange
		 */
		Views.OneTouchCriteriaTimedPropChange = Mn.ItemView.extend({
			initialize: function () {
				var data = this.model.get("data");

				// try getting the device name
				if (this.device) {
					this.deviceName = this.device.getDisplayName();
				} else {
					this.device = App.salusConnector.getDeviceByEUID(data.EUID);
					this.deviceName = this.device ? this.device.getDisplayName() : data.EUID;
				}

				this.newVal = data.newVal;
				this.propName = data.propName;
				this.hours = Math.floor(this.model.get("totalTime") / 60);
				this.minutes = this.model.get("totalTime") % 60;
			},
			onRender: function () {
				var descriptionString, timedString, hourString, minString, returnString,
					modelType = this.device ? this.device.modelType : "";

				if (modelType) {
					if (modelType === constants.modelTypes.SMARTPLUG || modelType === constants.modelTypes.MINISMARTPLUG) {
						descriptionString = App.translate("equipment.oneTouch.criteria.propVal.smartPlug.action", {
							device_name: this.deviceName,
							val: this._getValueForDevice(this.newVal)
						});
					} else if (modelType === constants.modelTypes.THERMOSTAT || modelType === constants.modelTypes.IT600THERMOSTAT) {
						descriptionString = App.translate("equipment.oneTouch.criteria.propVal.thermostat.action", {
							device_name: this.deviceName,
							val: this._getValueForDevice(this.newVal, this.propName)
						});
					} else if (modelType === constants.modelTypes.WATERHEATER) {
						descriptionString = App.translate("equipment.oneTouch.criteria.propVal.waterHeater.action", {
							device_name: this.deviceName,
							val: this._getValueForDevice(this.newVal, this.propName)
						});
					}
				} else {
					// bit of an error case
					descriptionString = App.translate("equipment.oneTouch.criteria.propVal.generic", {
						device_name: this.deviceName,
						val: this.newVal
					});
				}

				hourString = this.hours > 1 ? "equipment.oneTouch.criteria.timer.hour.hours" : "equipment.oneTouch.criteria.timer.hour.hour";
				minString = this.minutes > 1 ? "equipment.oneTouch.criteria.timer.min.minutes" : "equipment.oneTouch.criteria.timer.min.minute";

				if (this.hours > 0 && this.minutes > 0) {
					timedString = App.translate("equipment.oneTouch.criteria.timer.combined", {
						hour_string: App.translate(hourString, {time: this.hours}),
						min_string: App.translate(minString, {time: this.minutes})
					});
				} else if (this.hours > 0) {
					timedString = App.translate(hourString, {time: this.hours});

				} else if (this.minutes > 0) {
					timedString = App.translate(minString, {time: this.minutes});
				}

				if (timedString) {
					returnString = App.translate("equipment.oneTouch.criteria.timer.baseTime", {
						property_actions: descriptionString,
						time_string: timedString
					});
				} else {
					returnString = App.translate("equipment.oneTouch.criteria.timer.now", {
						property_actions: descriptionString
					});
				}

				this.ui.description.text(returnString);
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * undoTimer
		 */
		Views.OneTouchCriteriaUndoTimer = Mn.ItemView.extend({
			onRender: function () {
				this.ui.description.text("UndoTimerItemView");
			}
		}).mixin([OTCriteriaItemMixin]);

		/**
		 * OneTouchCriteriaItemView
		 * This view is used as an error case. It states that the rule criteria is currently unsupported.
		 * Used in the case of unparsable rules
		 */
		Views.OneTouchCriteriaItemView = Mn.ItemView.extend({
			onRender: function () {
				this.ui.description.text(App.translate("equipment.oneTouch.criteria.unsupported"));
			}
		}).mixin([OTCriteriaItemMixin]);
	});

	return App.Consumer.OneTouch.Views.OneTouchCriteriaCollectionView;
});