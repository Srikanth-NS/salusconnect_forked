"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView"
], function (App, constants, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
		Views.WaterHeaterTile = {};

		Views.WaterHeaterTile.FrontView = Mn.ItemView.extend({
			className: "water-heater-tile",
			template: consumerTemplates["dashboard/tile/equipmentWithSwitcherTileFront"],
			bindings: {
				":el": {
					observe: "RunningMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						$el.removeClass("on off");

						if (val === constants.runningModeTypes.HEAT) {
							$el.addClass("on");
						} else if (val === constants.runningModeTypes.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				},
				".switcher": {
					observe: "HoldType",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						$el.removeClass("on auto off");

						if (val === constants.waterHeaterHoldTypes.CONTINUOUS || val === constants.waterHeaterHoldTypes.BOOST) {
							$el.addClass("on");
						} else if (val === constants.waterHeaterHoldTypes.SCHEDULE) {
							$el.addClass("auto");
						} else if (val === constants.waterHeaterHoldTypes.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				}
			},
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}
		}).mixin([TileContentViewMixin]);

		Views.WaterHeaterTile.BackView = Mn.ItemView.extend({
			className: "water-heater-tile",
			template: consumerTemplates["dashboard/tile/equipmentWithHotWaterTileBack"],
			ui: {
				switcher: ".equipment-switch"
			},
			attributes: {
//				role: "button"
			},
			modelEvents: {
				"set:HoldType": "handleSetHoldType",
				"setting:HoldType": "handleSetttingHoldType",
				"change:cachedMode": "handleChangeCachedMode"
			},
			events: {
				"click .equipment-switch": "equipmentSwitchClick"
			},
			initialize: function () {
				var that = this;

				_.bindAll(this, "handleSetHoldType", "handleSetttingHoldType", "handleChangeCachedMode", "equipmentSwitchClick");

				this.model.getDevicePropertiesPromise().then(function () {
					that.listenTo(that.model.get("HoldType"), "propertiesSynced", that.hideSmallTileSpinner);
					that.listenTo(that.model.get("HoldType"), "propertiesFailedSync", function () {
						that.hideSmallTileSpinner();
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});
					that.listenTo(that.model.get("HoldType"), "getOtherProperty", function() {
						if (!that.model.isSmartPlug()) {
							that.model.get("RunningMode").load();
						}
					});
				});
			},
			bindings: {
				":el": {
					observe: "RunningMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						$el.removeClass("on off");

						if (val === constants.runningModeTypes.HEAT) {
							$el.addClass("on");
						} else if (val === constants.runningModeTypes.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				},
				".equipment-switch": {
					observe: "HoldType",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						$el.removeClass("on auto off");

						if (val === constants.waterHeaterHoldTypes.CONTINUOUS || val === constants.waterHeaterHoldTypes.BOOST) {
							$el.addClass("on");
						} else if (val === constants.waterHeaterHoldTypes.SCHEDULE) {
							$el.addClass("auto");
						} else if (val === constants.waterHeaterHoldTypes.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				}
			},
			equipmentSwitchClick: function () {
				if (!this.spinning && this.model.isOnline() && !this.model.isLeaveNetwork()) {
					this.model.switchWaterHeaterMode();
				}
			},
			handleSetttingHoldType: function () {
				this.showSpinner(this.tileSpinnerOptions);
				//this.showSmallTileSpinner();
			},
			handleSetHoldType: function () {
				this.hideSpinner();
			},
			handleChangeCachedMode: function (modeObj) {
				var mode = modeObj.mode, modes = constants.waterHeaterHoldTypes;

				this.ui.switcher.toggleClass("on", mode === modes.CONTINUOUS)
						.toggleClass("auto", mode === modes.SCHEDULE)
						.toggleClass("off", mode === modes.OFF);
			}
		}).mixin([TileContentViewMixin]);
	});

	return App.Consumer.Dashboard.Views.WaterHeaterTile;
});
