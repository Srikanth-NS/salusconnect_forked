"use strict";

define([
	"app"
], function (App) {
	App.module("Consumer.Dashboard.Models", function (Models, App, B) {
		Models.ModeMenuItemModel = B.Model.extend({
			defaults: {
				modeName: null,
				modeIconClass: null,
				modeColorClass: null,
				modeTypeId: null
			}
		});

		Models.ModeMenuItemCollection = B.Collection.extend({
			model: Models.ModeMenuItemModel
		});
	});

	return App.Consumer.Models.ModeMenuItemCollection;
});