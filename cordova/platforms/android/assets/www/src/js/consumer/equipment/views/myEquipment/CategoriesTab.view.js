"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusTabContent",
	"consumer/dashboard/views/tileContentViews/CategoryTile.view"
], function (App, templates, SalusTabView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.Categories = Mn.CompositeView.extend({
			template: templates["equipment/myEquipment/categoriesComposite"],
			childView: App.Consumer.Dashboard.Views.CategoryTileView,
			childViewContainer: ".list-container",
			initialize: function () {
				this.collection = App.salusConnector.getDeviceCollectionSortedByCategory();
			}
		});

		Views.CategoriesTabView = Mn.LayoutView.extend({
			id: "bb-categories-tab",
			template: templates["equipment/myEquipment/categoriesTab"],
			regions: {
				"categoriesRegion": "#bb-categories-region"
			},
			onRender: function () {
				var that = this;

				this.showSpinner(null, this.$el.find("#bb-categories-region"));

				App.salusConnector.getDataLoadPromise(["devices"]).then(function (/*arrayOfData*/) {
					that.categoriesRegion.show(new Views.Categories());
				});
			}
		}).mixin([SalusTabView]);
	});

	return App.Consumer.Equipment.Views.CategoriesTabView;
});