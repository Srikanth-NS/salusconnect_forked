"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.Validation = function () {
			this.setDefaults({

				_isNotEmptyTrim: function () {
					var val = this.$("input").val() || "";
					return val.trim() !== "";
				},

				isValid: function () {
					var validation = {valid: true};

					if (this.required && !this._isNotEmptyTrim()) {
						// first checks if the field is required

						validation = {
							valid: false,
							message: "common.validation.default.requiredField"
						};
					} else if (this.validationMethod) {
						// custom validation check

						validation = this.validationMethod();
					}

					return validation;
				},

				validateField: function () {
					var validation = this.isValid();

					if (validation && !validation.valid) {
						if (validation.messageText) {
							this._showErrorMesage(validation.messageText);
						} else {
							this.showErrors(App.translate(validation.message));
						}
					} else {
						this.hideErrors();
					}

					return validation;
				},

				addFeedback: function (isValid) {
					this.ui.formControl.toggleClass("has-error", !isValid);
					this.ui.formControl.toggleClass("has-success", isValid);
				},
				showErrors: function (errorMessageKey, insertObject) {
					this._showErrorMesage(App.translate(errorMessageKey, insertObject));
				},
				_showErrorMesage: function (errorMessage) {
					var errorsEl = this.$(".bb-error");

					this.addFeedback(false);
                    errorsEl.html(errorMessage);
					//errorsEl.text(errorMessage);
					errorsEl.removeClass("invisible");
				},

				hideErrors: function () {
					this.$(".bb-error").addClass("invisible");
					this.$(".bb-error").empty();
                    this.$(".bb-error").html("");
					this.addFeedback(true);
				},

				resetView: function () {
					var $bbError = this.$(".bb-error"),
						$bbFormGroup = this.$(".bb-form-group");

					$bbError.addClass("invisible");
					$bbError.empty();

					$bbFormGroup.removeClass("has-error");
					$bbFormGroup.removeClass("has-success");
				}
			});

			this.before("initialize", function () {
				this.validationMethod = this.options.validationMethod;
				this.required = !!this.options.required;
			});

			this.after("initialize", function () {
				var ui = {
					"formControl": ".bb-form-group"
				};

				// merge ui fields
				this.ui = _.extend(ui, _.result(this, "ui"));
			});

			this.after("render", function () {
				if (this.ui.formControl) {
					this.ui.formControl.toggleClass("has-feedback", !!this.options.showFeedback);
				}

				if (this.$(".bb-error").length === 0) {
					this.$el.prepend("<div class='form-error bb-error invisible'></div>");
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.Validation;
});