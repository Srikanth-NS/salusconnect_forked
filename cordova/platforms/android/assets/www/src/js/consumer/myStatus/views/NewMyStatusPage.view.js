"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/constants",
	"common/util/VendorfyCssForJs",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.validation",
	"common/model/salusWebServices/ruleGroups/RuleGroup.model",
	"consumer/myStatus/models/MyStatusModel",
	"consumer/myStatus/models/NewMyStatusCollection",
	"consumer/myStatus/models/NewMyStatusIT600Collection",
	"consumer/views/mixins/mixins",
	"consumer/views/FormTextInput.view",
	"consumer/myStatus/views/MyStatusGroup.view",
	"consumer/views/SalusButtonPrimary.view"
], function (App, config, templates, SalusView, constants, VendorfyCss, SalusPage) {

	App.module("Consumer.MyStatus.Views", function (Views, App, B, Mn, $, _) {
		Views.NewStatusTileView = Mn.ItemView.extend({
			className: "col-xs-4 col-sm-2 my-status-tile",
			template: templates["myStatus/myStatusTile"],
			attributes: {
				"role": "button"
			},
			ui: {
				tileBackground: ".bb-myStatus-tile-background",
				selected: ".bb-myStatus-selected"
			},
			events: {
				"click": "_toggleSelected"
			},
			bindings: {
				".bb-myStatus-selected": {
					observe: "selected",
					onGet: "displayRadioButtonStatus"
				}
			},
			initialize: function () {
				_.bindAll(this, "_toggleSelected");
			},
			onRender: function () {
				VendorfyCss.formatBgGradientWithImage(
						this.ui.tileBackground,
						"center / 48%",
						"no-repeat",
						App.rootPath(constants.myStatusIconPaths[this.model.get("icon")]),
						"145deg",
						this.model.get("topColor"),
						"0%",
						this.model.get("botColor"),
						"100%"
				);
			},
			/**
			 * updates selected icon based on model
			 * onGet method for stickit binding
			 * @param selected - Boolean if scene is selected
			 */
			displayRadioButtonStatus: function (selected) {
				if (selected) {
					this.ui.selected.addClass("selected");
					this.trigger("select:tile");
				} else {
					this.ui.selected.removeClass("selected");
				}
			},
			_toggleSelected: function () {
				if (!this.model.get("selected")) {
					// TODO: do something on ayla/web service
					this.trigger("change:myStatus");
					this.model.set("selected", true);
				}
			}
		}).mixin([SalusView]);

		Views.NewMyStatusPageView = Mn.LayoutView.extend({
			id: "newStatus",
			template: templates["myStatus/newStatus"],
			className: "container",
			regions: {
				statusNameRegion: ".bb-status-name",
				statusTileITRegion: ".bb-myStatus-IT-collection",
				statusTilesRegion: ".bb-myStatus-collection",
				cancelButtonRegion: ".bb-cancel-button",
				submitButtonRegion: ".bb-submit-button"
			},
			ui: {
				"toggleBackground": ".bb-toggle",
				"toggleText": ".bb-toggle-text"
			},
			events: {
				"click .bb-myStatus-icons-toggle": "handleToggleClicked",
				"input": "_validateContinue"
			},

			initialize: function () {
				this.show = false;
				this.selected = false;

				_.bindAll(this,
					"handleContinueClicked",
					"handleToggleClicked",
					"_deactivateFullCollection",
					"_deactivateITCollection",
					"_validateContinue"
				);

				this.statusNameField = new App.Consumer.Views.FormTextInput({
					labelText: "myStatus.newStatus.formFields.statusName"
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default cancel-button",
					clickedDelegate: this.handleCancelClicked
				});

				this.submitButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "myStatus.newStatus.formFields.continue",
					classes: "width100 btn continue-button disabled",
					clickedDelegate: this.handleContinueClicked
				});

				this.statusCollectionView = new Views.MyStatusCollectionView();
				this.statusIT600CollectionView = new Views.MyStatusIT600CollectionView();
				this.fullCollection = App.Consumer.MyStatus.Models.newMyStatusCollection;
				this.ITCollection = this.statusIT600CollectionView.collection;
				//make all models selected values false on initialization
				this._deactivateFullCollection();
				this._deactivateITCollection();

				//start collection with only 12 tiles. Will be added back later with button press
				this.slicedCollection = this.sliceCollection(12);
				this.statusCollectionView.setCollection(this.slicedCollection);

				this.listenTo(this.statusIT600CollectionView, "deactivate:collection", this._deactivateFullCollection);
				this.listenTo(this.statusCollectionView, "deactivate:collection", this._deactivateITCollection);
				this.listenTo(this.statusCollectionView, "select:tile", this._validateContinue);
				this.listenTo(this.statusIT600CollectionView, "select:tile", this._validateContinue);
			},

			sliceCollection: function (sliceAmount) {
				var newCollection = this.fullCollection.models.slice(0, sliceAmount);
				return new B.Collection(newCollection);
			},

			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices", "ruleGroups"]).then(function () {
					that.model = new App.Models.RuleGroupModel({dsn: App.getCurrentGatewayDSN()});
				});

				this.statusNameRegion.show(this.statusNameField);
				this.statusTileITRegion.show(this.statusIT600CollectionView);
				this.statusTilesRegion.show(this.statusCollectionView);
				this.cancelButtonRegion.show(this.cancelButton);
				this.submitButtonRegion.show(this.submitButton);
			},

			handleToggleClicked: function () {
				if (this.show === false) {
					this.statusCollectionView.setCollection(this.fullCollection);
					this.ui.toggleText.text(App.translate("myStatus.newStatus.showLessIcons"));
					this.ui.toggleBackground.removeClass("expand-down");
					this.ui.toggleBackground.addClass("collapse-up");
				}
				else {
					this.statusCollectionView.setCollection(this.slicedCollection);
					this.ui.toggleText.text(App.translate("myStatus.newStatus.showMoreIcons"));
					this.ui.toggleBackground.removeClass("collapse-up");
					this.ui.toggleBackground.addClass("expand-down");

					if (this._shouldDeactivateSelected(this.fullCollection, this.slicedCollection)) {
						this._deactivateFullCollection();
						this._invalidateContinue();
					}
				}

				this.statusCollectionView.render();
				this.show = !this.show;
			},
			handleCancelClicked: function () {
				window.history.back();
			},

			_validateContinue: function () {
				var name = this.statusNameRegion.currentView.getValue(),
						fullModels = this.fullCollection, IT600Models = this.ITCollection;
				var fullModel = fullModels.findWhere({selected: true}),
						IT600Model = IT600Models.findWhere({selected: true});

				if ((fullModel || IT600Model) && _.isString(name) && name.length > 0) {
					var model = fullModel || IT600Model;

					this.submitButtonRegion.currentView.enable();
					return {name: name, model: model};
				} else {
					this.submitButtonRegion.currentView.disable();
					return null;
				}
			},

			_invalidateContinue: function () {
				this.submitButtonRegion.currentView.disable();
			},

			handleContinueClicked: function () {
				var result = this._validateContinue();

				if (result) {
					this.model.set({name: result.name, icon: result.model.get("icon")});
					App.Consumer.MyStatus.Controller.currentPendingRuleGroup = this.model;
					App.navigate("/myStatus/" +  App.getCurrentGatewayDSN() + "/makeStatus");
				}
			},

			_getSelectedModels: function (collection) {
				var models = collection.filter(function (aModel) {
					return aModel.get('selected');
				});

				if (models.length === 1) {
					return models[0];
				}

				return null;
			},

			_shouldDeactivateSelected: function (collection1, collection2) {
				var model = this._getSelectedModels(collection1);
				if (model) {
					var inArray = $.inArray(model, collection2.toArray());

					if (inArray === -1) {
						return true;
					}
				}

				return false;
			},

			_setCollectionSelectedFalse: function (collection) {
				_.each(collection.models, function (myStatusModel) {
					myStatusModel.set("selected", false);
				});
			},

			//see if you can get rid of this
			_deactivateFullCollection: function () {
				this._setCollectionSelectedFalse(this.fullCollection);
			},

			_deactivateITCollection: function () {
				this._setCollectionSelectedFalse(this.ITCollection);
			}
		}).mixin([SalusPage], {
			analyticsSection: "myStatus",
			analyticsPage: "newMyStatus"
		});

		Views.MyStatusCollectionView = Mn.CollectionView.extend({
			childView: App.Consumer.MyStatus.Views.NewStatusTileView,
			childEvents: {
				"change:myStatus": "_deactivateSelected",
				"select:tile": function () {
					this.trigger("select:tile");
				}
			},
			initialize: function () {
				_.bindAll(this, "_deactivateSelected");
				this.collection = App.Consumer.MyStatus.Models.newMyStatusCollection;
			},

			_deactivateSelected: function () {
				_.each(this.collection.models, function (myStatusModel) {
					myStatusModel.set("selected", false);
				});

				this.trigger("deactivate:collection");
			},

			getCollection: function () {
				return this.collection;
			},

			setCollection: function (collection) {
				this.collection = collection;
			}
		}).mixin([SalusView]);

		Views.MyStatusIT600CollectionView = Mn.CollectionView.extend({
			childView: App.Consumer.MyStatus.Views.NewStatusTileView,
			childEvents: {
				"change:myStatus": "_deactivateSelected",
				"select:tile": function () {
					this.trigger("select:tile");
				}
			},
			initialize: function () {
				_.bindAll(this, "_deactivateSelected");

				this.collection = App.Consumer.MyStatus.Models.newMyStatusIT600Collection;
			},

			_deactivateSelected: function () {
				_.each(this.collection.models, function (myStatusModel) {
					myStatusModel.set("selected", false);
				});

				this.trigger("deactivate:collection");
			},

			getCollection: function () {
				return this.collection;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.MyStatus.Views.NewMyStatusPageView;
});
