"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/consumer.views"
], function (App, consumerTemplates, SalusViewMixin, ConsumerViews) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.SendConfirmationEmail = Mn.LayoutView.extend({
			className: "send-confirmation-email-content-well",
			template: consumerTemplates["login/sendConfirmationEmail"],
			regions: {
				submitButtonRegion: "#bb-email-submit-btn-region"
			},
			ui: {
				"bbEmailInputPlaceholder": ".bb-email-input-el-placeholder"
			},
			events: {
				"keydown": "handleEnterButtonPress"
			},
			initialize: function () {
				_.bindAll(this, "handleButtonClicked", "handleEnterButtonPress");

				this.emailBox = new ConsumerViews.SalusTextBox({
					inputClass: "email-form-box",
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2",
					iconPath: App.rootPath("/images/icons/icon_mail_blue.svg"),
					iconAlt: App.translate("login.forgotPassword.inputLabel"),
					inputPlaceholder: App.translate("login.forgotPassword.inputLabel"),
					inputType: "email",
					isRequired: true
				});

				this.submitEmailButton = new ConsumerViews.SalusButtonPrimaryView({
					id: "submit-email-btn",
					buttonTextKey: "login.login.resendConfirmationEmail",
					clickedDelegate: this.handleButtonClicked
				});
			},
			onRender: function () {
				this.ui.bbEmailInputPlaceholder.replaceWith(this.emailBox.render().$el);
				this.submitButtonRegion.show(this.submitEmailButton);
			},
			onDestroy: function () {
				this.emailBox.destroy();
			},
			goToChangePassword: function () {
				App.navigate("login/changePasswordWithToken");
			},
			goToConfirmAccount: function() {
				App.navigate("login/confirm");
			},
			/**
			 * Trigger the form submit when the enter key is pressed
			 */
			handleEnterButtonPress: function (event) {
				if (event.which && event.which === 13) {
					this.handleButtonClicked();
				}
			},
			handleButtonClicked: function () {
				var loginPromise, emailAddress = this.$(".form-control").val(),
					that = this;

				if (App.validate.email(emailAddress)) {
					that.emailBox.clearError();
					loginPromise = App.salusConnector.resendConfirmationToken(emailAddress);

					loginPromise.then(function () {
						that.goToConfirmAccount();
					}).catch(function (fail) {
						if (fail.status === 422) {
							//Check for "The email address has not signed up for an account." and display it if found
							var response = JSON.parse(fail.responseText);
							var failText;

							if (response.errors && response.errors.email)
							{
								failText = response.errors.email.toString();

								switch (failText) {
									case "was already confirmed, please try signing in":
										that.emailBox.registerError(App.translate("common.validation.email.alreadyConfirmed"));
										break;
									case "can't be blank":
										that.emailBox.registerError(App.translate("common.validation.email.notFound"));
										break;
									case "not found":
										that.emailBox.registerError(App.translate("common.validation.email.notFound"));
										break;
									default:
										App.log("SendConfirmationEmailWell: failText Error");
								}
							}
						}

						App.salusConnector.unsetAylaUser();
					});
				} else {
					// Display an error if the email address format is incorrect or the field is blank
					that.emailBox.registerError(App.translate("common.validation.email.notValid"));
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Login.Views.SendConfirmationEmailWell;
});

