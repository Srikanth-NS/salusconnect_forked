"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"common/model/salusWebServices/rules/Rule.model",
	"common/model/salusWebServices/rules/RuleDetails.model",
	"common/model/salusWebServices/rules/RuleMenuCollection",
	"consumer/oneTouch/views/DayOfWeekPicker.view",
	"consumer/oneTouch/views/TimeOfDayPicker.view",
	"consumer/oneTouch/views/ThermostatWhenPicker.view",
	"consumer/oneTouch/views/ContactInfo.view",
	"consumer/oneTouch/views/ThermostatModePicker.view",
	"consumer/oneTouch/views/OneTouchTimerSelect.view",
	"consumer/oneTouch/views/UndoActionSelect.view"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		// item in a menu
		Views.OneTouchMenuItemView = Mn.ItemView.extend({
			template: consumerTemplates["oneTouch/oneTouchMenuItem"],
			className: "menu-item",
			ui: {
				text: ".bb-menu-item-text",
				image: ".bb-menu-item-image",
				arrow: ".bb-menu-item-arrow"
			},
			attributes: {
				role: "button"
			},
			events: {
				click: "_handleClick"
			},
			bindings: {
				":el": {
					observe: "selected",
					onGet: function (selected) {
						return !!selected;
					},
					update: function ($el, selected) {
						$el.toggleClass("selected", selected);
					}
				}
			},
			initialize: function () {
				_.bindAll(this, "_handleClick");
			},
			onRender: function () {
				if (!this.model.childMenu) {
					this.ui.arrow.addClass("hidden");
				}

				if (this.model.get("image")) {
					this.ui.image.removeClass("hidden");
					this.ui.text.addClass("image-showing"); // adjust left: positioning
					this.ui.image.css("background-image", "url(" + this.model.get("image") + ")");
				}
			},
			_handleClick: function () {
				this.trigger("click:menuItem", this);
			},
			templateHelpers: function () {
				this.displayText = this.model.get("displayTextKey") ? App.translate(this.model.get("displayTextKey")) : this.model.get("displayText");

				return {
					displayText: this.displayText
				};
			}
		}).mixin([SalusView]);

		// CompositeView holds menu items and handles expanding
		// it will take in the type that it is handling
		Views.OneTouchMenuCompositeView = Mn.CompositeView.extend({
			template: consumerTemplates["oneTouch/oneTouchMenu"],
			className: "one-touch-menu",
			childView: Views.OneTouchMenuItemView,
			childViewContainer: ".bb-menu-items",
			childEvents: {
				"click:menuItem": "_handleMenuItemClick"
			},
			ui: {
				header: ".bb-menu-header",
				headerText: ".bb-header-text",
				headerIcon: ".bb-header-icon",
				menuItems: ".bb-menu-items",
				backButton: ".bb-back-button",
				finishedButton: ".bb-finished-button",
				uniqueViewContainer: ".bb-unique-view-container"
			},
			events: {
				"click .bb-menu-header": "_handleHeaderClick"
			},

			initialize: function (/*options*/) {
				_.bindAll(this,
						"_handleNextClick",
						"_handleFinishClick",
						"_handleBackClick",
						"_handleHeaderClick",
						"_handleMenuItemClick",
						"_expandMenu",
						"_collapseMenu",
						"resetMenuState",
						"_setChildViewSelected",
						"_getRuleDataForLeafMenuItem"
				);

				// handles our menu navigation
				// depth gets set 0 on init
				this.menuManager = new App.Models.OneTouchMenuManager();

				this.resetMenuState();
			},

			resetMenuState: function () {
				this.currentMenu = this.menuManager.getMenu(this.model.get("type"));
				this._parseMenuObj(this.currentMenu);
				this.oldMenus = [];
			},

			onRender: function () {
				var View,
						type = this.model.get("type");

				if (this.model.get("isLeafMenu")) {
					this.ui.backButton.removeClass("col-xs-12").addClass("col-xs-6");
					this.ui.finishedButton.addClass("col-xs-6");
				}
                
				if (this.model.get("uniqueView")) {
					View = this.getUniqueViewByType(this.model.get("uniqueView"));

					if (this.previousUniqueViewRuleData) {
						this.uniqueView = new View({
							menu: this.currentMenu,
							manager: this.menuManager,
							previousUniqueViewRuleData: this.previousUniqueViewRuleData
						});
					} else {
						this.uniqueView = new View({
							menu: this.currentMenu,
							manager: this.menuManager
						});
					}

					this.ui.uniqueViewContainer.append(this.uniqueView.render().$el);

					this.ui.backButton.removeClass("col-xs-12").addClass("col-xs-6");
					this.ui.finishedButton.addClass("col-xs-6");
				}
                
				if (type === constants.oneTouchMenuTypes.condition) {
					this.ui.headerText.text(App.translate("equipment.oneTouch.menus.when.header"));
				} else if (type === constants.oneTouchMenuTypes.action) {
					this.ui.headerText.text(App.translate("equipment.oneTouch.menus.doThis.header"));
				}  else if (type === constants.oneTouchMenuTypes.delayedAction) {
					this.ui.headerText.text(App.translate("equipment.oneTouch.menus.thenDoThis.header"));
				}
			},

			getUniqueViewByType: function (type) {
				var View;

				switch (type) {
					case "dow-picker":
						View = App.Consumer.OneTouch.Views.DayOfWeekPickerView;
						break;
					case "tod-picker":
						View = App.Consumer.OneTouch.Views.TimeOfDayPickerView;
						break;
					case "thermostat-select-view":
						View = App.Consumer.OneTouch.Views.ThermostatWhenPickerView;
						break;
					case "contact-info":
						View = App.Consumer.OneTouch.Views.ContactInfoView;
						break;
					case "thermostat-mode-picker": // system mode and fan mode
						View = App.Consumer.OneTouch.Views.ThermostatModePickerView;
						break;
					case "timer": // timer view for 'then do this later' menu items
						View = App.Consumer.OneTouch.Views.OneTouchTimerSelectView;
						break;
					case "undo-action-select":
						View = App.Consumer.OneTouch.Views.UndoActionSelectView;
						break;
					default:
						break;
				}

				return View;
			},

			_handleHeaderClick: function (/* event */) {
				if (!this.$el.hasClass("expanded")) {
					this._expandMenu();
				} else {
					this._collapseMenu();
				}
			},

			_expandMenu: function () {
				// expand the menu
				this.$el.addClass("expanded");

				// swap header icons
				this.ui.headerIcon.removeClass("plus").addClass("minus");

				// show the menu items
				if (this.model.get("uniqueView")) {
					this.ui.uniqueViewContainer.removeClass("hidden");
				} else {
					this.ui.menuItems.removeClass("hidden");
				}

				// show the cancel button for collapse
				this.backButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100 min-width-btn center-block",
					buttonTextKey: "common.labels.back",
					clickedDelegate: this._handleBackClick
				});
				this.ui.backButton.append(this.backButtonView.render().$el);

				this.ui.backButton.removeClass("hidden");

				// if we're at the end of a menu show finish
				if (this.model.get("isLeafMenu")) {
					this.finishedButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
						classes: "min-width-btn width100 center-block",
						buttonTextKey: "common.labels.set",
						clickedDelegate: this._handleFinishClick
					});
					this.ui.finishedButton.append(this.finishedButtonView.render().$el);

					this.ui.finishedButton.removeClass("hidden");
				} else if (!this.model.get("isLeafMenu") && this.model.get("uniqueView")) {
					this.finishedButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
						classes: "min-width-btn width100 center-block",
						buttonTextKey: "common.labels.next",
						clickedDelegate: this._handleNextClick
					});
					this.ui.finishedButton.append(this.finishedButtonView.render().$el);
					this.ui.finishedButton.removeClass("hidden");
				}
                
                if(this.currentMenu.preselected && this.currentMenu.preselected.length === 0) {
                    this.ui.finishedButton.children().attr("disabled", true);
                } else {
                    this.ui.finishedButton.children().attr("disabled", false);
                }
			},

			_collapseMenu: function () {
				if (!this.isDestroyed) {
					this.$el.removeClass("expanded");

					// swap header icons
					this.ui.headerIcon.removeClass("minus").addClass("plus");

					this.ui.uniqueViewContainer.addClass("hidden");
					this.ui.menuItems.addClass("hidden");

					// hide buttons
					this.ui.backButton.addClass("hidden");
					this.ui.finishedButton.addClass("hidden");

					// always reset depth when menu collapses
					this.menuManager.depth = 0;
					this.resetMenuState();

					// render loads things in collapsed state
					this.render();
				}
			},

			/**
			 * reRender the new collection and keep open
			 * todo: slide animations
			 * @private
			 */
			_transition: function () {
				if (this.uniqueView) {
					this.uniqueView.destroy();
					this.uniqueView = null;
				}

				this.render();
				this._expandMenu();
			},

			_setChildViewSelected: function (selectedChildView) {

				this.children.each(function(view) {
					view.model.set("selected", false);
				});

				selectedChildView.model.set("selected", true);
			},

			_getRuleDataForLeafMenuItem: function(menuType) {
				var ruleData,
					propName,
					that = this,
					selected = this.collection.findWhere({selected: true}),
					isCondition = menuType === constants.oneTouchMenuTypes.condition;

				// setup an array of property names if it is an array coming in
				if (selected && _.isArray(selected.get("condition"))) {
					propName = _.map(selected.get("condition"), function (prop) {
						if (isCondition) {
							return that.menuManager.currentDevice.getPropertyName(prop);
						} else {
							return that.menuManager.currentDevice.getSetterPropNameIfPossible(prop);
						}
					});
				} else {
					if (selected && selected.get("condition") && this.menuManager.currentDevice) {
						if (isCondition) {
							propName = this.menuManager.currentDevice.getPropertyName(selected.get("condition"));
						} else {
							propName = this.menuManager.currentDevice.getSetterPropNameIfPossible(selected.get("condition"));
						}
					}
				}

				if (selected && propName) {
					if (menuType === constants.oneTouchMenuTypes.delayedAction) {
						ruleData = {
							type: "propChange",
							propName: propName, // for rule
							eq:  null, // 0, 1 for smart plug - for condition
							newVal:  selected.get("propertyValue"),
							device: this.menuManager.currentDevice,
							EUID: this.menuManager.currentDevice.getEUID()
						};
					} else {
						ruleData = {
							type: !isCondition ? "propChange" : "propVal",
							propName: propName, // for rule
							eq: isCondition ? selected.get("propertyValue") : null, // 0, 1 for smart plug - for condition
							newVal: !isCondition ? selected.get("propertyValue") : null, // for action
							device: this.menuManager.currentDevice,
							EUID: this.menuManager.currentDevice.getEUID()
						};

						// don't allow duplicates
						if (this.menuManager.nonDuplicateCriteria(selected.get("type"), ruleData)) {
							this.menuManager.addToRule(selected, ruleData);
							this._collapseMenu();
						}
					}
				}

				return ruleData;
			},

			_handleMenuItemClick: function (childView) {
				if (childView.model.get("device")) {
					this.menuManager.setDevice(childView.model.get("device"));
				}

				if (childView.model.get("isLeafMenu")) {
                    if(childView.model.get("ruleData")){
                        this.menuManager.addToRule(childView.model, childView.model.get("ruleData"));
                        this._collapseMenu();
                    }else{
                        this._setChildViewSelected(childView);
                    }
				} else {

					if (childView.model.get("isTimed")) {
						// special case handling clicking a menu item for a delayed action
						this._setChildViewSelected(childView);

						this.previousUniqueViewRuleData = this._getRuleDataForLeafMenuItem(constants.oneTouchMenuTypes.delayedAction);
					}

					this.menuManager.depth++;

					// todo: append a root selection's text to the header

					this.oldMenus.push(this.currentMenu);
					this.currentMenu = childView.model.childMenu;

					this._parseMenuObj(this.currentMenu);

					this._transition();
				}
			},

			_handleNextClick: function () {
				this.menuManager.depth++;
				// todo: append a root selection's text to the header
				this.oldMenus.push(this.currentMenu);

				//we are coming in from a unique view, so grab the previous views rule data
				this.previousUniqueViewRuleData = this.uniqueView.getRuleData();

				// special case to handling a transition from unique view to unique view
				// unique view should only have a single child view it can link to.
				this.currentMenu = this.currentMenu.childMenuCollection.first().attributes;
				this._parseMenuObj(this.currentMenu);
				this._transition();
			},

			_handleFinishClick: function () {
				var ruleData;

				if (this.uniqueView) {
					ruleData = this.uniqueView.getRuleData();

					if (ruleData) {
						this.menuManager.addToRule(this.model, ruleData);
						this._collapseMenu();
					}
				} else if (this.model.get("isLeafMenu")) {
					this._getRuleDataForLeafMenuItem(this.model.get("type"));
				}
			},

			_handleBackClick: function () {
				if (this.menuManager.depth === 0) {
					this._collapseMenu();
				} else if (this.menuManager.depth > 0) {
					this.menuManager.depth--;
					this.currentMenu = this.oldMenus.pop();
					this._parseMenuObj(this.currentMenu);
					this._transition();
				}
			},

			_parseMenuObj: function (menu) {
				if (menu) {
					this.collection = menu.childMenuCollection;

					var currentSelected = this.collection ? this.collection.findWhere({selected: true}) : false;

					if (currentSelected) {
						currentSelected.set("selected", false);
					}

					this.model.set(menu.menuModel.toJSON());
				}
			},

			onDestroy: function () {
				if (this.uniqueView) {
					this.uniqueView.destroy();
				}

				if (this.finishedButtonView) {
					this.finishedButtonView.destroy();
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.OneTouchMenuCompositeView;
});