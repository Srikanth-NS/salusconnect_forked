"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	/**
	 * TRV stands for Thermostat Radiator Valve
	 */
	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.IT600TRV = B.Model.extend({
			tileType: "trv",
			modelType: constants.modelTypes.IT600TRV,
			defaults: {
				// get properties
				FirmwareVersion: null, //string
				MACAddress: null, //string length 16
				SubDeviceName_c: null, //string - not used _c naming is as defined on device wiki
				DeviceType: null, //integer 2:iT600TRV
				ManufactureName: null, //string length 32
				ModelIdentifier: null, //string length 32
				DeviceIndex: null, //integer No need to display in Apps

				// set properties
				SetIndicator: null //integer Turn on the device identification indicator with specified time in second
			},
			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 2,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_trv.svg"),
					device_category_type: constants.categoryTypes.TRVS,
					device_category_name_key: "equipment.myEquipment.categories.types.trvs"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_trv_grey.svg")
				});
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.IT600TRV;
});
