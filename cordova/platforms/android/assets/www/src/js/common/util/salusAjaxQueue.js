'use strict';
require([
	"app",
	"bluebird",
	"common/config"
], function (App, P, config) {
	App.module("Utilities", function (Utilities, App, B, Mn, $, _) {
		var queue = [], runningRequests = [];

		$.sAjaxQueue = function (opts) {
			function enqueue(cb) {
				// if there are less than 3 running requests we can execute it now
				if (runningRequests.length < config.salusAjaxQueueMaxRequests) {
					runningRequests.push(cb());
				} else {
					// else add it to the end of the queue
					queue.push(cb);
				}
			}

			function dequeue() {
				var completedXHR = arguments[2],
						cb = queue.shift();

				// remove the completed request from runningRequests
				runningRequests = _.filter(runningRequests, function (req) {
					return req !== completedXHR;
				});

				if (cb) {
					// execute the new request and push it onto running requests
					var xhr = cb();
					runningRequests.push(xhr);
				}
			}

			var options = _.extend({}, opts);
			enqueue(function () {
				var xhr = $.ajax(options);

				xhr.always(dequeue);

				return xhr;
			});
		};

		$.sAjaxQueue.isRunning = function () {
			return runningRequests.length > 0;
		};

		$.sAjaxQueue.getRunningRequests = function () {
			return runningRequests;
		};

		$.sAjaxQueue.abort = function () {
			_.each(runningRequests, function (req) {
				req.abort();
			});
			runningRequests = [];
		};

		$.sAjaxQueue.clear = function () {
			queue = [];
		};

	});
});