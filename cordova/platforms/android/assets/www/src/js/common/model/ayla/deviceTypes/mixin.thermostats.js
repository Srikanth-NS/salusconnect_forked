"use strict";
define([
	"app",
	"moment",
	"common/constants"
], function (App, moment, constants) {
	App.module("Models", function (Models) {
		Models.ThermostatModelMixin = function () {
			this.before("initialize", function () {
				var that = this;
				this.listenToOnce(this, "loading:deviceProperties", function () {
					that._propPromise.then(function () {
						if (that.modelType === constants.modelTypes.IT600THERMOSTAT || that.modelType === constants.modelTypes.THERMOSTAT) {
							that._setUpTemperatureListeners();
						}
					});
				});
			});

			this.addToObj({
				defaults: {
					temporaryTemperatureValue: null
				}
			});

			this.setDefaults({
				_setUpTemperatureListeners: function () {
					this.listenTo(this.get("HeatingSetpoint_x100"), "change:value", this._updateDisplaySetpoint);
					this.listenTo(this.get("CoolingSetpoint_x100"), "change:value", this._updateDisplaySetpoint);
					this.listenTo(this.get("SystemMode"), "change:value", this._updateDisplaySetpoint);
					this._updateDisplaySetpoint();
				},

				_updateDisplaySetpoint: function () {
					if (this.get("SystemMode") && this.allowChange) {
						// currently setting it to heating mode unless we are explicitly cooling;
						var prop = this.get("SystemMode") && this.getPropertyValue("SystemMode") === constants.thermostatModeTypes.COOL ?
								"CoolingSetpoint_x100" : "HeatingSetpoint_x100";
						
						this.set("temporaryTemperatureValue", this.getPropertyValue(prop) / 100);
					}
				}
			});
		};

		return Models.ThermostatModelMixin;
	});
});