"use strict";
define([
    "app",
    "bluebird",
    "moment",
    "common/constants",
    "common/AylaConfig",
    "common/model/ayla/mixin.AylaBacked",
    "common/model/ayla/AylaSchedule.model"
], function (App, P, moment, constants, AylaConfig, AylaBackedMixin) {
    App.module("Models", function (Models, App, B, Mn, $, _) {

        Models.AylaScheduleCollection = B.Collection.extend({
            model: Models.AylaSchedule,
            initialize: function (data, options) {
                this.deviceId = options.deviceId;
            },
            refresh: function (isLowPriority) {
                this._detailPromise = this.load(isLowPriority);
                return this._detailPromise;
            },
            load: function (isLowPriority) {
                var that = this, url = _.template(AylaConfig.endpoints.device.schedules.list)({deviceId: this.deviceId});

                return App.salusConnector.getDevice(this.deviceId).getDevicePropertiesPromise(isLowPriority).then(function () {
                    return App.salusConnector.makeAjaxCall(url, null, "GET", "json", {isLowPriority: isLowPriority}).then(function (data) {
                        that.set(data, {parse: true, deviceId: that.deviceId});

                        // load actions for all schedules
                        return P.all(that.map(function (schedule) {
                            return schedule.loadActions(isLowPriority);
                        }));
                    });
                });
            },
            saveAll: function () {
                var that = this;
                //保存前先将不用的置active为false且更新到ayla中,好让下面的再拿来复用而不会导致数据混乱
//                var promise = P.all(_.map(this.getDeleteMeSchedules(), function (schedule) {
//                    return schedule.remove();
//                }));
                var savePromise=P.defer();
//                deletePromise.promise.then(function (){
//                    that.sortAllScheduleEndTime();
//                    that.takeTurnSave(that.getActiveSchedules(),0,savePromise);
//                });
//                this.takeTurnDelete(this.getDeleteMeSchedules(),0,deletePromise);
                
                that.takeTurnSave(that.getSaveMeSchedules(),0,savePromise);
                
                
                return savePromise.promise;
                
                

//                return promise.then(function () {
//                    return P.all(_.map(that.getActiveSchedules(), function (schedule) {
//                        return schedule.saveDeleteOrUpdate();
//                    }));
//                });
            },
            //轮流删除schedule
            takeTurnDelete:function (schedules,i,promise){
                var that=this;
                if(schedules){
                    if(!i){
                        i=0;
                    }
                    if(schedules[i]){
                        schedules[i].remove().then(function (){
                            setTimeout(function (){
                                that.takeTurnDelete(schedules,++i,promise);
                            },200);
                        });
                    }else{
                        promise.resolve();
                    }
                }
            },
            //轮流保存schedule
            takeTurnSave:function (schedules,i,promise){
                var that=this;
                if(schedules){
                    if(!i){
                        i=0;
                    }
                    if(schedules[i]){
                        schedules[i].saveDeleteOrUpdate().then(function (){
                            if(schedules.length===i+2){
                                setTimeout(function (){
                                    that.takeTurnSave(schedules,++i,promise);
                                },1000);
                            } else {
                                that.takeTurnSave(schedules,++i,promise);
                            }
                            
                        });
                    }else{
                        promise.resolve();
                    }
                }
            },
            
            deleteAll: function () {
//                return P.all(_.map(this.getActiveSchedules(), function (schedule) {
//                    return schedule.remove();
//                }));
                
                var deletePromise = P.defer();
                this.takeTurnDelete(this.getActiveSchedules(),0,deletePromise);
                
                return deletePromise.promise;

            },
//			//跟deleteAll相比,只是标记为false,并未去更新到服务器
//			inactiveAll: function (){
//			    return P.all(_.map(this.getActiveSchedules(), function (schedule) {
//                	return schedule.set("active",false);
//                }));
//			},

            getScheduleById: function (id) {
                return this.findWhere({
                    "key": id
                });
            },
            getInactiveSchedules: function () {
                return this.filter(function (schedule) {
                    return !schedule.get("active") && schedule.get("key");
                });
            },
//            getDeleteMeSchedules: function () {
//                return this.filter(function (schedule) {
//                    return schedule._deleteMe && schedule.get("key");
//                });
//            },
            getActiveSchedules: function () {
                return this.filter(function (schedule) {
                    return !!schedule.get("active");
                });
            },
            getSaveMeSchedules: function (){
                return this.filter(function (schedule){
                    return schedule.isSaveMe();
                });
            },
            getWeekType: function () {
                var activeSchedules = this.getActiveSchedules();
                if (activeSchedules.length > 0) {
                    var firstSchedule = activeSchedules[0];
                    var daysOfWeek = firstSchedule ? firstSchedule.get("days_of_week") : null;

                    if (daysOfWeek.length === 7) {
                        return constants.scheduleConfigurationTypes.FULLWEEK;
                    } else if (daysOfWeek.length > 1) {
                        return constants.scheduleConfigurationTypes.WORKWEEK;
                    } else {
                        return constants.scheduleConfigurationTypes.DAILY;
                    }
                } else {
                    return false;
                }
            },
            /**
             * 先将collec跟schedule对比并将collec保存到schedule中,然后排序schedule的end time,并标记出哪些schedule修改过需要save的(saveMe为true)
             * 
             * @param {type} collec 用户操作页面中看到的schedule
             * @returns {undefined}
             */
            sortAndMarkSaveSchedule: function (collecs) {
                var that=this,existsKeys=[];
                
                //找出所有没有被删除的schedule的key
                collecs.each(function (collec) {
                    collec.get("scheduleCollection").each(function (scheduleData) {
                        if(scheduleData.get("key")){
                            existsKeys.push(scheduleData.get("key"));
                        }
                    });
                });
                
                //查找出哪些schedule已经被删除的,active置为false并标记为saveMe
                this.each(function (schedule) {
                    if (schedule.get("key") && !_.contains(existsKeys, schedule.get("key")) && schedule.get("active")) {
                        schedule.set("active",false);
                        schedule.markForSaveMe();
                    }
                });
                
                //新增schedule并判断存在key的schedule哪些被用户修改过数值的
                collecs.each(function (collec) {
                    collec.get("scheduleCollection").each(function (scheduleData) {
                        var schedule;
                        if(scheduleData.get("key")){
                            schedule=that.getScheduleById(scheduleData.get("key"));
                            //判断time是否修改了
                            if(scheduleData.get("time")!==schedule.get("start_time_each_day")){
                                schedule.markForSaveMe();
                            }else{
                                //判断value是否修改了
                                var actionChange=_.find(schedule.get("schedule_actions").models,function (action){
                                    return action.get("value")!==scheduleData.get("value");
                                });
                                if(actionChange){
                                    schedule.markForSaveMe();
                                }
                            }
                        } else {
                            var inactiveSchedules=that.getInactiveSchedules();
                            if(inactiveSchedules.length>0){
                                schedule=inactiveSchedules[0];
                                schedule.set("active",true);
                            }else{
                                schedule = new Models.AylaSchedule(null, {
                                    deviceId: that.deviceId
                                });
                                that.push(schedule);
                            }
                            schedule.markForSaveMe();
                            schedule.setScheduleData(scheduleData);
                        }
                        schedule.setFromData(scheduleData.toJSON(), collec.get("type"));
                    });
                });
                
                //调整所有schedule的end time的时间(第一个schedule的end time等于第二个schedule的start time的前一秒这样的逻辑)如果修改了则标记为saveMe
                var days=this.getWeekSchedule().collection.models;
                
                _.map(days,function (day){
                    var schedules=day.get("scheduleCollection").models;
                    var sortSchedules=_.sortBy(schedules,function (schedule){
                        return parseInt(moment(schedule.get('start_time_each_day'),"HH:mm:ss").format("HHmmss"));
                    });
                    for(var i=0;i<sortSchedules.length;i++){
                        var newEndTime;
                        if(i<sortSchedules.length-1){
                            newEndTime=moment(sortSchedules[i+1].get('start_time_each_day'),"HH:mm:ss").subtract(1, 'seconds').format("HH:mm:ss");
                        } else if(i == sortSchedules.length-1){
                            newEndTime="23:59:59";
                        }
                        
                        if(sortSchedules[i].get("end_time_each_day")!==newEndTime){
                            sortSchedules[i].set("end_time_each_day",newEndTime);
                            sortSchedules[i].markForSaveMe();
                        }
                        
                        
                    }
                });
                
                
                
//                var that = this, schedule, updatedKeys = [];
//
//                collec.each(function (model) {
//                    model.get("scheduleCollection").each(function (scheduleData) {
//                        //存在key,说明是需要更新的
//                        if (scheduleData.get("key")) {
//                            //根据key获取出this的具体schedule对象(scheduleData只有一部份值)
//                            schedule = that.getScheduleById(scheduleData.get("key"));
//                            updatedKeys.push(scheduleData.get("key"));
//                        } else {
//                            //不存在key,说明是新增的
//                            schedule = new Models.AylaSchedule(null, {
//                                deviceId: that.deviceId
//                            });
//                            schedule.setScheduleData(scheduleData);
//                            that.push(schedule);
//                        }
//                        schedule.setFromData(scheduleData.toJSON(), model.get("type"));
//                    });
//                });
//
//                //判断哪些是需要删除的
//                that.each(function (schedule) {
//                    if (schedule.get("key") && !_.contains(updatedKeys, schedule.get("key"))) {
//                        schedule.markForDeletion();
//                    }
//                });
            },
            getCurrentSchedule: function () {
                var that = this,
                        timeString = moment(new Date()).utc().format("HH:mm:ss");

                return this.find(function (model, index) {
                    return model.get("active") && moment(model.get("start_time_each_day")).utc().format("HH:mm:ss") >= timeString || index === that.length - 1;
                });
            },
            deepCopy: function () {
                return _.map(this.getActiveSchedules(), function (schedule) {
                    return schedule.deepCopy();
                });
            },
            setFromCopy: function (newJSON) {
                var that = this;
                
                this.markAllScheduleInactiveAndSaveMe();

                _.each(newJSON, function (scheduleJSON) {
                    var model, inactive = that.getFirstInactive();
                    scheduleJSON.device_id = that.deviceId;
                    if (inactive) {
                        inactive.setFromCopy(_.omit(scheduleJSON, "key"));
                        inactive.set("active", true);
                        inactive.markForSaveMe();
                    } else {
                        model = that.add(scheduleJSON, {parse: true});
                        model.updateActionsPostCopy(scheduleJSON);
                        model.markForSaveMe();
                    }
                });
                return that.saveAll();

            },
            
            /**
             * 将所有schedule active置为false且save me
             * @returns {undefined}
             */
            markAllScheduleInactiveAndSaveMe: function (){
                _.each(this.getActiveSchedules(),function (schedule){
                    schedule.set("active",false);
                    schedule.markForSaveMe();
                });
            },
            
            //返回分天数放置的schedule
            getWeekSchedule: function (){
                if (this.length > 0) {
                    var workWeekActionArray, actionArray, weekendActionArray,
                            fullWeekSchedules = [], workWeekSchedules = [], weekendSchedules = [], dailySchedules = [],
                            activeSchedules = this.getActiveSchedules();
                    _.each(activeSchedules, function (schedule) {
                        switch (schedule.get("days_of_week").length) {
                            case 7:
                                fullWeekSchedules.push(schedule);
                                break;
                            case 5:
                                workWeekSchedules.push(schedule);
                                break;
                            case 2:
                                weekendSchedules.push(schedule);
                                break;
                            case 1:
                                dailySchedules.push(schedule);
                                break;
                            default:
                                App.error("Unknown Day of Week On Schedule");
                        }
                    });

                    if (dailySchedules.length > 0) {
                        var returnObj = {
                            isEmpty: false,
                            collection: new B.Collection([])
                        };

                        for (var i = 1; i <= 7; i++) {
                            returnObj.collection.push({
                                "day": App.translate("common.daysOfWeek.abbreviations." + constants.daysOfWeekMap[i - 1]),
                                "type": i,
                                "scheduleCollection": new B.Collection()
                            });
                        }


                        _.each(dailySchedules, function (schedule) {
                            //creates a display friendly return object. Adds time, value, and key, to the each member of dailySchedules scheduleCollection
                            returnObj.collection.at(schedule.get("days_of_week")[0] - 1).get("scheduleCollection").add(schedule);
                        });

                        return returnObj;
                    } else if (fullWeekSchedules.length > 0 && workWeekSchedules.length === 0 && weekendSchedules.length === 0) {
                        actionArray = _.map(fullWeekSchedules, function (schedule) {
                            return schedule;
                        });

                        return {
                            isEmpty: false,
                            collection: new B.Collection([
                                {
                                    "type": constants.scheduleDaysTypes.FULLWEEK,
                                    "day": App.translate("schedules.schedule.weekConfigurations.MO-SU"),
                                    "scheduleCollection": new B.Collection(actionArray)
                                }
                            ])
                        };
                    } else {
                        weekendActionArray = _.map(weekendSchedules, function (schedule) {
                            return schedule;
                        });

                        workWeekActionArray = _.map(workWeekSchedules, function (schedule) {
                            return schedule;
                        });

                        return {
                            isEmpty: false,
                            collection: new B.Collection([
                                {
                                    "type": constants.scheduleDaysTypes.WORKWEEK,
                                    "day": App.translate("schedules.schedule.weekConfigurations.MO-FR"),
                                    "scheduleCollection": new B.Collection(workWeekActionArray)
                                },
                                {
                                    "type": constants.scheduleDaysTypes.WEEKEND,
                                    "day": App.translate("schedules.schedule.weekConfigurations.SA-SU"),
                                    "scheduleCollection": new B.Collection(weekendActionArray)
                                }
                            ])
                        };
                    }
                }

                return {
                    isEmpty: true,
                    collection: new B.Collection([
                        {
                            "type": constants.scheduleDaysTypes.WORKWEEK,
                            "day": App.translate("schedules.schedule.weekConfigurations.MO-FR"),
                            "scheduleCollection": new B.Collection()
                        },
                        {
                            "type": constants.scheduleDaysTypes.WEEKEND,
                            "day": App.translate("schedules.schedule.weekConfigurations.SA-SU"),
                            "scheduleCollection": new B.Collection()
                        }
                    ])
//                    collection: new B.Collection([
//                        {
//                            "type": "fullWeek",
//                            "day": App.translate("schedules.schedule.weekConfigurations.MO-SU"),
//                            "scheduleCollection": new B.Collection()
//                        }
//                    ])
                };
            },
            
            getDisplayCollectionObj: function () {
                if (this.length > 0) {
                    var workWeekActionArray, actionArray, weekendActionArray,
                            fullWeekSchedules = [], workWeekSchedules = [], weekendSchedules = [], dailySchedules = [],
                            activeSchedules = this.getActiveSchedules();
                    _.each(activeSchedules, function (schedule) {
                        switch (schedule.get("days_of_week").length) {
                            case 7:
                                fullWeekSchedules.push(schedule);
                                break;
                            case 5:
                                workWeekSchedules.push(schedule);
                                break;
                            case 2:
                                weekendSchedules.push(schedule);
                                break;
                            case 1:
                                dailySchedules.push(schedule);
                                break;
                            default:
                                App.error("Unknown Day of Week On Schedule");
                        }
                    });

                    if (dailySchedules.length > 0) {
                        var returnObj = {
                            isEmpty: false,
                            collection: new B.Collection([])
                        };

                        for (var i = 1; i <= 7; i++) {
                            returnObj.collection.push({
                                "day": App.translate("common.daysOfWeek.abbreviations." + constants.daysOfWeekMap[i - 1]),
                                "type": i,
                                "scheduleCollection": new B.Collection()
                            });
                        }


                        _.each(dailySchedules, function (schedule) {
                            //creates a display friendly return object. Adds time, value, and key, to the each member of dailySchedules scheduleCollection
                            returnObj.collection.at(schedule.get("days_of_week")[0] - 1).get("scheduleCollection").add({
                                "time": schedule.get("start_time_each_day"),
                                "value": schedule.get("schedule_actions").length > 0 ? schedule.get("schedule_actions").first().get("value") : null, // all schedule actions should have same value, different properties
                                "key": schedule.get("key")
                            });
                        });

                        return returnObj;
                    } else if (fullWeekSchedules.length > 0 && workWeekSchedules.length === 0 && weekendSchedules.length === 0) {
                        actionArray = _.map(fullWeekSchedules, function (schedule) {
                            return {
                                "time": schedule.get("start_time_each_day"),
                                "value": schedule.get("schedule_actions").length > 0 ? schedule.get("schedule_actions").first().get("value") : null, // all schedule actions should have same value, different properties
                                "key": schedule.get("key")
                            };
                        });

                        return {
                            isEmpty: false,
                            collection: new B.Collection([
                                {
                                    "type": constants.scheduleDaysTypes.FULLWEEK,
                                    "day": App.translate("schedules.schedule.weekConfigurations.MO-SU"),
                                    "scheduleCollection": new B.Collection(actionArray)
                                }
                            ])
                        };
                    } else {
                        weekendActionArray = _.map(weekendSchedules, function (schedule) {
                            return {
                                "time": schedule.get("start_time_each_day"),
                                "value": schedule.get("schedule_actions").length > 0 ? schedule.get("schedule_actions").first().get("value") : null, // all schedule actions should have same value, different properties
                                "key": schedule.get("key")
                            };
                        });

                        workWeekActionArray = _.map(workWeekSchedules, function (schedule) {
                            return {
                                "time": schedule.get("start_time_each_day"),
                                "value": schedule.get("schedule_actions").length > 0 ? schedule.get("schedule_actions").first().get("value") : null, // all schedule actions should have same value, different properties
                                "key": schedule.get("key")
                            };
                        });

                        return {
                            isEmpty: false,
                            collection: new B.Collection([
                                {
                                    "type": constants.scheduleDaysTypes.WORKWEEK,
                                    "day": App.translate("schedules.schedule.weekConfigurations.MO-FR"),
                                    "scheduleCollection": new B.Collection(workWeekActionArray)
                                },
                                {
                                    "type": constants.scheduleDaysTypes.WEEKEND,
                                    "day": App.translate("schedules.schedule.weekConfigurations.SA-SU"),
                                    "scheduleCollection": new B.Collection(weekendActionArray)
                                }
                            ])
                        };
                    }
                }

                return {
                    isEmpty: true,
                    collection: new B.Collection([
                        {
                            "type": constants.scheduleDaysTypes.WORKWEEK,
                            "day": App.translate("schedules.schedule.weekConfigurations.MO-FR"),
                            "scheduleCollection": new B.Collection()
                        },
                        {
                            "type": constants.scheduleDaysTypes.WEEKEND,
                            "day": App.translate("schedules.schedule.weekConfigurations.SA-SU"),
                            "scheduleCollection": new B.Collection()
                        }
                    ])
//                    collection: new B.Collection([
//                        {
//                            "type": "fullWeek",
//                            "day": App.translate("schedules.schedule.weekConfigurations.MO-SU"),
//                            "scheduleCollection": new B.Collection()
//                        }
//                    ])
                };
            },
            getFirstInactive: function () {
                var inactives = this.getInactiveSchedules();

                if (inactives && inactives.length > 0) {
                    return inactives[0];
                } else {
                    return null;
                }
            },
            _updateActionsPostCopy: function () {
                var that = this;
                _.each(this.getActiveSchedules(), function (schedule) {
                    schedule.updateActionsPostCopy(that.deviceId);
                });
            }
        }).mixin([AylaBackedMixin]);
    });

    return App.Models.AylaScheduleCollection;
});