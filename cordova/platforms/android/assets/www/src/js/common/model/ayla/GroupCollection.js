"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "common/AylaConfig",
    "common/model/ayla/mixin.AylaBacked",
    "common/model/ayla/Group.model"
], function (App, P, constants, AylaConfig, AylaBackedMixin, GroupModel) {

    App.module("Models", function (Models, App, B, Mn, $, _) {
        Models.GroupCollection = B.Collection.extend({
            model: GroupModel,
            initialize: function (data, options) {
                _.bindAll(this, "_loadEachGroup", "filterByCurrentGateway", "_onGroupAdd");

                options = options || {};

                this.gatewayDSN = options.gatwayDSN;

                if (options.gatewayDSN) {
                    this.listenTo(App.salusConnector.getFullGroupCollection(), "add", this._onGroupAdd);
                }
            },
            load: function (isLowPriority) {
                var that = this;

                // TODO figure out the underlying problem with using backbone fetch; see SCS-993
                return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.groups.list, null, "GET", null, {isLowPriority: isLowPriority}).then(function (data) {
                    var mappedData = _.map(data, function (groupData) {
                        return groupData.group;
                    });
                    that.set(mappedData, {parse: true});
                }).then(function () {
                    return that._loadEachGroup(isLowPriority);
                }).then(this._getResolvedPromise);
            },
            refresh: function (isLowPriority) {
                var promise = this.load(isLowPriority);
                App.salusConnector.setDataLoadPromise("groups", promise);

                return promise;
            },
            isLoaded: function () {
                return !!this.length; //todo
            },
            /**
             * returns a collection containing devices not found in the argument collection
             * @param devCollec deviceCollection to compare with groups
             * @returns {Backbone.Collection}
             */
            getDevicesWithoutGroupFrom: function (devCollec) {
                var outArray = [],
                        groupArray = this.toArray();
                devCollec.each(function (device) {
                    var isFound = false;

                    for (var i = 0; i < groupArray.length && !isFound; i++) {
                        if (groupArray[i].hasDevice(device.get('key'))) {
                            isFound = true;
                        }
                    }

                    if (!isFound) {
                        outArray.push(device);
                    }
                });

                return new B.Collection(outArray);
            },
            /**
             * filter the group collection by looking into the first device's gateway dsn
             * @returns {Models.GroupCollection}
             */
            filterByCurrentGateway: function (shouldReturnArray) {
                var filteredArray = this.filter(function (group) {
                    var devices = group.get("devices"),
                            firstDevice = App.salusConnector.getDevice(devices[0]);

                    // ambiguous group
                    if (!firstDevice) {
                        return true;
                    }

                    // first one might be a gateway
                    if (firstDevice.modelType === constants.modelTypes.GATEWAY) {
                        return firstDevice.get("dsn") === App.getCurrentGatewayDSN();
                    } else {
                        return firstDevice.get("gateway_dsn") === App.getCurrentGatewayDSN();
                    }
                });

                if (shouldReturnArray) {
                    return filteredArray;
                }

                return new Models.GroupCollection(filteredArray, {gatewayDSN: App.getCurrentGatewayDSN()});
            },
            _buildFetchArray: function (isLowPriority) {
                return this.map(function (group) {
                    return group.refresh(isLowPriority);
                });
            },
            _loadEachGroup: function (isLowPriority) {
                return P.Promise.all(this._buildFetchArray(isLowPriority));
            },
            _onGroupAdd: function (device) {
                if (device.get("gateway_dsn") === this.gatewayDSN || device.isGateway() && device.get("dsn") === this.gatewayDSN) {
                    this.add(device);
                }
            },
            getGroupByDevice: function (device) {
                var key = parseInt(device.get("key"));
                var findGroup;
                this.some(function (group) {
                    var res = _.contains(group.get("devices"), key);
                    if (res) {
                        findGroup = group;
                    }
                });
                return findGroup;
            }

        }).mixin([AylaBackedMixin], {
            apiWrapperObjectName: "group"
        });
    });

    return App.Models.GroupCollection;
});