//
//  AylaReachability.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 11/25/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

package com.aylanetworks.aaml;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;

/**
 * 
 * AylaReachability is responsible for detecting reachability to the service and/or the device in the same lan.
 * 
 * */
public class AylaReachability {
	

	private static final String REACHABILITY_REGISTER = "reachability_register";
	
	private static Handler reachabilityHandle = null;
	private static AylaRestService rsReachability = null;

	private static int connectivity = AylaNetworks.AML_REACHABILITY_UNKNOWN;
	private static int device = AylaNetworks.AML_REACHABILITY_UNKNOWN;
	private static int isDeviceReachable = AylaNetworks.AML_REACHABILITY_UNKNOWN; 
	private static int isServiceReachable = AylaNetworks.AML_REACHABILITY_UNKNOWN;    


	/**
	 * Register message handlers from main activity
	 */
	public static synchronized void register(Handler handler) {
		if (handler != null) {
			reachabilityHandle = handler;
			rsReachability = new AylaRestService(reachabilityHandle, REACHABILITY_REGISTER, AylaRestService.REACHABILITY);
			
			lastDeviceReachablityTest = 0L; // reset to frequent timer
			lastServiceReachablityTest = 0L; // reset to frequent timerlastDeviceReachablityTest = 0L; // reset to frequent timer
		}
	}

	
	/**
	 * Internet reachability access methods
	 */
	public static int getConnectivity() {
		return connectivity;
	}

	public static boolean isCloudServiceAvailable() {
		return (connectivity == AylaNetworks.AML_REACHABILITY_REACHABLE);               
	}
	
	/**
	 * Device reachability access methods
	 */
	public static int getDeviceReachability() {
		return device;
	}
	
	public static synchronized void setDeviceReachability(int reachable) {
		if (reachable != device) {
			device = reachable;
			returnReachability(); // notify main activity reachability has changed
		}
	}
	
	public static boolean isDeviceLanModeAvailable() {
		return (device == AylaNetworks.AML_REACHABILITY_REACHABLE);
	}

	/**
	 * This real-time method returns a combined reachability status. If either LME device or Ayla Device Service were reachable, this method would return reachability
	 * status AML_REACHABILITY_REACHABLE.
	 */	
	public static int getReachability() {
		if ( (device >= 0) || (connectivity >= 0) ) {
			return AylaNetworks.AML_REACHABILITY_REACHABLE;
		} else  {
			return AylaNetworks.AML_REACHABILITY_UNREACHABLE;
		}
	}
	
	/**
	 * Alternative for static getReachability().
	 * 
	 * @return true if either LME device or Ayla Device Service is reachable. 
	 * */
	public static boolean isReachabilityAvailable() {
		return isCloudServiceAvailable() || isDeviceLanModeAvailable();
	}

	/**
	 * Called mostly by AylaConnectivityListener on network state changes.
	 * This method will takes up to 1 second to resolve and set current values connectivity & device reachability
	 * 
	 * Make it public for Unit Test Program, should not be called in app level.
	 * 
	 * @param waitForResult
	 */
	public static synchronized void determineReachability(final boolean waitForResults) {
		boolean isConnected = false;
		
		isConnected = isConnected(null);
		if (!isConnected) {
			device = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
			connectivity = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
			returnReachability();
		} else {
			// check device reachability
			determineDeviceReachability(waitForResults); 
			
			// check service reachablity
			determineServiceReachability(waitForResults);
		}
	}

	private static long lastServiceReachablityTest = 0L;// ?
	/**
	 * Determine Servicer reachability, set connectivity boolean
	 * 
	 * @param waitForResults
	 * */
	static synchronized void determineServiceReachability(final boolean waitForResults) {
		
		if (AylaSystemUtils.serviceReachableTimeout == -1) {
			connectivity = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
			AylaSystemUtils.sleep(500);
			return;
		}
		
		// optimize
		//long delta = System.currentTimeMillis() - lastServiceReachablityTest;
		//AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "Reachability", "delta", delta,  "determineServiceReachability");
		//if (delta <= AylaSystemUtils.serviceReachableTimeout*2) {
		//	AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "Reachability", "service", connectivity,  "determineServiceReachability_optimized");
		//	return;
		//}
		
		isServiceReachable = AylaNetworks.AML_REACHABILITY_UNKNOWN;
		Thread thread = new Thread(new Runnable() {
			public void run() {
				
				try {
					String serviceURL = AylaSystemUtils.deviceServiceBaseURL();
					int lastForwardSlash = serviceURL.indexOf('/', 8);
					String hostName = serviceURL.substring(8,lastForwardSlash);
					
					InetAddress address = InetAddress.getByName(hostName); // lookup the ip address or throw UnknownHostException
					
					if (AylaSystemUtils.slowConnection == AylaNetworks.NO) {
						// Test host server is availability
						String serviceIpAddress = address.getHostAddress();
						SocketAddress sockaddr = new InetSocketAddress(serviceIpAddress, 80);
						Socket sock = new Socket();
						int timeoutMs = AylaSystemUtils.serviceReachableTimeout; // If times-out, SocketTimeoutException is thrown.
						sock.connect(sockaddr, timeoutMs);
						sock.close();
					}
					isServiceReachable = AylaNetworks.AML_REACHABILITY_REACHABLE;
				}
				catch(Exception e) {
					isServiceReachable = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
				}
			}
		});
		thread.start();
		
		if (waitForResults) { // blocking manner
            try {
                thread.join(AylaNetworks.AML_SERVICE_REACHABLE_TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            connectivity = isServiceReachable;
			returnReachability();
		} else {
            // Check every 100ms for reachability, and return if it changes or after the
            // total timeout period has elapsed
			new CountDownTimer(AylaNetworks.AML_SERVICE_REACHABLE_TIMEOUT ,100) {
				@Override
				public void onTick(long millisUntilFinished) {
                    if ( isServiceReachable != AylaNetworks.AML_REACHABILITY_UNKNOWN ) {
                        connectivity = isServiceReachable;
                        returnReachability();
                        this.cancel();
                    }
				}

				@Override
				public void onFinish() {
					connectivity = isServiceReachable;     
					returnReachability();
				}
			}.start();
		}
		
		AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "Reachability", "service", connectivity,  "determineServiceReachability");
		lastServiceReachablityTest = System.currentTimeMillis();
	}
	

	private static boolean isConnected(Context context) { 
		
		if (context == null) {
			context = AylaNetworks.appContext;
		}
		
		if (context == null) { // AylaNetworks.appContext gets unloaded by OS. 
			return false;
		}
		
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    
	    return (netInfo != null && netInfo.isConnected());
	}
	
	public static boolean isWiFiEnabled(Context context) { 
		if (context == null) {
			context = AylaNetworks.appContext;
		}
		
		if (context == null) { // AylaNetworks.appContext gets unloaded by OS.
			return false;
		}
		WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
	    return (wifi != null && wifi.isWifiEnabled());
	}
	
	public static boolean isWiFiConnected(Context context) {
		
		if (context == null) {
			context = AylaNetworks.appContext;
		}
		
		if (context == null) { // AylaNetworks.appContext gets unloaded by OS.
			return false;
		}
		NetworkInfo.DetailedState rcDetailedState;
		rcDetailedState = AylaHostWifiApi.NetworkState(context);
		
		return (NetworkInfo.DetailedState.CONNECTED == rcDetailedState);
	}
	

	private static long lastDeviceReachablityTest = 0L;
	/**
	 * Determine Device reachability, set device boolean
	 * */
	static synchronized void determineDeviceReachability(final boolean waitForResults) {
		if (!isWiFiConnected(null) || AylaLanMode.device == null) {
			AylaSystemUtils.saveToLog("%s, %s, %s.", "D", "AylaReachability.determinDeviceReachability", "No wifi connection or lanMode.device not initiated.");
			device = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
			return;
		}

		if (!AylaLanMode.device.lanEnabled) { // device is not lan mode enabled
			AylaSystemUtils.saveToLog("%s, %s, %s.", "D", "AylaReachability.determinDeviceReachability", "lanMode.device not lan enabled.");
			device = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
			return;
		}

		// optimize
		//long delta = System.currentTimeMillis() - lastDeviceReachablityTest;
		//AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "Reachability", "delta", delta,  "determineDeviceReachability_optimized");
		//if (delta <= AylaNetworks.AML_DEVICE_REACHABLE_TIMEOUT) {
			//AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "Reachability", "device:", device,  "determineDeviceReachability_optimized");
		//	return;
		//}
		
		isDeviceReachable = AylaNetworks.AML_REACHABILITY_UNKNOWN;
		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					SocketAddress sockaddr = null;
					if (AylaLanMode.device != null) {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s.", "D", "AylaReachability.determineDeviceReachability", "device.lanIp", AylaLanMode.device.lanIp+"");
						sockaddr = new InetSocketAddress(AylaLanMode.device.lanIp, 80);
						Socket sock = new Socket();
						int timeoutMs = AylaNetworks.AML_DEVICE_REACHABLE_TIMEOUT; // If times-out, SocketTimeoutException is thrown.
						sock.connect(sockaddr, timeoutMs);
						sock.close();

						isDeviceReachable = AylaNetworks.AML_REACHABILITY_REACHABLE;
						
					} else {
						isDeviceReachable = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
					}
				}
				catch(Exception e) {
					isDeviceReachable = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
				}
			}
		});
		thread.start();
		
		if (waitForResults) {
			try {
                thread.join(AylaNetworks.AML_DEVICE_REACHABLE_TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
			
			device = isDeviceReachable;
			returnReachability();
		} else {
			new CountDownTimer (AylaNetworks.AML_DEVICE_REACHABLE_TIMEOUT, 100) {

				@Override
				public void onTick(long millisUntilFinished) {
					if (isDeviceReachable != AylaNetworks.AML_REACHABILITY_UNKNOWN) {
						device = isDeviceReachable;
						returnReachability();
						this.cancel();
					}
				}

				@Override
				public void onFinish() {
					device = AylaNetworks.AML_REACHABILITY_UNREACHABLE;
					returnReachability();
				}
				
			}.start();
		}
		
		AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "I", "Reachability", "device", device,  "determineDeviceReachability");
		lastDeviceReachablityTest = System.currentTimeMillis();
	}

	// return reachability status to the registered message handler
	private static String logMsg = "";
	private static int savedDevice = -100;
	private static int savedConnectivity = -100;
	protected static void returnReachability() {
		if (reachabilityHandle == null) {
			return;
		}

		// no need to notifiy reachability handler if no change in reachability
		if ( (savedDevice == device) && (savedConnectivity == connectivity)) {
			return;
		}
		
		savedDevice = device;
		savedConnectivity = connectivity;
		
		
		String jsonText = "";
		jsonText = "{";
		jsonText = jsonText + "\"device\":" + device + ",";
		jsonText = jsonText + "\"connectivity\":" + connectivity ;
		jsonText= jsonText + "}";
		String msg = String.format("%s %s %s:%s %s", "I", "AylaReachability", "jsonText", jsonText, "returnReachability");
		if (TextUtils.equals(msg, logMsg)) {
			AylaSystemUtils.consoleMsg(logMsg, AylaSystemUtils.loggingLevel);
		} else { AylaSystemUtils.saveToLog("%s", msg); logMsg = msg; }
		
		returnToMainActivity(rsReachability, jsonText, 200, 0);
	}

	// TODO: put in a common utils class.
	private static void returnToMainActivity(AylaRestService rs, String thisJsonResults, int thisResponseCode, int thisSubTaskId) {
		rs.jsonResults = thisJsonResults;
		rs.responseCode = thisResponseCode;
		rs.subTaskFailed = thisSubTaskId;
		
		rs.execute(); 
	}
}// end of AylaReachability class     




