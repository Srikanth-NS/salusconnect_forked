/*
 * ITestResultDisplayable.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */


package com.aylanetworks.AMLUnitTest.interfaces;


public interface ITestResultDisplayable {

	public void init();
	
	public void setTag(final String tag);
	
	public void displayProgressInfo(final String msg);
	
	public void displayFailMessage(final String msg);
	
	public void displayPassMessage(final String msg);
}// end of ITestResultDisplayable interface      




