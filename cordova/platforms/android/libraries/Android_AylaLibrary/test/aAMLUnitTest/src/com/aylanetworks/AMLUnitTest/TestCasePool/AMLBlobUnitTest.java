/*
 * AMLBlobUnitTest.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.TestCasePool;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaBlob;
import com.aylanetworks.aaml.AylaDevice;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaProperty;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.aylanetworks.aaml.AylaUser;

// TODO: Needs further elaboration
@SuppressLint({ "HandlerLeak", "DefaultLocale" })
public class AMLBlobUnitTest implements IUnitTestable {

	private final String tag = AMLBlobUnitTest.class.getSimpleName();                    
	
	//TODO: Add testSequence implementation
	private Runnable mEndHandler = null;
	
	private ITestResultDisplayable mResultScreen = null;
	private AylaUser mUser = null;
	
	
	private final String kBlobUnitTestUpPropertyName = "stream_up";
	private final String kBlobUnitTestDownPropertyName = "stream_down";
	
	private AylaProperty mFileProperty = null;
	private AylaDevice mDevice = null;
	
	
	public AMLBlobUnitTest() {
		mResultScreen = new DefaultScreen();
	}
	
	public AMLBlobUnitTest(ITestResultDisplayable screen) {
		mResultScreen = screen;
	}
	
	@Override
	public void setResultScreen(ITestResultDisplayable screen) {
		mResultScreen = screen;
	}
	
	@Override
	public void onResume() {
		if (mUser == null) {
			setupUser();
		}
		
		// setupUser() would setup device and property sequentially.
	}

	@Override
	public void start() {
		amlTestCreateBlobs();
	}

	@Override
	public void onPause() {
	}
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onEnd() {
		if (mEndHandler != null) {
			mEndHandler.run();
		}
	}

	@Override
	public void setEndHandler(Runnable r) {
		mEndHandler = r;
	}

	// TODO: switch to testSequencer implementation
	@Override
	public boolean isRunning() {
//		if (testSequencer != null) {
//			return testSequencer.isTestSuiteRunning();
//		}
		return false;
	}
	
	
	public void setUser(AylaUser user) {
		mUser = user;
	}
	
	public void setDevice(AylaDevice device) {
		mDevice = device;
	}
	
	public void setProperty(AylaProperty property) {
		mFileProperty = property;
	}
	
	private void amlTestCreateBlobs() {
		if (mUser == null) {
			amlTestStatusMsg("Fail", "amlTestCreateBlobs", "mUser is null.");
			return;
		}
		
		if (mDevice == null) {
			amlTestStatusMsg("Fail", "amlTestCreateBlobs", "Device is not fetched properly.");
			return;
		}
		
		if (mFileProperty == null) {
			amlTestStatusMsg("Fail", "amlTestCreateBlobs", "Saved AylaProperty is null.");
			return;
		}
		
		if (mFileProperty.blob == null) {
			mFileProperty.blob = new AylaBlob();
		}
		
		mFileProperty.blob.createBlob(createBlobHandler, mFileProperty, null);
	}// end of amlTestCreateBlobs
	
	private final Handler createBlobHandler = new Handler() {
		public void handleMessage(Message msg) {
//			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				mFileProperty.blob = AylaSystemUtils.gson.fromJson(jsonResults, AylaBlob.class);   
//				
				String passMsg = String.format("%s, %s, %s", "P", "amlBlobUnitTest", "amlTestCreateBlobs() passed.");      
				amlTestStatusMsg("Pass", "AyalBlobUnitTest createBlobHandler", passMsg);
				
				amlTestGetBlobs();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
						"F", 
						"amlTest", 
						"error", 
						msg.arg1, 
						msg.obj, 
						"createBlobHandler");
				amlTestStatusMsg("Fail", "AyalBlobUnitTest createBlobHandler", errMsg);
			}
		}// end of handleMessage
	};
	
//	private void amlTestCreateBlobPostToFile() {
//		if (mUser == null) {
//			amlTestStatusMsg("Fail", "amlTestCreateBlobPostToFile", "mUser is null.");
//			return;
//		}
//		
//		if (mDevice == null) {
//			amlTestStatusMsg("Fail", "amlTestCreateBlobPostToFile", "Device is not fetched properly.");
//			return;
//		}
//		
//		if (mFileProperty == null) {
//			amlTestStatusMsg("Fail", "amlTestCreateBlobPostToFile", "Saved AylaProperty is null.");
//			return;
//		}
//		
//		if (mFileProperty.blob == null) {
//			amlTestStatusMsg("Fail", "amlTestCreateBlobPostToFile", "Blob is not retrived successfully.");
//			return;
//		}
//		
//		/* param null means "Blob_Stream" as file name
//		 * Environment.getExternalStorageDirectory() as path.
//		 * 
//		 * Map<String, String> param = new HashMap<String, String>();
//		 * param.put(AylaBlob.kAylaBlobFileLocalPath, "test_file_path");
//		 * param.put(AylaBlob.kAylaBlobFileSuffixName, "test_file_name");
//		 * 
//		 * mFileProperty.blob.createBlobPostToFile(createBlobPostToFileHandler, mFileProperty, param);
//		 * */   
//		File file = new File(Environment.getExternalStorageDirectory() + File.separator + "Blob_Stream");
//		if (!file.exists()) {
//			try {
//				file.createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		
//		mFileProperty.blob.createBlobPostToFile(createBlobPostToFileHandler, mFileProperty, null);
//	}// end of amlTestCreateBlobsPostToFile
//	
//	
//	private final Handler createBlobPostToFileHandler = new Handler() {
//		public void handleMessage(Message msg) {
//			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				
//				String passMsg = String.format("%s, %s, %s", "P", "amlBlobUnitTest", "AylaBlobUnitTest amlTestCreateBlobPostToFile");
//				amlTestStatusMsg("Pass", "AylaBlobUnitTest createBlobPostToFileHandler", passMsg);
//				
//				amlTestMarkFinished();
//			} else if (msg.what == AylaNetworks.AML_ERROR_TOKEN_EXPIRE) {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
//						"F", 
//						"amlBlobUnitTest", 
//						"error", 
//						msg.arg1, 
//						msg.obj, 
//						"AylaBlobUnitTest createBlobPostToFileHandler");
//				amlTestStatusMsg("Fail", "AylaBlobUnitTest createBlobPostToFileHandler", errMsg);
////				mFileProperty.blob.createBlob(createBlobHandler, mFileProperty, null);
//				amlTestCreateBlobs();
//			} else {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
//						"F", 
//						"amlBlobUnitTest", 
//						"error", 
//						msg.arg1, 
//						msg.obj, 
//						"AylaBlobUnitTest createBlobPostToFileHandler");
//				amlTestStatusMsg("Fail", "AylaBlobUnitTest createBlobPostToFileHandler", errMsg);
//			}
//		}// end of handleMessage
//	};
	
	
//	private void amlTestMarkFinished() {
//		if (mUser == null) {
//			amlTestStatusMsg("Fail", "AylaBlobUnitTest amlTestMarkFinished", "mUser is null.");
//			return;
//		}
//		
//		if (mDevice == null) {
//			amlTestStatusMsg("Fail", "AylaBlobUnitTest amlTestMarkFinished", "Device is not fetched properly.");
//			return;
//		}
//		
//		if (mFileProperty == null) {
//			amlTestStatusMsg("Fail", "AylaBlobUnitTest amlTestMarkFinished", "Saved AylaProperty is null.");
//			return;
//		}
//		
//		if (mFileProperty.blob == null) {
//			amlTestStatusMsg("Fail", "AylaBlobUnitTest amlTestMarkFinished", "Blob is not retrived successfully.");
//			return;
//		}
//		
//		mFileProperty.blob.markFinished(markFinishedHandler, null, false);
//	}// end of amlTestMarkFinished              
//	
//	private final Handler markFinishedHandler = new Handler() {
//		public void handleMessage(Message msg) {
//			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				
//				String passMsg = String.format("%s, %s, %s", "P", "amlBlobUnitTest", "markFinishedHandler");
//				amlTestStatusMsg("Pass", "markFinishedHandler", passMsg);
//				
//				amlTestGetBlobs();
//			} else {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
//						"F", 
//						"amlBlobUnitTest", 
//						"error", 
//						msg.arg1, 
//						msg.obj, 
//						"markFinishedHandler");
//				amlTestStatusMsg("Fail", "createBlobPostToFileHandler", errMsg);
//			}
//		}// end of handleMessage
//	};
	
	
	
	private void amlTestGetBlobs() {
		if (mUser == null) {
			amlTestStatusMsg("Fail", "amlTestGetBlobs", "mUser is null.");
			return;
		}
		
		if (mDevice == null) {
			amlTestStatusMsg("Fail", "amlTestGetBlobs", "Device is not fetched properly.");
			return;
		}
		
		if (mFileProperty == null) {
			amlTestStatusMsg("Fail", "amlTestGetBlobs", "Saved AylaProperty is null.");
			return;
		}
		
		if (mFileProperty.blob == null) {
			mFileProperty.blob = new AylaBlob();
		}
		
		mFileProperty.blob.getBlobs(getBlobsHandler, mFileProperty, null, false);
	}
	
	private final Handler getBlobsHandler = new Handler() {
		public void handleMessage(Message msg) {
//			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				
//				AylaBlob[] blobs = AylaSystemUtils.gson.fromJson(jsonResults, AylaBlob[].class);
//				if (blobs == null) {
//					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaBlob", "blobs", "null", "getBlobsHandler");
//					return;
//				}
//				
//				int count = blobs.length;
//				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaBlobUnitTest", "count", count, "getBlobsHandler");
//				
//				if (count < 1) {
//					amlTestStatusMsg("Fail", "getBlobsHandler", "Count is invalid.");
//					return;
//				}
//				
				String passMsg = String.format("%s, %s, %s", "P", "amlBlobUnitTest", "amlBlobUnitTest getBlobsHandler");
				amlTestStatusMsg("Pass", "amlBlobUnitTest getBlobsHandler", passMsg);
//				
//				// Good results at least one datapoint to process
//				mFileProperty.blob = blobs[0];
//				String url = blobs[0].value(); // optional.
//				String file = blobs[0].file();
//				
//				Map<String, String> callParams = new HashMap<String, String>();
//				callParams.put(AylaDatapoint.kAylaDataPointCount, "1");
//				callParams.put(AylaDatapoint.kAylaDataPointValue, url);
//				callParams.put(AylaBlob.kAylaBlobFile, file);   
				amlTestMarkFetched(null);
		    	
			} else {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaBlobUnitTest", "resultCode", msg.what+"", "getBlobURLs_handler");
				return;
			}
		}// end of handleMessage     
	};
	
//	private void amlTestGetBlobsSaveToFile(Map<String, String> params) {
//		if (mUser == null) {
//			amlTestStatusMsg("Fail", "amlTestGetBlobsSaveToFile", "mUser is null.");
//			return;
//		}
//		
//		if (mDevice == null) {
//			amlTestStatusMsg("Fail", "amlTestGetBlobsSaveToFile", "Device is not fetched properly.");
//			return;
//		}
//		
//		if (mFileProperty == null) {
//			amlTestStatusMsg("Fail", "amlTestGetBlobsSaveToFile", "Saved AylaProperty is null.");
//			return;
//		}
//		
//		if (mFileProperty.blob == null) {
//			amlTestStatusMsg("Fail", "amlTestGetBlobsSaveToFile", "Blob is not retrived successfully.");
//			return;
//		}
//		mFileProperty.blob.getBlobSaveToFile(getBlobSaveToFileHandler, params, false);
//	}
//	
//	private final Handler getBlobSaveToFileHandler = new Handler() {
//		public void handleMessage(Message msg) {
//			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				
//				String passMsg = String.format("%s, %s, %s", "P", "amlBlobUnitTest", "getBlobSaveToFileHandler");
//				amlTestStatusMsg("Pass", "getBlobSaveToFileHandler", passMsg);
//				
//				amlTestMarkFetched(null);           
//			} else {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
//						"F", 
//						"amlBlobUnitTest", 
//						"error", 
//						msg.arg1, 
//						msg.obj, 
//						"getBlobSaveToFileHandler");
//				amlTestStatusMsg("Fail", "getBlobSaveToFileHandler", errMsg);
//			}
//		}// end of handleMessage
//	};
	
	private void amlTestMarkFetched(Map<String, String> params) {
		if (mUser == null) {
			amlTestStatusMsg("Fail", "amlTestMarkFetched", "mUser is null.");
			return;
		}
		
		if (mDevice == null) {
			amlTestStatusMsg("Fail", "amlTestMarkFetched", "Device is not fetched properly.");
			return;
		}
		
		if (mFileProperty == null) {
			amlTestStatusMsg("Fail", "amlTestMarkFetched", "Saved AylaProperty is null.");
			return;
		}
		
		if (mFileProperty.blob == null) {
			amlTestStatusMsg("Fail", "amlTestMarkFetched", "Blob is not retrived successfully.");
			return;
		}
		
		mFileProperty.blob.markFetched(markedFetchedHandler, params, false);
	}

	private final Handler markedFetchedHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				
				String passMsg = String.format("%s, %s, %s", "P", "amlBlobUnitTest", "AylaBlobUnitTest markFetchedHandler");
				amlTestStatusMsg("Pass", "AylaBlobUnitTest markFetchedHandler", passMsg);
				
				onPause();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
						"F", 
						"amlBlobUnitTest", 
						"error", 
						msg.arg1, 
						msg.obj, 
						"AylaBlobUnitTest markFetchedHandler");
				amlTestStatusMsg("Fail", "AylaBlobUnitTest markFetchedHandler", errMsg);
			}
		}// end of handleMessage      
	};
	
	// =======================  Precondition Related  ======================
	private void setupUser() {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			Log.d(tag, 
					"user name:" + AMLUnitTestConfig.userName + 
					" \npassword:" + AMLUnitTestConfig.password + 
					" \nappId:" + AMLUnitTestConfig.appId + 
					" \nappSecrect:" + AMLUnitTestConfig.appSecret);
			AylaUser.login(loginHandler, 
					AMLUnitTestConfig.userName, 
					AMLUnitTestConfig.password, 
					AMLUnitTestConfig.appId, 
					AMLUnitTestConfig.appSecret);
		} else {
			// TODO: Add handling for Sync Mode
		}
	}
	
	private final Handler loginHandler = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaUser user = AylaSystemUtils.gson.fromJson(jsonResults, AylaUser.class);
				user = AylaUser.setCurrent(user);
				mUser = user;
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", 
						"amlTest", "secondsToExpiry", 
						user.accessTokenSecondsToExpiry(), "loginHandler");
				amlTestStatusMsg("Pass", "AylaBlobUnitTest loginHandler", passMsg);
				Log.d(tag, passMsg);
				
				setupDevice();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login2");
				amlTestStatusMsg("Fail", "" + "", errMsg);
				Log.e(tag, errMsg);
			}
		}// end of handleMessage  
	};
	
	private void setupDevice() {
		AylaDevice.getDevices(setupDeviceHandler);
	}
	
	private final Handler setupDeviceHandler = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDevice[] devices = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice[].class);
				for (AylaDevice device:devices) {
					if (TextUtils.equals(device.dsn, AMLUnitTestConfig.gblAmlTestEVBDsn)) {
						mDevice = device;
					}
				}
				
				if (mDevice!=null) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlBlobUnitTest", "count", devices.length, "setupDeviceHandler");
					amlTestStatusMsg("Pass", "AylaBlobUnitTest setupDeviceHandler", passMsg);
					
					setupBlobProperty();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "setupDeviceHandler", "gblAmlTestDevice", "null", "No device matches the given dsn.");
					amlTestStatusMsg("Fail", "AylaBlobUnitTest setupDeviceHandler", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlBlobUnitTest", "error", msg.arg1, msg.obj, "setupDeviceHandler");
				amlTestStatusMsg("Fail", "AylaBlobUnitTest setupDeviceHandler", errMsg);
			}
		}// end of handleMessage
	};
	
	private void setupBlobProperty() {
		Map<String, String> param = new HashMap<String, String>();
		param.put("names", kBlobUnitTestDownPropertyName +" "+ kBlobUnitTestUpPropertyName);
		
		mDevice.getProperties(getPropertyHandler, param);
	}
	
	private final Handler getPropertyHandler = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				mDevice.properties = AylaSystemUtils.gson.fromJson(jsonResults, AylaProperty[].class);
				
				for (AylaProperty property : mDevice.properties) {
					String name = property.name();
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s.", "P", "amlBlobUnitTest", "name", name, "value", property.value, "getPropertyHandler");
					if (TextUtils.equals(name, kBlobUnitTestDownPropertyName)) {
						mFileProperty = property;
					}
				}
				
				if (mFileProperty != null) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlBlobUnitTest", "count", mDevice.properties.length, "getPropertyHandler");
					amlTestStatusMsg("Pass", "AylaBlobUnitTest getPropertyHandler", passMsg);
					
					start();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlBlobUnitTest", "getPropertyHandler", "null", "No property named "+ kBlobUnitTestDownPropertyName);
					amlTestStatusMsg("Fail", "AylaBlobUnitTest getPropertyHandler", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlBlobUnitTest", "error", msg.arg1, msg.obj, "getPropertyHandler");
				amlTestStatusMsg("Fail", "AylaBlobUnitTest getPropertyHandler", errMsg);
			}
		}// end of handleMessage
	};
	
	// TODO: extract as a common utility function
	private void amlTestStatusMsg(String passFail, String methodName,
			String methodMsg) {
		String testMsg = "";
		if (passFail != null && passFail.contains("Pass")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s", methodName, "passed");

			if (!passFail.contains("Sync")) {
				mResultScreen.displayFailMessage(testMsg + "\n\n");
			} else {
				Log.i(tag, testMsg);
			}
		} else if (passFail != null && passFail.contains("Fail")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s.", methodName, "failed");
			if (!passFail.contains("Sync")) {
				mResultScreen.displayPassMessage(testMsg + "\n\n");
			} else {
				Log.e(tag, testMsg);
			}
		} else {
			// TODO: passFail null or no match.
		}
		AylaSystemUtils.saveToLog("%s", testMsg);

	}// end of amlTestStatusMsg 

}// end of AMLBlobUnitTest class                     





