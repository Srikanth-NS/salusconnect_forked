"use strict";

define([
	"app",
	"momentWrapper",
	"consumer/views/mixins/mixin.salusView",
	"consumer/consumerTemplates"
], function (App, moment, SalusViewMixin, templates) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {
		Views.WeatherView = Mn.ItemView.extend({
			template: templates["dashboard/header/weather"],
			className: "weather pull-right",
			ui: {
//				amPm: ".bb-gateway-time-amPm",
				weatherContainer: ".bb-icon-degrees-container",
				decimal: ".bb-degrees-label-decimal"
			},
			bindings: {
				".bb-gateway-time-local": {
					observe: "deviceTimeLocal",
					onGet: "formatDeviceTime"
				}
			},
			initialize: function () {
				var that = this;
				this.gateway = this.options.gateway;
                this.count = 60;

				if (this.model) {
					this.updateTime();
                    setTimeout(function(){
                        that.updateTime();
                        setInterval(function () {
                            that.updateTime();
                        }, 60000);
                    },this.count * 1000);
					
				}
			},
			onRender: function () {
				var bg = this.model.getWeatherIcon();
				this.$(".weather-icon").css("background-image", "url(" + bg + ")");

				if (isNaN(this.model.getTempInCelsius())) {
					this.ui.weatherContainer.hide();
					this.$(".bb-weather-separator").addClass("hidden");
				}

				// hide decimal when F
				if (this.model.get("country") === "US") {
					this.ui.decimal.addClass("hidden");
				}
			},
			/**
			 * updateTime
			 * called every minute to make a call to moment
			 * gets rendered with binding
			 */
			updateTime: function () {
				// moment allows you to pass in undefined but not null
				var time = moment(undefined, undefined, this.gateway);
                var seconds = time.format("ss");
                this.count = 60 - parseInt(seconds);
				this.model.set("deviceTimeLocal", time);
			},
            
            
			/**
			 * formatDeviceTime
			 * makes time binding pretty
			 * @param value
			 * @returns {*}
			 */
			formatDeviceTime: function (value) {
				//this.ui.amPm.text(value.format("a"));
				return value.format("H:mm");
			},
			templateHelpers: function () {
				var temperature = (this.model.get("country") === "US" ?
						this.model.getTempInFahrenheit() : this.model.getTempInCelsius());

				if (isNaN(temperature)) {
					return {
						degrees: null,
						decimal: null,
						metric: null
					};
				}

				temperature = temperature.toFixed(1);

				return {
					degrees: temperature.slice(0, -2),
					decimal: temperature.slice(-2),
					metric: this.model.get("country") === "US" ? "F" : "C"
				};
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Dashboard.Views.WeatherView;
});