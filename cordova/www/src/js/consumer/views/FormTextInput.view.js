"use strict";

define([
	"app",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusTextBox",
	"consumer/views/mixins/mixin.validation"
], function (App, commonTemplates) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.FormTextInput = Mn.ItemView.extend({
			template: commonTemplates["salusForms/formTextInput"]
		}).mixin([
			App.Consumer.Views.Mixins.SalusTextBox,
			App.Consumer.Views.Mixins.Validation
		]);
	});

	return App.Consumer.Views.FormTextInput;
});