"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Equipment.Models", function (Models, App, B) {
		Models.StackedBarGraphViewModel = B.Model.extend({
			defaults: {
				currentEndDate: null,
				currentStartDate: null,
				currentDeviceNames: ["Device1", "Device2", "Device3", "Device4"],
				currentDatapoints: null,
				currentTimespan: "1d"
			}
		});
	});

	return App.Consumer.Equipment.Models.StackedBarGraphViewModel;
});