"use strict";

define([
	"app",
	"consumer/login/models/PageContainer.model",
	"consumer/login/views/PageContainer.view",
	"consumer/login/views/Pinpad.view"
], function (App, LoginPageContainerModel, LoginPageContainerView, PinpadView) {

	App.module("Consumer.Login", function (Login, App, B) {
		Login.Controller = {
			login: function login(type, token) {
				var model = {
					contentWellType: type || "welcome"
				};

				if (token) {
					model.params = {};
					model.params.secureToken = token;
				}

				var loginPageContainerView = new LoginPageContainerView({
					model: new LoginPageContainerModel(model)
				});

				App.mainRegion.show(loginPageContainerView);
			},
			pinpad: function () {
				var model = new B.Model({
					mode:"create"
				});

				App.mainRegion.show(new PinpadView({
					model: model
				}));
			}
		};
	});

	return App.Consumer.Login.Controller;
});