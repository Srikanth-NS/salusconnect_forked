/*global require, window*/
(function () {
	"use strict";

	// THIS FILE IS A WRAPPER FOR REQUIRE IT SHOULD ONLY START THE APP

	require.config(window.requireConfig);

	require([
		"app",
		"underscore",
		"backbone",
		"consumer/consumer",
        "consumer/legal/legal",
		"consumer/login/login",
		"consumer/dashboard/dashboard",
		"consumer/newUser/newUser",
		"consumer/setupWizard/setupWizard",
		"consumer/equipment/equipment",
		"consumer/settings/settings",
		"consumer/oneTouch/oneTouch",
		"consumer/myStatus/myStatus",
		"consumer/schedules/schedules",
		"common/util/StringsFunctions"
	], function (app, _, Backbone) {
		// add state save to our backbone model
		Backbone.Model.prototype.saveState = function () {
			var json = _.clone(this.attributes);

			// in the case where the model has a nested model, serialize the nested model as well
			_.each(json, function (attr, key) {
				if (attr instanceof Backbone.Model) {
					json[key] = attr.toJSON();
				}
			});

			this._savedJsonState = json;
		};

		// revert the model to its saved state
		Backbone.Model.prototype.restoreState = function () {
			var that = this,
				removed = [];

			//Clone this so the original doesnt get messed up.
			var prevouseState = _.clone(this._savedJsonState);

			if (prevouseState) {
				_.each(prevouseState, function (attr, key) {
					if (_.has(that.attributes, attr, key)) {
						removed.push(key);
					}
				});

				// remove added attributes
				_.each(removed, function (removedAttr) {
					that.unset(removedAttr);
				});

				this.set(this.parse(prevouseState));
			}
		};

		app.backgroundImageUrl = window.backgroundImageUrl || null;

		app.start();
		
		//TODO: Data Refresh focus
		$(function () {
			$(window).on("focus", function() {
				if (!app.infocus) {
					app.infocus = true;
					
					if (app.salusConnector._aylaConnector.dataRefreshManager.dataRefreshPromise()) {
						app.salusConnector._aylaConnector.dataRefreshManager.dataRefreshPromise().cancel();
						app.salusConnector._aylaConnector.dataRefreshManager.clearTimeout();
						app.salusConnector._aylaConnector.dataRefreshManager.initiateDevicePoll();
					}
				}
			});
			
			$(window).on("blur", function() {
				if (app.infocus) {
					app.infocus = false;
				}
			});
		});
		
	});
}());