"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.IT600WC = B.Model.extend({
			tileType: "wud",
			modelType: constants.modelTypes.IT600WC,
			defaults: {
				// get properties
				FirmwareVersion: null, //string
				MACAddress: null, //string length 16
				SubDeviceName_c: null, //string - not used
				DeviceType: null, //integer 0:iT600 Wiring Center
				ManufactureName: null, //string length 32
				ModelIdentifier: null, //string length 32
				DeviceIndex:null, //integer 1-9 for WC Number, used to display below Error message.
					// Eg. DeviceNumber=1 and Error10 true, displays
					// 'Error 10: Wiring Center #1 lost link with Hot Water Timer'
				Error10: null, //boolean Wiring Center # lost link with Hot Water Timer
				Error11: null, //boolean Wiring Center # lost link with WC lost link with Zone 1 Thermostat
				Error12: null, //boolean Wiring Center # lost link with WC lost link with Zone 2 Thermostat
				Error13: null, //boolean Wiring Center # lost link with WC lost link with Zone 3 Thermostat
				Error14: null, //boolean Wiring Center # lost link with WC lost link with Zone 4 Thermostat
				Error15: null, //boolean Wiring Center # lost link with WC lost link with Zone 5 Thermostat
				Error16: null, //boolean Wiring Center # lost link with WC lost link with Zone 6 Thermostat
				Error17: null, //boolean Wiring Center # lost link with WC lost link with Zone 7 Thermostat
				Error18: null, //boolean Wiring Center # lost link with WC lost link with Zone 8 Thermostat
				Error19: null, //boolean Wiring Center # lost link with Coordinator
				Error20: null, //boolean Wiring Center # lost link with Receive 1
				Error26: null, //boolean Wiring Center # lost link with WC lost link with Zone 9 Thermostat
				Error27: null, //boolean Wiring Center # lost link with WC lost link with Zone 10 Thermostat
				Error28: null, //boolean Wiring Center # lost link with WC lost link with Zone 11 Thermostat
				Error29: null, //boolean Wiring Center # lost link with WC lost link with Zone 12 Thermostat

				// set properties
				SetIndicator: null //integer Turn on the device identification indicator with specified time in second
			},
			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 0,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_wud.svg"),
					device_category_type: constants.categoryTypes.WIRELESSUNCONTROLLABLEDEVICES,
					device_category_name_key: "equipment.myEquipment.categories.types.wirelessUncontrollableDevices"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_wud_grey.svg")
				});
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.IT600WC;
});
