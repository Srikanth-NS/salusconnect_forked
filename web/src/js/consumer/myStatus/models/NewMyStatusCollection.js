"use strict";

define([
	"app",
	"consumer/myStatus/models/MyStatusModel",
	"common/constants"
], function (App, MyStatusModel, constants) {
	App.module("Consumer.MyStatus.Models", function (Models, App, B) {
		var MyStatusModel = App.Consumer.MyStatus.Models.MyStatusModel;
		var topColors = constants.myStatusTileColors.topColors,
			botColors = constants.myStatusTileColors.botColors;

		Models.newMyStatusCollection =
			new B.Collection([
				new MyStatusModel({
					icon: "house",
					topColor: topColors.brown,
					botColor: botColors.brown
				}),
				new MyStatusModel({
					icon: "snowflake",
					topColor: topColors.turq,
					botColor: botColors.turq
				}),
				new MyStatusModel({
					icon: "family",
					topColor: topColors.brown,
					botColor: botColors.brown
				}),
				new MyStatusModel({
					icon: "umbrella",
					topColor: topColors.turq,
					botColor: botColors.turq
				}),
				new MyStatusModel({
					icon: "sun_cloud",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "airplane",
					topColor: topColors.turq,
					botColor: botColors.turq
				}),
				new MyStatusModel({
					icon: "gift",
					topColor: topColors.green,
					botColor: botColors.green
				}),
				new MyStatusModel({
					icon: "flame",
					topColor: topColors.orange,
					botColor: botColors.orange
				}),
				new MyStatusModel({
					icon: "man_hat",
					topColor: topColors.brown,
					botColor: botColors.brown
				}),
				new MyStatusModel({
					icon: "a",
					topColor: topColors.green,
					botColor: botColors.green
				}),
				new MyStatusModel({
					icon: "away",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "sun",
					topColor: topColors.orange,
					botColor: botColors.orange
				}),
				new MyStatusModel({
					icon: "martini",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "moon",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "cloud",
					topColor: topColors.brown,
					botColor: botColors.brown
				}),
				new MyStatusModel({
					icon: "house_sun",
					topColor: topColors.orange,
					botColor: botColors.orange
				}),
				new MyStatusModel({
					icon: "house_snowflake",
					topColor: topColors.turq,
					botColor: botColors.turq
				}),
				new MyStatusModel({
					icon: "candles",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "exclamation",
					topColor: topColors.green,
					botColor: botColors.green
				}),
				new MyStatusModel({
					icon: "eye",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "suitcase",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "leaf",
					topColor: topColors.brown,
					botColor: botColors.brown
				}),
				new MyStatusModel({
					icon: "balloons",
					topColor: topColors.green,
					botColor: botColors.green
				}),
				new MyStatusModel({
					icon: "cone",
					topColor: topColors.orange,
					botColor: botColors.orange
				}),
				new MyStatusModel({
					icon: "keys",
					topColor: topColors.blue,
					botColor: botColors.blue
				})
			]);
	});

	return App.Consumer.MyStatus.Models.newMyStatusCollection;
});