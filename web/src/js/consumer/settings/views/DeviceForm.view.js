"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixins",
	"common/model/ayla/DeviceCollection",
	"consumer/settings/views/DeleteDeviceShareModal.view",
	"consumer/settings/views/ShareableDeviceList.view",
	"consumer/models/SalusDropdownViewModel",
	"consumer/views/modal/ModalDefaultHeader.view",
	"consumer/views/SalusModal.view",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusDropdown.view",
	"consumer/views/SalusAlertModal.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/models/SalusAlertModalModel"
], function (App, config, templates, ConsumerMixins, AylaDeviceCollection) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.DeviceForm = Mn.LayoutView.extend({
			template: templates["settings/manageDevices/deviceForm"],
			className: "device-form edit-enabled",
			regions: {
				"firstName": ".bb-first-name",
				"lastName": ".bb-last-name",
				"country": ".bb-country",
				"email": ".bb-email",
				"saveButton": ".bb-button-save",
				"cancelButton": ".bb-button-cancel",
				"shareableDeviceList": ".bb-shareable-device-list"
			},

			events: {
				"click .bb-delete-user": "handleDeleteUserClick"
			},

			initialize: function () {
				var countriesCollection = new App.Consumer.Models.DropdownItemViewCollection();
				_.bindAll(this, "handleSaveClick", "handleCancelClick", "handleModalDeleteClick");

				this.mode = this.options.mode;
				this.sharedUserProfile = this.model.get("user_profile");

				App.Consumer.fillDropdownCollection(config.countries, config.countryPrefix, countriesCollection);

				this.firstNameField = this.registerRegion("firstName", new App.Consumer.Views.FormTextInput({
					labelText: "settings.manageDevices.devices.formLabels.firstName",
					value: this.sharedUserProfile ? this.sharedUserProfile.firstname : "",
					readonly: this.mode !== "create",
					required: true
				}));

				this.lastNameField = this.registerRegion("lastName", new App.Consumer.Views.FormTextInput({
					labelText: "settings.manageDevices.devices.formLabels.lastName",
					value: this.sharedUserProfile ? this.sharedUserProfile.lastname : "",
					readonly: this.mode !== "create",
					required: true
				}));

				this.emailField = this.registerRegion("email", new App.Consumer.Views.FormTextInput({
					labelText: "settings.manageDevices.devices.formLabels.email",
					model: this.model,
					modelProperty: "user_email",
					readonly: this.mode !== "create",
					required: true
				}));

				this.countryDropdown = this.registerRegion("country", new App.Consumer.Views.SalusDropdownWithValidation({
					collection: countriesCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "countryDropdown",
						dropdownLabelKey: "settings.manageDevices.devices.formLabels.countrySelect"
					}),
					required: true
				}));

				this.registerRegion("shareableDeviceList", new Views.ShareableDeviceListView({
					user_id : this.model.get("user_id"),
					isCreateMode: this.mode === "create",
					collection : new AylaDeviceCollection(App.salusConnector.getInteractableDevices())
				}));

				this.registerRegion("saveButton", new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100 " + (this.options.mode !== "edit" ? "disabled" : ""),
					buttonTextKey: "common.labels.save",
					clickedDelegate: this.handleSaveClick
				}));

				this.registerRegion("cancelButton", new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				}));

				this.validationNeeded.push(this.firstNameField, this.lastNameField, this.countryDropdown,
					this.emailField);

				this.attachValidationListener();
			},

			onRender: function () {
				this.$el.addClass("mode-" + this.options.mode);
			},

			handleSaveClick: function () {
				var that = this;

				// don't save invalid form in create mode
				if (this.mode === "create" && !this.isValid(true)) {
					return;
				}

				// Include gateway and its components DSNs to be shared
				var dsnsToShare = App.salusConnector.getGatewayAndDelegates().map(function (gatewayDevice) {
					return gatewayDevice.get("dsn");
				});

				// Add DSNs of devices that are checked in the list
				this.shareableDeviceList.$el.find("input").each(function(index, checkbox) {
					if (checkbox.checked) {
						dsnsToShare.push($(checkbox).data("dsn"));
					}
				});

				this.model.set("user_profile", {
					firstname: this.firstName.currentView.getValue(),
					lastname: this.lastName.currentView.getValue(),
					country: this.country.currentView.viewModel.get("value"),
					email: this.email.currentView.getValue()
				});

				if (this.mode === "create") {
					return App.Consumer.Settings.Controller.createShare(this.model, dsnsToShare).then(function () {
						that.trigger("save:success");
						return App.salusConnector.getShareCollection().refresh();
					});
				} else {
					return App.Consumer.Settings.Controller.updateShare(this.model, dsnsToShare).then(function () {
						that.trigger("save:success");
						return App.salusConnector.getShareCollection().refresh();
					});
				}
			},

			handleDeleteUserClick: function () {
				var that = this;
				var userProfile = this.model.get("user_profile");
				//TODO - delete DeleteDeviceShareModal.view and related files, since AlertModal replaces it
				App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
					model: new App.Consumer.Models.AlertModalViewModel({
						iconClass: "icon-warning",
						primaryLabelText: App.translate("settings.manageDevices.devices.deleteShareModal") +
						" " + userProfile.firstname + " " + userProfile.lastname,
						secondaryLabelText: App.translate("equipment.delete.sharesDontWorry"),
						rightButton: new App.Consumer.Views.SalusButtonPrimaryView({
							classes: "btn-danger width100",
							buttonTextKey: "common.labels.delete",
							clickedDelegate: that.handleModalDeleteClick
						}),
						leftButton: new App.Consumer.Views.ModalCloseButton({
							classes: "width100",
							buttonTextKey: "common.labels.cancel"
						})
					})
				}));

				App.showModal();
			},
			handleModalDeleteClick: function () {
				var email = this.model.get("user_email");
				App.Consumer.Settings.Controller.deleteDeviceShare(email).then(function () {
					App.hideModal();
				});
			},
			handleCancelClick: function () {
				this.trigger("cancel:click");
			},

			validateForm: function () {
				if (this.isValid()) {
					this.saveButton.currentView.enable();
				} else {
					this.saveButton.currentView.disable();
				}
			}

		}).mixin([ConsumerMixins.RegisteredRegions, ConsumerMixins.SalusCollapsableViewMixin, ConsumerMixins.FormWithValidation]);
	});

	return App.Consumer.Settings.Views.DeviceForm;
});