"use strict";

define([
    "app",
    "bluebird",
    "momentWrapper",
    "collapse",
    "common/config",
    "common/constants",
    "common/util/utilities",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/Breadcrumbs.view",
    "consumer/equipment/views/widgets/EquipmentStatusWidget.view",
    "consumer/equipment/views/widgets/EquipmentAliasEdit.view",
    "consumer/equipment/views/widgets/HistoryWidget.view",
    "consumer/equipment/views/widgets/InformationWidget.view",
    "consumer/equipment/views/widgets/StackedBarGraphWidget.view",
    "consumer/equipment/views/widgets/TRVWidgetContent.view",
    "consumer/schedules/views/ScheduleDaysCollection.view"
], function (App, P, moment, collapse, config, constants, utilities, consumerTemplates, SalusView, Breadcrumbs) {

    App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {

        /**
         * EquipmentSettingsWidget
         * this is to be used as a base view which shows a content view
         * found on the equipment page
         */
        Views.EquipmentSettingsWidget = Mn.LayoutView.extend({
            template: consumerTemplates["equipment/equipmentSettingsWidget"],
            className: "equipment-settings-widget",
            regions: {
                contentRegion: ".bb-widget-content-region",
                breadcrumbs: ".bb-breadcrumbs"
              
            },
            ui: {// ui elements of the panel header
                header: ".bb-widget-header",
                title: ".bb-widget-header-text",
                iconLeft: ".bb-widget-header-icon-left",
                iconRight: ".bb-widget-header-icon-right"
            },
            events: {
                "click .bb-widget-header-icon-right": "_handleRightIconClick"
            },
            initialize: function (options) {
                this.widgetType = options.type;
                if (this.widgetType === constants.widgetTypes.INFORMATION) {
                    this._convertFirmwareVersion();
                    this._changeModelName();
                }
                // flag
                this.headerExpanded = false;
            },
            onRender: function () {
                if (this.widgetType === constants.widgetTypes.STATUS) {
                    this._setupStatusWidget();
                } else if (this.widgetType === constants.widgetTypes.INFORMATION) {
                    this._setupInformationWidget();
                } else if (this.widgetType === constants.widgetTypes.HISTORY) {
                    this._setupHistoryWidget();
                } else if (this.widgetType === constants.widgetTypes.USAGE) {
                    this._setupStackedBarGraphWidget();
                } else if (this.widgetType === constants.widgetTypes.TRV) {
                    this._setupTRVWidget();
                } else if (this.widgetType === constants.widgetTypes.SCHEDULE) {
                    this._setupScheduleWidget();
                }
				
				this.addBinding(this.model, ".bb-widget-header-icon-right",{
					observe: ["connection_status", "LeaveNetwork"],
					update: function ($el, val, model) {
						if (!model.isOnline() || model.isLeaveNetwork()) {
							$el.addClass("invisible");
						} else {
							$el.removeClass("invisible");
						}
					}
				});
            },
            onShow: function () {
                // show the temperature arc on thermostat status widget when DOM is there
                // we have to do this here, and not on the equipmentStatusWidget
                if (this.widgetType === constants.widgetTypes.STATUS && this.contentRegion.currentView.state.currentView.handleDomLoadComplete) {
                    this.contentRegion.currentView.state.currentView.handleDomLoadComplete();
                }

                // this was done as part of SCS-2683 to achieve scrolling to the graph region from the dashboard tile
                if (this.widgetType === constants.widgetTypes.USAGE) {
                    this.trigger("show:usage");
                }
            },
            _initContentCollapse: function () {
                var that = this;

                this.contentRegion.currentView.$el.addClass("collapse in");
                this.ui.iconRight.addClass("icon-collapse-carat");

                this.contentRegion.on("before:empty", function (view) {
                    view.$el.off();
                });

                this.contentRegion.currentView.$el.on('hidden.bs.collapse', function () {
                    that.contentRegion.$el.hide();
                });

                this.contentRegion.currentView.$el.on('show.bs.collapse', function () {
                    that.contentRegion.$el.show();
                });
            },
            _toggleCollapseWidget: function () {
                var isExpanded = this.contentRegion.currentView.$el.hasClass("in");

                if (isExpanded) {
                    this.ui.iconRight.removeClass("icon-collapse-carat").addClass("icon-expand-carat");
                    this.$el.find(".panel-body-widget").addClass("collapsed");
                } else {
                    this.ui.iconRight.removeClass("icon-expand-carat").addClass("icon-collapse-carat");
                    this.$el.find(".panel-body-widget").removeClass("collapsed");
                }

                this.contentRegion.currentView.$el.collapse('toggle');
            },
            /**
             * called for device alias editing in status widget
             * create a view and listen on it
             * @private
             */
            _editDeviceAlias: function () {
                var that = this,
                        $widgetHeader = this.$(".bb-widget-header"),
                        aliasEditView = new Views.EquipmentAliasEdit({
                            model: this.model,
                            page: "equipment"
                        });

                this.listenTo(aliasEditView, "close", function () {
                    aliasEditView.destroy();

                    that.ui.title.removeClass("invisible");
                    that.ui.iconRight.removeClass("invisible");

                    that.headerExpanded = false;
                });
                 this.listenTo(aliasEditView, "click:saveName", function (childView) {
					 that.trigger("click:saveName",that);
//					 that.breadcrumbs.show(new Breadcrumbs.BreadcrumbsView(
//                        {
//                            crumbs: [
//                                {
//                                    textKey: "equipment.myEquipment.title",
//                                    href: "/equipment"
//                                },
//                                {
//                                    text: that.model.getDisplayName(),
//                                    href: "/equipment/myEquipment/" + that.model.get("dsn"),
//                                    active: false
//                                }
//                            ]
//                        }
//                    ));
				});

                // add the view to the header
                $widgetHeader.append(aliasEditView.render().$el);

                this.headerExpanded = true;

                // invisible instead of hidden because we want them to keep their spacing in the header
                this.ui.title.addClass("invisible");
                this.ui.iconRight.addClass("invisible");
            },
            _setupStatusWidget: function () {
                this.addBinding(null, ".bb-widget-header-text", {
                    observe: "name",
                    onGet: function (value) {
                        return utilities.shortenText(value, config.widgets.statusWidget.titleMaxLength);
                    }
                });

                this.ui.iconLeft.addClass("hidden");
                this.ui.iconRight.addClass("edit-icon");

                this.contentRegion.show(new Views.EquipmentStatusWidget({
                    model: this.model
                }));
            },
            _setupInformationWidget: function () {
                this.ui.title.text(App.translate("equipment.information.title"));
                this.ui.iconLeft.addClass("hidden");

                this.$el.addClass("informationWidget");

                this.contentRegion.show(new Views.EquipmentInfoWidgetContentView({
                    model: this.model,
                    isSettingsPage: this.options.isSettingsPage || false
                }));
            },
            _setupHistoryWidget: function () {
                this.ui.title.text(App.translate("equipment.history.title"));
                this.ui.iconLeft.addClass("history-icon");
                this.contentRegion.show(new Views.EquipmentHistoryWidget());

                this._initContentCollapse();
            },
            _setupScheduleWidget: function () {
                var that = this;

                this.ui.title.text(App.translate("equipment.schedule.widget.title"));
                this.ui.iconLeft.addClass("header-schedule-icon");

                App.Consumer.Schedules.Controller.idIndex = 0;
                
                if(this.model){
                    var targetDevice = this.model;

                    //判断是否是某single control的一员,如果是则显示的schedule是master的schedule
                    if (this.model.get("device_category_type") === constants.categoryTypes.THERMOSTATS) {
                        var tscCollection = App.salusConnector.getTSCCollection();
                        var device = App.salusConnector.getDeviceByKey(this.model.get("key"));
                        var singleControl = tscCollection.getSingleControlByThermostat(device);
                        if (singleControl) {
                            var EUID = singleControl.get("defaultDevice");
                            targetDevice = App.salusConnector.getDeviceByEUID(EUID);
                        }
                    }

                    this.schedulePromise=targetDevice.getSchedulePromise();
                }else{
                    this.schedulePromise= P.resolve();
                }

                this.schedulePromise.then(function () {
                    if (!that.model) {
                        // couldn't get
                        App.navigate("equipment");
                        return;
                    }
                    
                    var targetCollection = targetDevice.getSchedules().getDisplayCollectionObj().collection;

                    that.contentRegion.show(new App.Consumer.Schedules.Views.ScheduleDaysMobileView({
                        device: targetDevice,
                        collection: targetCollection,
                        viewType: "widget"
                    }));

                    //hide edit icons in interval
                    that.$('.bb-interval').find('.bb-edit-interval').addClass("hidden");
                    that.$('.bb-add-interval').addClass("hidden");
                });
            },
            _setupStackedBarGraphWidget: function () {
                var that = this;

                this.ui.title.text(App.translate("equipment.usage.title"));
                this.ui.iconLeft.addClass("header-usage-icon");
                this.model.getMetaDataPromise().then(function () {
                    if (!that.isDestroyed) {
                        that.contentRegion.show(new Views.StackedBarGraphWidget({
                            device: that.model
                        }));
                    }
                });
            },
            _setupTRVWidget: function () {
                var title = App.translate("equipment.trv.title");
                if(title && title.length > 28) {
                    this.ui.header.addClass("header-newclass");
                    this.ui.iconLeft.addClass("icon-left-newclass");
                    this.ui.title.addClass("text-newclass");
                }
                
                //this.ui.title.text(App.translate("equipment.trv.title"));
                this.ui.iconLeft.addClass("header-trv-icon");
                
                var trvCount = this.model.getAssociatedTRVs().length;
                this.ui.title.text(trvCount + "    " + App.translate("equipment.trv.title"));
                
                this.ui.iconRight.addClass("edit-icon");
                this.$(".spinner").addClass("hidden");
//                this.contentRegion.show(new Views.TRVWidgetContentView({
//                    model: this.model
//                }));
            },
            _handleRightIconClick: function () {
				if (!this.model.isOnline() || this.model.isLeaveNetwork()) {
					return true;
				}
                if (this.widgetType === constants.widgetTypes.STATUS && !this.headerExpanded) {
                    this._editDeviceAlias();
                }

                if (this.widgetType === constants.widgetTypes.HISTORY) {
                    this._toggleCollapseWidget();
                }
                
                 if (this.widgetType === constants.widgetTypes.TRV) {
                    this._showTRVModal();
                }
            },
            _showTRVModal: function () {
                if (App.salusConnector.getTRVCollection().isEmpty()) {
					return App.navigate("setup/pairing");
				}
                $(".spinner").removeClass("hidden");
                
                this.dependencies = [];
                var that = this;
                
                App.salusConnector.getTRVCollection().each(function (device) {
                    if(device.get("EUID") === undefined || 
                           (device.get("EUID") !== undefined && device.get("EUID").getProperty() === null)){
                        that.dependencies.push(device.getDeviceProperties(true));
                    }else{
                        that.dependencies.push(device.getDevicePropertiesPromise());
                    }
				});

				// get it600 so we can see which trv connected to which
				App.salusConnector.getUserDeviceCollection().getIT600s().each(function (device) {
                    that.dependencies.push(device.getDevicePropertiesPromise());
				});
                
                
                P.all(that.dependencies).then(function () {
                    $(".spinner").addClass("hidden");
                    var contentView = new App.Consumer.Equipment.Views.TRVEditModalView({
                        model: that.model
                    });

                    App.modalRegion.show(new App.Consumer.Views.SalusModalView({
                        size: "modal-lg",
                        contentView: contentView
                    }));

                    App.showModal();
                });
                
				

				
			},
            
            _convertFirmwareVersion: function () {
                var start, end, oldFirmware, newFirmware, oemModel;
                oemModel = this.model.get("oem_model");
                oldFirmware = this.model.get("FirmwareVersion") ? this.model.get("FirmwareVersion").get("value") : null;
                if(oldFirmware) {
                    if(typeof oldFirmware === "number" || (oldFirmware.indexOf("+") !== -1)) {
                        return;
                    }
                }
                
                if (oemModel === "it600HW" || oemModel === "it600HW-AC" || oemModel === "it600ThermHW" || oemModel === "it600ThermHW-AC" 
                        || oemModel === "it600MINITRV" || oemModel === "it600TRV") {
                    if(oldFirmware) {
                        start = oldFirmware.substr(0, 4);
                        end = oldFirmware.substr(4, 4);

                        newFirmware = parseInt(start, 16)/10 + " + " + parseInt(end, 16)/10;
                        this.model.get("FirmwareVersion").set("value", newFirmware);
                    }
                } else if(oemModel === "SP600" || oemModel === "SPE600") {
                    if(oldFirmware) {
                        start = oldFirmware.substr(0, 4);

                        newFirmware = start/100;
                        this.model.get("FirmwareVersion").set("value", newFirmware);
                    }
                } else if(oemModel === "ECM600") {
                    var appVersion =  this.model.get("ApplicationVersion_d");
                    if(appVersion.models) {
                        if(appVersion.models[0].get("name") === "ep_1:sBasicS:ApplicationVersion_d") {
                            if(appVersion.models[0].get("value")) {
                                newFirmware = this._hexConversion(appVersion.models[0].get("value").toString(16));
                                this.model.get("FirmwareVersion").models[0].set("value", newFirmware);
                            }
                        }
                    }
                }
            },
            
            _hexConversion: function(value) {
                var arr = value.split("");
                if(arr) {
                    if(arr.length > 1) {
                        return parseInt(arr[arr.length - 2], 16) + "." + parseInt(arr[arr.length - 1], 16);
                    } else {
                        return parseInt(arr[arr.length - 1], 16);
                    }
                }
            },
            
            _changeModelName: function() {
                var oemModel = this.model.get("oem_model");
                switch(oemModel) {
                    case "it600ThermHW":
                    case "it600ThermHW-AC":
                        this.model.set("localOem", "VS10/20 " + App.translate("equipment.names.thermostat"));
                        break;
                    case "it600HW":
                    case "it600HW-AC":
                        this.model.set("localOem", "VS10/20 " + App.translate("equipment.names.waterHeater"));
                        break;
                    case "SP600":
                        this.model.set("localOem", "SP600 " + App.translate("equipment.names.smartPlug"));
                        break;
                    case "SPE600":
                        this.model.set("localOem", "SPE600 " + App.translate("equipment.names.smartPlug"));
                        break;
                    case "OS600":
                        this.model.set("localOem", "OS600 " + App.translate("equipment.provisioning.windowSensor"));
                        break;
                    case "ECM600":
                        this.model.set("localOem", "ECM600 " + App.translate("equipment.names.energyMeter"));
                        break;
                    case "sau2ag1":
                        this.model.set("localOem", "UG600 " + App.translate("equipment.names.gateway"));
                        break;
                    case "it600WC":
                        this.model.set("localOem", "KL08RF/10RF " + App.translate("equipment.names.iT600WiringCenter"));
                        break;
                    case "it600MINITRV":
                        this.model.set("localOem", "TRV10RFM " + App.translate("equipment.names.iT600Trv"));
                        break;
                    default:
                        this.model.set("localOem", oemModel);
                        break;
                }
            }
            
        }).mixin([SalusView]);
    });

    return App.Consumer.Equipment.Views.EquipmentSettingsWidget;
});
