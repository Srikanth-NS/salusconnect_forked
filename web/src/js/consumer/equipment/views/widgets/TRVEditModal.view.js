"use strict";

define([
	"app",
	"common/config",
    "common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/views/SalusButtonPrimary.view"
], function (App, config, constants, templates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.TRVEditModalEntryView = Mn.LayoutView.extend({
			className: "col-xs-12 trv-item",
			template: templates["equipment/widgets/trvEditModalItem"],
			regions: {
				checkboxRegion: ".bb-checkbox"
			},
			bindings: {
				".bb-identify": {
					observe: "SetIndicator",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
                         if(this.model.modelType === constants.modelTypes.WINDOWMONITOR || 
                                this.model.modelType === constants.modelTypes.DOORMONITOR){
                            $el.addClass("invisible");
                        }
                        else{
                            $el.toggleClass("active", !!val);
                        }
						
					}
				}
			},
			events: {
				"click .bb-identify": "toggleSetIndicator"
			},
			initialize: function (/*options*/) {
				var that = this;
                
                this.orginIndicator = this.model.get("SetIndicator").getProperty();

				_.bindAll(this, "toggleSetIndicator");
                
				this.checkboxView = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "equipmentCheckbox",
						nonKeyLabel: this.model.get("name"),
						secondaryIconClass: "",
						isChecked: this.options.checked,
						disabled: this.options.disabled
					})
				});

				this.listenTo(this.checkboxView, "clicked:checkbox", function () {
					that.trigger("click:checkbox");
				});
			},
			onRender: function () {
				this.checkboxRegion.show(this.checkboxView);
			},
			toggleSetIndicator: function () {
				this.model.toggleSetIndicator();
			},
			onDestroy: function () {
                if(this.model.get("SetIndicator").getProperty() !== this.orginIndicator){
                    this.model.toggleSetIndicator();
                   // this.model.undoSetIndicator();
                }
				
			}
		}).mixin([SalusView]);

		Views.TRVEditModalCollectionView = Mn.CollectionView.extend({
			className: "trv-collection-view row",
			childView: Views.TRVEditModalEntryView,
			childViewOptions: function (model) {
				var checked = this.model.hasTRV(model),
					foundAssociation = this.it600s.findTRV(model);

				// if we find an association, check that it is not on the current it600
				return {
					model: model,
					checked: checked,
					disabled: foundAssociation ? !(foundAssociation && checked) : foundAssociation
				};
			},
			childEvents: {
				"click:checkbox": "handleCheckboxClick"
			},
			initialize: function () {
				this.collection = App.salusConnector.getTRVCollectionWithLive();

				this.it600s = App.salusConnector.getUserDeviceCollection().getIT600s();
			},
			getCheckedItems: function () {
				var array = [];

				if (!this.collection.isEmpty()) {
					this.children.each(function (childView) {
						if (childView.checkboxView.getIsChecked()) {
							array.push(childView.model);
						}
					});
				}

				return array;
			},
			anyChecked: function () {
				if (!this.collection || this.collection.isEmpty()) {
					return false;
				} else {
					return _.some(this.children._views, function (childView) {
						return childView.checkboxView.getIsChecked();
					});
				}
			},
			handleCheckboxClick: function () {
				this.trigger("click:checkbox");
			}
		}).mixin([SalusView]);

		Views.TRVEditModalView = Mn.LayoutView.extend({
			className: "modal-body trv-edit-modal",
			template: templates["equipment/widgets/trvEditModal"],
			regions: {
				trvCollectionRegion: ".bb-trv-collection-region",
				saveButtonRegion: ".bb-save-button",
				cancelButtonRegion: ".bb-cancel-button"
			},
			ui: {
				error: ".bb-error"
			},
			bindings: {
				".bb-title": {
					observe: "name",
					update: function ($el, name) {
						$el.text(App.translate("equipment.trv.modal.title", {
							name: name
						}));
					}
				}
			},
			initialize: function () {
				var that = this;

				_.bindAll(this, "handleSaveClicked", "handleCancelClicked");
                
                
                
                
                

				this.trvCollectionView = new Views.TRVEditModalCollectionView({
					model: this.model
				});

				// this will always return an array
				var isEdit = this.model.getAssociatedTRVs().length > 0;

				this.listenTo(this.trvCollectionView, "click:checkbox", function () {
					// check started updated/edit
					if (that.trvCollectionView.anyChecked() || isEdit) {
						that.saveButton.enable();
					} else {
						that.saveButton.disable();
					}
				});

				this.saveButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.save",
					classes: "width100 btn-primary disabled",
					clickedDelegate: this.handleSaveClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "width100",
					className: "btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});
			},
			onRender: function () {
                
				this.trvCollectionRegion.show(this.trvCollectionView);
				this.saveButtonRegion.show(this.saveButton);
				this.cancelButtonRegion.show(this.cancelButton);
			},
			handleSaveClicked: function () {
				var that = this, checked = this.trvCollectionView.getCheckedItems();

				this.ui.error.addClass("hidden");

				if (checked.length > 6) {
					this.ui.error.removeClass("hidden");
					return false;
				}

				this.saveButton.showSpinner();

				return this.model.associateTRVs(checked).then(function () {
					that.saveButton.hideSpinner();
					that.trigger("save:associations");
					App.hideModal();
				});
			},
			handleCancelClicked: function () {
				App.hideModal();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.TRVEditModalView;
});
