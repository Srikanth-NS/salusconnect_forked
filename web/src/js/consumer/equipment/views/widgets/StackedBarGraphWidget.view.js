"use strict";

define([
	"app",
	"d3",
	"momentWrapper",
	"common/util/utilities",
    "bluebird",
    "common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/equipment/views/UsageCheckboxCollection.view",
	"consumer/models/SalusDropdownViewModel",
	"consumer/equipment/models/StackedBarGraphView.model"
], function (App, d3, moment, utils, P, constants, consumerTemplates, SalusView, SalusPrimaryButtonView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.StackedBarGraphWidget = Mn.LayoutView.extend({
			template: consumerTemplates["equipment/widgets/stackedBarGraphWidget"],
			className: "equipment-stacked-bar-graph",
			ui: {
				graphContent: "#bb-graph",
				timeSpanDropdown: "#bb-time-span-dropdown",
				timeMachineRange: "#bb-display-range",
				spinnerView: ".bb-spinner-view",
				graphError: "#bb-graph-error"
			},
			regions: {
				interactiveModeBtnRegion: "#bb-interactive-mode-btn",
				graphOptionsRegion: "#bb-graph-display-options"
			},
			bindings: {
				"#bb-start-date": {
					observe: "currentStartDate",
					onGet: function (startDate) {
                        var timeSpan = this.model.get("currentTimespan");
                        if(timeSpan === "1h"){
                            return moment(startDate).format("MMM-DD 00:00");
                        }else{
                            return moment(startDate).format("MMM-DD");
                        }
						
					}
				},
				"#bb-end-date": {
					observe: "currentEndDate",
					onGet: function (endDate) {
                        var timeSpan = this.model.get("currentTimespan");
                        if(timeSpan === "1h"){
                            return moment(endDate).format("23:59");
                        }else{
                            return moment(endDate).format("MMM-DD");
                        }
						
					}
				}
			},
			events: {
				"click #bb-start-date": "_goBackInTime",
				"click #bb-end-date": "_goForwardInTime",
				"click .bb-reload-error-text": "_reloadGraph"
			},
			_toggleGraphSpinner: function (loading) {
                $(this.ui.spinnerView).toggleClass("hidden", !loading);
				//this.ui.spinnerView.toggleClass("hidden", !loading);
			},
			initialize: function (options) {
				this.device = options.device;
                //this.tempStartDate = "2016-06-01";
                
                //Bug_SCS-3285
                this.tempStartDate = "2016-07-18";

				this._setupModel();

				if (App.isDevOrInt()) {
					this.interactiveModeBtn = new SalusPrimaryButtonView({
						id: "interactive-mode-btn",
						classes: "center-block invisible",
						buttonTextKey: "equipment.usage.interactiveMode",
						clickedDelegate: this._handleInteractiveModeClick
					});
				}

				this._setupDropDown();

				//this.listenTo(this.model, "change:currentTimespan", this._updateGraph);
				//this.listenTo(this.model, "change:currentEndDate", this._updateGraph);
			},
			onRender: function () {
				var that = this;

				if (this.interactiveModeBtn) {
					this.interactiveModeBtnRegion.show(this.interactiveModeBtn);
				} else {
					this.$("#bb-interactive-mode-btn").detach();
					this.$el.addClass("padding-t-40");
				}

				this.ui.timeSpanDropdown.append(this.dropdownView.render().$el);

				this.device.getDevicePropertiesPromise().then(function () {
					return that.updateGraph();
				});
			},
			onDestroy: function () {
				this.device.killGraphRequests();
				this.dropdownView.destroy();
			},
			_reloadGraph: function () {
				this.ui.graphContent.removeClass("hidden");
				this.ui.timeSpanDropdown.removeClass("hidden");
				this.ui.timeMachineRange.removeClass("hidden");
				this.ui.graphError.addClass("hidden");
				this.updateGraph();
			},
			_updateGraphClicked: function (checkedDevices) {
				this.drawGraph(this.model.get("currentDatapoints"), checkedDevices);
			},
			_setupDropDown: function () {
				var timeSpanCollection = new App.Consumer.Models.DropdownItemViewCollection();
                 timeSpanCollection.add(new App.Consumer.Models.DropdownItemViewModel({
					value: 0,
					displayText: App.translate("common.labels.hourly")
				}));

				timeSpanCollection.add(new App.Consumer.Models.DropdownItemViewModel({
					value: 1,
					displayText: App.translate("common.labels.daily")
				}));
				timeSpanCollection.add(new App.Consumer.Models.DropdownItemViewModel({
					value: 2,
					displayText: App.translate("common.labels.weekly")
				}));
				timeSpanCollection.add(new App.Consumer.Models.DropdownItemViewModel({
					value: 3,
					displayText: App.translate("common.labels.monthly")
				}));
              
//				timeSpanCollection.add(new App.Consumer.Models.DropdownItemViewModel({
//					value: 4,
//					displayText: App.translate("common.labels.yearly")
//				}));
               

				this.dropdownView = new App.Consumer.Views.SalusDropdownViewNoi18N({
					collection: timeSpanCollection,
					viewModel: new B.Model({
						id: "timeSpanDropdown",
						dropdownLabelKey: timeSpanCollection.models[0].get("displayText")
					})
				});

				this.listenTo(this.dropdownView ,"value:change", this._updateTimeSpan);
			},
			_setupModel: function () {
				this.model = new App.Consumer.Equipment.Models.StackedBarGraphViewModel();
				this.model.set("currentEndDate", new Date());
				this.model.set("currentStartDate", new Date(this.model.get("currentEndDate")));
				
				this.model.set("currentTimespan", "1h");
			},
			_goBackInTime: function () {
				this._timeMachine(false);
			},

			_goForwardInTime: function () {
				this._timeMachine(true);
			},
           
           //use this function to fix bug that the date format can not be change
           //when timespan changed
           _setStartEndDate: function(){
                var startDate = this.model.get("currentStartDate");
                var endDate = this.model.get("currentEndDate");
                var timeSpan = this.model.get("currentTimespan");
                var startValue = moment(startDate).format("MMM-DD");
                var endValue = moment(endDate).format("MMM-DD");
                if(timeSpan === "1h"){
                    startValue = moment(startDate).format("MMM-DD 00:00");
                    endValue =  "23:59";
                }
                $("#bb-start-date").text(startValue);
                $("#bb-end-date").text(endValue);
            },
            

			_timeMachine: function (direction) {
				var timespan = this.model.get("currentTimespan"),
						endDate = this.model.get("currentEndDate"),
						newEndDate = new Date(endDate),
						modifier = direction ? 1 : -1;

                if (timespan === "1h") {
					newEndDate.setDate(endDate.getDate() + modifier * 1); // 1 day
                }else if (timespan === "1d") {
                    //Dev_SCS-3281 start
                    var day = moment(newEndDate).format("d");
                    if(day > 0){
                        newEndDate.setDate(endDate.getDate() + (7 - day));
                    }
                    newEndDate.setDate(endDate.getDate() + modifier * 7);
                    //Dev_SCS-3281 end
					/*newEndDate.setDate(endDate.getDate() + modifier * 6); // 7 days*/
                } else if (timespan === "1w") {
                    if(modifier < 1){
                        newEndDate = moment(endDate).subtract(1, 'M');
                    }
                    else{
                        newEndDate = moment(endDate).add(1, 'M');
                    }
                    newEndDate = new Date(newEndDate);
                } 
                else if (timespan === "1mon") {
					newEndDate.setMonth(endDate.getMonth() + modifier * 5); // 6 months                   
				} else if (timespan === "12mon") {
					newEndDate.setYear(endDate.getFullYear() + modifier * 5); // 5 years
				}
                var formatEndDate = moment(newEndDate).format("YYYY-MM-DD");
                var formatCurrentDate = moment(new Date()).format("YYYY-MM-DD");
                if(moment(formatEndDate).isAfter(formatCurrentDate)){
                    this.model.set("currentEndDate", new Date());
                }
               
                else if(moment(formatEndDate).isBefore(this.tempStartDate)){
                    return;
                
                }
//              else if(moment(formatEndDate).isBefore(this.tempStartDate) || formatEndDate === this.tempStartDate){
//                    return;
//                
//              }
            //Bug_SCS-3285
                else{
                    this.model.set("currentEndDate", newEndDate);
                }
                if(this.checkDateRange()){
                     this._setStartEndDate();
                     this.updateGraph();
                }
                
			},
           
           

			_updateTimeSpan: function (data) {
				var newTimespan;
				switch (data.selectedValue) {
                    case 0:
                        newTimespan = "1h";
						break;
					case 1:
						newTimespan = "1d";
						break;
					case 2:
						newTimespan = "1w";
						break;
					case 3:
						newTimespan = "1mon";
						break;
					case 4:
						newTimespan = "12mon";
						break;
                    
					default:
						newTimespan = "1h";
				}
				this.timespanChanged = true;
				this.model.set("currentTimespan", newTimespan);
                if(this.checkDateRange()){
                    this._setStartEndDate();
                    this.updateGraph();
                }
			},
            
             checkDateRange: function () {
				var timespan = this.model.get("currentTimespan"),
						endDate = this.model.get("currentEndDate"),
						newEndDate = new Date(endDate),
                        newStartDate =  new Date(newEndDate);
                if (timespan === "1h") {
                    newStartDate = new Date(newEndDate);
                }else if (timespan === "1d") {
                    /*
                    newStartDate = new Date(newEndDate);
                    newStartDate.setDate(newEndDate.getDate() - 6);*/
                     
                    //Dev_SCS-3281 start
                    //make sure the day is from Mon to Sun
                    newStartDate = new Date(newEndDate);  
                    var day = moment(newEndDate).format("d");
                    if(day > 0){
                        newEndDate.setDate(endDate.getDate() + (7 - day));
                    }
                    newStartDate.setDate(newEndDate.getDate() - 6);
                    //Dev_SCS-3281 end
                } else if (timespan === "1w") {
                    var formatEndDate = moment(newEndDate).format("YYYY-MM");
                    var formatCurrentDate = moment(new Date()).format("YYYY-MM");
                    if(moment(formatEndDate).isBefore(formatCurrentDate) &&
                            formatEndDate !== formatCurrentDate){
                        
                        newEndDate = moment(newEndDate).endOf('month').startOf('isoweek').toDate();
                        if(moment(newEndDate).format("d") === "1")//if the day is Monday,substract two day
                        {
                            newEndDate = new Date(newEndDate);
                            newEndDate.setDate(newEndDate.getDate() - 2);
                        }
                        else if(moment(newEndDate).format("d") === "0"){//if the day is Sunday,subtract one day
                            newEndDate = new Date(newEndDate);
                            newEndDate.setDate(newEndDate.getDate() - 1);
                        }
                    }
                    else{
                        newEndDate = new Date();
                    }
                  
                    
                    newStartDate = new Date(newEndDate);
                    newStartDate.setDate(1);
                    //if the fisrt day is not Sunday,then set the date to last Sunday from last month
                   // if(moment(newStartDate).format("d") !== "0"){
                     	newStartDate = newStartDate.setMonth(newStartDate.getMonth() - 1);      
                        newStartDate = moment(newStartDate).endOf('month').startOf('isoweek').toDate();
                        if(moment(newStartDate).format("d") === "1")//if the day is Monday,substract one day
                        {
                            newStartDate = new Date(newStartDate);
                            newStartDate.setDate(newStartDate.getDate() - 1);
                        }
                } 
                else if (timespan === "1mon") {
                    var formatEndDate = moment(newEndDate).format("YYYY-MM");
                    var formatCurrentDate = moment(new Date()).format("YYYY-MM");
                    if(moment(formatEndDate).isBefore(formatCurrentDate) &&
                            formatEndDate !== formatCurrentDate){
                       var lastDate = moment(newEndDate).endOf('month').get("date");
                       newEndDate.setDate(lastDate);   
                       newEndDate.setHours(23);
                    }else{
                        newEndDate = new Date();
                    }
                    
                    newStartDate = new Date(newEndDate);
                    newStartDate.setDate(1);
					newStartDate.setMonth(newEndDate.getMonth() - 5);
                    newStartDate.setHours(0);
				} else if (timespan === "12mon") {
				}
                
                //endDate can not be larger than today, startdate can not be small than 2016/06/01
                var formatEndDate = moment(newEndDate).format("YYYY-MM-DD");
                var formatStartDate = moment(newStartDate).format("YYYY-MM-DD");
                var formatCurrentDate = moment(new Date()).format("YYYY-MM-DD");
                if(moment(formatEndDate).isAfter(formatCurrentDate)){
                    newEndDate = new Date();
                    //return false;
                }
                if(moment(formatStartDate).isBefore(this.tempStartDate) || formatStartDate === this.tempStartDate){
                    newStartDate = new Date(this.tempStartDate);
                }
                
                this.model.set("currentEndDate", newEndDate);
                this.model.set("currentStartDate", newStartDate);
                return true;
                
			},
			updateGraph: function () {
                
				var newDatapoints, amount, that = this,
						startDate = new Date(),
						dateObj = {},
						timespan = this.model.get("currentTimespan"),
						endDate = this.model.get("currentEndDate"),
                        startDate = this.model.get("currentStartDate");

				this._toggleGraphSpinner(true);
                if(timespan === "1h") { //daily
					
                    dateObj[moment(endDate).format("YYYY-MM-DD")] = [];
                    
					newDatapoints = this.device.getSummationGraphData("1h", startDate, endDate, dateObj);
                    this._showCommonDataResult(newDatapoints,startDate);
				} 
				else if (timespan === "1d") { //daily
					//use  "1d" to get the data not including current day,
                    //use "1h" to get the current day data.
                    //finally ,calculate it
                    amount = 6;

					for (;amount >= 0; amount--) {
						dateObj[moment(endDate).subtract(amount, "days").format("YYYY-MM-DD")] = [];
					}
                    dateObj[moment(endDate).format("YYYY-MM-DD")] = [];
					
                    var that = this;
                    var currentDate = new Date();
                    var strCurrentDate = moment(currentDate).format("YYYY-MM-DD"),
                        strCurrentEndDate = moment(endDate).format("YYYY-MM-DD");
                    if( strCurrentDate === strCurrentEndDate){                        
                        
                        var  dateObj1 = {};
                        dateObj1[moment(endDate).format("YYYY-MM-DD")] = [];
                       return this.device.getSummationGraphData("1d", startDate, endDate, dateObj).then(function(data){
                            that.device.getSummationGraphData("1h", currentDate, endDate, dateObj1).then(function(currentData){
                                 data = that._calculateDataForLastDay(currentData,data,strCurrentDate);
                                 that._showNameGraph(data);
                            });
                        }).catch(function () {
                            that._showGraphError();
                        });
                    }else{
                        newDatapoints = this.device.getSummationGraphData("1d", startDate, endDate, dateObj);
                        this._showCommonDataResult(newDatapoints,startDate);
                    }
                    
				} else if (timespan === "1w") { //weekly
                    //if the search condition including the current day, then use the below logic
					//use "1w" get the datas from Saturday to Sunday
                    //use "1d" get the datas from Sunday to current date
                    //use "1h" get the data for current day.
                    //finally, calculate it.
                    var currentDate = new Date();
                    var strCurrentDate = moment(currentDate).format("YYYY-MM"),
                        strCurrentEndDate = moment(endDate).format("YYYY-MM");
                    if( strCurrentDate === strCurrentEndDate){
                        dateObj[moment(endDate).format("YYYY-MM-DD")] = [];
                        
                         var that = this;
                        return this.device.getSummationGraphData("1w", startDate, endDate, dateObj).then(function(monData){
                             var  dateObj1 = {};
                            
                             dateObj1[moment(currentDate).format("YYYY-MM-DD")] = [];
                             
                             var currentMonDay = new Date();
                             
                           //  currentMonDay.setDate(currentDate.getDate() - parseInt(moment(currentDate).format("d")));
                             //Dev_SCS-3281 start
                             currentMonDay.setDate(currentDate.getDate() - (moment(currentDate).format("d") - 1));
                               //Dev_SCS-3281 end
                             that.device.getSummationGraphData("1d", currentMonDay, currentDate, dateObj1).then(function(dailyData){
                                var dateObj2 = {};
                                dateObj2[moment(endDate).format("YYYY-MM-DD")] = [];

                                that.device.getSummationGraphData("1h", new Date(), new Date(), dateObj2).then(function(hourData){
                                    dailyData = that._calculateDataForLastDay(hourData,dailyData,moment(endDate).format("YYYY-MM-DD"));
                                    
                                    monData = that._calculateDataForLastDay(dailyData,monData,moment(endDate).format("YYYY-MM-DD"));
                                    
                                   
                                    that._showNameGraph(monData);
                                });
                            });
                       }).catch(function () {
                            that._showGraphError();
                        });
                        
                    }else{
                        newDatapoints = this.device.getSummationGraphData("1w", startDate, endDate, dateObj);
                        this._showCommonDataResult(newDatapoints,startDate);
                    }
				} else if (timespan === "1mon") {
                    //use "1mon" get the current month data,
                    //use "1d" to get current month data from 1 date to current date,
                    //use "1h" to get the current date data.
                    var currentDate = new Date();
                    var strCurrentDate = moment(currentDate).format("YYYY-MM"),
                        strCurrentEndDate = moment(endDate).format("YYYY-MM");
                    if( strCurrentDate === strCurrentEndDate){
                        dateObj[moment(endDate).format("YYYY-MM-DD")] = [];
                        var that = this;
                        return this.device.getSummationGraphData("1mon", startDate, endDate, dateObj).then(function(monData){
                             var  dateObj1 = {};
                             
                             dateObj1[moment(currentDate).format("YYYY-MM-DD")] = [];
                             
                             var currentMonDay = new Date();
                             currentMonDay.setDate(1);
                             
                             that.device.getSummationGraphData("1d", currentMonDay, currentDate, dateObj1).then(function(dailyData){
                                var dateObj2 = {};
                                dateObj2[moment(endDate).format("YYYY-MM-DD")] = [];

                                that.device.getSummationGraphData("1h", new Date(), new Date(), dateObj2).then(function(hourData){
                                    dailyData = that._calculateDataForLastDay(hourData,dailyData,moment(endDate).format("YYYY-MM-DD"));
                                     
                                    monData = that._calculateDataForLastDay(dailyData,monData,moment(endDate).format("YYYY-MM-DD"));
                                     that._showNameGraph(monData);
                                });
                            });
                       }).catch(function () {
                            that._showGraphError();
                        });
                       
                    }
                    else{
                        newDatapoints = this.device.getSummationGraphData("1mon", startDate, endDate, dateObj);
                        this._showCommonDataResult(newDatapoints,startDate);
                    }
				} else if (timespan === "12mon") {
					amount = 6;
					startDate = new Date(endDate);
					startDate.setYear(endDate.getFullYear() - amount);
                    

					for (;amount >= 0; amount--) {
						dateObj[moment(endDate).subtract(amount, "years").format("YYYY-MM-DD")] = [];
					}
					newDatapoints = this.device.getSummationGraphData("12mon", startDate, endDate, dateObj);
                    this._showCommonDataResult(newDatapoints,startDate);
				}
                
                

				

				// Mock data example. Comment if statement above to view
				// 'date' just needs to be a string, used for naming purposes
				// Number of cols for each dropdown option: Daily: 7 days, Weekly: 5 weeks, Monthly: 6 months, Yearly: 5 years
				//var data1 = [
				//	{"Device1" : 1000, "Device2" : 1100, "Device3" : 600, "Device4" : 100, "date" : "Sep-09"},
				//	{"Device1" : 200, "Device2" : 800, "Device3" : 600, "Device4" : 560,"date" : "Sep-10"},
				//	{"Device1" : 800, "Device2" : 100, "Device3" : 400, "Device4" : 220, "date" : "Sep-11"},
				//	{"Device1" : 100, "Device2" : 300, "Device3" : 200, "Device4" : 90, "date" : "Sep-12"},
				//	{"Device1" : 100, "Device2" : 200, "Device3" : 470, "Device4" : 222, "date" : "Sep-13"}
				//];
				//var deviceNames = ["Device1", "Device2", "Device3", "Device4"];
				//that.drawGraph(data1, deviceNames);
			},
            _showNameGraph: function(data){
                var that = this;
                data = that._filterDateRange(data);
                that._parseThreePhase(data, that.model.get("currentDeviceNames"));
                that.drawGraph(that.model.get("currentDatapoints"), that.model.get("currentDeviceNames"));

                that._toggleGraphSpinner(false);
            },
            _showGraphError: function(){
                var that = this;
                that.ui.graphContent.addClass("hidden");
                that.ui.timeSpanDropdown.addClass("hidden");
                that.ui.timeMachineRange.addClass("hidden");
                that.ui.graphError.removeClass("hidden");

                that._toggleGraphSpinner(false);
            },
            
            _calculateDataForLastDay: function(currentData,sumData,strCurrentDate){
                var valDev1=0,valDev2=0,valDev3=0,valDev4=0; 
                _.each(currentData,function(obj){
                     valDev1 += parseFloat(obj.Device1);
                     valDev2 += parseFloat(obj.Device2);
                     valDev3 += parseFloat(obj.Device3);
                     valDev4 += parseFloat(obj.Device4);
                 });

                _.each(sumData,function(obj){
                    if(obj.date === strCurrentDate){
                        obj.Device1 = valDev1;
                        obj.Device2 = valDev2;
                        obj.Device3 = valDev3;
                        obj.Device4 = valDev4;
                    }
                    return obj;
                });
                return sumData;
            },
            _showCommonDataResult: function(newDatapoints,startDate){
                var that = this;
                if (newDatapoints) {
					//that.model.set("currentStartDate", startDate);
					return newDatapoints.then(function (data) {
                        that._showNameGraph(data);
					}).catch(function () {
						that._showGraphError();
					});
				}
            },
            //filter date range
            _filterDateRange: function(data){
                var timespan = this.model.get("currentTimespan");
                if(timespan === "1h"){
                    return data;
                }else{
                    var that = this;
                    var newResult = [];
                     _.each(data,function(object){
                        var singleDate = object.date;
                        if(timespan === "1w"){
                            if(singleDate !== moment(new Date()).format("YYYY-MM-DD")){
                                var newDate = new Date(singleDate);
                                newDate.setDate(newDate.getDate() - 6);
                                singleDate = moment(newDate).format("YYYY-MM-DD");
                            }
//                            else{
//                                var currentMDay = new Date();
//                                currentMDay.setDate(currentMDay.getDate() - parseInt(moment(currentMDay).format("d")));
//                                singleDate = moment(currentMDay).format("YYYY-MM-DD");
//                            }
                           
                        }
                        if(timespan === "1mon"){
                            object.date = moment(new Date(singleDate)).tz("Asia/Shanghai").format("MMM");
                        }
                       
                        else{
                            object.date = moment(new Date(singleDate)).tz("Asia/Shanghai").format("MMM-D");
                        }
                        if(that.isValidDateRange(singleDate)){
                            newResult.push(object);
                        }
                        
                    });
                    return newResult;
                }
               
            },
             isValidDateRange: function(singleDate){
                var that = this;
                var endDate = this.model.get("currentEndDate");
                var startDate = this.model.get("currentStartDate");
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                var isBetween = moment(singleDate).isBetween(startDate, endDate); // true
                var isSame1 = singleDate === startDate; 
                var isSame2 = singleDate === endDate; 
                if(isBetween || isSame1 || isSame2){
                   return true;
                }   
                return false;
            },
			// checks the text to be set on the tooltip and truncates if too long
			_setTooltipText: function (text, maxLength) {
				if(text.length > maxLength) {
					return text.substring(0, maxLength) + '...';
				}
				else {
					return text;
				}
			},
			// check device data
			_parseThreePhase: function (data, deviceNames) {
				var phases = this.device.get("ACPhase_c"),
						threePhaseDeviceIndexes = [],
						isThreePhase = false,
						displayNames = null,
						displayData = null;

				_.each(phases.models, function(phase, index) {
					if (phase.get("value") === 3) {
						isThreePhase = true;
						threePhaseDeviceIndexes.push(index);
					}
				});

				var clampsUsed = this.device.get("ClampsUsed");
                
                //if em's attribute ClampsUsed == [],then write value
                if(this.device.modelType === constants.modelTypes.ENERGYMETER && 
                        (_.isNull(clampsUsed) || clampsUsed.length === 0)){
                    var tempCheckedClamps = [
                        {clamp: 1, name: "clamp1", phase: 1},
                        {clamp: 2, name: "clamp2", phase: 1},
                        {clamp: 3, name: "clamp3", phase: 1},
                        {clamp: 4, name: "clamp4", phase: 1}];
                    this.device.set("ClampsUsed", tempCheckedClamps);
                }

				if (isThreePhase) {
					var phaseThreeName, phaseOneName;

					if (!clampsUsed || clampsUsed.length < 0 || !_.isObject(clampsUsed[0])) {
						phaseThreeName = "3 Phase";
						phaseOneName = "1 Phase";
					} else {
						phaseThreeName = _.findWhere(this.device.get("ClampsUsed"), {
							phase: 3
						}).name;

						phaseOneName = _.findWhere(this.device.get("ClampsUsed"), {
							phase: 0
						}).name;
					}
                    displayNames = [phaseThreeName];
					//displayNames = [phaseOneName, phaseThreeName];
					displayData = _.map(data, function (dataObj) {
						var threePhaseTotal = 0, singlePhaseTotal = 0;
						var retObj = {date: dataObj.date};
						var deviceNames = _.without(_.keys(dataObj), "date");

						// For each device, get it's value and aggregate with proper phase
						_.each(deviceNames, function(name, i) {
							if (_.indexOf(threePhaseDeviceIndexes, i) >= 0) { // if device is three phase
								threePhaseTotal += dataObj[name];
							} else {
								singlePhaseTotal += dataObj[name];
							}
						});

						retObj[phaseOneName] = singlePhaseTotal;
						retObj[phaseThreeName] = threePhaseTotal;

						return retObj;
					});
				} else {
                    displayNames = [];
                    //if clampsUsed is undefined, the device is smartplug,don't show the name.
                    if(clampsUsed === undefined){
                        displayNames.push("");
                    }else{
                         _.each(clampsUsed, function (clamp) {
                            if(clamp.phase === 1){
                                displayNames.push(clamp.name);
                            }
                        });
                    }
                   
					displayData = _.map(data, function (dataObj) {
						var returnObj = {};
						_.each(_.keys(dataObj), function (key) {
							if (key.indexOf("Device") >= 0) {
								returnObj[displayNames[parseInt(key.slice(-1))-1]] = dataObj[key];
							}
						});

						returnObj.date = dataObj.date;
						return returnObj;
					});
				}

				this.model.set("currentDeviceNames", displayNames || deviceNames);
				this.model.set("currentDatapoints", displayData || data);

				// set bar graph colors
				var colorArr = ["#0097a7", "#ff6d00", "#ffc30f", "#6b486b"];
				this.colorMap = _.object(this.model.get("currentDeviceNames"), colorArr);

				// setup the checkboxes above the graph
                if(this.device.modelType === constants.modelTypes.ENERGYMETER){
                    this._setCheckboxes(this.model.get("currentDeviceNames"));
                }
				
			},
			_setCheckboxes: function (deviceNames) {
				var colors = ["green", "orange", "yellow", "purple"];
				var checkColl = new B.Collection();

				_.each(deviceNames, function(device, index) {
					checkColl.add([{
						"id": device,
						"color": colors[index],
						"isChecked": true
					}]);
				});

				this.usageCheckboxCollectionView = new Views.UsageCheckboxCollectionView({
					collection: checkColl
				});

				this.graphOptionsRegion.show(this.usageCheckboxCollectionView);

				this.listenTo(this.usageCheckboxCollectionView, "trigger:checkboxClicked", this._updateGraphClicked);
			},
			_createStackedBarGraph: function (datapoints, deviceNames) {
				// graph set up
				var graphMargins = {top: 100, right: 80, bottom: 60, left: 80},
				// viewbox is the region in which the graph can be drawn
				viewBoxWidth = 700,
				viewBoxHeight = 700,
				tooltipHeight = 100,
				tooltipWidth = 180,
				tooltipArrowHeight = 20,
				tooltipArrowWidth = 16,

				// subtract margins from viewBox values to ensure graph is drawn within viewbox bounds
				xAxisLength = viewBoxWidth - graphMargins.left - graphMargins.right,
				yAxisLength = viewBoxHeight - graphMargins.top - graphMargins.bottom,

				xScale = d3.scale.ordinal()
						.rangeRoundBands([0, xAxisLength], 0.50) // second param is width of the bars
						.domain(datapoints.map(function (d) { return d.date; })), // this should be the x axis label

				yScale = d3.scale.linear()
						.rangeRound([yAxisLength, 0]);

				// setup a color map, we map the colors to each category in the legend (year ranges)
				var color = d3.scale.ordinal()
						//						.range(["#0097a7", "#ff6d00", "#ffc30f", "#6b486b"])
						.domain(d3.keys(datapoints[0]).filter(function(key) { return key !== "date"; }));
				var colorRange = [];
				for (var i = 0; i < deviceNames.length; i++) {
					colorRange.push(this.colorMap[deviceNames[i]]);
				}
				color.range(colorRange);

				// create the stackVal and totalStackVal properties to our data
				datapoints.forEach(function (d) {
					var y0 = 0;
					// stackVal is the color corresponding with its size in the total stack
					d.stackVal = color.domain().map(function(displayName) { return {displayName: displayName, y0: y0, y1: y0 += +d[displayName], date: d.date};});
					// totalStackVal is total value for the stack
					if (d.stackVal.length > 0) {
						d.totalStackVal = d.stackVal[d.stackVal.length - 1].y1;
					} else {
						d.totalStackVal = 0;
					}
				});
				// set up the ticks for the y axis
				yScale.domain([0, d3.max(datapoints, function(d) { return d.totalStackVal; })]);

				// Draw graph area with x and y axes
				var svg = this._createSvg(viewBoxWidth, viewBoxHeight, graphMargins, xScale, yScale, xAxisLength, yAxisLength);
				// Draw the colored bars that display the data
				this._drawVerticalBars(svg, datapoints, xScale, yScale, color, tooltipHeight, tooltipWidth, tooltipArrowWidth, tooltipArrowHeight);
				// Position and hide tooltips for each bar
				this._drawTooltips(svg, datapoints, tooltipWidth, tooltipHeight, tooltipArrowWidth, tooltipArrowHeight, xScale);
				this._resizeSvg();
			},
			_resizeSvg : function() {
				//Resizes container when width/height changes
				var chart = $("svg"),
						aspect = chart.width() / chart.height(),
						//aspect = chart.width() / chart.height(),
						container = chart.parent();
				$(window).on("resize", function () {
					// Adjust graph width/height based on initial ratio.  also adjust graph height as graph grows
					var targetWidth = container.width();
					chart.attr("width", targetWidth);
					chart.attr("height", (targetWidth / aspect));
					$(".graph-content").css("height", (targetWidth / aspect + 60) + "px");
				}).trigger("resize");
			},
			_createSvg: function(viewBoxWidth, viewBoxHeight, margin, xScale, yScale, xAxisLength, yAxisLength) {
				// declare xAxis with settings
				var xAxis = d3.svg.axis()
						.scale(xScale)
						.orient("bottom")
						.tickSize(0)
						.outerTickSize(0);

				// declare yAxis with settings
				var yAxis = d3.svg.axis()
						.scale(yScale)
						.orient("left")
						.ticks(6)
						.tickSize(0)
						.outerTickSize(0);

				// Set up the x and y gridlines
				var gridDomain = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; // placeholders so there are 13 lines drawn
				var yGridLinesScale = d3.scale.ordinal()
						.rangeRoundBands([0, yAxisLength])
						.domain(gridDomain);
				var yGridLinesAxis = d3.svg.axis()
						.scale(yGridLinesScale)
						.orient("left")
						.innerTickSize(-yAxisLength)
						.outerTickSize(0);

				var xGridLinesScale = d3.scale.ordinal()
						.rangeRoundBands([0, xAxisLength])
						.domain(gridDomain);
				var xGridLinesAxis = d3.svg.axis()
						.scale(xGridLinesScale)
						.orient("bottom")
						.innerTickSize(-xAxisLength)
						.outerTickSize(0);

				d3.select(window).on('resize', this._resizeSvg);

				// responsive SVG needs viewBox and preserveAspectRatio with no width and height attr
				// note : small text may become hard to read
				var svg = d3.select(this.ui.graphContent[0]).append("svg")
						.attr("width", viewBoxWidth)
						.attr("height", viewBoxHeight)
						.attr("viewBox", "0 0 " + viewBoxWidth + " " + viewBoxHeight)
						// svg-content has resizing styles
						.attr("class", "svg-content")
						.append("g")
						.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

				// does the gray background
				svg.append("rect")
						.attr("width", "100%")
						.attr("height", "100%")
						.attr("fill", "#f5f7f8")
						.attr("transform", "translate(" + -margin.left + "," + -margin.top + ")");

				// append the x and y gridline groups to the svg
				svg.append("g")
						.attr("class", "y grid-lines")
						.call(yGridLinesAxis)
						.append("text")
						.attr("transform", "rotate(-90)")
						.attr("x", -(viewBoxHeight / 3))
						.attr("y", -55);

				svg.append("g")
						.attr("class", "x grid-lines")
						.attr("transform", "translate(0," + yAxisLength + ")")
						.call(xGridLinesAxis)
						.attr("y", 50)
						.attr("x", viewBoxWidth / 2.5);

				// set up x and y axes
				svg.append("g")
						.attr("class", "x axis")
						.attr("transform", "translate(0," + yAxisLength + ")")
						.call(xAxis)
						.append("text")
						.attr("y", 50)
						.attr("x", viewBoxWidth / 2.5)
						.style("text-anchor", "end")
						.text(App.translate("equipment.stackedBarGraph.time"));

				svg.append("g")
						.attr("class", "y axis")
						.call(yAxis)
						.append("text")
						.attr("transform", "rotate(-90)")
						.attr("x", -(viewBoxHeight / 3))
						.attr("y", -55)
						.style("text-anchor", "end")
                        .text("kWh");
						//.text(App.translate("equipment.stackedBarGraph.watts"));

				return svg;
			},
			_drawVerticalBars: function (svg, datapoints, xScale, yScale, color, tooltipHeight, tooltipWidth, tooltipArrowWidth, tooltipArrowHeight) {
				var that = this;
				// for each date, create a g tag to hold graph bars
				var date = svg.selectAll(".date")
						.data(datapoints)
						.enter().append("g")
						.attr("class", "g")
						.attr("transform", function(d) { return "translate(" + xScale(d.date) + ",0)"; });

				// rect represents each group of bars for a date
				date.selectAll("rect")
						.data(function(d) { return d.stackVal; })
						.enter().append("rect")
						.attr("y", function(d) { return yScale(d.y1)-1; }) //-1 cause axis has 2px stroke
						.attr("width", xScale.rangeBand())
						.attr("height", function(d) { return yScale(d.y0) - yScale(d.y1); })
						.attr("cursor", "pointer")
						.style("fill", function(d) { return color(d.displayName); })
						.on("click", function(d) {
							//prevent two tooltips from being open at once
							svg.selectAll(".tooltip-wrapper")
									.transition()
									.duration(500)
									.style("opacity", "0")
									.style("display", "none");
							//show the tooltip for the clicked section
							svg.selectAll(".date-" + utils.stripNonAlphanumericChars(d.date))
									.transition()
									.duration(500)
									.style("opacity", "1")
									.style("display", "block");
							//set the position for the rect, text, and arrow
							var yPos = ((yScale((d.y0)) + yScale((d.y1))) / 2) - tooltipHeight - tooltipArrowHeight;
							svg.select(".date-" + utils.stripNonAlphanumericChars(d.date) + " rect").attr("y", yPos);
							svg.selectAll(".date-" + utils.stripNonAlphanumericChars(d.date) + " text").attr("y", yPos);
							svg.select(".date-" + utils.stripNonAlphanumericChars(d.date) + " path")
									.attr("transform", function() { return "translate(" + (tooltipWidth/2 - tooltipArrowWidth/2) + ", " + (yPos+tooltipHeight-1) + ")"; });
							//set the text to be displayed on the tooltip
							svg.selectAll(".date-" + utils.stripNonAlphanumericChars(d.date) + " .title-text").text(that._setTooltipText(d.displayName, 30));
							svg.selectAll(".date-" + utils.stripNonAlphanumericChars(d.date) + " .date-text").text(that._setTooltipText(d.date, 16));
							svg.selectAll(".date-" + utils.stripNonAlphanumericChars(d.date) + " .watts-text").text(that._setTooltipText((d.y1 - d.y0).toFixed(2) +  " kWh", 30));
						});
			},
			//For each date, add a hidden tooltip.  g tag contains a rect, a path, and 3 text tags
			_drawTooltips: function (svg, datapoints, tooltipWidth, tooltipHeight, tooltipArrowWidth, tooltipArrowHeight, xScale) {
				//Drop shadow for tooltip
				var defs = svg.append("defs");
				var filter = defs.append("filter")
						.attr("id", "drop-shadow")
						.attr("height", "200%")
						.attr("width", "300%");
				filter.append("feGaussianBlur")
						.attr("in", "SourceAlpha")
						.attr("stdDeviation", 5)
						.attr("result", "blur");
				filter.append("feOffset")
						.attr("in", "blur")
						.attr("dx", 4)
						.attr("dy", 4)
						.attr("result", "offsetBlur");
				var feMerge = filter.append("feMerge");
				feMerge.append("feMergeNode")
						.attr("in", "offsetBlur");
				feMerge.append("feMergeNode")
						.attr("in", "SourceGraphic");

				var tooltipWrappers = svg.selectAll(".date")
						.data(datapoints)
						.enter().append("g")
						.attr("class", function(d) { return "tooltip-wrapper date-" + utils.stripNonAlphanumericChars(d.date);}) //custom class based on date to find unique tooltips
						.attr("transform", function(d) { return "translate(" + (xScale(d.date) - (tooltipWidth/2) + (xScale.rangeBand()/2)) + ",0)"; })
						.style("opacity", "0")
						.style("display", "none")
						.on("click", function(d) {
							//If the tooltip is clicked hide it
							svg.selectAll(".date-" + utils.stripNonAlphanumericChars(d.date))
									.transition()
									.duration(500)
									.style("opacity", "0")
									.style("display", "none");
						});

				tooltipWrappers.append("rect")
						.attr("width", tooltipWidth)
						.attr("height", tooltipHeight)
						.attr("ry", 3)
						.attr("rx", 3)
						.attr("stroke", "white")
						.attr("stroke-width", 0)
						.style("filter", "url(#drop-shadow)")
						.style("fill", "white");

				tooltipWrappers.append('path')
						.attr('d',function() {
							return "M 0 0 L " + tooltipArrowWidth +" 0 L 8 " + tooltipArrowHeight + " z";
						})
						.style("filter", "url(#drop-shadow)")
						.attr("fill", "white");

				tooltipWrappers.append("text")
						.attr("class", "title-text")
						.attr("font-size", 20)
						.attr("font-weight", 600)
						.attr("transform", function() { return "translate(20, 30)"; });

				tooltipWrappers.append("text")
						.attr("class", "date-text")
						.attr("font-size", 16)
						.attr("transform", function() { return "translate(20, 50)"; });

				tooltipWrappers.append("text")
						.attr("class", "watts-text")
						.attr("font-size", 20)
						.attr("transform", function() { return "translate(20, 80)"; });
			},
			_handleInteractiveModeClick: function () {
				this._notImplementedAlert();
			},

			drawGraph: function (datapoints, devicesToDisplay) {
				if ($(".svg-content").length) {
					var parent = this.$("#bb-graph")[0];
					var child = this.$(".svg-content")[0];
					parent.removeChild(child);
				}

				this.model.set("currentDeviceNames", devicesToDisplay);
                
				this.model.set("currentDatapoints", datapoints);

				var filtered = this._getFilteredDatapoints(this.model.get("currentDatapoints"), this.model.get("currentDeviceNames"));

				this._createStackedBarGraph(filtered, this.model.get("currentDeviceNames"));
			},
			_getFilteredDatapoints: function (theData, checkedDevices) {
				return _.map(theData, function (datapoint) {
					var filteredData = {};
					for (var i = 0; i < checkedDevices.length; i++) {
						filteredData[checkedDevices[i]] = datapoint[checkedDevices[i]];
					}

					filteredData.date = datapoint.date;

					return filteredData;
				});
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.StackedBarGraphWidget;
});