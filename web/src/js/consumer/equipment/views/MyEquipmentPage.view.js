"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusTabbedSection.view",
	"consumer/equipment/views/myEquipment/CategoriesTab.view",
	"consumer/equipment/views/myEquipment/GroupsTab.view"
], function (App, templates, SalusPageMixin, SalusTabbedSection) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.MyEquipmentPageView = Mn.LayoutView.extend({
			id: "bb-my-equipment-page",
			className: "my-equipment-page container",
			template: templates['equipment/myEquipment'],
			regions: {
				"tabViewRegion": "#bb-tab-view-region"
			},
			initialize: function () {
				this.groupsView = new Views.GroupsTabView();
				this.categoriesView = new Views.CategoriesTabView();
			},
			onRender: function () {
				var options = {
					className: "nav-tabs-centered",
					tabs: [
						{
							key: "bb-groups-tab",
							classes: "groups-tab",
							i18n: "equipment.myEquipment.groups.title",
							view: this.groupsView
						},
						{
							key: "bb-categories-tab",
							classes: "categories-tab",
							i18n: "equipment.myEquipment.categories.title",
							view: this.categoriesView
						}
					]
				};

				this.tabViewRegion.show(new SalusTabbedSection(options));
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "equipment",
			analyticsPage: "myEquipment"
		});
	});

	return App.Consumer.Equipment.Views.MyEquipmentPageView;
});