"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/equipment/views/EquipmentCollection.view",
	"common/model/ayla/Group.model",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/FormTextInput.view",
	"consumer/views/mixins/mixin.validation",
	"consumer/views/SalusModal.view",
	"consumer/views/SalusGroupPin.view",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/SalusAlertModal.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/models/SalusAlertModalModel"
], function (App, P, consumerTemplates, SalusPageMixin) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.NewGroupPageView = Mn.LayoutView.extend({
			id: "bb-new-group-page",
			className: "new-group-page container",
			template: consumerTemplates["equipment/myEquipment/newGroupPage"],
			regions: {
				groupNameTextbox: ".bb-group-name-textbox",
				equipmentRegion: ".bb-equipment-available",
				finishButtonRegion: ".bb-finish-button",
				cancelButtonRegion: ".bb-cancel-button",
				deleteButtonRegion: ".bb-delete-group",
				pinButtonRegion: ".bb-pinops-group"
			},
			initialize: function () {
				_.bindAll(this, "_createGroup", "handleFinishClick", "handleCancelClick", "handleDeleteClick");

				this.isEdit = this.options.isEdit || false;
				this.model = null;
			},
			onRender: function () {
				var that = this,
						key = !!this.isEdit ? parseInt(that.options.key) : undefined,
						name = "";

				App.salusConnector.getDataLoadPromise(["devices", "groups"]).then(function (/*arrayOfData*/) {
					var devices = App.salusConnector.getDevicesWithoutGroup(), isPinned = false;
					if (!!that.isEdit) {
						if(_.isNumber(key)) {
							that.model = App.salusConnector.getGroup(key);
							name = that.model ? that.model.get("name") : "";
						}

						var devicesIdList = that.model.get("devices");
						var allDevices = App.salusConnector.getUserDeviceCollection();
						var devicesOfThisGroup = allDevices.filter(function(item) {
							if(devicesIdList.indexOf(item.get("key")) >= 0) {
								return true;
							}
						});
						var concernedDevices = devices.clone();
						concernedDevices.add(devicesOfThisGroup);
						devices = concernedDevices;
					}

					that.groupNameTextbox.show(new App.Consumer.Views.FormTextInput({
						labelText: "equipment.myEquipment.newGroup.newGroupText",
						value: name
					}));

					that.equipmentRegion.show(new Views.EquipmentCollectionView({
						collection: devices,
						groupId: that.model ? that.model.get("key") : null,
						deviceType: "all"
					}));

					if (that.isEdit) {
						that.deleteButtonRegion.show(new App.Consumer.Views.SalusLinkButtonView({
							buttonText: App.translate("equipment.myEquipment.newGroup.delete.linkText"),
							cssClass: "margin-l-10",
							clickedDelegate: that.handleDeleteClick
						}));
						try {
							isPinned = !!App.salusConnector.getSessionUser().get("tileOrderCollection").toJSON()[App.getCurrentGatewayDSN()].find(function(item) {
								return item.get("referenceId") === that.model.get("key");
							});
						} catch(e) {
							console.log(e, "unable to get pin data");
						}
					}
					that.pinButtonRegion.show(new App.Consumer.Views.GroupPin({isPinned: isPinned}));

					that.finishButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
						classes: "width100",
						buttonTextKey: "common.labels.finished",
						clickedDelegate: that.handleFinishClick
					}));

					that.cancelButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
						className: "btn btn-default width100",
						buttonTextKey: "common.labels.cancel",
						clickedDelegate: that.handleCancelClick
					}));
				});
			},
			handleFinishClick: function () {
				var that = this, pinStatus;

				this.groupNameTextbox.currentView.hideErrors();
				this.finishButtonRegion.currentView.showSpinner();
                
                var groupCollection = App.salusConnector.getGroupCollection(), that = this,
                    groupName = this.groupNameTextbox.currentView.getValue();
                var valid = _.some(groupCollection.models, function(model) {
                    return model.get("name") === groupName && 
                            model.get("key") !== (that.model ? that.model.get("key") : null);
                });
                if(valid) {
                    this.groupNameTextbox.currentView.showErrors(App.translate("equipment.error.renameError"));
                    this.finishButtonRegion.currentView.hideSpinner();
                    return false;
                }

                
				if (!this.isEdit) {
					this._createGroup().then(function () {
						if(pinStatus = that._savePin()) {
							pinStatus.then(function() {
								App.salusConnector.getFullGroupCollection().refresh().then(function () {
									that.finishButtonRegion.currentView.hideSpinner();

									App.navigate("equipment/groups/" + that.model.get("key"));
								});
							});
						} else {
							App.salusConnector.getFullGroupCollection().refresh().then(function () {
								that.finishButtonRegion.currentView.hideSpinner();

								App.navigate("equipment/groups/" + that.model.get("key"));
							});
						}
					}).catch(function (err) {
						console.log("error:::", err);
						that.finishButtonRegion.currentView.hideSpinner();

						if (_.isString(err)) {
							if (err.indexOf("at least one") > -1) {
								that.groupNameTextbox.currentView.showErrors("equipment.myEquipment.newGroup.deviceError");
							} else if (err.indexOf("valid name") > -1) {
								that.groupNameTextbox.currentView.showErrors("equipment.myEquipment.newGroup.nameError");
							}
						} else {
							// general error
							that.groupNameTextbox.currentView.showErrors("equipment.myEquipment.newGroup.createError");
						}
					});
				} else {
					this._updateGroup().then(function () {
						App.salusConnector.getFullGroupCollection().refresh().then(function () {
							that.finishButtonRegion.currentView.hideSpinner();

							App.navigate("equipment/groups/" + that.model.get("key"));
						});
					}).catch(function (err) {
						that.finishButtonRegion.currentView.hideSpinner();

						if (_.isString(err)) {
							if (err.indexOf("at least one") > -1) {
								that.groupNameTextbox.currentView.showErrors("equipment.myEquipment.newGroup.deviceError");
							} else if (err.indexOf("valid name") > -1) {
								that.groupNameTextbox.currentView.showErrors("equipment.myEquipment.newGroup.nameError");
							}
						} else {
							// general error
							that.groupNameTextbox.currentView.showErrors("equipment.myEquipment.newGroup.updateError");
						}
					});
				}
			},
			handleCancelClick: function () {
				window.history.back();
			},
			handleDeleteClick: function () {
				var that = this;

				App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
					model: new App.Consumer.Models.AlertModalViewModel({
						iconClass: "icon-warning",
						primaryLabelText: App.translate("equipment.myEquipment.newGroup.delete.modalLabel") + " " + that.model.get("name"),
						secondaryLabelText: App.translate("equipment.myEquipment.newGroup.delete.warning"),
						rightButton: new App.Consumer.Views.SalusButtonPrimaryView({
							classes: "btn-danger width100",
							buttonTextKey: "common.labels.delete",
							clickedDelegate: function () {
								var self = this; // this is button, that is still new group page view

								this.showSpinner();

								that.model.unregister().then(function () {
									that.model.destroy();
									self.hideSpinner();
									App.hideModal();
									App.navigate("equipment");
								}).catch(function (err) {
									self.hideSpinner();

									return P.reject(err);
								});
							}
						}),
						leftButton: new App.Consumer.Views.ModalCloseButton({
							classes: "width100",
							buttonTextKey: "common.labels.cancel"
						})
					})
				}));

				App.showModal();
			},
			_createGroup: function () {
				var deviceKeys = this.equipmentRegion.currentView.getCheckedItemKeys(),
						groupName = this.groupNameTextbox.currentView.getValue();

				if (deviceKeys && deviceKeys.length === 0) {
					return P.reject("Need at least one device for groups");
				}

				if (!_.isString(groupName) || (_.isString(groupName) && groupName.length === 0)) {
					return P.reject("Not valid name");
				}

				this.model = new App.Models.GroupModel({
					name: groupName,
					devices: deviceKeys
				});
                
				return this.model.add();
			},
			_updateGroup: function () {
				var that = this,
						promiseArray = [],
						groupCurrentKeys = this.model.get("devices"),
						currentDeviceCount = groupCurrentKeys.length,
						selectedKeys = this.equipmentRegion.currentView.getCheckedItemKeys(),
						groupName = this.groupNameTextbox.currentView.getValue(),
						// keys present in selected but not in current group should be added
						addThese = _.difference(selectedKeys, groupCurrentKeys),
						// keys present in the current group but are not selected should be removed
						removeThese = _.difference(groupCurrentKeys, selectedKeys),
						pinStatus;

				if (!_.isString(groupName) || (_.isString(groupName) && groupName.length === 0)) {
					return P.reject("Not valid name");
				}

				// make sure still at least 1 device
				if (currentDeviceCount + addThese.length - removeThese.length === 0) {
					this.handleDeleteClick();
					return P.reject("Would Delete");
				}

				if (_.isString(groupName) && groupName !== this.model.get("name") && groupName.length > 0) {
					promiseArray.push(this.model.updateName(groupName));
				}

				if(pinStatus = that._savePin()) {
					promiseArray.push(pinStatus);
				}

				_.each(addThese, function (key) {
					promiseArray.push(that.model.addDevice(key));
				});

				_.each(removeThese, function (key) {
					promiseArray.push(that.model.removeDevice(key));
				});

				return P.all(promiseArray);
			},
			_savePin: function() {
				if(this.pinButtonRegion.currentView.hasChanged()) {
					if(this.pinButtonRegion.currentView.getPinStatus() === true) {
						return App.salusConnector.pinToDashboard(this.model, "group");
					} else {
						return App.salusConnector.unpinFromDashboard(this.model, "group");
					}
				} else {
					return App.salusConnector.pinToDashboard(this.model, "group");
				}
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "equipment",
			analyticsPage: "newGroup"
		});
	});

	return App.Consumer.Equipment.Views.NewGroupPageView;
});