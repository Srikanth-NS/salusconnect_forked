"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs",
	"common/util/utilities"
], function (App, config, constants, P, consumerTemplates, SalusView, VendorfyCssForJs, utilities) {

	// note that this is separate from the dashboard and tiles
	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.GroupView = Mn.ItemView.extend({
			template: consumerTemplates["equipment/myEquipment/groupsTile"],
			className: "group-tile col-xs-6 col-sm-4",
			attributes: {
				"role": "button"
			},
			ui: {
				alert: ".bb-group-alert-icon",
				pin: ".bb-pin-to-dashboard",
				gradientWithIcon: ".bb-group-tile-background"
			},
			events: {
				click: "_handleClick"
			},
			onRender: function () {
				this._setIdleStyleOnTile();
			},
			_setIdleStyleOnTile: function () {
				VendorfyCssForJs.formatBgGradientWithImage(
						this.ui.gradientWithIcon,
						"center 50%/48% 48%",
						"no-repeat",
						App.rootPath("/images/icons/dashboard/icon_groups.svg"),
						"145deg",
						constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
						"0%",
						constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
						"100%"
				);
			},
			/**
			 * TODO Wire this up to actual alerts when those occur
			 * @private
			 */
			_setAlertStyleOnTile: function () {
				VendorfyCssForJs.formatBgGradientWithImage(
						this.ui.gradientWithIcon,
						"center 50%/48% 48%",
						"no-repeat",
						App.rootPath("/images/icons/dashboard/icon_groups.svg"),
						"145deg",
						constants.tileGradients.TILE_RED_GRADIENT_TOP,
						"0%",
						constants.tileGradients.TILE_RED_GRADIENT_BOTTOM,
						"100%"
				);
			},
			_handleClick: function (/*evt*/) {
				// todo: check click was on pin, pin group to dashboard
				this._goToGroupPage(this.model);
			},
			_goToGroupPage: function (group) {
				App.navigate("equipment/groups/" + group.get("key"));
			}
		}).mixin([SalusView]);

		Views.UngroupedDeviceView = Mn.ItemView.extend({
			template: consumerTemplates["equipment/myEquipment/groupsDeviceTile"],
			className: "ungrouped-equipment-tile col-xs-3 col-sm-2",
			attributes: {
				"role": "button"
			},
			bindings: {
				".bb-ungrouped-equip-text": {
					observe: "name",
					onGet: function (name) {
						return utilities.shortenText(name, config.tiles.ungroupedEquipment.titleMaxLength);
					}
				},
				".bb-pin-to-dashboard": {
					observe: "isInDashboard",
					update: function ($el, val, model) {
                        var deviceType = model.modelType;
                        if(deviceType === constants.modelTypes.GATEWAY || deviceType === constants.modelTypes.IT600THERMOSTAT ||
                                deviceType === constants.modelTypes.WATERHEATER || deviceType === constants.modelTypes.SMARTPLUG ||
                                deviceType === constants.modelTypes.DOORMONITOR || deviceType === constants.modelTypes.WINDOWMONITOR ||
                                deviceType === constants.modelTypes.ENERGYMETER || deviceType === constants.modelTypes.THERMOSTAT ||
                                deviceType === constants.modelTypes.MINISMARTPLUG){
                                    var modelVal = model.get("isInDashboard");
                                    if (modelVal === null || _.isUndefined(modelVal)) {
                                        $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                                    } else if (modelVal === false) {
                                        $el.removeClass("hidden").removeClass("pin-icon").addClass("unpin-icon");
                                    } else if (modelVal === true) {
                                        $el.removeClass("hidden").removeClass("unpin-icon").addClass("pin-icon");
                                    }
                        }
                        else{
                            $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                        }
                        
//						var modelVal = model.get("isInDashboard");
//
//						if(modelVal === null || _.isUndefined(modelVal)) {
//							$el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
//						} else if (modelVal === false) {
//							$el.removeClass("hidden").removeClass("pin-icon").addClass("unpin-icon");
//						} else if (modelVal === true) {
//							$el.removeClass("hidden").removeClass("unpin-icon").addClass("pin-icon");
//						}
					}
				}
			},
			ui: {
				icon: ".bb-ungrouped-equip-icon",
				pin: ".bb-pin-to-dashboard"
			},
			events: {
				"click": "_handleClick"
			},
			onRender: function () {
				var icon = this.model.get("device_category_icon_url");

				// interpret temp
				if (this.model.modelType === constants.modelTypes.THERMOSTAT && this.model.get("SystemMode")) {
					icon = this._getThermostatIcon();
				}

				// TODO: water heater mode as well.

				this.ui.icon.css("background-image", "url(" + icon + ")");
			},
			_handleClick: function (event) {
				var $eventElement = $(event.target);
				
				if ($eventElement.hasClass("bb-pin-to-dashboard")) {
					this._pinUnpinDeviceToDashboard();
				} else {
					this._goToEquipmentPage(this.model);
				}				
			},
			_goToEquipmentPage: function (equipment) {
				App.navigate("equipment/myEquipment/" + equipment.get("dsn"));
			},
			_getThermostatIcon: function () {
				var mode = this.model.getPropertyValue("SystemMode");

				// TODO: thermostat has temp also

				switch (mode) {
					case constants.thermostatModeTypes.EMERGENCYHEATING:
					case constants.thermostatModeTypes.HEAT:
						return App.rootPath("/images/icons/dashboard/icon_temp_heat.svg");
					case constants.thermostatModeTypes.AUTO:
						return App.rootPath("/images/icons/dashboard/icon_temp_off.svg");
					case constants.thermostatModeTypes.COOL:
						return App.rootPath("/images/icons/dashboard/icon_temp_cool.svg");
					case constants.thermostatModeTypes.OFF:
						return App.rootPath("/images/icons/dashboard/icon_temp_off.svg");
					default:
						return App.rootPath("/images/icons/dashboard/icon_temp_idle.svg");
				}
			},
			_pinUnpinDeviceToDashboard: function () {
				if (this.model.get("isInDashboard")) {
					App.salusConnector.unpinFromDashboard(this.model, "device");
				} else {
					App.salusConnector.pinToDashboard(this.model, "device");
				}
			}
		}).mixin([SalusView]);

		// TODO: pull this out to it's own file and make generic name for groups+categories
		Views.GroupedDeviceView = Mn.ItemView.extend({
			template: consumerTemplates["equipment/myEquipment/groupedDevice"],
			className: "grouped-tile col-xs-6 col-sm-4",
			attributes: {
				role: "button"
			},
			bindings: {
				".bb-device-name": {
					observe: "name"
				},
				".bb-pin-to-dashboard": {
					observe: "isInDashboard",
					update: function ($el, val, model) {
                        var deviceType = model.modelType;
                        if(deviceType === constants.modelTypes.GATEWAY || deviceType === constants.modelTypes.IT600THERMOSTAT ||
                                deviceType === constants.modelTypes.WATERHEATER || deviceType === constants.modelTypes.SMARTPLUG ||
                                deviceType === constants.modelTypes.DOORMONITOR || deviceType === constants.modelTypes.WINDOWMONITOR ||
                                deviceType === constants.modelTypes.ENERGYMETER || deviceType === constants.modelTypes.THERMOSTAT ||
                                deviceType === constants.modelTypes.MINISMARTPLUG){
                                    var modelVal = model.get("isInDashboard");
                                    if (modelVal === null || _.isUndefined(modelVal)) {
                                        $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                                    } else if (modelVal === false) {
                                        $el.removeClass("hidden").removeClass("pin-icon").addClass("unpin-icon");
                                    } else if (modelVal === true) {
                                        $el.removeClass("hidden").removeClass("unpin-icon").addClass("pin-icon");
                                    }
                        }
                        else{
                            $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                        }
                        
//						var modelVal = model.get("isInDashboard");
//
//						if(modelVal === null || _.isUndefined(modelVal)) {
//							$el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
//						} else if (modelVal === false) {
//							$el.removeClass("hidden").removeClass("pin-icon").addClass("unpin-icon");
//						} else if (modelVal === true) {
//							$el.removeClass("hidden").removeClass("unpin-icon").addClass("pin-icon");
//						}
					}
				}
			},
			ui: {
				alert: ".bb-group-alert-icon",
				pin: ".bb-pin-to-dashboard",
				gradientWithIcon: ".bb-group-tile-background"
			},
			events: {
				click: "_handleClick"
			},
			onRender: function () {
				this._setIdleStyleOnTile();
			},
			_setIdleStyleOnTile: function () {
				VendorfyCssForJs.formatBgGradientWithImage(
						this.ui.gradientWithIcon,
						"center center / 48% 48%",
						"no-repeat",
						this.model.get("device_category_icon_url"),
						"145deg",
						constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
						"0%",
						constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
						"100%"
				);
			},
			/**
			 * TODO Wire this up to actual alerts when those occur
			 * @private
			 */
			_setAlertStyleOnTile: function () {
				VendorfyCssForJs.formatBgGradientWithImage(
						this.ui.gradientWithIcon,
						"center 50%/48% 48%",
						"no-repeat",
						this.model.get("device_category_icon_url"),
						"145deg",
						constants.tileGradients.TILE_RED_GRADIENT_TOP,
						"0%",
						constants.tileGradients.TILE_RED_GRADIENT_BOTTOM,
						"100%"
				);
			},
			_handleClick: function (event) {
				var $eventElement = $(event.target);

				if ($eventElement.hasClass("bb-pin-to-dashboard")) {
					this._pinUnpinDeviceToDashboard();
				} else {
					this._goToEquipmentPage(this.model);
				}
			},
			_goToEquipmentPage: function (equipment) {
				App.navigate("equipment/myEquipment/" + equipment.get("dsn"));
			},
			_pinUnpinDeviceToDashboard: function () {
				if (this.model.get("isInDashboard")) {
					App.salusConnector.unpinFromDashboard(this.model, "device");
				} else {
					App.salusConnector.pinToDashboard(this.model, "device");
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views;
});
