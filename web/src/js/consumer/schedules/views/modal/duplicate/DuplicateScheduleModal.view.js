"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"common/constants",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/schedules/views/modal/duplicate/CheckboxCollection.view"
], function (App, P, templates, constants) {

	App.module("Consumer.Schedules.Views.Modal.Duplicate", function (Views, App, B, Mn, $, _) {
		Views.DuplicateScheduleModalView = Mn.LayoutView.extend({
			id: "duplicate-schedule-modal",
			className: "duplicate-schedule-modal modal-body",
			template: templates["schedules/modal/duplicate/duplicateScheduleModal"],
			regions: {
				deviceCollectionRegion: ".bb-schedule-region",
				individualRegion: ".bb-individual-region",
				cancelButtonRegion: ".bb-cancel-button",
				duplicateButtonRegion: ".bb-duplicate-button"
			},
			ui: {
			    duplicateMessage:".duplicate-message",
				error: ".bb-duplicate-error"
			},

			initialize: function () {
				_.bindAll(this, "handleCancelClicked", "handleDuplicateClicked");

				var categories = constants.categoryTypes,
					collection,
					that = this;

				switch (this.options.deviceCategory) {
					case categories.THERMOSTATS:

						collection = new B.Collection(App.salusConnector.getTSCCollection().models);
						collection.add(App.salusConnector.getIndividualControlThermostats().models);
						break;
					case categories.WATERHEATERS:
						collection = App.salusConnector.getWaterHeaterCollection();
						break;
					case categories.SMARTPLUGS:
						collection = App.salusConnector.getSmartPlugCollection();
						break;
					default:
						throw new Error("Invalid device category.");
				}

				collection.remove(collection.findWhere({key: this.model.get("key")}));

				this.deviceCollectionView = new App.Consumer.Schedules.Views.Modal.Duplicate.CheckboxCollection({
					collection: collection
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					className: "width100 btn",
					clickedDelegate: this.handleCancelClicked
				});

				this.duplicateButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "schedules.thermostat.duplicateSchedule.duplicate",
					className: "width100 btn btn-primary disabled",
					clickedDelegate: this.handleDuplicateClicked
				});

				this.listenTo(this.deviceCollectionView, "save:duplication", function () {
					that._canDuplicate();
				});
			},

			onRender: function () {

			    switch (this.options.deviceCategory) {
            		case constants.categoryTypes.THERMOSTATS:
            		    this.ui.duplicateMessage.text(App.translate("schedules.thermostat.duplicateSchedule.description"));
            		    break;
            		case constants.categoryTypes.WATERHEATERS:
            		    this.ui.duplicateMessage.text(App.translate("schedules.thermostat.duplicateSchedule.waterHeaterDuplicateSchedule"));
					    break;
   					case constants.categoryTypes.SMARTPLUGS:
   					    this.ui.duplicateMessage.text(App.translate("schedules.thermostat.duplicateSchedule.smartPlugDuplicateSchedule"));
   					    break;
   			    }

				var schedules = this.model.getSchedules();

				this.deviceCollectionRegion.show(this.deviceCollectionView);
				this.cancelButtonRegion.show(this.cancelButton);
				this.duplicateButtonRegion.show(this.duplicateButton);

				if (schedules.isEmpty()) {
					this.ui.error.removeClass("hidden");
				}
				else {
					this.ui.error.addClass("hidden");
				}
			},

			handleCancelClicked: function () {
				App.hideModal();
			},

			_canDuplicate: function () {
				var schedules = this.model.getSchedules();

				if (!schedules.isEmpty() && this.deviceCollectionView.oneIsChecked) {
					this.duplicateButtonRegion.currentView.enable();
				} else {
					this.duplicateButtonRegion.currentView.disable();
				}
			},

			handleDuplicateClicked: function () {
				var that = this,
					selectedModels,
					deepCopyJSON = this.model.getSchedules().deepCopy();

				if (this.individualCollectionView) {
					selectedModels = this.deviceCollectionView.getSelectedModels().concat(this.individualCollectionView.getSelectedModels());
				} else {
					selectedModels = this.deviceCollectionView.getSelectedModels();
				}

				this.duplicateButton.showSpinner();

				return P.all(_.map(selectedModels, function (model) {
					var schedules = model.getSchedules();
					return schedules.setFromCopy(deepCopyJSON);
				})).finally(function () {
					App.hideModal();
					that.duplicateButton.hideSpinner();
					App.Consumer.Schedules.PageEventController.trigger("duplicated:schedule");
					that.destroy();
				});
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView]);
	});

	return App.Consumer.Schedules.Views.Modal.Duplicate.DuplicateScheduleModalView;
});
