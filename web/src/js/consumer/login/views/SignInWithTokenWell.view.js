"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view"
], function (App, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.SignInWithTokenWell = Mn.ItemView.extend({
			className: "sign-in-with-token-well",
			template: consumerTemplates["login/signInWithTokenWell"],
			initialize: function () {
				_.bindAll(this, "handleButtonClicked");

				this.associateDeviceButton = new SalusPrimaryButtonView({
					classes: "associate-device-btn",
					buttonTextKey: "login.signInWithToken.button",
					clickedDelegate: this.handleButtonClicked
				});
			},

			onRender: function () {
				this.$(".bb-associate-device-btn-container").append(this.associateDeviceButton.render().$el);
				this.$(".bb-secure-token").attr("placeholder", App.translate("login.signInWithToken.inputLabel"));

				return this;
			},
			onDestroy: function () {
				this.associateDeviceButton.destroy();
			},
			goToHomePage: function () {
				//TODO: Make this go to home page. For now it goes to index.
				App.navigate("", {trigger: true});
			},
			handleButtonClicked: function () {
				var that = this,
					token = this.$(".bb-secure-token").val();

				if (token) {
					that.goToHomePage(); //Temp:
					//TODO Add salus connector promise calls here
//				var loginPromise = App.salusConnector.loginWithUsername(email, password);
//				loginPromise.then(function (result) {
//					that.goToHomePage();
//				}).catch(function (e) {
//					if (e.status === 401) {
//
//						if (e.responseText.includes("Invalid email or password.")) {
//							alert(App.translate("login.login.error.invalidUsernamePassword"));
//							return;
//						}
//					}
//
//					//TODO: error popups
//					App.log("Failed to login");
//				});
				} else {
					//TODO: error popups
					throw new Error("local validation: missing token");
				}
			}

		}).mixin([SalusViewMixin]);

	});

	return App.Consumer.Login.Views.SignInWithTokenWell;
});
