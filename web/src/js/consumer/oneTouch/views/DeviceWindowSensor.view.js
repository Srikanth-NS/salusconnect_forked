"use strict";

//Dev_SBO-606 start
define([
	"app",
	"bluebird",
    "consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
    "common/constants",
    "common/model/salusWebServices/rules/recommendedRules",
    "common/model/salusWebServices/rules/RuleDetails.model",
],function (App, P, consumerTemplates, SalusPageMixin, constants, ruleMap, Models){
    App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
        Views.DeviceWindowSensor = Mn.LayoutView.extend({
            className: "device-window-sensor-page container",
            template: consumerTemplates["oneTouch/deviceWindowSensor"],
            regions: {
                "windowOpenCheckboxRegion": ".bb-window-open-checkbox",
                "windowCloseCheckboxRegion": ".bb-window-close-checkbox",
                "equipmentListRegion": ".bb-equipment-available",
                "saveButtonRegion": ".bb-save-button",
                "cancelButtonRegion": ".bb-cancel-button"
            },
            initialize: function (options) {
                var that=this,openIsChecked=false,closeIsChecked=false;
                
                _.bindAll(this, "handleFinishClick", "handleCancelClick");
                
                this.device=App.salusConnector.getDeviceByKey(options.key);
                this.rules=[];
                
                var ruleCollection = App.salusConnector.getRuleCollection();

                if (ruleCollection) {
                    ruleCollection.refresh().then(function (){
                        if (ruleCollection.models.length) {
                            that.rules=ruleCollection.filter(function (rule){
                                return rule.get("name").indexOf(constants.hideOneTouchKey + "-" + that.device.getEUID())===0;
                            });

                            // and rule
                            that.andRule=_.find(that.rules,function (rule){
                                if(rule.get("name").indexOf("-AND")!==-1){
                                    return rule;
                                }
                            });
                            
                            //判断lock跟unlock是否钩选
                            _.each(that.rules,function (rule){
                                if(rule.get("name").indexOf("-AND")!==-1 && rule.get("actions").length===2){
                                    openIsChecked=true;
                                }else if(rule.get("name").indexOf("-OR")!==-1 && rule.get("actions").length===2){
                                    closeIsChecked=true;
                                }

                            });
                        }
                        
                        that.windowOpenCheckboxModel=new App.Consumer.Models.CheckboxViewModel({
                            name: "equipmentCheckbox",
                            nonKeyLabel: App.translate("equipment.thermostat.menus.modeLabels.lock"),
                            secondaryIconClass: "",
                            isChecked: openIsChecked
                        });

                        that.windowCloseCheckboxModel=new App.Consumer.Models.CheckboxViewModel({
                            name: "equipmentCheckbox",
                            nonKeyLabel: App.translate("equipment.thermostat.menus.modeLabels.unlock"),
                            secondaryIconClass: "",
                            isChecked: closeIsChecked,
                            disabled: true
                        });

                        that.windowOpenCheckbox=new App.Consumer.Views.CheckboxView({
                            model: that.windowOpenCheckboxModel
                        });

                        that.windowCloseCheckbox=new App.Consumer.Views.CheckboxView({
                            model: that.windowCloseCheckboxModel
                        });

                        that.listenTo(that.windowOpenCheckbox,"clicked:checkbox",function (model){
                            that.windowCloseCheckboxModel.set("isChecked",model.get("isChecked"));
                        });
                        
                    });
                }
                
                

                
            },
            onRender: function (){
                var that=this;
                

                
                var showDevice=App.salusConnector.getWindowSensorCollection();
                showDevice.models=showDevice.filter(function (device){
                    if(device.get("LeaveNetwork") && device.get("OnlineStatus_i") && (device.get("connection_status")!=="Offline")){
                        return device;
                    }
                });
                        
                
                App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
                    that.equipmentListRegion.show(new App.Consumer.Equipment.Views.EquipmentCollectionView({
						collection: showDevice,
                        deviceType: "all",
                        windowSensorRule: that.andRule
					}));
                });
                
                this.windowOpenCheckboxRegion.show(this.windowOpenCheckbox);
                
                this.windowCloseCheckboxRegion.show(this.windowCloseCheckbox);
                
                this.saveButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.save",
					clickedDelegate: that.handleFinishClick
				}));
                
                this.cancelButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: that.handleCancelClick
				}));
            },
            
            handleFinishClick: function(event, button){
                var that=this,windowSensors=[],keys=this.equipmentListRegion.currentView.getCheckedItemKeys(),
                         openRuleModel,closeRuleModel;

                
                if(keys.length){
                    if(this.rules && this.rules.length){
                        //更新
                        // AUTO 需要判断condition及action中的lock是否更改
                        
                        //1.找出新增加的
                        var euids=[];
                        _.each(keys,function(key){
                            var device=App.salusConnector.getDeviceByKey(key)
                            euids.push(device.getEUID());
                            if(!that.rules[0].get("conditions").findWhere({EUID:device.getEUID()})){
                                var omitCondition0=new Models.RuleCondition;
                                omitCondition0.set(that.rules[0].get("conditions").models[0].toJSON());
                                omitCondition0.set("EUID",device.getEUID());
                                that.rules[0].get("conditions").add(omitCondition0);
                                
                                
                                var omitCondition1=new Models.RuleCondition;
                                omitCondition1.set(that.rules[1].get("conditions").models[0].toJSON());
                                omitCondition1.set("EUID",device.getEUID());
                                that.rules[1].get("conditions").add(omitCondition1);
                                
                            }
                        });
                        
                        //2.找出被删除的
                        this.rules[0].get("conditions").reset(this.rules[0].get("conditions").filter(function (condition){
                           if($.inArray(condition.get("EUID"),euids)!==-1){
                               return condition;
                           }
                        }));
                        this.rules[1].get("conditions").reset(this.rules[1].get("conditions").filter(function (condition){
                           if($.inArray(condition.get("EUID"),euids)!==-1){
                               return condition;
                           }
                        }));
                        
                        
                        //3.更新lock跟unlock
                        var rule0Actions=this.rules[0].get("actions").models;
                        var rule1Actions=this.rules[1].get("actions").models;
                        if(this.windowOpenCheckbox.getIsChecked()){
                            if(rule0Actions.length!==2){
                                var newAction=new Models.RuleAction;
                                newAction.set(ruleMap[constants.modelTypes.WINDOWMONITOR][0].actions[1]);
                                newAction.set("EUID",rule0Actions[0].get("EUID"));
                                newAction.set("propName",this.device.getSetterPropNameIfPossible(newAction.get("propName")));
                                this.rules[0].get("actions").add(newAction);
                            }
                            if(rule1Actions.length!==2){
                                var newAction=new Models.RuleAction;
                                newAction.set(ruleMap[constants.modelTypes.WINDOWMONITOR][1].actions[1]);
                                newAction.set("EUID",rule1Actions[0].get("EUID"));
                                newAction.set("propName",this.device.getSetterPropNameIfPossible(newAction.get("propName")));
                                this.rules[1].get("actions").add(newAction);
                            }
                        } else {
                            if(rule0Actions.length===2){
                                this.rules[0].get("actions").remove(rule0Actions[1]);
                            }
                            if(rule1Actions.length===2){
                                this.rules[1].get("actions").remove(rule1Actions[1]);
                            }
                        }
                        this.windowCloseCheckbox.getIsChecked()
                        
                        this.saveButtonRegion.currentView.showSpinner();
                        P.all([this.rules[0].update(),this.rules[1].update()]).then(function (){
                            window.history.back();
                        });
                        
                    }else{
                        //新增
                        _.each(keys,function (key){
                            windowSensors.push(App.salusConnector.getDeviceByKey(key)); 
                        });

                        openRuleModel=new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});
                        closeRuleModel=new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});


                        openRuleModel.buildWindowOpenRule(windowSensors, this.device, this.windowOpenCheckbox.getIsChecked());
                        closeRuleModel.buildWindowCloseRule(windowSensors, this.device, this.windowCloseCheckbox.getIsChecked());

                        this.saveButtonRegion.currentView.showSpinner();
                        P.all([openRuleModel.add(),closeRuleModel.add()]).then(function (){
                            window.history.back();
                        });
                    }
                } else if(this.rules && this.rules.length){
                    this.saveButtonRegion.currentView.showSpinner();
                    P.all([this.rules[0].unregister(),this.rules[1].unregister()]).then(function (){
                        window.history.back();
                    });
                    
                }
                
                
            },
            handleCancelClick: function (){
                window.history.back();
            }
        }).mixin([SalusPageMixin]);
    });
    
    return App.Consumer.OneTouch.Views.DeviceWindowSensor;
});

//Dev_SBO-606 end