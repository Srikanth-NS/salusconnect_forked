"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusLinkButton.view",
	"common/util/VendorfyCssForJs",
	"consumer/views/Breadcrumbs.view",
	"consumer/views/SalusRadio.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/oneTouch/views/OneTouchCriteriaCollection.view",
	"consumer/views/SalusModal.view",
	"consumer/oneTouch/views/DeleteOneTouchModal.view",
	"consumer/myStatus/views/NewMyStatusPage.view"
], function (App, P, constants, consumerTemplates, SalusPage, SalusView, CheckboxView, CheckboxModel, SalusButton, SalusLink, VendorfyCss, BreadcrumbsView) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.OneTouchDetailPage = Mn.LayoutView.extend({
			id: "one-touch-detail-page",
			template: consumerTemplates["oneTouch/oneTouchDetailPage"],
			className: "container",
			ui: {
				pageTitle: "#bb-one-touch-name-page-title",
				enableCheckbox: ".bb-enable-checkbox",
				editLink: ".bb-edit-link",
				deleteLink: ".bb-delete-link",
				runNow: ".bb-run-now",
				ruleCriteriaAnchor: ".bb-rule-criteria",
				pinToDashAnchor: ".bb-pin-to-dash"
			},
			regions: {
				myStatusRegion: ".bb-include-in-myStatus-region",
				breadcrumbs: ".bb-breadcrumbs"
			},
			initialize: function (/*options*/) {
				var that = this;

				_.bindAll(this, "_handleRunNowClick", "_handleEditClick", "_handleDeleteClick");

				this.key = decodeURIComponent(this.options.key);

				this.model = App.salusConnector.getRule(this.key);

				this.checkboxView = new CheckboxView({
					model: new CheckboxModel({
						name: "enable-checkbox",
						isChecked: false, // fill in later
						secondaryIconClass: "",
						labelTextKey: "common.labels.enable"
					})
				});

				this.listenTo(this.checkboxView, "clicked:checkbox", function () {
					if (that.model && that.model.toggleActive) {
						that.model.toggleActive();
					}
				});
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices", "rules"]).then(function (/*arrayOfData*/) {
					// try to setup model again (page refresh)
					that.model = !that.model ? App.salusConnector.getRule(that.key) : that.model;

					if (!that.model) {
						// gateway changed on us
						return App.navigate("oneTouch");
					}                    

					that.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
						crumbs: [
							{
								textKey: "equipment.oneTouch.automations.title",
								href: "/oneTouch"
							},
							{
								text: that.model.get("name"),
								active: true
							}
						]
					}));

					// our new current
					App.Consumer.OneTouch.ruleMakerManager.setCurrentRule(that.model);

					var unparsable = that.model.get("unparsable");

					// set page title
					that.ui.pageTitle.text(that.model.get("name"));
                    
                    // rule criteria
					// first check if the rule is parsable
					// if it's not, we show them a criteria for unparsable
					if (!unparsable) {
						// conditions
						that.conditionsView = new Views.OneTouchCriteriaCollectionView({
							collection: that.model.get("conditions"),
							type: constants.oneTouchMenuTypes.condition,
							readonly: true
						});

						if (that.model.includesRunNow()) {
							that.buttonPressView = new Views.OneTouchCriteriaCollectionView({
								collection: new B.Collection([{type: "buttonPress"}]), // pseudo collection
								type: constants.oneTouchMenuTypes.condition,
								readonly: true
							});
						}

						// actions
						that.actionsView = new Views.OneTouchCriteriaCollectionView({
							collection: that.model.get("actions"),
							type: constants.oneTouchMenuTypes.action,
							readonly: true
						});

						// delayed
						that.delayedActionsView = new Views.OneTouchCriteriaCollectionView({
							collection: that.model.get("delayedActions"),
							type: constants.oneTouchMenuTypes.delayedAction,
							readonly: true
						});

						// appending to anchor
						that.ui.ruleCriteriaAnchor.append(that.conditionsView.render().$el);

						if (that.buttonPressView) {
							that.ui.ruleCriteriaAnchor.append(that.buttonPressView.render().$el);
						}

						that.ui.ruleCriteriaAnchor
								.append(that.actionsView.render().$el)
								.append(that.delayedActionsView.render().$el);
					} else {
						that.unparsableCriteriaView = new Views.OneTouchCriteriaCollectionView({
							collection: new B.Collection([{type: "unparsable"}]),
							type: "unparsable",
							readonly: true
						});

						that.ui.ruleCriteriaAnchor.append(that.unparsableCriteriaView.render().$el);
					}
                    
                    if(that.model.checkExistsInvalidDevice()){
                        that.ui.enableCheckbox.addClass("hidden");
                    }
                    
                    if (!!that.model.uxhint) {
                        that.ui.enableCheckbox.addClass("hidden");
                        that.$(".button-link-row").addClass("hidden");
                        that.ui.pinToDashAnchor.addClass("hidden");
                        that.$("#bb-myStatus-spinner").detach();
                    } else {
                        // setup checkbox
                        that.checkboxView.model.set("isChecked", that.model.get("active"));
                        that.ui.enableCheckbox.append(that.checkboxView.render().$el);

                        // run now button
                        that.runNowBtnView = new SalusButton({
                            buttonTextKey: "equipment.oneTouch.details.runNow",
                            id: "run-now-btn",
                            classes: "min-width-btn",
                            clickedDelegate: that._handleRunNowClick
                        });
                        that.ui.runNow.append(that.runNowBtnView.render().$el);

                        // edit link
                        that.editLinkView = new SalusLink({
                            buttonTextKey: App.translate("common.labels.edit"),
                            clickedDelegate: that._handleEditClick
                        });

                        that.ui.editLink.append(that.editLinkView.render().$el);

                        // delete link
                        that.deleteLinkView = new SalusLink({
                            buttonTextKey: App.translate("common.labels.delete"),
                            clickedDelegate: that._handleDeleteClick
                        });

                        that.ui.deleteLink.append(that.deleteLinkView.render().$el);
                        
                        that.pinToDashView = new Views.PinRuleRadioView({
                            model: that.model,
                            type: "pin"
                        });

                        that.unpinToDashView = new Views.PinRuleRadioView({
                            model: that.model,
                            type: "unpin"
                        });

                        that.ui.pinToDashAnchor
                            .append(that.pinToDashView.render().$el)
                            .append(that.unpinToDashView.render().$el);

                        App.salusConnector.getDataLoadPromise(["ruleGroups"]).then(function (/*arrayOfData*/) {
                            var ruleGroupCollection = App.salusConnector.getRuleGroupCollection();

                            if (ruleGroupCollection && !ruleGroupCollection.isEmpty()) {
                                that.myStatusCollectionView = new Views.IncludeInMyStatusCompositeView({
                                    collection: ruleGroupCollection,
                                    rule: that.model
                                });

                                that.myStatusRegion.show(that.myStatusCollectionView);
                            } else {
                                // remove spinner
                                that.$("#bb-myStatus-spinner").detach();
                            }
                        });
                    }
				});
			},
			onDestroy: function () {
				if (this.checkboxView) {
					this.checkboxView.destroy();
				}

				if (this.runNowBtnView) {
					this.runNowBtnView.destroy();
				}

				if (this.editLinkView) {
					this.editLinkView.destroy();
				}

				if (this.deleteLinkView) {
					this.deleteLinkView.destroy();
				}

				if (this.conditionsView) {
					this.conditionsView.destroy();
				}

				if (this.actionsView) {
					this.actionsView.destroy();
				}

				if (this.delayedActionsView) {
					this.delayedActionsView.destroy();
				}

				if (this.buttonPressView) {
					this.buttonPressView.destroy();
				}

				if (this.unparsableCriteriaView) {
					this.unparsableCriteriaView.destroy();
				}

				if (this.pinToDashView) {
					this.pinToDashView.destroy();
				}

				if (this.unpinToDashView) {
					this.unpinToDashView.destroy();
				}
			},
			_handleRunNowClick: function () {
				if (!this.model.includesRunNow()) {
					return false;
				}

				var that = this,
						triggerRuleProp,
						gatewayNode = App.getCurrentGateway().getGatewayNode();

				if (gatewayNode) {
					this.runNowBtnView.showSpinner();

					gatewayNode.getDevicePropertiesPromise().then(function () {
						if ((triggerRuleProp = gatewayNode.get("TriggerRule"))) {
							that.listenToOnce(triggerRuleProp, "propertiesSynced", function () {
								that.runNowBtnView.hideSpinner();
							});

							that.listenToOnce(triggerRuleProp, "propertiesFailedSync", function () {
								that.runNowBtnView.hideSpinner();
							});

							return App.salusConnector.triggerRule(that.model);
						} else {
							that.runNowBtnView.hideSpinner();
						}
					}).catch(function (err) {
						that.runNowBtnView.hideSpinner();

						return P.reject(err);
					});
				}
			},
			_handleEditClick: function () {
				var uriKey = encodeURIComponent(this.key);

				App.navigate("oneTouch/" + App.getCurrentGatewayDSN() + "/" + uriKey + "/edit");
			},
			_handleDeleteClick: function () {
				App.modalRegion.show(new App.Consumer.Views.SalusModalView({
					contentView: new Views.DeleteOneTouchModalView({
						model: this.model
					})
				}));

				App.showModal();
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "detail"
		});

		Views.IncludeInMyStatusTile = Mn.ItemView.extend({
			className: "col-xs-3 my-status-tile",
			template: consumerTemplates["myStatus/myStatusTile"],
			attributes: {
				role: "button"
			},
			ui: {
				tileBackground: ".bb-myStatus-tile-background",
				selected: ".bb-myStatus-selected"
			},
            triggers: {
                "click": "click:item"
            },
			initialize: function (/*options*/) {
				_.bindAll(this, "setSelectedUI");

				this.rule = this.options.rule;
			},
			onRender: function () {
				var icon = this.model.get("icon"),
					colorObj = constants.getStatusIconBackgroundColor(icon);

				VendorfyCss.formatBgGradientWithImage(
					this.ui.tileBackground,
					"center / 48%",
					"no-repeat",
					App.rootPath(constants.myStatusIconPaths[icon]),
					"145deg",
					colorObj.topColor,
					"0%",
					colorObj.botColor,
					"100%"
				);

				this.setSelectedUI(this.model.hasRule(this.rule));
			},
			setSelectedUI: function (toggleBool) {
				this.ui.selected.toggleClass("selected", toggleBool);
				this.ui.selected.toggleClass("deselected", !toggleBool);
			}
		}).mixin([SalusView]);

		Views.IncludeInMyStatusCompositeView = Mn.CompositeView.extend({
			className: "include-in-myStatus",
			template: consumerTemplates["oneTouch/includeInMyStatusComposite"],
			childViewContainer: ".bb-myStatus-container",
			childView: Views.IncludeInMyStatusTile,
			childViewOptions: function (model) {
				return {
					model: model,
					rule: this.options.rule
				};
			},
            childEvents: {
                "click:item": "_handleItemClick"
            },
            _handleItemClick: function(childView) {
                var that = this;
                var model = childView.model, addRemovePromise = [];
                var collection = this.collection;
                
                _.each(collection.models, function(m) {
                    if(m.hasRule(childView.rule)) {
                        addRemovePromise.push(m.removeRule(childView.rule));
                    } else {
                        if(m.get("key") === model.get("key")) {
                            addRemovePromise.push(model.addRule(childView.rule));
                        }
                    }
                });
				childView.showSpinner();
                return P.all(addRemovePromise).then(function() {
                    _.each(that.children._views, function(view) {
                        view.setSelectedUI(false);
                    });
                    childView.hideSpinner();
					childView.setSelectedUI(model.hasRule(childView.rule));
                });
            }
		}).mixin([SalusView]);

		Views.PinRuleRadioView = Mn.ItemView.extend({
			className: "one-touch-pin-radio col-xs-12",
			template: consumerTemplates["oneTouch/pinRuleRadio"],
			ui: {
				pinImage: ".bb-pin-image",
				pinRadio: ".bb-pin-radio"
			},
			events: {
				"change input[name=pinToDashRadio]": "handleRadioChange"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "handleRadioChange");

				this.isPin = this.options.type === "pin";

				var textKey = this.isPin ? "equipment.oneTouch.create.pin.pinText" : "equipment.oneTouch.create.pin.unpinText",
					isChecked = this.isPin ? this.model.get("isInDashboard") : !this.model.get("isInDashboard");

				this.radioView = new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						name: "pinToDashRadio",
						value: true,
						isChecked: isChecked,
						labelTextKey: textKey
					})
				});
			},
			onRender: function () {
				this.ui.pinImage.toggleClass("pin", this.isPin);
				this.ui.pinRadio.append(this.radioView.render().$el);
			},
			handleRadioChange: function () {
				if (this.isPin) {
					this.model.pinToDashboard();
				} else {
					this.model.unpinFromDashboard();
				}
			},
			onDestroy: function () {
				this.radioView.destroy();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.OneTouchDetailPage;
});