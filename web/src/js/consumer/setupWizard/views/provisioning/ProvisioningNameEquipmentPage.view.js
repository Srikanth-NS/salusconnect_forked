"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "common/config",
    "consumer/consumer.views",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/mixins/mixin.salusPage",
    "consumer/views/SalusButtonPrimary.view",
    "consumer/views/FormTextInput.view",
    "consumer/setupWizard/views/provisioning/ProvisioningNameEquipmentModal.view",
    "consumer/settings/views/EmptyList.view"
], function (App, P, constants, config, ConsumerViews, consumerTemplates, SalusView, SalusPage, SalusButton, FormTextInput, ProvisioningNameEquipmentModalView) {

    App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {
        Views.ProvisioningNameEquipmentPageView = Mn.LayoutView.extend({
            template: consumerTemplates["setupWizard/provisioning/provisioningNameEquipmentPage"],
            id: "bb-provision-name-equipment-page",
            className: "provision-name-equipment-page",
            regions: {
                equipmentRegion: ".bb-equipment-region",
                backButtonRegion: ".bb-back-button",
                nextButtonRegion: ".bb-next-button"
            },
            initialize: function () {
				_.bindAll(this, "_handleBackClick", "_handleNextClick", "showProvisioningNameEquipmentModal");
                

                this.gateway = App.getCurrentGateway();

                if (!this.gateway || this.gateway.pairingDevices.length === 0) {
                    // get out
                    return App.navigate("setup/pairing");
                }

                this.equipmentCompositeView = new Views.ProvisioningNameEquipmentComposite({
                    model: this.gateway
                });

                this.backButtonView = new SalusButton({
                    id: "back-button",
                    className: "min-width-btn btn btn-default",
                    buttonTextKey: "common.labels.back",
                    clickedDelegate: this._handleBackClick
                });

                this.nextButtonView = new SalusButton({
                    id: "next-button",
                    classes: "min-width-btn btn btn-warning",
                    buttonTextKey: "common.labels.next",
                    clickedDelegate: this._handleNextClick
                });
            },
            onRender: function () {
                this.equipmentRegion.show(this.equipmentCompositeView);
                this.backButtonRegion.show(this.backButtonView);
                this.nextButtonRegion.show(this.nextButtonView);
                ////Dev_SCS-3282 start
                this.showProvisioningNameEquipmentModal();
                //Dev_SCS-3282 end
            },
            _handleBackClick: function () {
                window.history.back();
            },
            _handleNextClick: function () {
                this.equipmentCompositeView.saveDeviceNames().then(function () {
                    
                    App.navigate("setup/customize");
                });
            },
            ////Dev_SCS-3282 start
            showProvisioningNameEquipmentModal: function () {
                this.modalView = new ConsumerViews.SalusModalView({
                    contentView: new ProvisioningNameEquipmentModalView({model: this.gateway})
				});
                App.modalRegion.show(this.modalView);
				App.showModal();
                this.modalView.hideClose();

            }
            //Dev_SCS-3282 end
             
            
        }).mixin([SalusPage], {
            analyticsSection: "setupWizard",
            analyticsPage: "provisioningNameEquipment"
        });

        // name the device view
        Views.ProvisioningNameEquipmentItemView = Mn.ItemView.extend({
            template: consumerTemplates["setupWizard/provisioning/provisioningNameEquipmentListItem"],
            className: "provision-device row",
            ui: {
                icon: ".bb-device-icon",
                deviceType: ".bb-device-type",
                nameTextbox: ".bb-name-textbox"
            },
            bindings: {
                ".bb-identify-button": {
                    observe: "SetIndicator",
                    onGet: function (prop) {
                        return prop ? prop.getProperty() : prop;
                    },
                    update: function ($el, val) {
                         if(this.model.modelType === constants.modelTypes.WINDOWMONITOR || 
                                this.model.modelType === constants.modelTypes.DOORMONITOR){
                            $el.addClass("invisible");
                        }
                        else{
                            if (_.isArray(val)) {
                            // eMeter case - if any are, toggle
                                $el.toggleClass("active", _.some(val, function (singleVal) { return !!singleVal; }));
                            } else {
                                $el.toggleClass("active", !!val);
                            }
                        }
                        
                    }
                }
            },
            events: {
                "click .bb-identify-button": "toggleSetIndicator"
            },
            initialize: function () {
                _.bindAll(this, "getName", "toggleSetIndicator");
                this.textboxView = new FormTextInput({
                    value: "",
                    labelText: "setupWizard.provisioning.naming.textbox",
                    className: "bb-name-textbox name-textbox col-xs-12 col-sm-5"
                });

                this.deviceTextKey = constants.deviceTypeMapping(this.model);
            },
            onRender: function () {
                this.ui.nameTextbox.replaceWith(this.textboxView.render().$el);
                this.ui.deviceType.text(App.translate(this.deviceTextKey));
                this.ui.icon.css("background-image", "url(" + this.model.get("equipment_page_icon_url") + ")");
            },
            getName: function () {
                return this.textboxView.getValue();
            },
            toggleSetIndicator: function () {
                this.model.toggleSetIndicator();
            },
            onDestroy: function () {
                this.model.undoSetIndicator();
                this.textboxView.destroy();
            },
            showError: function() {
                this.textboxView.showErrors(App.translate("equipment.error.renameError"));
            },
            hideError: function() {
                this.textboxView.hideErrors();
            }
            
        }).mixin([SalusView]);

        // equipment container collection
        Views.ProvisioningNameEquipmentComposite = Mn.CollectionView.extend({
            template: false,
            className: "equipment-provision-list",
            childView: Views.ProvisioningNameEquipmentItemView,
            initialize: function () {
                var deviceModels = [];

                _.bindAll(this, "saveDeviceNames");

                // to be filled
                this.collection = new B.Collection(deviceModels);

                // get device models from salusConnector
                deviceModels = _.map(this.model ? this.model.pairingDevices : [], function (deviceObj) {
                    return App.salusConnector.getDevice(deviceObj.dsn);
                });

                // set collection
                this.collection.set(deviceModels);
                
            },
            saveDeviceNames: function () {
                var childViews = this.children._views,
                        promiseArray = [];
                _.each(childViews, function(childView) {
                    childView.hideError();
                });
                var valid = _.some(childViews, function(childView) {
                    return _.some(App.salusConnector.getDeviceCollection().models, function(model) {
                        return  model.get("name") === childView.getName() && model.get("dsn") !== childView.model.get("dsn");
                    });
                });
                
                if(valid || this.isRepeated(childViews)) {
                    _.each(childViews, function(childView) {
                        childView.showError();
                    });
                    return P.reject("Duplicate name");
                }

                // get the text from each child view and save it as the device alias
                _.each(childViews, function (childView) {
                    var alias = childView.getName();
//                    childView.model.set("alias", _.isString(alias) ? alias : "");    
//                    promiseArray.push(childView.model.persist());

                    var finalName = _.isEmpty(alias) ? App.translate(constants.deviceTypeMapping(childView.model)) + " " + childView.model.get("dsn") : alias ;
                    childView.model.set("alias", finalName);
                    childView.model.set("product_name", finalName);
                    childView.model.saveProductName();
                    promiseArray.push(childView.model.persist());
                    
                });

                return P.all(promiseArray);
            },
            
            // to determine if the device name is repeated
            isRepeated: function(views) {
                var hash = {}, isRepeat = false;
                _.each(views, function(view) {
                    if(hash[view.getName()] && !_.isEmpty(view.getName())) {
                        return isRepeat = true;
                    }
                    hash[view.getName()] = true;
                });
                return isRepeat;
            }
           
        }).mixin([SalusView]);
    });

    return App.Consumer.SetupWizard.Provisioning.Views.ProvisioningNameEquipmentPageView;
});