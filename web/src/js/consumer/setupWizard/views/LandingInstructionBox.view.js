"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusButtonPrimary"
], function (App, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn, $, _) {
		Views.SetupLandingInstructionBoxView = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/landingInstructionBox"],
			className: "col-xs-12 instruction-block",
			bindings: {
				".bb-instruction-paragraph": {
					observe: "isActive",
					onGet: "setParagraphText",
                    update: function($el,val){
                        $el.html(val);
                    }
				},
				":el": {
					observe: "isActive",
					update: function ($el, val) {
						$el.toggleClass("active", val);
					}
				}
			},
			initialize: function (options) {
				this.clickedDelegate = options.clickedDelegate;
				_.bindAll(this, "clickedDelegate");
                
                var btnClass = "preserve-case";
                if(this.model.get("isDisable")){
                    btnClass = "disabled preserve-case";
                }

				this.buttonView = new App.Consumer.Views.SalusButtonPrimaryView({
                    classes: btnClass,
					clickedDelegate: this.clickedDelegate,
					buttonTextKey: this.model.get("i18nKey") + ".button"
				});
			},

			setParagraphText: function (value) {
				var ending = value ? ".active" : ".inactive";
				return App.translate(this.model.get("i18nKey") + ending);
			},

			onRender: function () {
				// 2x2 or 1x3 display
				var mdSizeClass = App.isDevOrInt() ? "col-md-6" : "col-md-4";

				if (!this.model.get("isActive")) {
					this.$(".bb-instruction-button-area").append(this.buttonView.render().$el);
				}

				this.$el.addClass(mdSizeClass);
			},
			onDestroy: function () {
				this.buttonView.destroy();
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.SetupWizard.Views.SetupLandingInstructionBoxView;
});