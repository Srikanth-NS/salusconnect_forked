"use strict";

/**
 * The SalusPage is the basic page implementation that should be used by all page level views in the project. It is
 * intended to be mixed-in to the marionette view object and has a few basic responsibilities:
 * - hold boilerplate page level view code
 * - manage analytics integration
 */
define([
	"app",
	"consumer/views/mixins/mixin.salusView",
	"common/util/AnalyticsWrapper"
], function (App, SalusView, AnalyticsWrapper) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.SalusPage = function (options) {
			this.mixin([
				SalusView // by mixing in SalusPage we also mixin SalusView
			], options);
			
			this.before("destroy", function () {
				if (this._controller && _.isFunction(this._controller.cleanPageEventController)) {
					this._controller.cleanPageEventController();
				}
			});

			this.after("onShow", function () {
				if (!!this.scrollTo) {
					// TODO: we set up this listener so that a layoutView's children (regions) finish rendering first.
					// TODO: this is a manual trigger and must be set up if we want to do this in other places.
					this.listenToOnce(this, "show:children", function () {
						var position = $(this.scrollTo).offset().top;
						$("body").animate({scrollTop: position});
					});
				} else {
					// always scroll top
					$("body").scrollTop(0);
				}
			});

			this.after("render", function () {
				this._triggerPageAnalyticsEvent();
			});

			this.setDefaults({
				_analyticsSection: options.analyticsSection,
				_analyticsPage: options.analyticsPage,
				_controller: options.controller,

				_triggerPageAnalyticsEvent: function () {
					AnalyticsWrapper.onPageLoaded(this._analyticsSection, this._analyticsPage);
				},

				setAnalyticsSection: function (sectionName) {
					this._analyticsSection = sectionName;
				},

				setAnalyticsPage: function (pageName) {
					if (!this._analyticsSection) {
						throw new Error("cannot set a section without a page");
					}

					this._analyticsPage = pageName;
				},

				getAnalytics: function () {
					return {section: this._analyticsSection, page: this._analyticsPage};
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusPage;
});
