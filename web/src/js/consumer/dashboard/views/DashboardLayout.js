"use strict";

define([
	"app",
	"common/util/utilities",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/dashboard/views/header/DashboardHeaderLayout.view",
	"consumer/dashboard/views/TileManager.view",
	"slick",
	"jquery.mobile",
	"touch-punch"
], function (App, utils, consumerTemplates, SalusPageMixin, RegisteredRegions) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn, $, _) {
		Views.DashboardPage = Mn.LayoutView.extend({
			template: consumerTemplates["dashboard/dashboard"],
			className: "container",
			regions: {
				header: ".bb-dashboard-header"
			},
			ui: {
				tiles: ".bb-dashboard-tiles",
				innerHeader: ".bb-dashboard-inner-header"
			},
			initialize: function () {
				_.bindAll(this, "_buildTileManagerViews", "_showCurrentTileManager");
				this.manager = this.options.manager;
				this._tileViews = [];
				this._headerViews = [];
				this.listenTo(this.manager, "gatewayInfo:loaded", this.render);
			},

			onBeforeRender: function() {
				if (this.isSlickInit) {
					this.ui.innerHeader.off();

					try {
						this.ui.innerHeader.slick('unslick');
					} catch (e) {
						//do nothing.
					}
					this.isSlickInit = false;
				}
			},

			onRender: function () {
				// Add the blue dashboard color to the body so it covers 100% of the background
				$("body").addClass("dashboard-background-color");
				this._buildHeader();
			},

			_showCurrentTileManager: function () {
				_.each(this._tileViews,function(tileView){
					if(!tileView.options.shouldBeHidden){
						tileView.closeTiles();
					}
				});

				setTimeout(function(){
					var currentTile = this.$(".bb-tile-manager").not(".not-visible"),
						nextTile = this.$('.bb-tile-manager[data-gateway-dsn="' + App.getCurrentGatewayDSN() + '"]');
				
				currentTile.addClass("not-visible");
				currentTile.find(".bb-arc-group").hide();
				nextTile.find(".bb-arc-group").show();
				nextTile.removeClass("not-visible");


				// swapping the old tile manager view with the new view
				// so the dom is at the top of the container
				currentTile.before(nextTile);

				if (!App.justLogIn) {
					App.salusConnector.trigger("changed:currentGateway");
				}
				App.justLogIn = false;
			},1000);
				
			},


			/**
			 * This will not be using the register region mixin
			 * because we need to wait for the view to show on the page instead of just render
			 * for the tiles to calculate their positions correctly
			 */
			onShow: function () {
				var that = this;

				this.listenTo(this.manager, "change:currentGateway", this._showCurrentTileManager);
				// We defer the show of the tile manager until we have a collection of devices for tile construction
				this.manager.loadPromise.then(function () {
					if (!that.manager) {
						return null;
					}

					var user = that.manager.get("sessionUser");

					if (!user) {
						return [];
					}

					// user has already visited the dashboard during the session and has a living tile order
					if (user.get("tileOrderCollection").isLoaded()) {
						that._buildTileManagerViews();

						return true;
					} else if (user.get("tileOrderCollection").isLoading()) {
						return user.get("tileOrderCollection").loadPromise.then(that._buildTileManagerViews);
					}

					return that.manager.loadPromise.then(user.get('tileOrderCollection').load).then(that._buildTileManagerViews);
				});
			},

			_buildTileManagerViews: function () {
				var that = this, tileView,
						currentGatewayDSN = App.getCurrentGatewayDSN();

				this.$(".bb-spinner").remove();

				this._tileViews.forEach(function (tileView) {
					tileView.destroy();
				});
				this._tileViews = [];

				tileView = new Views.TileManagerView({
					dashboardManager: this.manager,
					collection: App.salusConnector.getSessionUser().getTileOrderForGateway(currentGatewayDSN),
					gatewayDSN: currentGatewayDSN,
					shouldBeHidden: false
				}).render();

				this._tileViews.push(tileView);

				if (!_.isString(this.ui.tiles) && this.ui.tiles.is(":visible")) {
					this.ui.tiles.append(tileView.$el);
				}

				tileView.completeOnShow();

				App.salusConnector.getGatewayCollection().each(function (gateway) {
					var dsn = gateway.get("dsn");
					if (dsn !== currentGatewayDSN) {
						tileView = new Views.TileManagerView({
							dashboardManager: that.manager,
							collection: App.salusConnector.getSessionUser().getTileOrderForGateway(dsn),
							gatewayDSN: dsn,
							shouldBeHidden: true
						}).render();
						that._tileViews.push(tileView);

						if (!that.isDestroyed && that.ui.tiles.is(":visible")) {
							that.ui.tiles.append(tileView.$el);
						}

						tileView.completeOnShow();
					}
				});
	        },

			_buildHeader: function () {
				var that = this;
				var gatewayInfos = this.manager.get("gatewayInfoCollection");

				this._headerViews.forEach(function (headerView) {
					headerView.destroy();
				});
				this._headerViews = [];

				if (gatewayInfos && !gatewayInfos.isEmpty()) {
					gatewayInfos.each(function (gatewayInfo) {
						var headerView = new App.Consumer.Dashboard.Views.DashboardHeaderLayout({
							manager: that.manager,
							gateway: gatewayInfo.get("gateway")
						});

						that._headerViews.push(headerView);
						that.ui.innerHeader.append(headerView.render().$el);
					});
				} else {
					var headerView = new App.Consumer.Dashboard.Views.DashboardHeaderLayout({
						manager: that.manager
					});

					this._headerViews.push(headerView);

					this.ui.innerHeader.append(headerView.render().$el);
				}

				//after init, navigate to the current gateways slide
				this.ui.innerHeader.on("init", function (event, slickTrack) {
					that.isSlickInit = true;
					var currentGateway = App.getCurrentGateway();

					if (currentGateway) {
						var slideNum = that.$('[data-gateway-dsn="' + currentGateway.get("dsn") + '"]').first().data("slick-index");
						slickTrack.slickGoTo(slideNum, true); // false means dont animate the slide change
					}

					that.listenTo(that.manager, "allPhotos:loaded", function () {
						that._fixSlickClones();
					});
				});

				this.ui.innerHeader.slick({
					arrows: false,
					lazyLoad: "progressive",
					infinite: true
				});

				this.ui.innerHeader.on("afterChange", function (event, slick, slide) {
					var $lide = $(event.currentTarget).find('[data-slick-index="' + slide + '"]');
					App.salusConnector.changeCurrentGateway($lide.data("gateway-dsn"));
				});
			},

			_fixSlickClones: function () {
				var that = this;
				this.$(".slick-cloned").each(function (i, clone) {
					var $clone = $(clone),
							dsn = $clone.data("gateway-dsn");
					if (dsn) {
						var backgroundUrl,
								gateway = App.salusConnector.getDevice(dsn);

						// we get it from the manager if we have no gateway
						if (gateway) {
							backgroundUrl = that.manager.getGatewayInfo(gateway).get("backgroundImage") || that.manager.defaultBackground(gateway);
						} else {
							backgroundUrl = that.manager.get("backgroundImage");
						}

						if (backgroundUrl) {
							utils.setBackgroundImage($clone.find(".bb-dashboard-header-main"), backgroundUrl);
						}
					}
				});
			},

			onBeforeDestroy: function () {
				// Remove the blue background color from the body
				$("body").removeClass("dashboard-background-color");
				this.ui.innerHeader.off();
				this.ui.innerHeader.slick('unslick');

				if (App.salusConnector._aylaConnector.deviceRefreshPromise) {
					App.salusConnector._aylaConnector.deviceRefreshPromise.cancel();
				}

				if (App.salusConnector._aylaConnector.dataRefreshManager.dataRefreshPromise()) {
					App.salusConnector._aylaConnector.dataRefreshManager.dataRefreshPromise().cancel();
				}
			},

			onDestroy: function () {
				var dc = this.manager.get("deviceCollection");
				if (dc) {
					dc.destroy();
				}

				this.manager.off();

				this._tileViews.forEach(function (tileView) {
					tileView.destroy();
				});

				this._headerViews.forEach(function (headerView) {
					headerView.destroy();
				});
			}
		}).mixin([SalusPageMixin, RegisteredRegions], {
			analyticsSection: "dashboard",
			analyticsPage: "dashboard"
		});
	});

	return App.Consumer.Dashboard.Views.DashboardPage;
});