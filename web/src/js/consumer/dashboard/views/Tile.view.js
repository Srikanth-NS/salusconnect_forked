"use strict";

define([
    "app",
    "common/config",
    "common/util/utilities",
    "bluebird",
    "modernizr",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/dashboard/views/tileContentViews/WUDTile.view",
    "consumer/dashboard/views/tileContentViews/EquipmentTile.view",
    "consumer/dashboard/views/tileContentViews/GatewayTile.view",
    "consumer/dashboard/views/tileContentViews/WelcomeTile.view",
    "consumer/dashboard/views/tileContentViews/ThermostatTile.view",
    "consumer/dashboard/views/tileContentViews/SmartPlugTile.view",
    "consumer/dashboard/views/tileContentViews/WaterHeaterTile.view",
    "consumer/dashboard/views/tileContentViews/DoorMonitorTile.view",
    "consumer/dashboard/views/tileContentViews/WindowMonitorTile.view",
    "consumer/dashboard/views/tileContentViews/COMonitorTile.view",
    "consumer/dashboard/views/tileContentViews/SmokeDetectorTile.view",
    "consumer/dashboard/views/tileContentViews/EnergyMeterTile.view",
    "consumer/dashboard/views/tileContentViews/RuleTile.view",
    "consumer/dashboard/views/tileContentViews/GroupTile.view",
    "jquery.mobile",
], function(App, config, utilities, P, modernizr, consumerTemplates, SalusViewMixin) {
    /**
     * This is a generic tile. use it when building out dashboard tiles.
     * It takes in a title string as well as an inner content view.
     */
    App.module("Consumer.Dashboard.Views", function(Views, App, B, Mn, $, _) {

        Views.SyncSpinnerView = Mn.ItemView.extend({
            template: false,
            className: "hidden bb-sync-spinner sync-spinner"
        });

        var eventName = "click";
        if(document.ontouchend) {
            eventName = "touchend";
        }

        Views.TileView = Mn.ItemView.extend({
            className: "tile-container bb-tile-container",
            template: consumerTemplates["dashboard/tile/tile"],
            ui: {
                tile: ".bb-tile",
                tileFrontFace: ".bb-front",
                tileBackFace: ".bb-back"
            },
            events: {},
            deviceTypesWithAttrs: {
                // these correspond to the tileType attribute in a device model
                thermostat: {
                    name: "thermostat",
                    canFlip: true,
                    growType: "2x2",
                    canBeSmallTile: false,
                    views: Views.TileContentViews.ThermostatTile
                },
                smartPlug: {
                    name: "smartPlug",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.SmartPlugTile
                },
                hotWaterHeater: {
                    name: "hotWaterHeater",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.WaterHeaterTile
                },
                doorMonitor: {
                    name: "doorMonitor",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.DoorMonitorTile
                },
                windowMonitor: {
                    name: "windowMonitor",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.WindowMonitorTile
                },
                coMonitor: {
                    name: "COMonitor",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.COMonitorTile
                },
                smokeDetector: {
                    name: "smokeDetector",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.SmokeDetectorTile
                },
                energyMeter: {
                    name: "energyMeter",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.EnergyMeterTile
                },
                wud: {
                    name: "wud",
                    canFlip: false,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.WUDTile
                },
                trv: {
                    name: "trv",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.EquipmentTile
                },
                gateway: {
                    name: "gateway",
                    canFlip: true,
                    canBeSmallTile: true,
                    growType: "1x1",
                    views: Views.TileContentViews.GatewayTile
                },
                welcome: {
                    name: "welcomeTile",
                    customTileSize: "2x1",
                    canFlip: false,
                    canBeSmallTile: false,
                    views: Views.TileContentViews.WelcomeTile
                },
                rule: {
                    name: "rule",
                    canFlip: true,
                    canBeSmallTile: false,
                    growType: "1x1",
                    views: Views.TileContentViews.RuleTile
        },
        group: {
            name: "group",
            canFlip: false,
            canBeSmallTile: true,
            growType: "1x1",
            views: Views.TileContentViews.GroupTile
                }
            },
            attributes: function() {
                return {
                    'data-view-cid': this.cid
                };
            },
            initialize: function(options) {
                var that = this,
                    reference, referenceType;

                _.bindAll(this,
                    "_handleStaticClick",
                    "_setTileSize",
                    "_setTileContentView",
                    "_flipTile");
                var events = {};
                events[eventName] = "_handleStaticClick";
                this.delegateEvents(_.extend(_.clone(this.events), events));

                this.$el.addClass(options.classes || "");

                // Set the varied long tap length based on the environment.
                try {
                    $.event.special.tap.tapholdThreshold = 400;
                } catch(exp) {
                    console.warn(exp);
                }

                /**
                 * By setting a mouse out boolean when the mouse leaves an element, we avoid a bug where if the
                 * pointer left a tile prematurely while in the process of a long hold, it would inconsistently
                 * trigger the tile drag
                 *
                 * vmouseout is the name of the virtual event triggered by jquery mobile
                 * @type {boolean}
                 */
                this.vmouseOutFlag = false;

                referenceType = this.model.get("referenceType");

                if (referenceType === "device") {
                    reference = App.salusConnector.getDevice(this.model.get("referenceId"));
                } else if (referenceType === "rule") {
                    reference = App.salusConnector.getRule(this.model.get("referenceId"));

                    // rules not loaded
                    if (!reference) {
                        reference = new B.Model({
                            referenceId: this.model.get("referenceId")
                        });
                    }
                } else if(referenceType === "group" && this.model.get("referenceId")) {
                        reference = App.salusConnector.getGroup(this.model.get("referenceId"));
                        reference = reference.length ? reference[0] : reference;
                }

                if (reference && reference.tileType) {
                    this.type = this.deviceTypesWithAttrs[reference.tileType];
                } else if (referenceType === "welcome" || referenceType === "rule" || referenceType === "group") {
                    // we have 'rule' check here too in case we are waiting for rule download
                    this.type = this.deviceTypesWithAttrs[referenceType];
                } else {
                    this.type = this.deviceTypesWithAttrs.wud;
                }

                this.frontView = new this.type.views.FrontView({
                    model: reference,
                    isLargeTile: this.model.get("isLargeTile")
                });
                if(referenceType === "group") {
                  this.listenTo(this.frontView, "show:group", function(groupId) {
                    this.trigger("show:group", groupId);
                  }, this);
                }

                if (this.type.canFlip) {
                    this.backView = new this.type.views.BackView({
                        model: reference,
                        isLargeTile: this.model.get("isLargeTile"),
                        frontView: this.frontView
                    });

                    this.syncSpinnerView = new Views.SyncSpinnerView();

                    this.listenTo(this.backView, "toggle:smallSpinner", function(shouldShow) {
                        that.toggleSmallSpinner(shouldShow);
                    });
                }

                this._bindLongPress();

                //Need this setTimout for IE11 on window7
                this.listenTo(this, "flipping", function(isFlippingBack) {
                    window.setTimeout(function() {
                        that.ui.tileFrontFace.toggleClass("hidden", isFlippingBack);
                        that.ui.tileBackFace.toggleClass("flipping-back", isFlippingBack);

                    }, 200);
                });

            },

            toggleSmallSpinner: function(shouldShow) {
                console.info('toggleSmallSpinner');
                if (shouldShow) {
                    this.syncSpinnerView.$el.removeClass("hidden");
                } else {
                    this.syncSpinnerView.$el.addClass("hidden");
                }
            },

            completeOnShow: function() {
                console.info('completeOnShow')

                // Trigger any logic on the child views that can only be done once the DOM has loaded
                if (this.type.canFlip && this.backView.handleDomLoadComplete) {
                    this.backView.handleDomLoadComplete();
                }

                if (this.frontView.handleDomLoadComplete) {
                    this.frontView.handleDomLoadComplete();
                }
            },
            onRender: function() {
                console.info('onRender')

                this._setTileSize();
                this._setTileContentView();
                this.addBinding(this.frontView.model, ".tile-content-view", {
                    observe: "connection_status",
                    update: function($el, val, model) {
                        if (val !== "Online" && model.get("device_type")) {
                            $el.addClass("disconnected");
                        } else {
                            $el.removeClass("disconnected");
                        }
                    }
                });

                this.addBinding(this.frontView.model, ".tile-content-view", {
                    observe: "LeaveNetwork",
                    update: function($el, val, model) {
                        if (val && val.getProperty() === 1) {
                            $el.addClass("no-device");
                        } else {
                            $el.removeClass("no-device");
                        }
                    }
                });

              /*  if (App.isMSBrowser()) {
                    this.$el.on("click", this._handleStaticClick);
                }*/
            },
            isFlipped: function() {
                return this.model.get("isFlipped");
            },
            /**
             * Cleanup any event handlers in here when the view is destroyed
             */
            onBeforeDestroy: function() {
                this.$el.off();
            },
            _setTileSize: function() {
                var sizeClass;

                if (this.type.canBeSmallTile && !this.model.get("isLargeTile")) {
                    sizeClass = "small-size";
                } else {
                    sizeClass = "large-size";
                }

                // this is to create a custom sized tile
                // we append to the size class eg. "large-size double-width"
                if (this.type.customTileSize) {
                    if (this.type.customTileSize === "2x1") {
                        sizeClass = sizeClass + " double-width";
                    } else if (this.type.customTileSize === "2x2") {
                        sizeClass = sizeClass + " double-width-height";
                    } else if (this.type.customTileSize === "1x2") {
                        sizeClass = sizeClass + " double-height";
                    }
                }

                // Add the size classes to both this element and it's parent. See the equivalent CSS in _tiles.scs
                // to see why this needs to happen
                this.ui.tile.addClass(sizeClass);
                this.ui.tile.parents(".bb-tile-container").addClass(sizeClass);
            },
            _setTileContentView: function() {
                this.ui.tileFrontFace.append(this.frontView.render().$el);

                if (this.type.canFlip) {
                    this.ui.tileBackFace.append(this.backView.render().$el);
                    this.ui.tileBackFace.append(this.syncSpinnerView.render().$el);
                }

                // Speed up render performance by hiding the responsively sized
                // inner element on the back of the tile since the user cannot see it
                this.$(".bb-hook-for-hiding-back").hide();
            },
            _flipTile: function(preventFlipCompleteTrigger) {
                // Only flip if the tile can flip and the tile is not in a loading state with a spinner
                if (this.type.canFlip) {
                    var that = this,
                        tileGrowSizeClass = this._getGrowClass(),
                        $packeryContainer = $(".packery");

                    this.shouldGrow = !tileGrowSizeClass ? false : this.$el.hasClass(tileGrowSizeClass);

                    this.preventFlipCompleteTrigger = preventFlipCompleteTrigger;

                    this.model.set("isFlipped", !this.isFlipped());

                    // For accessibility
                    setTimeout(function() {
                        that.$(".bb-title-text").focus();
                    }, 200);
                    // Wait for the grow transition to end before we retrigger a packery layout and show various elements
                    this.$el.on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(event) {
                        var eventPropertyName = event.originalEvent.propertyName,
                            isFlipped = that.isFlipped();

                        // The following checks allow us to show or hide the face of the tile when we know all
                        // the animations are done. For the front, we are done when the event is a transform. For the back,
                        // it is when the event is a width change or we detect no grow class.
                        if (isFlipped) {
                            if (eventPropertyName === "transform" || eventPropertyName === "-webkit-transform") {
                                that.$el.off("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
                                that.$(".bb-hook-for-hiding-back").show();
                            }
                        } else {
                            // Use that.shouldGrow to detect if it was a smart plug or other non growing tile that
                            // ends its animation chain in a transform and not a width or height change
                            if (eventPropertyName === "width" || !that.shouldGrow) {
                                // that.$el.css({ zIndex: 2 });

                                that.$el.off("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
                                that.$(".bb-hook-for-hiding-front").show(function() {
                                    that.$el.parents('.bb-dashboard-tile-manager-container').find('.tile-placeholder').remove();
                                    that.$el.removeClass('grow-container');
                                });
                            }
                        }

                        if (!!tileGrowSizeClass) {
                            // Do the inverse of the animation when flipping to the opposite face
                            if (!isFlipped) {
                                // This if check fixes a bug where tile sizes would freak out when we don't pass in a grow size class
                                if (tileGrowSizeClass) {
                                    that.$el.toggleClass(tileGrowSizeClass, isFlipped);
                                }
                            } else {
                                that.ui.tile.toggleClass("flipped", isFlipped);
                                that.trigger("flipping", true);

                            }
                        }

                        // If the tile is growing then trigger a re-layout. If it is shrinking, then fit to the shrunken element
                        if (that.shouldGrow) {
                            // $packeryContainer.packery("layout");
                        } else {

                            // Note that this uses 'this.el' and not 'this.$el'
                            // $packeryContainer.packery("fit", that.el);
                        }

                        if (!that.preventFlipCompleteTrigger) {
                            that.trigger("flipComplete", that);
                        }
                    });

                    // Speed up render performance by hiding the responsively sized
                    // inner elements of the front and back before a flip.
                    // Also set aria-hidden attributes
                    if (this.isFlipped()) {
                        this.backView.trigger("tile:flipToBack");
                        this.$(".bb-hook-for-hiding-front").hide();
                        this.ui.tileFrontFace.attr("aria-hidden", "true");
                        this.ui.tileBackFace.attr("aria-hidden", "false");
                        this.ui.tileBackFace.css("opacity", 1.0);

                        // This if check fixes a bug where tile sizes would freak out when we don't pass in a grow size class
                        if (tileGrowSizeClass) {
                            this.$el.toggleClass(tileGrowSizeClass, this.isFlipped());
                        } else {
                            this.ui.tile.toggleClass("flipped", this.isFlipped());
                            that.trigger("flipping", true);
                        }
                    } else {
                        // that.$el.css({ zIndex: 3 });

                        this.backView.trigger("tile:flipToFront");

                        this.$(".bb-hook-for-hiding-back").hide();
                        this.ui.tileFrontFace.attr("aria-hidden", "false");
                        this.ui.tileBackFace.attr("aria-hidden", "true");
                        this.ui.tile.toggleClass("flipped", this.isFlipped());
                        that.trigger("flipping", false);

                    }

                    // If the tile is growing then trigger a re-layout. If it is shrinking, then fit to the shrunken element
                    if (this.shouldGrow) {
                        // $packeryContainer.packery("layout");
                    } else {
                        // Note that this uses 'this.el' and not 'this.$el'
                        // $packeryContainer.packery("fit", this.el);
                    }
                }
            },
            _getGrowClass: function() {
                var containerWidth = this.$el.parents(".bb-dashboard-tile-manager-container").width();
                // var currentTileOffset = $(this.)
                var growClass;

                // If this is a small tile, we auto grow the tile on flip to it's equivalent 2x2 size even if the
                // tile view options for growing to 1x2 or 2x2 were not set on instantiation
                if (!this.model.get("isLargeTile")) {
                    growClass = "double-height-width-sm-tile";
                } else {
                    if (this.type.growType === "2x2") {
                        growClass = "double-height-width-lg-tile";
                    } else if (this.type.growType === "1x2") {
                        growClass = "double-height-lg-tile";
                    }
                }

                return growClass;
            },
            _handleStaticClick: function(event) {
                console.info('handle Static click', performance.now());
                var $container = this.$el.parents(".bb-dashboard-tile-manager-container");
              if (this.model.get("referenceType") === "group") {
                    if ($container.find('.flipped').length) {
                        this.trigger("flipComplete");
                    }
                    this.trigger("show:group", this.model.get("referenceId"));
                    return false;
                } 

                var containerWidth = $container.width();
                var $placeholder = $('<div class="tile-placeholder"></div>');
                // console.info('Init placeholder', performance.now());
                var $originalEventTarget = $(event.originalEvent.target),
                    eventOnFlipButton = $originalEventTarget.hasClass("bb-flip-button"),
                    eventOnText = $originalEventTarget.hasClass("bb-title-text"),
                    eventOnPin = $originalEventTarget.hasClass("bb-pin-icon");

                var growClass = this._getGrowClass();

                // console.info('1', performance.now());

                var tileMoverModeActive = $(event.currentTarget).closest(".tile-container").hasClass("tile-mover-mode-active");
                if(tileMoverModeActive) {
                    this._handleTileMoverNavigation(event);
                    return;
                }

                // Allow static click on a tile only if the tiles are not in drag mode
                if (!this.$el.parents(".bb-tile-manager").first().hasClass("drag-mode-enabled")) {
                    // console.info('2', performance.now());

                    // actions for tile back-face
                    if (this.isFlipped()) {
                        if (eventOnFlipButton) {
                            // if ($container.hasClass('ui-sortable-disabled')) {
                            //     $container.sortable('enable');
                            // }
                            // console.info('3', performance.now());

                            this._flipTile();
                            // console.info('4', performance.now());

                        } else if (!(eventOnText || eventOnPin)) {
                            // event was not on title text or the pin, check if the back face has a static click handler
                            if (this.backView.handleStaticClick) {
                                // console.info('5', performance.now());

                                this.backView.handleStaticClick();
                                // console.info('6', performance.now());

                            }
                        }
                    } else {
                        // console.info('7', performance.now());

                        // clicking front of tile should just flip
                        if (!_.isUndefined(this.backView.model.get("ruleTriggerKey")) ||
                            _.isObject(this.backView.model.get("EUID")) || this.backView.model.isGateway()) {
                            if (growClass === 'double-height-width-lg-tile') {
                                // console.info('8', performance.now());

                                //Auto scroll if tile doesn't fit in screen
                                var offset = this.$el.offset();
                                var heightDouble = ($(".large-size").height() * 2) + parseInt(this.$el.css('margin-left'), 10) + parseInt(this.$el.css('margin-right'), 10);
                                // var heightDouble = ($(".large-size").height() * 2) + (parseInt(this.$el.css('margin'), 10) * 2);

                                // This should not be calculated here instead should be assigned at APP level or CREATION of NAVBAR
                                var navBarheight = $(".container").height() + 2;
                                var photoViewportOffsetTop = offset.top - $(document).scrollTop();
                                var diffWindow = window.innerHeight - photoViewportOffsetTop;
                                // console.info('9', performance.now());

                                if (heightDouble > diffWindow) {

                                    //Instead of using scrollBy use jquery animate
                                    window.scrollBy(0, (heightDouble - diffWindow));
                                } else if ((photoViewportOffsetTop - navBarheight) < 0) {
                                    window.scrollBy(0, (photoViewportOffsetTop - navBarheight));
                                }

                                // console.info('10', performance.now());


                                window.targetElementOffset = this.$el.position();
                                this.$el.addClass('grow-container');
                                //this styles can be moved to css
                                this.$el.children('.bb-tile').css({ position: 'absolute', left: 0 });
                                // console.info('11', performance.now());

                                if (targetElementOffset.left !== 0) {

                                    var targetWidth = this.$el.width();
                                    var targetRight = targetElementOffset.left + (targetWidth * 2);


                                    if (targetRight > containerWidth) {
                                        this.$el.children('.bb-tile').css({ left: 'auto', right: 0 });
                                        // this.$el.children('.bb-tile').css({ left: 'auto', right: -(parseInt(this.$el.css('margin-left'), 10) + parseInt(this.$el.css('margin-right'), 10)) });

                                        // // this.$el.css({ right: targetRight });
                                        // console.info('this.$el[0].style ::  ', this.$el.css('margin-left'));
                                        // this.$el.css({ right: containerWidth - (targetElementOffset.left + targetWidth + parseInt(this.$el.css('margin-left'), 10) + parseInt(this.$el.css('margin-right'), 10)) });
                                    }
                                    // else {
                                    //     this.$el.css({ left: targetElementOffset.left });
                                    // }

                                }
                                /*else {
                                    this.$el.css({ left: targetElementOffset.left });
                                }*/
                                // this.$el.css({ top: targetElementOffset.top });

                                //Placeholder can be placed in the DOM itself, and treated as display:none

                                // console.info('12', performance.now());

                                this.$el.append($placeholder);

                                this.$el.find('.tile-placeholder').addClass('db');
                                // console.info('13', performance.now());

                            }


                            // this.$el.parents('.bb-dashboard-tile-manager-container').sortable("refresh");
                            // console.info('14', performance.now());

                            this._flipTile();
                            // console.info('15', performance.now());


                        }
                    }
                }
            },

            _handleTileMoverNavigation: function(evt) {
                console.log('CAME TO NAVIGATE');
                this.trigger("tileMover:actionButton:click", {
                    evt: evt,
                    view: this
                });
                evt.preventDefault();
            },

            _bindLongPress: function() {
                var that = this;

                this.$el.on("vmousedown", function() {
                    that.vmouseOutFlag = false;

                    that.$el.on("vmouseout", function() {
                        that.vmouseOutFlag = true;
                        that.$el.off("vmouseout");
                    });
                });

                this.$el.on("taphold", function(e) {
                    if (!that.vmouseOutFlag && !that.isFlipped()) {
                        // that.$el.trigger("dragStart");
                        that.trigger("longPress", {
                            evt: e,
                            view: that
                        });
                    } else {
                        that.vmouseOutFlag = false;
                    }
                });
            },
            closeTile: function() {
                 if (this.frontView instanceof Views.TileContentViews.GroupTile.FrontView) {
                    this.trigger("close:group");
                } else {
                    if (this.isFlipped()) {
                        this._flipTile(true);
                    }
                }
            },
            onDestroy: function () {
                this.$el.off();

                if (this.frontView) {
                    this.frontView.destroy();
                }

                if (this.backView) {
                    this.backView.destroy();
                }
            }
        }).mixin([SalusViewMixin]);
    });

    return App.Consumer.Dashboard.Views.TileView;
});