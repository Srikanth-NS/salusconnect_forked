"use strict";

/**
 * This is a collection of dashboard tile models representing the order on the dashboard.
 */
define([
	"app",
	"common/constants",
	"consumer/dashboard/models/TileOrderCollection"
], function (App, constants) {

	App.module("Consumer.Dashboard.Models", function (Models, App, B, Mn, $, _) {
		Models.FullDashboardTileOrderModel = B.Model.extend({
			initialize: function () {
				_.bindAll(this, "load");

				this.listenTo(App.vent, "gateway:added", function (gateway) {
					this.setTileOrderCollection(gateway);
				});

				this.listenTo(App.vent, "gateway:removed", function (gateway) {
					if (App.getCurrentGateway() === gateway) {
                         App.navigate("/equipment");
						//App.salusConnector.nextGateway();
					}

					var gatewayDsn = gateway.get("dsn");
					if (this.has(gatewayDsn)) {
						this.unset(gatewayDsn);
						this.save();
					}
				});
			},

			isLoaded: function () {
				return Object.keys(this.attributes).length > 0;
			},

			isLoading: function () {
				return this.loadPromise && !this.loadPromise.isFulfilled();
			},

			save: function () {
				return App.salusConnector.getSessionUser().saveTileOrder(this.getSaveObj());
			},

			getSaveObj: function () {
				var json = this.toJSON();

				return _.mapObject(json, function (order) {
					return order.map(function (tileModel) {
						return tileModel.pick("referenceType", "referenceId");
					});
				});
			},

			getTileOrderForGateway: function (gatewayDsn) {
				return this.get(gatewayDsn);
			},

			findAndRemove: function (tileReference, referenceType) {
				var dsn, tile;

				if (!tileReference && referenceType === "welcome") {
					dsn = App.getCurrentGatewayDSN();
				} else if (tileReference.modelType === constants.modelTypes.GATEWAY) {
					dsn = tileReference.get("dsn");
				} else if (referenceType === "rule") {
					dsn = tileReference.get("dsn");
				} else if (referenceType === "group") {
					dsn = tileReference.get("dsn") || App.getCurrentGatewayDSN();
				} else {
					dsn = tileReference.get("gateway_dsn");
				}

				if (dsn) {
					tile = this.get(dsn);
					if (tile) {
						tile.findAndRemove(tileReference, referenceType);
					} else {
						App.warn("No tile found.");
					}
				}
			},

			load: function () {
				var that = this;

				this.loadPromise = App.salusConnector.getSessionUser().fetchTileOrder().then(function (tileOrder) {
					var gateways;

					if (Object.keys(tileOrder).length > 0) {
						_.each(Object.keys(tileOrder), function (key) {
							if (!that.get(key)) {
								that.set(key, new Models.TileOrderCollection(null, key));
								that.listenTo(that.get(key), "tileOrder:modification",  that._onModification);
							}

							that.get(key).setOrder(tileOrder[key]);
						});
					} else {
						gateways = App.salusConnector.getGatewayCollection();

						if (!gateways.isEmpty()) {
							gateways.each(function (gateway) {
								that.setTileOrderCollection(gateway);
							});
						} else {
							// set for no gateways
							that.setTileOrderCollection();
						}
					}

					// picked up by isInDashboard mixin
					that.trigger("tileOrder:modification");

					return that;
				});

				return this.loadPromise;
			},

			/**
			 * set some properties and listeners for the tile order collection of a gateway, or no gateway
			 * @param gateway
			 * @private
			 */
			setTileOrderCollection: function (gateway) {
				var dsn = gateway ? gateway.get("dsn") : constants.noGateway,
						devices = App.salusConnector.getDevicesOnGateway(gateway || dsn).filterOutDelegates();

				this.set(dsn, new Models.TileOrderCollection(null, dsn));
				this.get(dsn).buildStartingTilesOnCreate(devices);
				this.listenTo(this.get(dsn), "tileOrder:modification", this._onModification);
			},

			_onModification: function (argument) {
				// actually save
				this.save();

				// propagate up, used by isInDashboard mixin
				this.trigger("tileOrder:modification", argument);
			}
		});
	});
	return App.Consumer.Dashboard.Models.FullDashboardTileOrderModel;
});
