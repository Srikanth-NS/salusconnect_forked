"use strict";

/**
 * represents an ayla user
 */
define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/constants",
	"common/util/equipmentUtilities",
	"common/model/ayla/deviceTypes/COMonitor",
	"common/model/ayla/deviceTypes/CoordinatorDevice",
	"common/model/ayla/deviceTypes/DefaultAylaDevice",
	"common/model/ayla/deviceTypes/DoorMonitor",
	"common/model/ayla/deviceTypes/EnergyMeterDevice",
	"common/model/ayla/deviceTypes/GatewayAylaDevice",
	"common/model/ayla/deviceTypes/IT600BoilerReceiver",
	"common/model/ayla/deviceTypes/IT600Device",
	"common/model/ayla/deviceTypes/IT600TRV",
	"common/model/ayla/deviceTypes/IT600WC",
	"common/model/ayla/deviceTypes/MiniSmartPlugUS",
	"common/model/ayla/deviceTypes/SmartPlugEU",
	"common/model/ayla/deviceTypes/SmokeDetector",
	"common/model/ayla/deviceTypes/Thermostat",
	"common/model/ayla/deviceTypes/WindowMonitor",
	"common/model/ayla/deviceTypes/GatewayNode"
], function (App, P, AylaConfig, AylaBacked, Constants, equipUtils) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		/*var GATEWAY_OEM_MODEL_ID = "OTEST";*/

		Models.AylaDeviceCollection = B.Collection.extend({
			_deviceTypeMapping: {
				// currently this is based on the "oem_model property
				// but I don't know if that is correct. Please read mixin.baseDevice.js
				"DEFAULTAYLADEVICE": Models.DefaultAylaDevice,
				"TTEST": Models.DefaultAylaDevice,
				"OTEST": Models.GatewayAylaDevice, //Update GATEWAY_OEM_MODEL_ID if you change the key.
				"ledevb": Models.GatewayAylaDevice, // These are probably not right but used to load Salus_tester
				"zigbee1": Models.MiniSmartPlugUS,
				"salus-sau2ag1": Models.Thermostat,
				"sau2ag1Coordinator": Models.CoordinatorDevice,
				"sau2ag1Gateway": Models.GatewayNode,
				"CTL3351": Models.EnergyMeterDevice,
				"CTL6182": Models.MiniSmartPlugUS,
				"SAU2CB1": Models.DefaultAylaDevice,
				"WTS10": Models.IT600Device,  //?Developemnt?

				//----------------------------------------------------------------------
				//https://salusinc.atlassian.net/wiki/display/CI/Model+Identifier+List
				// Keep devices in order.
				//----------------------------------------------------------------------
				"sau2ag1": Models.GatewayAylaDevice, //Gateway in Ayla system
				"SAU2AG1-ZC": Models.CoordinatorDevice, //Zigbee Coordinator node in Gateway
				"SAU2AG1-GW": Models.GatewayNode, //Gateway node in Gateway
				"it600ThermHW_AC": Models.IT600Device, //iT600 AC Thermostat
				"it600ThermHW-AC": Models.IT600Device, //iT600 AC Thermostat
				"it600ThermHW": Models.IT600Device, //iT600 DC Thermostat
				"it600HW-AC": Models.IT600Device, //iT600 AC Hot Water
				"it600HW": Models.IT600Device, //iT600 DC Hot Water
				"it600WC": Models.IT600WC, //iT600 Wiring Center
				"it600Receiver": Models.IT600BoilerReceiver, //iT600 Boiler Receiver
				"it600MINITRV": Models.IT600TRV, //iT600 TRV
				"it600TRV": Models.IT600TRV, //iT600 TRV (saw in the wild)
				"ECM600": Models.EnergyMeterDevice, //Energy Meter
				"SAL2EM1": Models.EnergyMeterDevice, //Energy Meter (will not use in future)
				"SX885ZB": Models.MiniSmartPlugUS, //Mini Smart Plug for US
				"SPE600": Models.SmartPlugEU, //Smart Plug for EU and UK
				"SP600": Models.SmartPlugEU,
				"SAL2": Models.SmartPlugEU, //Smart Plug for UK (will not use in future)
				"ST880ZB": Models.Thermostat, //Optima Thermostat
				"ST898ZB": Models.Thermostat, //Optima S Thermostat
				"iT530TR": Models.Thermostat, //iT530 Thermostat
				//"iT330TX": Models.DefaultAylaDevice, //iT530 Sensor  (TYPO?)
				//"iT530TX": Models.DefaultAylaDevice, //iT530 Sensor
				//"iT530RX": Models.DefaultAylaDevice, //iT530 3-Channel Receiver
				"CO-8ZBS": Models.COMonitor, //Carbon Monoxide Detector
				"SD-8ZBS-EL": Models.SmokeDetector, //Smoke Detector
				"SD-8SCZBS": Models.SmokeDetector, //Smoke Detector with Siren
				"SS881ZB": Models.DoorMonitor, //Door Sensor
				"SS882ZB": Models.WindowMonitor,  //Window Sensor
                "OS600": Models.WindowMonitor  //Window Sensor
			},
			_baseDeviceId: null,

			// todo: figure out what to do with this for groups
//			modelId: function () {
//				return "key";
//			},

			/**
			 * to get all devices below a device pass the base device id in here.
			 * !!!! Note: this does not work yet, but I think that is a data issue!!!
			 * @param baseDeviceId
			 */
			initialize: function (data, options) {
				_.bindAll(this,
					"_beforeFetch",
					"_fetchDeviceMetaData",
					"_getDeviceWeather",
					"_fetchDeviceProperties",
					"getDevicesSortedByCategory",
					"filterByCurrentGateway",
					"loadDetails"
				);

				options = options || {};

				this._baseDeviceId = options.baseDeviceId;
				this.listenTo(this, "add", this._onAdd);
				this.listenTo(this, "remove", this._onRemove);
			},

			findWhereByType: function (type) {
				return _.findWhere(this.models, {
					modelType: type
				});
			},

			getPropertiesPromise: function () {
				return this.propertiesPromise ||  this._fetchDeviceProperties(true);
			},

			load: function () {
				var that = this;
				//TODO: investigate the ayla sdk for why the android version is slower then ios
				//TODO: this is a work around to the sdk since it currently lacking the ability to filter devices per gateway
				if (App.isCordovaApiReady("getDevices") && !this._baseDeviceId) {
					this._fetchPromise = App.salusConnector.makeCordovaCall("getDevices", [])
						.then(function (data) {
							var devices = _.map(data, function (obj) {
								//parse the returned json into an actual device model
								return that._buildDevice(obj);
							});

							that.set(devices);
							return data;
						});
				} else {
					this._fetchPromise = this.fetch();
				}

				return this._fetchPromise;
			},

			loadDetails: function (isLowPriority) {
				var that = this;
				return P.all([this._fetchDeviceMetaData(isLowPriority), (!!that._baseDeviceId) ? that._fetchDeviceProperties(isLowPriority) : P.resolve([])])
//				return P.all([this._fetchDeviceMetaData(isLowPriority), (!!that._baseDeviceId && !App.isEU) ? P.resolve([]) : that._fetchDeviceProperties(isLowPriority)])
//					.then(function () {
//						return that._getDeviceWeather(isLowPriority);
//					})
					.then(this._getResolvedPromise);
			},
			
			getFetchPromise: function () {
				return this._fetchPromise;
			},

			softRefresh: function (isLowPriority) {
				var that = this;
				var promise = this.load().then(function () {
					that.loadDetails(isLowPriority);
					return that;
				});
				
				// only set DLP when we do a softRefresh for the current gateway's devices
				if (!this.isFullCollection) {
					App.salusConnector.setDataLoadPromise("devices", promise);
				}
				
				return promise;
			},

			refresh: function (isLowPriority) {
				var that = this;

				var promise = this.load().then(function () {
					return that.loadDetails(isLowPriority);
				}).then(that.filterByCurrentGateway).catch(function (error) {
					if (error.status === 405) {
						//Nodes call will error with 405 if gateway has no items.
						return that;
					}

					return P.reject(error);
				});

				App.salusConnector.setDataLoadPromise("devices", promise);

				return promise;
			},

			getGateways: function () {
				var filtered = this.filter(function (device) {
					return device.modelType === Constants.modelTypes.GATEWAY;
				});

				return new Models.AylaDeviceCollection(filtered);
			},

			getWaterHeaters: function () {
				var filtered = this.filter(function (device) {
					return device.modelType === Constants.modelTypes.WATERHEATER;
				});

				return new Models.AylaDeviceCollection(filtered);
			},

			getThermostats: function () {
				var filtered = this.filter(function (device) {
					return device.isThermostat();
				});

				return new Models.AylaDeviceCollection(filtered);
			},

			getSmartPlugs: function () {
				var filtered = this.filter(function (device) {
					return device.isSmartPlug();
				});

				return new Models.AylaDeviceCollection(filtered);
			},
            
            getWindowSensor: function (){
                var filtered = this.filter(function (device) {
					return device.isWindowSensor();
				});

				return new Models.AylaDeviceCollection(filtered);
            },

			getTRVs: function () {
				var filtered = this.filter(function (device) {
					return device.modelType === Constants.modelTypes.IT600TRV;
				});

				return new Models.AylaDeviceCollection(filtered);
			},
			
			getTRVsWithLive: function () {
				var filtered = this.filter(function (device) {
					var isTRVLive = false;
					if (device.modelType === Constants.modelTypes.IT600TRV) {
						if (device.getPropertyValue("LeaveNetwork") === 0 && 
								device.get("connection_status") === "Online" && 
								!_.isNull(device.getPropertyValue("ShortID_d"))) {
							isTRVLive = true;
						}
					}
					return isTRVLive;
				});

				return new Models.AylaDeviceCollection(filtered);
			},

			/**
			 * find trv association for a collection of it600s
			 * @param trv
			 * @returns {boolean|*}
			 */
			findTRV: function (trv) {
				var hexID, id = trv.getPropertyValue("ShortID_d");

				hexID = equipUtils.integerTo64BitHex(id);

				return this.some(function (device) {
					var associated = device.getAssociatedTRVs();

					return _.contains(associated, hexID);
				});
			},

			/**
			 * given a collection of TRVs, search each for matching ShortID_d
			 * @param hex - base 16 string
			 */
			findByShortID: function (hex) {
				return this.find(function (trv) {
					if (trv.get("ShortID_d")) {
						var val = trv.getPropertyValue("ShortID_d");

						return equipUtils.hexToInt(hex) === val;
					}

					return false;
				});
			},

			/**
			 * devices that can read a property / set a property / send an alert
			 * that is useful to the user
			 * @returns {*}
			 */
			filterForInteractableDevices: function () {
				var interactable = [Constants.modelTypes.SMARTPLUG,
					Constants.modelTypes.THERMOSTAT,
					Constants.modelTypes.IT600THERMOSTAT,
					Constants.modelTypes.MINISMARTPLUG,
					Constants.modelTypes.WATERHEATER,
					Constants.modelTypes.CARBONMONOXIDEDETECTOR,
					Constants.modelTypes.DOORMONITOR,
					Constants.modelTypes.SMOKEDETECTOR,
					Constants.modelTypes.WINDOWMONITOR
				];

				return this.filter(function (device) {
					return _.indexOf(interactable, device.modelType) > -1;
				});
			},

			filterForActionableDevices: function () {
				var actionable = [Constants.modelTypes.SMARTPLUG,
					Constants.modelTypes.THERMOSTAT,
					Constants.modelTypes.IT600THERMOSTAT,
					Constants.modelTypes.MINISMARTPLUG,
					Constants.modelTypes.WATERHEATER
				];

				return this.filter(function (device) {
					return _.indexOf(actionable, device.modelType) > -1;
				});
			},

			/*
			 * gateway, coordinator, and gateway node
			 */
			getGatewayAndDelegates: function () {
				var modelTypes = [
					Constants.modelTypes.GATEWAY,
					Constants.modelTypes.GATEWAYNODE,
					Constants.modelTypes.COORDINATORDEVICE
				];

				return this.filter(function (device) {
					return _.indexOf(modelTypes, device.modelType) > -1;
				});
			},

			filterByCurrentGateway: function (shouldReturnArray) {
				var filteredArray = this.filter(function (device) {
					return device.get("gateway_dsn") === App.getCurrentGatewayDSN() || device === App.getCurrentGateway();
				});
				
				if (shouldReturnArray) {
					return filteredArray;
				}

				var id =  App.getCurrentGateway() ? App.getCurrentGateway().get("key") : null;

				return new Models.AylaDeviceCollection(filteredArray, {baseDeviceId: id});
			},
			
			filterByCurrentGatewayInDashboard: function (shouldReturnArray) {
				var filteredArray = this.filter(function (device) {
					if (device.get("isInDashboard")) {
						return device.get("gateway_dsn") === App.getCurrentGatewayDSN() || device === App.getCurrentGateway();
					}
				});
				
				if (shouldReturnArray) {
					return filteredArray;
				}

				var id =  App.getCurrentGateway() ? App.getCurrentGateway().get("key") : null;

				return new Models.AylaDeviceCollection(filteredArray, {baseDeviceId: id});
			},
			
			filterByCurrentGatewayAndIT600s: function (shouldReturnArray) {
				var filteredArray = this.filter(function (device) {
					if (!device.isIT600) {
						return device.get("gateway_dsn") === App.getCurrentGatewayDSN() || device === App.getCurrentGateway();
					}
				});
				
				if (shouldReturnArray) {
					return filteredArray;
				}

				return new Models.AylaDeviceCollection(filteredArray, {baseDeviceId: App.getCurrentGateway() ? App.getCurrentGateway().get("key") : null});
			},

			/**
			 * filter out Coordinator and Gateway Node from a device collection
			 * @returns {*}
			 */
			filterOutDelegates: function () {
				return new Models.AylaDeviceCollection(this.filter(function (device) {
					// we should already be filtered by current gateway
					return device.modelType !== Constants.modelTypes.COORDINATORDEVICE && device.modelType !== Constants.modelTypes.GATEWAYNODE;
				}), {baseDeviceId: App.getCurrentGateway() ? App.getCurrentGateway().get("key") : null});
			},

			/**
			 * Parse an array of devices grouped by category into an array with a category name and device count
			 * @param data
			 * @returns {Array}
			 */
			categoryParse: function (data) {
				return _.map(data, function (categoryModels, categoryTypeHeader) {
					return {
						category_icon_url: categoryModels[0] ? categoryModels[0].get("device_category_icon_url") : "",
						category_name_key: categoryModels[0] ? categoryModels[0].get("device_category_name_key") : "",
						category_type: categoryTypeHeader,
						category_device_count: categoryModels.length,
						category_devices: categoryModels
					};
				});
			},
			/**
			 * returns a collection containing devices organized into separate collections based on category types
			 * @param devCollec deviceCollection to sort
			 * @returns {Backbone.Collection}
			 */
			getDevicesSortedByCategory: function () {
				var finalCategoryParse,
					devicesGroupedByCategory = this.filterByCurrentGateway().groupBy(function (device) {
						return device.get("device_category_type"); //Every device model has this. If it does not, you're doing it wrong.
					});

				finalCategoryParse = this.categoryParse(devicesGroupedByCategory);

				// Throw these into new generic collection since we don't need to make a Category type collection
				return new B.Collection(finalCategoryParse);
			},

			getDeviceByEUID: function (EUID) {
				var devicesWithEUID = this.filter(function (device) {
                    if(device.get("gateway_dsn") === App.getCurrentGatewayDSN() || device === App.getCurrentGateway()){
                        return device.get("EUID");
                    }
				});

				return _.find(devicesWithEUID, function (device) {
					return device.getEUID() === EUID;
				});
			},

			loadSchedules: function (isLowPriority) {
				var schedulePromises = [], scheduleModelTypes = [
					Constants.modelTypes.THERMOSTAT,
					Constants.modelTypes.IT600THERMOSTAT,
					Constants.modelTypes.WATERHEATER,
					Constants.modelTypes.SMARTPLUG,
					Constants.modelTypes.MINISMARTPLUG
				];

				this.each(function (device) {
					if (_.contains(scheduleModelTypes, device.modelType)) {
						schedulePromises.push(device.loadSchedules(isLowPriority));
					}
				});

				return P.all(schedulePromises);
			},

			/**
			 * when a device is added, register some listeners on it
			 * @param device
			 * @private
			 */
			_onAdd: function (device) {
				var that = this;

				this.listenTo(device, "unregistered:device", function () {
					that._onDeviceUnregistered(device);
				});
			},
			/**
			 * when a device is removed stop listening to all events on it
			 * @param device
			 * @private
			 */
			_onRemove: function (device) {
				this.stopListening(device);
			},

			getIT600s: function () {
				var filtered = this.filter(function (device) {
					return !!device.isIT600;
				});

				return new Models.AylaDeviceCollection(filtered);
			},
			
			getIT600sByCurrentGateway: function () {
				var filtered = this.filter(function (device) {
					if (device.get("gateway_dsn") === App.getCurrentGatewayDSN()) {
						return !!device.isIT600;
					}
				});

				return new Models.AylaDeviceCollection(filtered);
			},
			
			getNotIT600sByCurrentGateway: function () {
				var filtered = this.filter(function (device) {
					if (device.get("gateway_dsn") === App.getCurrentGatewayDSN()) {
						return !device.isIT600;
					}
				});

				return new Models.AylaDeviceCollection(filtered);
			},
			
			/**
			 * when a device is unregistered - event:unregistered:device remove it from the collection
			 * @param device
			 * @private
			 */
			_onDeviceUnregistered: function (device) {
				this.remove(device);
			},
			/**
			 * has this model been loaded from ayla yet?
			 * @returns {boolean}
			 */
			isLoaded: function () {
				return !!this.length; //todo
			},
			_buildDevice: function (data) {
				var device, DevClass,
					typeName = data.oem_model,
					typeNameTwo = data.product_name,
					currentModel = this.findWhere({key: data.key});

				// set and short circuit if device already exists
				if (!!currentModel) {
					currentModel.set(data);

					if (data.properties && _.isArray(data.properties)) {
						currentModel.parseDeviceProps(data.properties);
					}

					return currentModel;
				}

				// TODO This needs to be refactored to find a better way to detect what device type we are building
				if (data.device_type === "Gateway" || data.device_type === "Wifi") {
					DevClass = Models.GatewayAylaDevice;
				} else {
					//Checking for class existence.
					DevClass = this._deviceTypeMapping[typeNameTwo] || this._deviceTypeMapping[typeName];
				}

				if (!DevClass) {
					// Set any new and unknown device to the test device model so we can keep processing the page.
					// This also drops a app.log alerting us to go map this new device to a real device model
					DevClass = this._deviceTypeMapping.DEFAULTAYLADEVICE;
					App.warn("Unknown device type: " + typeName +
						", with DSN: " + typeNameTwo +
						". Assigning device to a test device model. TODO: Match this device type to an actual device model!");
				}

				device = new DevClass(data);

				if (data.properties && _.isArray(data.properties)) {
					device.parseDeviceProps(data.properties);
				}

				return device;
			},
			aParse: function (data) {
				var self = this;
				var out = [],
					fullDeviceCollection = App.salusConnector.getFullDeviceCollection();

				_.each(data, function (entry) {
					var deviceData = entry.device;

					var device = self._buildDevice(deviceData);

					out.push(device);

					if (!fullDeviceCollection.findWhere({key: device.get("key")})) {
						fullDeviceCollection.add(device);
					}
				});

				return out;
			},

			/**
			 * allows us to request "node" devices below a device. I think this means get all devices for a gateway. currently
			 * this does not work, but I think that is because I don't have the right device setup!!!
			 * @private
			 */
			_beforeFetch: function () {
				//TODO Nodes call fails if not a real gateway, not real gateways are most likely virtual devices
				//TODO Figure out how to handle this situation
				if (this._baseDeviceId && this.length > 0) {
//					var url = App.isEU ? AylaConfig.endpoints.device.listByNode : AylaConfig.endpoints.device.nodesAndProperties;
					var url = AylaConfig.endpoints.device.listByNode;
					this.url = AylaConfig.buildLocalURLForAyla((_.template(url)({id: this._baseDeviceId})));
				}
			},

			_getDeviceWeather: function (isLowPriority) {
				var collection = this.getGateways();

				var promiseArray = collection.map(function (gateway) {
					return gateway.getWeather(isLowPriority);
				});
				
				collection.destroy();

				return P.all(promiseArray);
			},

			_fetchDeviceMetaData: function (isLowPriority) {
				var promiseArray = this.map(function (device) {
					if (device.isGateway()) {
						return device.fetchMetaData(isLowPriority);
					}
				});

				return P.all(promiseArray);
			},

			_fetchDeviceProperties: function (isLowPriority) {
				if (this.propertiesPromise && this.propertiesPromise.isPending()) {
					return this.propertiesPromise;
				} else {
					this.propertiesPromise = P.all(this.map(function (device) {
						return device.getDeviceProperties(isLowPriority);
					}));

					return this.propertiesPromise;
				}
			},
			onCleanUp: function () {
				this.reset();
				this.stopListening();
				this.propertiesPromise = null;
				this._fetchPromise = null;
				this._detailPromise = null;
			}
		}).mixin([AylaBacked], {
			fetchUrl: AylaConfig.endpoints.device.list
		});
	});

	return App.Models.AylaDeviceCollection;
});
