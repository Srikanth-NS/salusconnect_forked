"use strict";
define([
    "app",
    "momentWrapper",
    "common/util/utilities",
    "common/AylaConfig",
    "common/model/ayla/mixin.AylaBacked",
    "common/model/ayla/AylaScheduleAction.model"
], function (App, moment, utils, AylaConfig, AylaBackedMixin) {
    App.module("Models", function (Models, App, B, Mn, $, _) {
        Models.AylaSchedule = B.Model.extend({
            defaults: {
                name: null,
                device_id: null,
                direction: "input",
                display_name: null,
                active: true,
                days_of_week: null,
                start_time_each_day: null,
                end_time_each_day: "23:59:59",
                schedule_actions: null,
                key: null,
                utc: false,
            },
            _weekTypeMap: {
                "fullWeek": [1, 2, 3, 4, 5, 6, 7],
                "workWeek": [2, 3, 4, 5, 6],
                "weekend": [1, 7]
            },
            initialize: function (data, options) {
                options = options || {};
                _.bindAll(this, "setScheduleActionId");
                this.set("device_id", options.deviceId || this.get("device_id") || null);
                this.listenTo(this, "change:key", this.setScheduleActionId);
//                this._deleteMe = false;
            },
            saveDeleteOrUpdate: function () {
//                if (this._deleteMe) {
//                    return this.remove();
//                } else 
                if (!this.get("key")) {
                    return this.add();
                } else {
                    return this.update();
                }
            },
            add: function () {
                var that = this,
                        url = _.template(AylaConfig.endpoints.device.schedules.create)(this.toJSON()),
                        payload = this._buildPayload();
                        //获取出那些被删除过的schedule(后台并不会删除,只是将active为false),然后拿来重复使用,就不需要再新增schedule了
//                        availableSchedules = App.salusConnector.getDevice(this.get("device_id")).getSchedules().getInactiveSchedules();
//
//                if (availableSchedules.length > 0) {
//                    var reusedSchedule = availableSchedules[0];
//                    reusedSchedule.setFromCopy(this.deepCopy());
//                    reusedSchedule._deleteMe = false;
//                    reusedSchedule.set('active', true);
//                    if (this.getScheduleData() != undefined) {
//                        reusedSchedule.setScheduleData(this.getScheduleData());
//                    }
//                    return reusedSchedule.update().then(function () {
//                        that.destroy();
//                    });
//                }

                return App.salusConnector.makeAjaxCall(url, payload, "POST", "json").then(function (data) {
                    that.removeSaveMeMarker();
                    that.set(that.aParse(data));
                    if(that.getScheduleData()){
                        that.getScheduleData().set("key", data.schedule.key);
                    }
                            
                });
            },
            update: function () {
                var that = this,
                        url = _.template(AylaConfig.endpoints.device.schedules.update)(this.toJSON()),
                        payload = this._buildPayload(false);

                return App.salusConnector.makeAjaxCall(url, payload, "PUT", "json").then(function (data) {
                    that.removeSaveMeMarker();
                    that.set(that.aParse(_.omit(data, "schedule_actions")));
                    that.setScheduleActionId(that.get("key"));
                    if (that.getScheduleData() != undefined) {
                        that.getScheduleData().set("key", data.schedule.key);
                        that.setScheduleData(undefined);
                    }

                    // inactive schedules are for recycling so it doesn't
                    // need to save the schedule actions as they will change later
//                    if (that.get("active")) {
//                        return that.get("schedule_actions").saveAll();
//                    }
                });
            },
            remove: function () {
                this.set("active", false);
                return this.update();
            },
            loadActions: function (isLowPriority) {
                return this.get("schedule_actions").load(isLowPriority);
            },
            _buildScheduleActions: function (deviceKey, scheduleKey) {
                var actionsArray = App.salusConnector.getDevice(deviceKey).getBaseScheduleProps(scheduleKey);
                return new App.Models.AylaScheduleActionCollection(actionsArray, {
                    schedule_id: scheduleKey
                });
            },
            _buildPayload: function (shouldOmitScheduleActions) {
                this._setName();
                var payload = this.toJSON();

                payload = this.wrapApiData(_.pick(payload, ["name", "active", "days_of_week", "direction", "device_id", "start_time_each_day", "end_time_each_day", "utc"]));

                if (!shouldOmitScheduleActions) {
                    this.get("schedule_actions").each(function (action){
                        action.set("at_start",true);
                        action.set("in_range",false);
                    });
                    payload.schedule.schedule_actions = this.get("schedule_actions").toJSON();
                }

                // cleaning up any old schedules that may have been saved in utc mode
                if (payload.utc) {
                    payload.utc = false;
                }

                return payload;
            },
            setFromData: function (data, weekType) {
                var scheduleActions = this.get("schedule_actions");
                this.set("start_time_each_day", data.time);
                this.set("days_of_week", _.isNumber(weekType) ? [weekType] : this._weekTypeMap[weekType]);
                if (!scheduleActions || scheduleActions.length == 0) {
                    scheduleActions = this._buildScheduleActions(this.get("device_id"));
                    this.set("schedule_actions", scheduleActions);
                }

                scheduleActions.each(function (action) {
                    action.set("value", data.value);
                });
            },
            aParse: function (data) {
                var out, schedule = data.schedule ? data.schedule : data,
                        scheduleActions = this.get("schedule_actions");


//				if (!scheduleActions) {
                if (!scheduleActions || scheduleActions.length == 0) {
                    var actionsArray = App.salusConnector.getDevice(schedule.device_id).getBaseScheduleProps(schedule.key || null);
                    scheduleActions = new App.Models.AylaScheduleActionCollection(actionsArray, {
                        schedule_id: schedule.key || null
                    });
                }

                out = _.omit(schedule, ["schedule_actions"]);

                out.schedule_actions = scheduleActions;

                return out;
            },
            setFromCopy: function (copyJSON) {
                this.set(this.aParse(copyJSON));
                this.updateActionsPostCopy(copyJSON);
            },
            deepCopy: function () {
                var model = _.omit(this.toJSON(), ["schedule_actions", "key", "name"]);
                model.schedule_actions = this.get("schedule_actions").deepCopy();
                return model;
            },
//            markForDeletion: function () {
//                this._deleteMe = true;
//            },
            //标记为需要save的
            markForSaveMe: function () {
                this._saveMe = true;
            },
            removeSaveMeMarker: function (){
                this._saveMe = false;
            },
            isSaveMe: function (){
                return !!this._saveMe;
            },
            setScheduleActionId: function () {
                this.get("schedule_actions").setScheduleActionId(this.get("key"));
            },
            updateActionsPostCopy: function (copyJSON) {
                var actionValue = copyJSON.schedule_actions[0].value;

                this.get("schedule_actions").each(function (action) {
                    action.set("value", actionValue);
                });
            },
            _setName: function () {
                if (!this.get("name")) {
//                    this.set("name", atob(utils.md5HashString(JSON.stringify(this) + new Date().toString()))); // btoa converts to base64 which should shrink the size enough to meet aylas requirements
                    this.set("name", utils.md5HashString(JSON.stringify(this) + new Date().toString() + Math.random() * 100000).substr(0, 25));
                }
            },
            //保存当前schedule对应的界面上的schedule的条目对象,方便更新他的key,否则再次apply时,没有key的会被置为delete
            setScheduleData: function (scheduleData) {
                this.scheduleData = scheduleData;
            },
            getScheduleData: function (scheduleData) {
                return this.scheduleData;
            },
        }).mixin([AylaBackedMixin], {
            apiWrapperObjectName: "schedule"
        });
    });

    return App.Models.AylaSchedule;
});