"use strict";

/**
 * represents an ayla device.
 * todo: we may need to break these out by type
 */
define([
    "app",
    "underscore",
    "backbone",
    "bluebird",
    "common/constants",
    "common/AylaConfig",
    "common/config",
    "common/model/ayla/mixin.AylaBacked",
    "common/model/ayla/mixin.metaDataProperties",
    "common/model/ayla/DeviceProperty.model",
    "common/model/ayla/LinkedDeviceProperty.model",
    "common/model/ayla/mixin.IsInDashboard"
], function (App, _, B, P, constants, AylaConfig, config, AylaBacked, MetaDataMixin, DevicePropModel, LinkedDevicePropModel) {

    /**
     * Todo: this may need some intermediate steps between here and devices. Things that may be worth splitting out
     * - wifi vs non-wifi devices (things like mac, ip, lan_enabled) would then move to the wifi device maybe.
     * - base thermostat
     *
     * Also, what is the thing I key off of to determine type? is it product_class, template_id, oem_model?
     */

    var BaseAylaDevice = function (options) {
        _.extend(options, {
            fetchUrl: AylaConfig.endpoints.device.detail,
            apiWrapperObjectName: "device",
            localOnlyProperties: ["address", "name"],
            unsupportedProps: [],
            metaDataProperties: ["alias"],
            metaDataFetchUrl: AylaConfig.endpoints.device.metaData.fetch,
            metaDataCreateUrl: AylaConfig.endpoints.device.metaData.create,
            metaDataSaveUrl: AylaConfig.endpoints.device.metaData.save
        });

        this.mixin([
            AylaBacked,
            MetaDataMixin,
            App.Models.IsInDashboardMixin
        ], options);

        this.setDefaults({
            // the property call is a way to get details about the properties of a device
            _propertyListUrl: _.template(AylaConfig.endpoints.device.property.list),
            _physAddressUrl: _.template(AylaConfig.endpoints.device.address),
            registrationTypes: {
                "SameLAN": "SameLAN",
                "ButtonPush": "ButtonPush",
                "APMode": "APMode",
                "Display": "Display",
                "Dsn": "Dsn",
                "None": "None",
                "Node": "Node"
            },
            modelType: constants.modelTypes.GENERIC,
            ruleRefType: "device",
            //TODO: wire this up once we can test it
            _detailFields: ["registration_type", "ans_server"],
            defaults: {
                /**
                 * This list is based on the test (virtual) gateway. It is unclear if all of these will stay in the base
                 * class. we will need to understand what devices look like.
                 */
                // list call data
                product_name: null, // string : appears to be friendly name
                model: null, // ???
                dsn: null, // ??
                oem_model: null, // ???
                template_id: null, // integer
                mac: null, // mac address, string, nullable
                connected_at: null, // datetime:
                key: null, // integer: ???
                has_properties: null, // boolean: if we have properties for the device
                connection_status: null, // enum? : "Online" | ??
                device_type: null, // enum?: "Wifi" | ??
                lan_enabled: null, // boolean
                lat: null, // ??
                lng: null, // ??
                utc_offset: null, // pull utc offset from ayla
                dst_active: null, // daylight savings time
                product_class: null, // ??
                lan_ip: null, // ip address
                gateway_dsn: null,
                grant: null, // Valid on a shared device.
                

                // extended data
                regtoken: null, // used to register a device to an account

                // local data
                address: null, // the address object - street address
                displayName: null, // display name which will update with alias and product_name and can be used with stickit

                //meta-data properties
                alias: null,
                // non-ayla properties for device category id, icon url, and category name
                device_category_id: null,
                device_category_icon_url: null,
                device_category_type: null,
                // same as above but for equipment page
                equipment_page_icon_url: null
            },
            /**
             * This is an attempt to implement the "register" call to discover devices. It does not work as currently it only
             * returns: "Status:404 Not Found, Text:No candidates found" It is unclear if I can test this without real devices
             *
             *  this method is complex and poorly defined within ayla: see AylaSericeAPISpecificationV1.10
             * section 5.8
             * @returns {*}
             */
            discover: function () {
                var data = {};
                var dsn, regtoken, ip, time;

//				if (true) {
//					throw new Error("todo: we can't test this against current virtual devices. Not implemented!");
//				}

                dsn = this.get("dsn");
                if (dsn) {
                    data.dsn = dsn;
                }

                regtoken = this.get("regtoken");
                if (regtoken) {
                    data.regtoken = regtoken;
                }

                ip = this.get("ip");
                if (ip) {
                    data.ip = ip;
                }

                // TODO: how is this used?
                time = this.get("time");
                if (time) {
                    data.time = time;
                }

                return App.salusConnector.makeAjaxGet(AylaConfig.endpoints.device.register, null);
            },
            _setDeviceJsonData: function (devData) {
                this.set(devData.device);
                return devData;
            },
            /**
             * add a device to the ayla profile
             * @returns {*}
             */
            add: function () {
                var that = this;

                var data = {};
                var dsn, regtoken, ip;

                dsn = this.get("dsn");
                if (dsn) {
                    data.dsn = dsn;
                }

                regtoken = this.get("regtoken");
                if (regtoken) {
                    data.regtoken = regtoken;
                }

                ip = this.get("ip");
                if (ip) {				//optional, without this, it will be inferred from our IP
                    data.ip = ip;
                }

                data = this.wrapApiData(data);
                return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.device.add, data, "POST")
                        .then(that._setDeviceJsonData)
                        .then(function (/*devData*/) {
                            that.trigger("created", that);
                            App.salusConnector.addDeviceToCollection(that);
                            return P.all([that._persistMetaData(), App.salusConnector.getShareCollection().addDeviceToAllShares(that)]);
                        });
            },
			saveProductName: function () {
				var url, payload;
				url = this._prepareUrl(_.template(AylaConfig.endpoints.device.save)),
				payload = {
					device: {
						product_name: this.get("product_name")
					}
				};
				return App.salusConnector.makeAjaxCall(url, payload, "PUT", "json");
            },
            _deleteDevice: function () {
                var that = this,
                        unregisterUrl = this._prepareUrl(_.template(AylaConfig.endpoints.device.remove)),
                        promise;

                if (this._onBeforeUnregister) {
                    promise = this._onBeforeUnregister();
                } else {
                    promise = P.resolve();
                }

                return promise.then(function () {
                    return App.salusConnector.makeAjaxCall(unregisterUrl, null, "DELETE", "text").then(function () {
                        return that._onUnregistered();
                    });
                });
            },
            _createNewMissingPropertyError: function (propName) {
                return new Error("Property " + propName + " does not exist on this device (" + this.get("dsn") + ").");
            },
            /**
             * A check to see if the device is shared to the current user.
             * */
            isSharedDevice: function () {
                return !!this.get("grant");
            },
            /**
             * remove a device from the ayla profile
             */
            unregister: function () {
                if (this.isGateway()) {
                    return this._unregisterGateway();
                } else {
                    return this._unregisterDevice();
                }
            },
            _unregisterGateway: function () {
                var that = this,
                        setPropertyPromise,
                        unregisterPromise = P.defer();

                if (!this.isGateway() || this.isSharedDevice()) {
                    return P.reject("Not an owned gateway");
                }

                var gatewayNode = this.getGatewayNode(),
                        factoryResetProp = gatewayNode ? gatewayNode.get("SetFactoryReset_d") : false;

                // if gateway, set gateway nodes prop SetFactoryReset_d
                // using deferred promise pattern - normally shouldn't have to do this but we want to make sure
                // that setFactoryReset gets synced on ayla
                if (gatewayNode && factoryResetProp) {
                    setPropertyPromise = gatewayNode.setProperty("SetFactoryReset_d", 1);
                } else {
                    App.warn("Did not set SetFactoryReset_d on gateway node!");
                    setPropertyPromise = P.resolve();
                }

                return setPropertyPromise.then(function () {
                    
                    // wait for sync
                    if (factoryResetProp && setPropertyPromise) {
                        that.listenTo(factoryResetProp, "propertiesSynced", function () {
                            unregisterPromise.resolve();
                        });

                        // if factory reset already 1
                        if (factoryResetProp && factoryResetProp.getProperty()) {
                            unregisterPromise.resolve();
                        }
                    } else {
                        // no factory reset property
                        unregisterPromise.resolve();
                    }

                    // we set the property, remove the gateway
                    // TODO: we may need to check that all nodes are gone
                    // 1/26/16 says next GW firmware will setLeaveNetwork on all nodes,
                    return unregisterPromise.promise.then(that._deleteDevice).then(function () {
                        App.vent.trigger("gateway:removed", that);
                    });
                });
            },
            //Bug_CASBW-216 start
            setLeaveNetwork: function () {
                var that = this,
                        setPropPromise,
                        leaveNetwork = this.get("LeaveNetwork");

                if (this.isSharedDevice()) {
                    return P.reject("Not an owned device");
                }

                if (leaveNetwork) {
                    setPropPromise = this.setProperty("LeaveNetwork", 1);
                } else {
                    App.warn("Did not set LeaveNetwork for unregistering this device!");
                    setPropPromise = P.resolve();
                }

                return setPropPromise;
                   
                    
            },
            
            _unregisterDevice: function () {
                var that = this,
                    unregisterPromise,
                    factoryResetUrl = this._prepareUrl(_.template(AylaConfig.endpoints.device.factory_reset));

                if (this.isSharedDevice()) {
                    return P.reject("Not an owned device");
                }
                
                 // set up deferred promise, so we can wait for the properties to be synced
               unregisterPromise = P.defer();
               
               unregisterPromise.resolve();

                // resolved when leaveNetwork is 1
                return unregisterPromise.promise.then(that._unregisterDeviceMetaData).then(function () {
                    return that.checkAndRemoveSingleControl();
                }).then(function (){
                    that.chechAndDisableOneTouch();
                }).then(function () {
                    return that.checkAndRemoveGroup();
                }).then(function () {
                    // call factory_reset.json on device to unregister
                    return App.salusConnector.makeAjaxCall(factoryResetUrl, null, "PUT", "text").then(that._onUnregistered);
                });
      
            },
            
//            _unregisterDevice: function () {
//                var that = this,
//                        setPropPromise,
//                        unregisterPromise,
//                        leaveNetwork = this.get("LeaveNetwork"),
//                        factoryResetUrl = this._prepareUrl(_.template(AylaConfig.endpoints.device.factory_reset));
//
//                if (this.isSharedDevice()) {
//                    return P.reject("Not an owned device");
//                }
//
//                // set up deferred promise, so we can wait for the properties to be synced
//                unregisterPromise = P.defer();
//
//                if (leaveNetwork) {
//                    that.listenTo(leaveNetwork, "propertiesSynced", function () {
//                        unregisterPromise.resolve();
//                    });
//                    setPropPromise = this.setProperty("LeaveNetwork", 1);
//                } else {
//                    App.warn("Did not set LeaveNetwork for unregistering this device!");
//                    setPropPromise = P.resolve();
//                }
//
//                return setPropPromise.then(function () {
//                    // wait for properties to be synced
//                    if (!leaveNetwork || !setPropPromise) {
//                        // devices doesn't have leaveNetwork
//                        unregisterPromise.resolve();
//                    }
//
//                    // resolved when leaveNetwork is 1
//                    return unregisterPromise.promise.then(that._unregisterDeviceMetaData).then(function () {
//                        return that.checkAndRemoveSingleControl();
//                    }).then(function (){
//                        that.chechAndDisableOneTouch();
//                    }).then(function () {
//                        return that.checkAndRemoveGroup();
//                    }).then(function () {
//                        // call factory_reset.json on device to unregister
//                        return App.salusConnector.makeAjaxCall(factoryResetUrl, null, "PUT", "text").then(that._onUnregistered);
//                    });
//                });
//            },

            //Bug_CASBW-216 end
            /**
             * 检查当前温控器所在single control中是否是最后两个如果是则把single control删除
             * @returns {undefined} promise
             */
            checkAndRemoveSingleControl: function () {
                if (this.get("device_category_type") === constants.categoryTypes.THERMOSTATS) {

                    var tscCollection = App.salusConnector.getTSCCollection();

                    var singleControl = tscCollection.getSingleControlByThermostat(this);

                    if (singleControl && singleControl.get("devices").length == 2) {
                        return singleControl.unregister();
                    }
                }

                var promise = P.defer();
                promise.resolve();
                return promise;
            },
            /**
             * 检查并disable存在该设备的one touch
             * @returns {unresolved}
             */
            chechAndDisableOneTouch: function (){
                var euid,rules=App.salusConnector.getDisplayRuleCollection();
                
                if(this.get("EUID")){
                    euid=this.get("EUID").get("value");
                }

                if(rules.models){
                    var disableRules=_.filter(rules.models,function (rule){
                        var conditions=_.find(rule.get("conditions").models,function (condition){
                            if(condition.get("EUID")===euid){
                                return true;
                            }
                        });
                        
                        if(conditions){
                            return true;
                        }
                        
                        var actions=_.find(rule.get("actions").models,function (action){
                            if(action.get("EUID")===euid){
                                return true;
                            }
                        });
                        
                        if(actions){
                            return true;
                        }
                        
                        var delayedActions=_.find(rule.get("delayedActions").models,function (delayedAction){
                            if(delayedAction.get("data").EUID===euid){
                                return true;
                            }
                        });
                        
                        if(delayedActions){
                            return true;
                        }
                        
                    });
                    
                    _.map(disableRules,function (rule){
                       if(rule.get("active")){
                           rule.toggleActive();
                       } 
                    });
                    
                    //遍历disableRules全部置为disabled
                }
            },
            
            /**
             * 检查当前设备所在的group是否是最后一个,如果是则删除group
             * @returns {undefined}
             */
            checkAndRemoveGroup: function () {
                var groupCollection = App.salusConnector.getGroupCollection();
                var groups = groupCollection.getGroupsByDevice(this);
                var promises = [];
                
                _.each(groups, function(group) {
                    if (group && group.get("devices").length === 1) {
                        promises.push(group.unregister());
                    }
                });
                return P.all(promises);
            },
            /**
             * things to do on unregister
             * Note - GatewayAylaDevice provides its' own!
             * @private
             */
            _onUnregistered: function () {
                this.set("unregistered", true);
                this.trigger("sync", this);


                if (this.isThermostat()) {
                    App.salusConnector._aylaConnector.trigger("remove:thermostat", this);
                }

                this.trigger("unregistered:device", this);

                if (this.modelType === constants.modelTypes.IT600TRV) {
                    App.salusConnector._aylaConnector.trigger("remove:trv", this);
                }

                App.salusConnector.getSessionUser().get("tileOrderCollection").findAndRemove(this, "device");
                this.destroy();
            },
            /**
             * get all metaData keys, iterate and delete them
             * @returns {*}
             */
            _unregisterDeviceMetaData: function () {
                var that = this, deletePromises, dsn = this.get("dsn"),
                        fetchUrl = _.template(AylaConfig.endpoints.device.metaDataApi.fetchAll)(this.toJSON());

                return App.salusConnector.makeAjaxCall(fetchUrl, null, "GET", "json").then(function (metas) {
                    deletePromises = _.map(metas, function (meta) {
                        var url = _.template(AylaConfig.endpoints.device.metaDataApi.delete)({
                            dsn: dsn,
                            propKey: meta.datum.key
                        });

                        return App.salusConnector.makeAjaxCall(url, null, "DELETE", "text");
                    });

                    return P.all(deletePromises).then(function () {
                        that.trigger("sync", that);
                    });
                });
            },
            refresh: function () {
                var that = this;
                this._detailPromise = this.fetch().then(function () {
                    return P.all([that.getDeviceProperties(), that.fetchMetaData()]);
                });
                return this._detailPromise;
            },
            /**
             * after creation, load details and save the address if we have one
             * @private
             */
            _onCreated: function () {
                this.refresh();

                var addr = this.get("address");

                if (addr) {
                    addr.persist();
                }
            },
            getEUID: function () {
                return this.getPropertyValue("EUID") || null;
            },
            /**
             * this is mostly a development tool to ask ayla about the properties of a device. Note!! I have never
             * actually seen it return anything interesting, but it succeeds.
             * @returns {*} a promise for the request
             */
            getDeviceProperties: function (isLowPriority) {
                var promise, that = this;
                if (App.isCordovaApiReady("getProperties")) {
                    var key = this.get("key");
                    promise = App.salusConnector.makeCordovaCall("getProperties", [key]);
                } else {
                    promise = App.salusConnector.makeAjaxCall(this._propertyListUrl(this._getUrlRenderModel()), null, "GET", null, {isLowPriority: isLowPriority});
                }
				
				//BUG_SCS-3279 start
		 		promise.catch(function(e) {
					if(e.statusText === "Not Found" || e.statusText === "Forbidden"){
						that._onUnregistered();
						return promise;                        
					}  
				});
				var tempPropPromise = promise.then(that._mapPropArray).then(that.parseDeviceProps);
//              var tempPropPromise = promise.then(that._mapPropArray).then(that.parseDeviceProps).then(function(){
//					if (that.get("EUID") && _.isNull(that.getPropertyValue("EUID")) && that.isOnline() && !that.isLeaveNetwork()) {
//						that.setProperty("SetRefresh_d", 1);
//					}
//				});
				//BUG_SCS-3279 end
				
                if (that._propPromise) {
                    tempPropPromise.then(function () {
                        that._propPromise = tempPropPromise;
                    });
                } else {
                    that._propPromise = tempPropPromise;
                }

                this.trigger("loading:deviceProperties");

                return tempPropPromise;
            },
            getDevicePropertiesPromise: function () {
				if (this.get("gateway_dsn") !== App.getCurrentGatewayDSN()) {
					return P.Promise.resolve();
				}
                if (this._propPromise && this._propPromise.isFulfilled()) {
                    return this._propPromise;
                } else if (this._propPromise && this._propPromise.isPending()) {
					return this._propPromise;
				} else {
                    return this.getDeviceProperties(true);
                }
            },
            toggleSetIndicator: function () {
                var that = this;

                if (!this.get("SetIndicator")) {
                    return P.reject("No SetIndicator property on this device");
                }

                return this.getDevicePropertiesPromise().then(function () {
                    var val = that.getPropertyValue("SetIndicator");

                    if (_.isArray(val)) {
                        // eMeter - if any
                        if (_.some(val, function (individualVal) {
                            return !!individualVal;
                        })) {
                            return that.setProperty("SetIndicator", 0);
                        } else {
                            return that.setProperty("SetIndicator", config.setIndicatorPeriodLength);
                        }
                    } else if (!!val) {
                        return that.setProperty("SetIndicator", 0);
                    } else {
                        // set a timeout to turn SetIndicator back off after 60 seconds
                        that.setIndicatorTimeout = setTimeout(that.undoSetIndicator, config.setIndicatorPeriodLength * 1000);

                        return that.setProperty("SetIndicator", config.setIndicatorPeriodLength);
                    }
                });
            },
            isOnline: function () {
                return this.get("connection_status") === "Online";
            },
            isLeaveNetwork: function () {
                return this.get("LeaveNetwork") === 1;
            },
            undoSetIndicator: function () {
                var that = this;

                if (!this.get("SetIndicator")) {
                    return P.reject("No SetIndicator property on this device");
                }

                clearTimeout(that.setIndicatorTimeout);

                return this.getDevicePropertiesPromise().then(function () {
                    return that.setProperty("SetIndicator", 0);
                });
            },
            parseDeviceProps: function (properties) {
                var that = this;

                if (this.unsupportedProps && this.unsupportedProps.length) {
                    properties = this._filterUnsupportedProperties(properties);
                }

                var readProps = properties.filter(function (property) {
                    return property.direction === "output";
                }),
                        writeProps = properties.filter(function (property) {
                            return property.direction === "input";
                        });

                _.each(readProps, function (prop) {
                    writeProps = that._parseDeviceProperty(prop, writeProps);
                });

                _.each(writeProps, function (prop) {
                    that._parseDeviceProperty(prop);
                });
            },
            _parseDeviceProperty: function (prop, writeProps) {
                var that = this, linkedWriteProp,
                        key = prop.display_name, setKey = "Set" + key;

                var getWritePropIfExists = function (key) {
                    var contains = false, writeProp = null;

                    contains = _.reduce(writeProps, function (contains, thisWriteProp) {
                        var returnVal;
                        if (contains) {
                            returnVal = contains;
                        } else if (thisWriteProp.display_name === key) {
                            returnVal = true;
                            writeProp = thisWriteProp;
                        } else {
                            returnVal = false;
                        }

                        return returnVal;
                    }, contains);

                    if (contains) {
                        writeProps = _.without(writeProps, writeProp);
                        return writeProp;
                    } else {
                        return false;
                    }
                };

                if (writeProps && writeProps.length > 0) {
                    linkedWriteProp = getWritePropIfExists(setKey);

                    if (linkedWriteProp) {
                        this.updateOrCreateProp(key, [null, {
                                getterProp: prop,
                                setterProp: linkedWriteProp
                            }], LinkedDevicePropModel);
                    } else {
                        this.updateOrCreateProp(key, [prop], DevicePropModel);
                    }
                } else {
                    this.updateOrCreateProp(key, [prop], DevicePropModel);
                }

				if (!this.get(key).hasListenToChangeValue) {
					this.listenTo(this.get(key), "change:value", function () {
						var prop = that.get(key);
						if (!prop.dontTriggerChange) {
							that.trigger("change:" + key, prop);
						}
					});
					this.get(key).hasListenToChangeValue = true;
				}

                return writeProps || true;
            },
            _filterUnsupportedProperties: function (properties) {
                var getter, setter;

                // each of the unsupported props, look for getter and setter
                _.each(this.unsupportedProps, function (propName) {
                    getter = _.findWhere(properties, {display_name: propName});
                    setter = _.findWhere(properties, {display_name: "Set" + propName});

                    if (getter) {
                        properties = _.without(properties, getter);
                    }

                    if (setter) {
                        properties = _.without(properties, setter);
                    }
                });

                return properties;
            },
            updateOrCreateProp: function (key, args, Class) {
                if (this.get(key)) {
                    this.get(key).updateData.apply(null, args);
                } else {
                    this.set(key, new Class(args[0], args[1]));
                }
            },
            setProperty: function (propName, value) {
                var property = this.get(propName);

                if (!property) {
                    throw this._createNewMissingPropertyError(propName);
                }

                return property.setProperty(value);
            },
            getPropertyValue: function (propName) {
                var property = this.get(propName);

                if (!property) {
                    throw this._createNewMissingPropertyError(propName);
                }

                return property.getProperty();
            },
            getSetterPropNameIfPossible: function (propName) {
                var property = this.get(propName);

                if (!property) {
//					throw this._createNewMissingPropertyError(propName);
                    //不存在的话,举个例子,如要查找的是OnOff,则有可能并没有OnOff这个属性,但有可能会有SetOnOff这个属性,此时SetOnOff就直接get("SetOnOff")就可以获取得到
                    property = this.get("Set" + propName);
                    if (property) {
                        return property.get("name");
                    } else {
                        return "";
                    }
                } else if (property.isLinkedDeviceProperty) {
                    return property.get("setterProperty").get("name");
                } else if(property.models && property.models.length > 0 && property.models[0].get("setterProperty")){
                    return property.models[0].get("setterProperty").get("name");
                } else {
                    return property.get("name") || ((property.models && property.models.length > 0) ? property.models[0].get("name") : property.get("name"));
                }
            },
            getPropertyName: function (propName) {
                var property = this.get(propName);

                if (!property) {
                    throw this._createNewMissingPropertyError(propName);
                }

                return property.get("name") || ((property.models && property.models.length > 0) ? property.models[0].get("name") : property.get("name"));
            },
            hasProperty: function (propName) {
                return !!this.get(propName) && this.get(propName) instanceof B.Model;
            },
            getDisplayName: function () {
                return this.get("product_name");
            },
            _setDisplayName: function () {
                this.set("name", this.getDisplayName());
            },
            isGateway: function () {
                return this.modelType === constants.modelTypes.GATEWAY;
            },
            isSmartPlug: function () {
                var modelTypes = constants.modelTypes;

                return this.modelType === modelTypes.SMARTPLUG || this.modelType === modelTypes.MINISMARTPLUG;
            },
            isThermostat: function () {
                var modelTypes = constants.modelTypes;

                return this.modelType === modelTypes.THERMOSTAT || this.modelType === modelTypes.IT600THERMOSTAT;
            },
            isWindowSensor: function (){
                var modelTypes = constants.modelTypes;

                return this.modelType === modelTypes.WINDOWMONITOR;
            },
            supportsRecommendedRules: function () {
                var able = [
                    constants.modelTypes.SMARTPLUG,
                    constants.modelTypes.MINISMARTPLUG,
                    constants.modelTypes.THERMOSTAT,
                    constants.modelTypes.WATERHEATER
                ];

                return _.contains(able, this.modelType);
            },
            supportsSchedules: function () {
                var able = [
                    constants.modelTypes.SMARTPLUG,
                    constants.modelTypes.MINISMARTPLUG,
                    constants.modelTypes.IT600THERMOSTAT,
                    constants.modelTypes.THERMOSTAT,
                    constants.modelTypes.WATERHEATER
                ];

                return _.contains(able, this.modelType);
            },
            _mapPropArray: function (propArray) {
                return propArray.map(function (prop) {
                    return prop.property;
                });
            }
        });

        this.before("initialize", function () {
            var that = this;

            _.bindAll(this,
                    "parseDeviceProps",
                    "getDeviceProperties",
                    "_setDeviceJsonData",
                    "_onUnregistered",
                    "_setDisplayName",
                    "_deleteDevice",
                    "_unregisterDeviceMetaData",
                    "toggleSetIndicator",
                    "undoSetIndicator"
                    );

            this.listenTo(this, "created", this._onCreated);

            this.set("name", this.get("product_name"));
            this.listenTo(this, "change:product_name", this._setDisplayName);
            this.listenTo(this, "change:AppData_c", function () {
				if (_.isString(that.getPropertyValue("AppData_c"))) {
					that.set(JSON.parse(that.getPropertyValue("AppData_c")));
				}
            });

            this.unsupportedProps = _.union(this._unsupportedProps || [], options.unsupportedProps);
        });
		
		this.before("destroy", function () {
			var schedules = this.get("schedules");
			
			if (schedules && schedules.destroy) {
				schedules.destroy();
			}

			this._propPromise = null;
			this._detailPromise = null;
		});
        
        /**
         * set the ID attribute - this is used for defining the primary key.
         * Ayla has both "id" and "key" and both are the same, but "key" comes down in the summary call where ID is only the detail call
         */
        this.clobber("idAttribute", "key");

        /**
         * decide if we are loaded
         * TODO: might be nice to move this and the getAsPromise singleton wrapper stuff into aylaBacked
         */
        this.clobber("isLoaded", function () {
            return this._detailPromise && this._detailPromise.isLoaded();
        });

        this.clobber("persist", function () {
            this._persistMetaData();
            if (this.get("AppData_c")) {
                return this.get("AppData_c").setProperty(JSON.stringify(this.pick(this._metaDataProperties)));
            }

            return P.resolve("No AppData_c Properties");
        });
    };

    return BaseAylaDevice;
});
