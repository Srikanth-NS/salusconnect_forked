"use strict";

/**
 * The salus connector is for talking to TBD salus services. The intent is that this is a singleton for the app.
 * TODO:
 *  - should salus connector broker ayla connector calls, or just expose the ayla connector?
 */
define([
	"app",
	"bluebird",
	"common/constants",
	"common/AylaConfig",
	"common/util/PromiseHelper",
	"common/model/ayla/DeviceCollection",
	"common/model/ayla/User",
	"common/model/ayla/GroupCollection",
	"common/model/ayla/AlertCollection",
	"common/model/ayla/ShareCollection",
	"common/model/salusWebServices/NotificationCollection",
	"common/util/DataRefreshManager"
], function (App, P, constants, AylaConfig, PromiseHelper, AylaDeviceCollection, AylaUser, AylaGroupCollection, AylaAlertCollection, ShareCollection, NotificationCollection) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.AylaConnector = B.Model.extend({
			defaults: {
				currentUser: null,		// the current user
				currentPhone: null,		// the current phone (used for app use case)
				deviceCollection: null,
				groupCollection: null,
				alertCollection: null,
				notificationCollection: null,
				shareCollection: null
			},
			initialize: function (opts, sessionManager) {
				_.bindAll(this,
						"_cleanUpAylaProperties",
						"saveDataCollection",
						"fetchDataCollection",
						"_createDataCollection",
						"doOnGatewayChange",
						"updateTileOrderGroup"
				);

				var deviceCollection = new AylaDeviceCollection();
				deviceCollection.isFullCollection = true;
				
				this.set("deviceCollection", deviceCollection);
				this.set("groupCollection", new AylaGroupCollection());
				this.set("alertCollection", new AylaAlertCollection());
				this.set("notificationCollection", new NotificationCollection());

				//notificationCollection.fillWithCannedData(); //TODO Hook up to api

				this.depPromise = {};

				this.set("shareCollection", new ShareCollection());

				this._sessionManager = sessionManager;
				this.dataRefreshManager = new Models.DataRefreshManager();
				
				this.listenTo(this._sessionManager, "onLogin", this._doOnLogin);
				
				if (this._sessionManager.get("isLoggedIn")) {
					this._doOnLogin();
				}
			},

			saveDataCollection: function (dataCollectionOff) {
				var that = this, payload = {
					datum: {
						value: _.isNumber(dataCollectionOff) ? dataCollectionOff : 1
					}
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.dataCollectionOff.save, payload, "PUT", null, null, [404])
						.catch(function (err) {
							if (err.status === 404) {
								return that._createDataCollection(1);
							}

							throw err;
						}).then(function (data) {
							var dataCollectionOff = JSON.parse(data.datum.value),
									currentUser = that.get("currentUser");
							if (currentUser) {
								currentUser.set("dataCollectionOff", dataCollectionOff);
							}
							App.setDataCollectionOff(!!dataCollectionOff);
							window.localStorage.setItem("dataCollectionOff", !!dataCollectionOff);

							return dataCollectionOff;
						});
			},

			fetchDataCollection: function () {
				var that = this,
						localDataCollecBool = window.localStorage.getItem("dataCollectionOff");

				if (_.isString(localDataCollecBool)) {
					localDataCollecBool = localDataCollecBool === "true";
					App.setDataCollectionOff(localDataCollecBool);
					window.localStorage.setItem("dataCollectionOff", localDataCollecBool);

					App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.dataCollectionOff.fetch, null, "GET", null, null, [404]).then(function (data) {
						var dataCollectionOff = !!JSON.parse(data.datum.value);

						if (localDataCollecBool !== dataCollectionOff) {
							window.localStorage.setItem("dataCollectionOff", dataCollectionOff);
							App.setDataCollectionOff(dataCollectionOff);
							App.navigate("");

							return dataCollectionOff;
						}

					}).catch(function (err) {
						if (err.status === 404) {
							return that._createDataCollection(0).then(function (dataCollection) {
								var dataCollectionOff = !!JSON.parse(dataCollection.datum.value);

								window.localStorage.setItem("dataCollectionOff", dataCollectionOff);
								App.setDataCollectionOff(!!dataCollectionOff);
							});
						}

						throw err;
					});

					return P.resolve(localDataCollecBool);
				}

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.dataCollectionOff.fetch, null, "GET", null, null, [404]).catch(function (err) {
					if (err.status === 404) {
						return that._createDataCollection(0);
					}

					throw err;
				}).then(function (data) {
					var dataCollectionOff = JSON.parse(data.datum.value);

					App.setDataCollectionOff(!!dataCollectionOff);
					window.localStorage.setItem("dataCollectionOff", !!dataCollectionOff);

					return !!dataCollectionOff;
				});
			},

			_createDataCollection: function (dataCollectionOff) {
				var payload = {
					datum: {
						key: "dataCollectionOff",
						value: dataCollectionOff
					}
				};
				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.dataCollectionOff.create, payload, "POST");
			},

			/**
			 * things to do once login is triggered
			 * @private
			 */
			_doOnLogin: function () {
				var that = this;

				this.doOnLoginPromise = this.fetchDataCollection().then(function (dataCollectionOff) {
					if (!dataCollectionOff) {
						that.set("currentUser", new AylaUser());

						var userPromise = that.userFetchPromise = that.get("currentUser").fetch().then(that.get("currentUser").fetchMetaData);

						//Set user's language perference.
						userPromise = userPromise.then(function (data) {
							var language = that.get("currentUser").get("language");
							if (language) {
								return App.changeLanguage(language).catch(function (err){
									//catch error and do nothing. Will default to dev/english
									App.error("Language file " + language + " : " + err);
									return data;
								});
							}
							return data;
						});
						
						that.depPromise.devices = that.get("deviceCollection").load().then(function () {
//							that.trigger("toset:currentGateway", that.get("deviceCollection"));
							
//							var devicesPromise = that.get('deviceCollection').filterByCurrentGatewayInDashboard().getPropertiesPromise();

//							return devicesPromise.then(function () {
								return userPromise.then(function () {
									that.trigger("loaded:deviceCollection", that.get("deviceCollection"));

									return that.get("deviceCollection");
								});
//							});
						});

						App.vent.trigger("authenticated");

						that.depPromise.groups = that.get("groupCollection").load(true);
						that.depPromise.shares = that.get("shareCollection").load(true);

						that.depPromise.devices.then(function () {

							that.listenTo(App.salusConnector, "changed:currentGateway", that.doOnGatewayChange);
							
 							App.salusConnector.loadDashboardData().then(function () {
								that.depPromise.groups.then(function() {
									var tileOrderGroup = App.salusConnector.getGroupCollection().getTileOrderGroup();
									if (tileOrderGroup === null) {
										App.salusConnector.getGroupCollection().createTileOrderGroup().then(function(){
											that.get("groupCollection").refresh(true);
										});
									}
									that.listenTo(App.salusConnector.getSessionUser().get("tileOrderCollection"), "tileOrder:modification", that.updateTileOrderGroup);
								});
							});
							
							this.deviceRefreshPromise = P.all(App.salusConnector.getDeviceCollection().map(function (device) {
								return device.getDevicePropertiesPromise();
							})).then(function(){
								that.dataRefreshManager.initiateDevicePoll();
							});

							if (App.salusConnector.getTSCCollection()) {
								that.depPromise.tscs = App.salusConnector.getTSCCollection().load(true);
							}

							if (App.salusConnector.getRuleCollection()) {
								that.depPromise.rules = App.salusConnector.getRuleCollection().load(true).then(function () {
									that.trigger("loaded:ruleCollection");
								});
							}

							if (App.salusConnector.getRuleGroupCollection()) {
								that.depPromise.ruleGroups = App.salusConnector.getRuleGroupCollection().load(true).then(function () {
									that.trigger("loaded:ruleGroupCollection");
								});
							}
							
							//only in the equipment page load device schedule
							//that.get("deviceCollection").loadSchedules(true);
							
							return that.get("deviceCollection");
						});
						
						var cannedAlerts = [
							{
								name: "Sample Alert 1",
								deviceIdRef: 55219,
								dateTime: new Date(),
								alarmSounding: true
							},
							{
								name: "Sample Alert 2",
								deviceIdRef: 55220,
								dateTime: new Date()
							}
						];

						that.get('alertCollection').add(cannedAlerts);

						P.all([
							that.depPromise.devices,
							that.depPromise.groups,
							that.depPromise.shares
						]).catch(function (err) {
							App.warn("Ayla Connector: Showing error page.");
//							App.Consumer.Controller.showErrorPage(constants.errorTypes.REQUEST, {
//								status: err.status,
//								statusText: err.statusText,
//								response: err.responseText,
//								url: ""
//							});
						});

						return userPromise;

					} else {
						that.getSessionUserPromise().then(function () {
							App.vent.trigger("onLogin");
						});
						// data collection is off and site is unusable
						that.depPromise.devices = P.resolve(that.get("deviceCollection"));
						that.depPromise.groups = P.resolve(that.get("groupCollection"));
						that.depPromise.shares = P.resolve(that.get("shareCollection"));
						App.navigate("settings/dataCollection");
					}
				}).catch(function () {
					App.vent.trigger("authenticated");
				});
			},
			/**
			 * login with a username and password (web use case)
			 * @param username the user email address
			 * @param password the plain-text user password
			 * @returns {*} promise
			 */
			//TODO: this should not work on app
			loginWithUsername: function (username, password) {
				return this._sessionManager.loginWithUsername(username, password);
			},
			/**
			 * log in with one time token (app use case)
			 * @param token one time app connection token
			 */
			//TODO: this should not work on web
			loginWithToken: function (/*token*/) {
				throw new Error("method not implemented, API not avalible");
			},

			/**
			 * log out of the current user
			 */
			logout: function () {
				return this._sessionManager.logout().then(this._cleanUpAylaProperties).then(function () {
					App.vent.trigger("logout");
				});
			},
			/**
			 * construct a new user for the new user flow
			 * @returns {AylaUser}
			 */
			newUser: function () {
				if (this.get("currentUser")) {
					throw new Error("why are we creating a new user with an existing user session?");
				}

				var user = new AylaUser(null, {isNewUser: true});
				return user;
			},

			unsetCurrentUser: function () {
				this.unset("currentUser");
			},

			doOnGatewayChange: function () {
				var that = this;
				
				if (this.deviceRefreshPromise && this.deviceRefreshPromise.isPending()) {
					this.deviceRefreshPromise.cancel();
				}
				
				if (this.dataRefreshManager.dataRefreshPromise()) {
					this.dataRefreshManager.dataRefreshPromise().cancel();
				}
				this.dataRefreshManager.clearTimeout();

				App.salusConnector.clearCurrentCollections();
                
				this.depPromise.groups.then(function() {
					var tileOrderGroup = App.salusConnector.getGroupCollection().getTileOrderGroup();
					if (tileOrderGroup === null) {
						App.salusConnector.getGroupCollection().createTileOrderGroup().then(function(){
							that.get("groupCollection").refresh(true);
						});
					}
				});
				
				if(App.getCurrentGateway()){
                    App.getCurrentGateway().loadTimeZone();
                }
				
				this.deviceRefreshPromise = P.all(App.salusConnector.getDeviceCollection().map(function (device) {
					return device.getDevicePropertiesPromise();
				})).then(function(){
					that.dataRefreshManager.initiateDevicePoll();
				});
				
				if (App.salusConnector.getTSCCollection()) {
					App.salusConnector.getTSCCollection().refresh(true);
				}

				if (App.salusConnector.getRuleCollection()) {
					App.salusConnector.getRuleCollection().refresh(true);
				}

				if (App.salusConnector.getRuleGroupCollection()) {
					App.salusConnector.getRuleGroupCollection().refresh(true);
				}
			},
			
			updateTileOrderGroup: function () {
				var promiseArray = [],
					groupCurrentKeys = [],
					groupSelectedKeys = [];
				
				var tileOrderGroup = App.salusConnector.getGroupCollection().getTileOrderGroup();
				groupCurrentKeys = tileOrderGroup.get("devices");
				
				if (groupCurrentKeys.length === 0) {
					return P.reject("Group devices is 0");
				}
				
				var devices = App.salusConnector.getDeviceCollection(true);
				devices.map(function (device) {
					groupSelectedKeys.push(device.get("key"));
                });
				
				var addThese = _.difference(groupSelectedKeys, groupCurrentKeys),
					removeThese = _.difference(groupCurrentKeys, groupSelectedKeys);

				_.each(addThese, function (key) {
					promiseArray.push(tileOrderGroup.addDevice(key));
				});

				_.each(removeThese, function (key) {
					promiseArray.push(tileOrderGroup.removeDevice(key));
				});

				return P.all(promiseArray);
            },

			/**
			 * get the currently user. This will return a promise. In the case that we already know the user, this
			 * promise will be fulfilled immediately, in the case we need to request this data it will be fofilled
			 * once data is loaded
			 * @returns {*}
			 */
			getSessionUserPromise: function () {
				var result = PromiseHelper.getModelAsPromise(this.get("currentUser"), AylaUser, null);
				this.set("currentUser", result.model);
				return result.promise;
			},

			_cleanUpAylaProperties: function () {
				this.unset("currentUser");
				this.unset("shareCollection");
				this.unset("deviceCollection");
				this.unset("groupCollection");
				this.unset("alertCollection");
			}
		});
	});


	return App.Models.AylaConnector;
});
