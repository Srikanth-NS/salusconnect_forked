"use strict";

/**
 * The salus connector is for talking to TBD salus services. The intent is that this is a singleton for the app.
 * TODO:
 *  - should salus connector broker ayla connector calls, or just expose the ayla connector?
 */
define([
    "app",
    "common/util/salusAjaxQueue",
    "bluebird",
    "common/constants",
    "common/model/api/SessionManager",
    "common/model/api/AylaConnector",
    "common/model/api/SalusWSConnector",
    "common/util/CookieMgr",
    "common/AylaConfig",
    "moment",
    "consumer/consumer.controller"
], function(App, sAjaxQueue, P, constants, SessionManager, AylaConnector, SalusWSConnector, CookieMgr, AylaConfig, moment) {

    App.module("Models", function(Models, App, B, Mn, $, _) {
        /**
         * development method so we can keep an eye on how long api calls take. This function will return the time in ms
         * with the best resolution we are able to manage
         * @type {Function}
         */
        var getCurrentTimeFunc = function() {
            if (window.performance && window.performance.now) {
                return window.performance.now();
            } else {
                return Date.now();
            }
        };

        /**
         * development method so we can keep an eye on how long api calls take. This will log the elapsed time
         * @type {Function}
         */
        var recordApiCallTime = function(success, start, end, url) {
            var duration = end - start;
            if (App.isDevOrInt() || duration >= 1000) {
                var path = _.last(url.split("/"));
                App.log("call to ayla (" + path + ") took " + duration + " ms to " + (success ? "succeed" : "fail"));
            }
        };


        Models.SalusConnector = B.Model.extend({
            initialize: function() {
                var that = this;

                // take over backbone.ajax and send it here. This allows us to inject authentication info into
                // each request
                B.ajax = function(options) {
                    return that.ajax(this, options);
                };

                _.bindAll(this, "_handleUnauthorizedResponse", "_handleTooManyRequests", "getSessionUserPromise");
                this._sessionManager = new SessionManager();

                this._aylaConnector = new AylaConnector(null, this._sessionManager);
                this._salusWSConnector = new SalusWSConnector(null, this._sessionManager);

                this.set("currentGateway", null);
                this.listenTo(this._aylaConnector, "loaded:ruleCollection", function() {
                    that.trigger("loaded:ruleCollection");
                });

                this.listenTo(this, "change:currentGateway", function() {
                    var currentGateway = App.getCurrentGatewayDSN();

                    if (currentGateway !== constants.noGateway) {
                        window.localStorage.setItem("currentGateway", App.getCurrentGatewayDSN());
                    }
                });

//				this.listenTo(this._aylaConnector, "toset:currentGateway", function(deviceCollec) {
//                    var userGatewayDsn = window.localStorage.getItem("currentGateway");
//
//                    if (userGatewayDsn) {
//                        var currentGateway = that.getDevice(userGatewayDsn);
//
//                        if (!currentGateway) {
//                            that.set("currentGateway", deviceCollec.getGateways().first());
//                        } else {
//                            that.set("currentGateway", currentGateway);
//                        }
//                    } else if (deviceCollec.getGateways().first()) {
//                        that.set("currentGateway", deviceCollec.getGateways().first());
//                    }
//                });

                this.listenToOnce(this._aylaConnector, "loaded:deviceCollection", function(deviceCollec) {
					App.justLogIn = true; //Avoid trigger "changed:currentGateway" when on login

                    var userGatewayDsn = window.localStorage.getItem("currentGateway");

                    if (userGatewayDsn) {
                        var currentGateway = that.getDevice(userGatewayDsn);

                        if (!currentGateway) {
                            that.set("currentGateway", deviceCollec.getGateways().first());
                        } else {
                            that.set("currentGateway", currentGateway);
                        }
                    } else if (deviceCollec.getGateways().first()) {
                        that.set("currentGateway", deviceCollec.getGateways().first());
                    }

                    // load timezones for all gateway
                    deviceCollec.getGateways().each(function(gateway) {
                        gateway.loadTimeZone();
                    });

                    that.trigger("set:currentGateway");
                    App.vent.trigger("onLogin");
                });

				this.listenTo(this._aylaConnector.get("deviceCollection"), "remove", function (model) {
						if (model === that.get("currentGateway")) {
							var gatewayCollection  = this.getGatewayCollection();
							if ( gatewayCollection && gatewayCollection.lenght > 0) {
								this.nextGateway();
							} else {
								this.set("currentGateway", null);
							}
						}
				});
            },
            setKeepSignedIn: function(keepSignedIn) {
                this._sessionManager.set("keepLoggedIn", keepSignedIn);
            },
            /**
             * starts the session manager
             */
            startSessionManager: function() {
                this._sessionManager.start();
            },
            getWeatherForDevice: function(device) {
                return this._salusWSConnector.getWeatherForDevice(device, true);
            },
            getPhotoUploadUrl: function() {
                return this._salusWSConnector.getPhotoUploadUrl();
            },
            /**
             * login with a username and password (web use case)
             * @param username the user email address
             * @param password the plain-text user password
             * @returns {*}
             */
            //TODO: this should not work on app
            loginWithUsername: function(username, password) {
                return this._aylaConnector.loginWithUsername(username, password);
            },
            /**
             * log in with one time token (app use case)
             * @param token one time app connection token
             */
            //TODO: this should not work on web
            loginWithToken: function(token) {
                return this._aylaConnector.loginWithToken(token);
            },
            /**
             *  return boolean if tokens cookie exists in the browser storage
             */
            hasStoredToken: function() {
                return !(!window.localStorage.getItem("tokens") && !window.sessionStorage.getItem("tokens"));
            },
            firstTimeLoginRouteCheck: function() {
                App.salusConnector.getDataLoadPromise(["devices"]).then(function() {
                    var devices = App.salusConnector.getFullDeviceCollection();
                    if (devices.getGateways().length <= 1) {
                        var nonGatewayDevices = devices.filter(function(device) {
                            return device.modelType === constants.modelTypes.GATEWAY ||
                                device.modelType === constants.modelTypes.COORDINATORDEVICE ||
                                device.modelType === constants.modelTypes.GATEWAYNODE;
                        });

                        if (nonGatewayDevices.length === 0) {
                            App.navigate("setup");
                            // short circuiting the dashboard navigate
                            return true;
                        }
                    }
                });

                if (App.onMobile() && window.location.hash === "#dashboard") {
                    App.navigate("");
                } else {
                    App.navigate("dashboard");
                }
            },
            /**
             * log out of the current user
             */
            logout: function() {
                return this._aylaConnector.logout();
            },
            /**
             * returns boolean if currently logged in
             */
            isLoggedIn: function() {
                return this._sessionManager.get("isLoggedIn");
            },
            isLoggingIn: function() {
                return this._sessionManager.get("isLoggingIn");
            },
            parseRule: function(rule) {
                return this._salusWSConnector.ruleParser.parseRule(rule);
            },
            buildRule: function(rule) {
                return this._salusWSConnector.ruleBuilder.buildRule(rule);
            },
            /**
             * get the currently user. This will return a promise. In the case that we already know the user, this
             * promise will be fulfilled immediately, in the case we need to request this data it will be fofilled
             * once data is loaded
             * @returns {*}
             */
            newUser: function() {
                return this._aylaConnector.newUser();
            },
            setAylaUser: function(user) {
                if (user instanceof B.Model) {
                    this._aylaConnector.set("currentUser", user);
                }
            },
            unsetAylaUser: function() {
                return this._aylaConnector.unsetCurrentUser();
            },
            setAylaUserPassword: function(password) {
                this._aylaConnector.set("currentPassword", password);
            },
            getAylaUserPassword: function() {
                return this._aylaConnector.get("currentPassword");
            },
            /**
             * confirm the email token ownership
             * @param token
             */
            confirmUserWithEmailVerificationToken: function(token) {
                return this._sessionManager.confirmUserWithEmailVerificationToken(token);
            },
            resendConfirmationToken: function(email) {
                return this._sessionManager.resendConfirmationToken(email);
            },
            sendResetPasswordToken: function(email) {
                return this._sessionManager.sendResetPasswordToken(email);
            },
            setPasswordWithToken: function(pass1, pass2, token) {
                return this._sessionManager.setPasswordWithToken(pass1, pass2, token);
            },
            getSessionUser: function() {
                return this._aylaConnector.get("currentUser");
            },
            getSessionUserPromise: function() {
                return this._aylaConnector.getSessionUserPromise();
            },
            setDataLoadPromise: function(type, promise) {
                this._aylaConnector.depPromise[type] = promise;
            },
            getDoOnLoginPromise: function() {
                return this._aylaConnector.doOnLoginPromise;
            },
            addDeviceToCollection: function(gateway) {
                this._aylaConnector.get("deviceCollection").add(gateway);
            },
            getDevicesOnGateway: function(gatewayObj) {
                if (_.isString(gatewayObj) || _.isNumber(gatewayObj)) {
                    gatewayObj = this.getDevice(gatewayObj);
                }

                return new Models.AylaDeviceCollection(this._aylaConnector.get("deviceCollection").filter(function(dev) {
                    return dev.get("gateway_dsn") === gatewayObj.get("dsn") || dev === gatewayObj;
                }));
            },
            getDataLoadPromise: function(dependencies) {
                if (!dependencies) {
                    return P.resolve([]);
                }

                var that = this,
                    promiseArray = [];
                _.each(dependencies, function(dependency) {
                    promiseArray.push(that._aylaConnector.depPromise[dependency]);
                });

                return this.getDoOnLoginPromise().then(function() {
                    return P.all(promiseArray);
                });
            },
            getDevice: function(value) {
                if (!value) {
                    return null;
                }

                var findWhereParam = {};

                if (_.isNumber(value)) {
                    findWhereParam = {
                        key: value
                    };
                } else if (_.isString(value)) {
                    findWhereParam = {
                        dsn: value
                    };
                } else if (value instanceof B.Model) {
                    return value;
                } else {
                    return null;
                }

                return this._aylaConnector.get('deviceCollection').findWhere(findWhereParam);
            },
            getDeviceByKey: function(key) {
                var intKey = parseInt(key);
                return this._aylaConnector.get('deviceCollection').findWhere({
                    "key": intKey
                });
            },
            getDeviceByEUID: function(EUID) {
                if (!EUID || !_.isString(EUID) || !this.getFullDeviceCollection()) {
                    return null;
                }

                return this.getFullDeviceCollection().getDeviceByEUID(EUID);
            },
            isSiteIdle: function() {
                return this._aylaConnector.dataRefreshManager.isRunning();
            },
            clearCurrentCollections: function() {
                this._currentDeviceCollection = null;
                this._currentGroupCollection = null;
            },
            triggerRule: function(rule) {
                var gatewayNode = App.getCurrentGateway().getGatewayNode(),
                    triggerKey = rule.get("ruleTriggerKey");

                if (gatewayNode && gatewayNode.get("TriggerRule")) {
                    return gatewayNode.setProperty("TriggerRule", triggerKey);
                }
            },
            /**
             * get all devices on current gateway
             * @returns {*}
             */
            getDeviceCollection: function(isInDashboard) {
                if (isInDashboard) {
                    return this._aylaConnector.get("deviceCollection").filterByCurrentGatewayInDashboard();
                } else {
                    return this._aylaConnector.get("deviceCollection").filterByCurrentGateway();
                }
            },
            /**
             * get current gateway device collection minus coordinator and gw node
             * should be used for display purposes
             * @returns {*}
             */
            getUserDeviceCollection: function() {
                return this.getDeviceCollection().filterOutDelegates();
            },
            getWindowSensorCollection: function (){
                return this.getDeviceCollection().getWindowSensor();
            },
            /**
             * get all devices on account
             * @returns {*}
             */
            getFullDeviceCollection: function() {
                return this._aylaConnector.get("deviceCollection");
            },
            getInteractableDevices: function() {
                return this.getUserDeviceCollection().filterForInteractableDevices();
            },
            getAllInteractableDevices: function() {
                return this.getFullDeviceCollection().filterForInteractableDevices();
            },
            /**
             * devices that work in an 'action' sense - for rules
             * @returns {*}
             */
            getActionableDevices: function() {
                return this.getUserDeviceCollection().filterForActionableDevices();
            },
            getDeviceCollectionSortedByCategory: function() {
                return this.getUserDeviceCollection().getDevicesSortedByCategory();
            },
            getGatewayCollection: function() {
                return this._aylaConnector.get("deviceCollection").getGateways();
            },
            getThermostatCollection: function() {
                return this.getDeviceCollection().getThermostats();
            },
            getTRVCollection: function() {
                return this.getDeviceCollection().getTRVs();
            },
            getTRVCollectionWithLive: function() {
                return this.getDeviceCollection().getTRVsWithLive();
            },
            getWaterHeaterCollection: function() {
                return this.getDeviceCollection().getWaterHeaters();
            },
            getSmartPlugCollection: function() {
                return this.getDeviceCollection().getSmartPlugs();
            },
            /**
             * @param hex - base 16 string
             * @returns {*}
             */
            getTRVByShortID: function(hex) {
                return this.getTRVCollection().findByShortID(hex);
            },
            /**
             * check a trv is associated to any it600
             * @param trv - device model
             */
            getTRVAssociatedToIT600s: function(trv) {
                return this.getUserDeviceCollection().getIT600s().findTRV(trv);
            },
            getTSCCollection: function() {
                var gateway = App.getCurrentGateway();

                return gateway ? gateway.get("tscCollection") : null;
            },
            addToRuleCollection: function(model) {
                this.getRuleCollection().add(model);
            },
            getDisplayRuleCollection: function() {
                var gateway = App.getCurrentGateway();

                return gateway ? gateway.get("ruleCollection").getNonHiddenRules() : null;
            },
            getRuleCollection: function() {
                var gateway = App.getCurrentGateway();

                return gateway ? gateway.get("ruleCollection") : null;
            },
            getRuleGroupCollection: function() {
                var gateway = App.getCurrentGateway();

                return gateway ? gateway.get("ruleGroupCollection") : null;
            },
            getRuleLoadPromise: function() {
                return this._aylaConnector.depPromise.rules;
            },
            /**
             * series of helper functions for displaying recommended rules
             */
            shouldListRecommendedRules: function() {
                return this.getDeviceCollection().some(function(device) {
                    return device.supportsRecommendedRules();
                });
            },
            hasSmartPlugs: function() {
                var smartPlugs = this.getDeviceCollection().filter(function(device) {
                    return device.isSmartPlug();
                });

                return !!smartPlugs.length;
            },
            hasThermostats: function() {
                var thermostats = this.getDeviceCollection().filter(function(device) {
                    // no it600s
                    return device.modelType === constants.modelTypes.THERMOSTAT;
                });

                return !!thermostats.length;
            },
            hasIt600Thermostats: function() {
                var thermostats = this.getDeviceCollection().filter(function(device) {
                    // no it600s
                    return device.modelType === constants.modelTypes.IT600THERMOSTAT;
                });

                return !!thermostats.length;
            },
            hasWindowsSensor: function() {
                var windowSensor = this.getDeviceCollection().filter(function(device) {
                    // no it600s
                    return device.modelType === constants.modelTypes.WINDOWMONITOR;
                });

                return !!windowSensor.length;
            },
            hasWaterHeaters: function() {
                return !!this.getWaterHeaterCollection().length;
            },
            getMyStatus: function() {
                var gateway = App.getCurrentGateway();
                if (gateway) {
                    var node = gateway.getGatewayNode();
                    if (node) {
                        return node.get("MyStatus").get("value");
                    }
                }
                return false;
            },
            setMyStatus: function(statusKey) {
                var gateway = App.getCurrentGateway();

                if (gateway) {
                    var node = gateway.getGatewayNode();

                    if (node) {
                        return node.getDevicePropertiesPromise().then(function() {
                            return node.setProperty("MyStatus", statusKey);
                        });
                    }
                }

                return P.resolve();
            },
            getFullGroupCollection: function() {
                return this._aylaConnector.get("groupCollection");
            },
            getGroupCollection: function() {
                return this._aylaConnector.get("groupCollection").filterByCurrentGateway();
            },
            getGroup: function(groupId) {
                if (!groupId) {
                    return null;
                }

                var findWhereParam = {};

                if (_.isNumber(groupId)) {
                    findWhereParam = {
                        key: groupId
                    };
                } else if (_.isString(groupId)) {
                    findWhereParam = {
                        name: groupId
                    };
                } else {
                    return null;
                }

                return this.getFullGroupCollection().findWhere(findWhereParam);
            },
            getRule: function(ruleId) {
                if (!ruleId || !_.isString(ruleId) || !this.getRuleCollection()) {
                    return null;
                }

                return this.getRuleCollection().findWhere({
                    key: ruleId
                });
            },
            getRuleByTimerKey: function(timerKey) {
                if (!timerKey || !_.isString(timerKey) || !this.getRuleCollection()) {
                    return null;
                }

                return this.getRuleCollection().findWhere({
                    ruleTriggerKey: timerKey
                });
            },
            getAlertCollection: function() {
                return this._aylaConnector.get("alertCollection");
            },
            getNotificationCollection: function() {
                return this._aylaConnector.get("notificationCollection");
            },
            getDevicesWithoutGroup: function() {
                return this.getGroupCollection().filterByNotTileOrderGroup().getDevicesWithoutGroupFrom(this.getUserDeviceCollection());
            },
            getIndividualControlThermostats: function() {
                return this.getTSCCollection() ? this.getTSCCollection().getThermostatsWithIndividualControls() : this.getThermostatCollection();
            },
            getExistEuidIndividualControlThermostats: function() {
                var allIndividualControlThermostats=this.getIndividualControlThermostats();
                allIndividualControlThermostats.models=_.filter(allIndividualControlThermostats.models,function (device){
                    if(device.getEUID()){
                    	return true;
                    }
                });
                
                return allIndividualControlThermostats;
                
            },
            getShareCollection: function() {
                return this._aylaConnector.get("shareCollection");
            },
            getSharedUsersEmails: function() {
                return this.getShareCollection().getSharedUsersEmails();
            },
            // returns current Gateway and it's coordinator & gateway node
            getGatewayAndDelegates: function() {
                return this.getDeviceCollection().getGatewayAndDelegates();
            },
            pinToDashboard: function(reference, referenceType) {
                this.getSessionUser().addToTileOrder(reference, referenceType);
            },
            unpinFromDashboard: function(reference, referenceType) {
                this.getSessionUser().get("tileOrderCollection").findAndRemove(reference, referenceType);
            },
            loadDashboardData: function() {
                return this.getSessionUser().get("tileOrderCollection").load();
            },
            changeCurrentGateway: function(newGateway) {
                var nextGateway;
                if (_.isString(newGateway) || _.isNumber(newGateway)) {
                    nextGateway = this.getDevice(newGateway);
                } else if (newGateway instanceof B.Model) {
                    nextGateway = newGateway;
                } else {
                    throw Error("Please pass either a gateway model, gateway dsn, or gateway key");
                }

                this.set("currentGateway", nextGateway);
            },
            nextGateway: function() {
                var gateways = this.getGatewayCollection(),
                    currentGateway = this.get("currentGateway");

                var index = gateways.indexOf(currentGateway);

                // cycling between 0 and the length of gateways - 1
                index = (index + 1) % gateways.length;

                this.changeCurrentGateway(gateways.at(index));
            },
            /**
             * Adds salus/ayla Authorization headers to the passed in options.
             * Will not override existing Authorization header.
             * @param options
             */
            addAuthHeaders: function(options) {
                var token = this._sessionManager.get("accessToken");
                if (token) {
                    options.headers = options.headers || {};
                    if (!options.headers.Authorization) {
                        //_.extend(options.headers, {"Authorization": "auth_token " + token});
                        options.headers.Authorization = "auth_token " + token;
                    }
                }
            },
            makeDelayedAjaxCall: function(delayedMs, url, data, verb, dataType, extraOptions) {
                var deferred = P.pending(),
                    that = this;

                setTimeout(function() {
                    deferred.resolve(that.makeAjaxCall(url, data, verb, dataType, extraOptions));
                }, delayedMs);

                return deferred.promise;
            },
            /**
             * internal ajax implementation - we add some headers here prior to sending off the ajax call via jquery.ajax.
             * This implementation also manages retrying calls if the authentication token expires.
             * @param context the "this" ajax was called with
             * @param options the options object
             * @returns {*}
             */
            ajax: function(context, options, retryCount) {
                var startTime, success, mySuccess, error, myError,
                    that = this;

                options = options || {};

                if (retryCount && retryCount >= 3) { // todo, is this the right thing to do?
                    options.error();
                    return;
                }

                this.addAuthHeaders(options);

                if (!options.prevent401Reauthoization) {
                    options.statusCode = {
                        // register a special 401/unauthorized handler to log us back in
                        401: function(request, textStatus, errorType) {
                            that._handleUnauthorizedResponse(context, options, request, textStatus, errorType, retryCount);
                        },
                        429: function(request, textStatus, errorType) {
                            that._handleTooManyRequests(context, options, request, textStatus, errorType, retryCount);
                        }
                    };
                } else {
                    options.statusCode = {
                        429: function(request, textStatus, errorType) {
                            that._handleTooManyRequests(context, options, request, textStatus, errorType, retryCount);
                        }
                    };
                }

                //Explicitly check for undefined to allow false as a valid contentType for file uploads.
                if (options.contentType === undefined) {
                    options.contentType = "application/json";
                }

                if (_.isObject(options.queryString)) {
                    _.each(_.keys(options.queryString), function(key, index) {
                        var leadingSymbol;
                        if (index === 0 && options.url.indexOf("?") === -1) {
                            leadingSymbol = "?";
                        } else {
                            leadingSymbol = "&";
                        }

                        options.url += (leadingSymbol + key + "=" + encodeURIComponent(options.queryString[key]));
                    });
                }

                var stackObj = {};
                if (Error && _.isFunction(Error.captureStackTrace)) {
                    Error.captureStackTrace(stackObj);
                }

                return new P.Promise(function(resolve, reject, onCancel) {
                    success = options.success;
                    mySuccess = function(data, textStatus, jqXHR) {
                        recordApiCallTime(true, startTime, getCurrentTimeFunc(), options.url);
                        if (success && _.isFunction(success)) {
                            success(data, textStatus, jqXHR);
                        }
                        // return the response, status, and xhr inside of the promise
                        if (options.returnFullStatus) {
                            resolve({
                                data: data,
                                status: textStatus,
                                xhr: jqXHR
                            });
                        } else {
                            resolve(data, textStatus, jqXHR);
                        }
                    };
                    options.success = mySuccess;

                    error = options.error;
                    myError = function(jqXHR, textStatus, errorThrown) {
                        recordApiCallTime(false, startTime, getCurrentTimeFunc(), options.url);
                        if (error && _.isFunction(error)) {
                            error(jqXHR, textStatus, errorThrown);
                        }

                        if (App.isDevOrInt() && !options.allowAllRequestFailures && options.failureStatusCodes && options.failureStatusCodes.indexOf(jqXHR.status) < 0 && jqXHR.statusText !== "abort") {
                            //							App.Consumer.Controller.showErrorPage(constants.errorTypes.REQUEST, {
                            //								status: jqXHR.status,
                            //								statusText: jqXHR.statusText,
                            //								endpoint: options.url.substring(options.url.indexOf("salusconnect.io")),
                            //								response: jqXHR.responseText
                            //							});
                        }
                        // still fail the promise but the site will be in error mode
                        reject(jqXHR, textStatus, errorThrown);
                    };
                    options.error = myError;

                    startTime = getCurrentTimeFunc();

                    if (options.isLowPriority) {
                        $.sAjaxQueue(options);
                        onCancel(function() {
                            $.sAjaxQueue.clear();
                            $.sAjaxQueue.abort();
                        });
                    } else {
                        var xhr = $.ajax(options);

                        onCancel(function() {
                            xhr.abort();
                        });
                    }

                }).catch(function(err) {
                    var message = options.type + " " + options.url + " " + err.status + " (" + err.statusText + ") " +
                        "\nStack: " + stackObj.stack;

                    if (options.url && options.url.indexOf(AylaConfig.endpoints.log.create) !== -1) {
                        //Use console log to prevent infinite looping as app.error does the network call.
                        console.error(message);
                    } else {
                        App.error(message);
                    }

                    return P.reject(err);
                });
            },
            /**
             * make a get call with all the authentication tokens set
             * @param url
             * @param data
             * @returns {*}
             */
            makeAjaxGet: function(url, data, failureStatusCodes) {
                return this.makeAjaxCall(url, data, "get", null, null, failureStatusCodes);
            },
            /**
             * Make a form POST ajax style call with all authentication tokens needed to talk to Ayla.
             * @param url
             * @param formData
             * @returns {*}
             */
            makeFormAjaxCall: function(url, data) {
                var options = {
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST'
                };

                //TODO: TEMP FIX
                var origin = window.location.origin;
                url = url.replace("https://salus-web-services.appspot.com",
                    origin + "/salus-web-services.appspot.com");
                url = url.replace("https://eu-salus-web-svc.appspot.com",
                    origin + "/eu-salus-web-svc.appspot.com");

                options.data = data;
                options.url = AylaConfig.buildLocalURLForAyla(url);

                return B.ajax(options);
            },
            /**
             * make a generic ajax call with all authentication tokens needed to talk to Ayla set
             * @param url
             * @param data
             * @param verb GET|POST|PUT...
             * @returns {*}
             */
            makeAjaxCall: function(url, data, verb, dataType, extraOptions, failureStatusCodes) {
                //				if (!App.ip) {
                //					App.getIP();
                //				}

                var options = {};

                extraOptions = extraOptions || {};

                options.type = verb;
                options.data = data ? JSON.stringify(data) : "";
                options.url = AylaConfig.buildLocalURLForAyla(url);
                options.dataType = dataType || "json";
				options.timeout = 90000;
                options.allowAllRequestFailures = extraOptions.allowAllRequestFailures;
                options.failureStatusCodes = failureStatusCodes || [];
                options.returnFullStatus = extraOptions.returnFullStatus; // returns status, responseText, and xhr request in resolved promise if true
                options.isLowPriority = !!extraOptions.isLowPriority;
                options.queryString = extraOptions.queryString;
                options.headers = extraOptions.headers;
                options.prevent401Reauthoization = extraOptions.prevent401Reauthoization;
                //				options.beforeSend = function(request) {
                //					request.setRequestHeader("X-Forwarded-For", !!App.ip ? App.ip : "");
                //				};
                return B.ajax(options);
            },
            makeCordovaCall: function(method, paramList) {
                var that = this;

                if (!window.cordova) {
                    return P.reject("Cordova is not installed.");
                }

                return new P.Promise(function(resolve, reject) {
                    return that._sessionManager.makeCordovaCall(resolve, reject, method, paramList);
                }).then(function(data) {
                    App.log("makeCordovaCall -> " + method + ": Success");
                    App.log(data);
                    return data;
                }).catch(function(err) {
                    App.log("makeCordovaCall -> " + method + ": Error");
                    App.log(err);
                    return P.reject(err);
                });
            },
            saveDataCollection: function(dataCollectionOff) {
                return this._aylaConnector.saveDataCollection(dataCollectionOff);
            },
            refreshSession: function() {
                return this._sessionManager.refreshSession();
            },
            /**
             * handle the not-authorized case by trying to refresh our access token. This method will request a refresh from
             * the session manager and then recall the original ajax call
             * @param context the context that "ajax" was called from originally
             * @param options the ajax options
             * @param request the request that failed
             * @param textStatus status text from failed request
             * @param errorType the type of failure
             * @private
             */
            _handleUnauthorizedResponse: function(context, options, request, textStatus, errorType, retryCount) {
                var that = this;
                this._sessionManager.refreshSession().then(function() {

                    if (_.isNumber(retryCount)) {
                        retryCount = retryCount + 1;
                    } else {
                        retryCount = 0;
                    }

                    return that.ajax(context, options, retryCount);
                });
            },
            _handleTooManyRequests: function(context, options, request, textStatus, errorType, retryCount) {
                if (_.isNumber(retryCount)) {
                    retryCount = retryCount + 1;
                } else {
                    retryCount = 0;
                }

                return this.ajax(context, options, retryCount);
            },
            //Temporary Method: Shows a sample call to cordov's getDevices functions.
            //window.debugApp.salusConnector.getDeviceCollection().tempLoadCordova()
            tempLoadCordova: function() {
                return App.salusConnector.makeCordovaCall("getDevices", []);
            },
            //Temporary Method: window.debugApp.salusConnector.getDeviceCollection().tempLoadDeviceProps(53651)
            tempLoadDeviceProps: function(deviceKey) {
                deviceKey = deviceKey || 61720; //temporary.

                return App.salusConnector.makeCordovaCall("getProperties", [deviceKey]);
            },
            tempSetDataPoint: function(deviceKey, propKey, baseType, value) {
                deviceKey = deviceKey || 61720; //temporary.
                propKey = propKey || 846302;
                baseType = baseType || "integer";
                value = value || 1901;

                return App.salusConnector.makeCordovaCall("createDatapoint", [deviceKey, propKey, baseType, value]);
            },
            tempGetDatapoints: function(deviceKey, propKey, options) {
                deviceKey = deviceKey || 61720; //temporary.
                propKey = propKey || 846302;
                options = options || {};

                //If you want only 1 option send this.
                //options.count = 1;

                return App.salusConnector.makeCordovaCall("getDatapointsByActivity", [deviceKey, propKey, JSON.stringify(options)]);
            },
            log: function(obj) {
                return this._salusWSConnector.log(obj);
            },
            // Bug_SCS-3286	Use start instead of in-range for schedule
            /**
             * 获取当前gateway的时间
             * @returns {unresolved} "YYYY-MM-DD HH:mm:ss"
             */ 
            getCurrentGatewayDatetime: function (){
                var currentGateway=App.getCurrentGateway();
                
                var m=moment().utc().zone(currentGateway.get("utc_offset"));
                if(currentGateway.get("utc_dst")){
                    m=m.add(1,"hours");
                }
                
                return m.format("YYYY-MM-DD HH:mm:ss");
            },
            // Bug_SCS-3286 end
        });
    });

    return App.Models.SalusConnector;
});
