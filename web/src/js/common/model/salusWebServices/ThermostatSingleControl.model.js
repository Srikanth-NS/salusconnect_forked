"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/constants"
], function (App, P, AylaConfig, AylaBackedMixin, Constants) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.ThermostatSingleControlModel = B.Model.extend({
			defaults: {
				name: null,
				key: null,
				dsn: null,
				defaultDevice: null,
				devices: []
			},

			idAttribute: "key",

			getDefaultDevice: function () {
				if (this.get("devices").length > 0) {
					if (!this.get("defaultDevice")) {
						var EUID = this.get("devices")[0].EUID;
						this.set("defaultDevice", EUID);

						if (this.get("key")) {
							this.update(); // if we already have saved before update that with the default device, otherwise were a new tsc and will get saved later
						}
					}

					return App.salusConnector.getDeviceByEUID(this.get("defaultDevice"));
				}
			},

			_buildPayload: function () {
				var that = this,
						payload, isNotOneGateway, subPayload = {}, isNotUniformType,
						name = this.get("name"),
						devices = this.get("devices"),
						realDevices = _.map(devices, function (device) {
							return App.salusConnector.getDeviceByEUID(device.EUID);
						});

				if (name) {
					subPayload.name = name;
				} else {
					return P.reject("A linked thermostat must have a valid name.");
				}

				// break when we find a device with gateway_dsn that doesn't match TSC dsn
				isNotOneGateway = _.some(realDevices, function (device) {
					return device.get("gateway_dsn") !== that.get("dsn");
				}, true);

				// !isNotOneGateway -> all on one gateway
				if (devices.length > 0 && !isNotOneGateway) {

					var baseType = realDevices[0].modelType;

					isNotUniformType = _.some(realDevices, function (device) {
						return device.modelType !== baseType;
					});

					if (isNotUniformType) {
						return P.reject("A linked thermostat group must only be for a single type of thermostat.");
					}

					// get key and oem_model for devices array
					subPayload.devices = devices;
				} else {
					return P.reject("A linked thermostat must have at least one device, and all devices must be paired with the same gateway");
				}

				if (!this.get("defaultDevice")) {
					this.set("defaultDevice", this.get("devices")[0].EUID);
				}

				subPayload.defaultDevice = this.get("defaultDevice");
				payload = this.wrapApiData(subPayload);

				return payload;
			},

			load: function (isLowPriority) {
				var that = this,
						urlModel = { key: this.get('key'), dsn: this._getGatewayNodeDSN()};

				if (!urlModel.dsn) {
					return P.resolve();
				}

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.linkedThermostats.fetch)(urlModel), null, "GET", {isLowPriority: isLowPriority}).then(function (devData) {
					that.set(that.aParse(devData));
				});
			},

			add: function () {
				var that = this,
						payload = this._buildPayload(),
						urlModel = { dsn: this._getGatewayNodeDSN()};

				if (!urlModel.dsn) {
					return P.resolve();
				}

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.linkedThermostats.create)(urlModel), payload, "POST").then(function (devData) {
					that.trigger("created", that);
					that.set(that.aParse(devData));
                    setTimeout(function (){
                        that.resetDeviceProperty(App.salusConnector.getDeviceByEUID(devData.linkedDevices.defaultDevice));
                    }, 5000);
                    
					return that._clearSchedules();
				});
			},

			refresh: function (isLowPriority) {
				this._detailPromise = this.load(isLowPriority);

				return this._detailPromise;
			},

			/**
			 *  updates a linked Thermostat to the server
			 */
			update: function () {
				var  copyJSON,
						that = this,
						currDefaultDevice = App.salusConnector.getDeviceByEUID(this.get("defaultDevice")),
						payload = this._buildPayload(),
						urlModel = { dsn: this._getGatewayNodeDSN(), key: this.get("key")};

				if (!urlModel.dsn) {
					return P.resolve();
				}

				if (!this.hasThermostat(currDefaultDevice)) {
					copyJSON = currDefaultDevice.getSchedules().deepCopy();
					this.set("defaultDevice", this.get("devices")[0].EUID);
					payload.linkedDevices.defaultDevice=this.get("devices")[0].EUID;
				}

				payload.key = this.get("key");

				if (this.get("devices").length > 0) {
					return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.linkedThermostats.create)(urlModel), payload, "PUT").then(function (devData) {
						that.set(that.aParse(devData));
						that.trigger("created", that);
                        setTimeout(function (){
                            that.resetDeviceProperty(App.salusConnector.getDeviceByEUID(devData.linkedDevices.defaultDevice));
                        }, 5000);
						if (copyJSON) {
							return App.salusConnector.getDeviceByEUID(that.get("defaultDevice")).getSchedules().setFromCopy(copyJSON);
						}
					}).catch(function (e) {
						App.error(e);
					});
				} else {
					return this.unregister();
				}
			},
            
            resetDeviceProperty: function (device) {
                var propertyArr=[];
                propertyArr.push(device.get("MaxHeatSetpoint_x100"));
                propertyArr.push(device.get("MinCoolSetpoint_x100"));
                propertyArr.push(device.get("HeatingSetpoint_x100"));
                propertyArr.push(device.get("CoolingSetpoint_x100"));
                propertyArr.push(device.get("HoldType"));
                propertyArr.push(device.get("AutoHeatingSetpoint_x100"));
                propertyArr.push(device.get("AutoCoolingSetpoint_x100"));
                
                _.each(propertyArr,function (property){
                    if(property){
                        property.setProperty(property.getProperty());
                    }
                });
                
            },
			unregister: function () {
				var that = this,
						urlModel = { dsn: this._getGatewayNodeDSN(), key: this.get("key")};

				if (!urlModel.dsn) {
					return P.resolve();
				}

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.linkedThermostats.delete)(urlModel), null, "DELETE", "text").then(function () {
					that.trigger("sync", that);
					that.destroy();
				});
			},

			aParse: function (data) {
				var out, tsc = data.linkedDevices;

				out = {
					key: data.key,
					devices: tsc.devices,
					name: tsc.name,
					dsn: data.dsn || App.getCurrentGatewayDSN(),
					defaultDevice: tsc.defaultDevice || null
				};

				return out;
			},

			hasThermostat: function (deviceData) {
				// get EUID
				 var EUID = deviceData instanceof B.Model ? deviceData.getEUID() : deviceData;

				return !!_.findWhere(this.get("devices"), {EUID: EUID});
			},

			/**
			 * add a thermostat to the devices array
			 * @param device - model
			 * @returns {boolean}
			 */
			addThermostat: function (device) {
				if (!device || !(device instanceof B.Model)) {
					return false;
				}

				var tstatModels = [Constants.modelTypes.IT600THERMOSTAT, Constants.modelTypes.THERMOSTAT];

				// make sure thermostat
				if (_.indexOf(tstatModels, device.modelType) !== -1) {
					if (!this.hasThermostat(device)) {
						this.get("devices").push(device);
						return true;
					}
				}

				return false;
			},

			isLoaded: function () {
				return this._detailPromise && this._detailPromise.isLoaded();
			},

			getSchedules: function () {
				return this.getDefaultDevice().get("schedules");
			},

			isThermostat: function () {
				return true; // sorta true
			},

			_getGatewayNodeDSN: function () {
				var node = App.salusConnector.getDevice(this.get('dsn')).getGatewayNode();
				return node ? node.get("dsn") : null;
			},

			_clearSchedules: function () {
				var promiseArr = [], defaultDevice = this.getDefaultDevice();
				_.each(this.get("devices"), function (device) {
					if (device.EUID !== defaultDevice.getEUID()) {
						promiseArr.push(App.salusConnector.getDeviceByEUID(device.EUID).getSchedules().deleteAll());
					}
				});

				return promiseArr;
			},

			getSchedulesPromise: function () {
				return this.getDefaultDevice().getSchedulesPromise();
			}
		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "linkedDevices"
		});
	});

	return App.Models.ThermostatSingleControlModel;
});