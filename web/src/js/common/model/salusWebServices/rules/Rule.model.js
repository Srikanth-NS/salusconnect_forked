"use strict";

define([
	"app",
	"bluebird",
	"moment",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
    "common/model/salusWebServices/rules/recommendedRules",
    "common/constants",
	"common/model/ayla/mixin.IsInDashboard",
	"common/model/salusWebServices/rules/RuleDetails.model",
	"common/model/salusWebServices/rules/recommendedRules"
], function (App, P, moment, AylaConfig, AylaBackedMixin, ruleMap, constants) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.RuleModel = B.Model.extend({
			defaults: {
				name: null,
				key: null,
				conditions: null, // collection
				actions: null, // collection
				delayedActions: null, // collection
				dsn: null, // gateway dsn todo: consider moving rule collection to gateway node - rules are saved with node dsn
				unparsable: null,
				active: null,
				ruleTriggerKey: null
			},

			idAttribute: "key",
			tileType: "rule",
			ruleRefType: "rule",

			initialize: function (data, options) {
				var that = this;

				this.listenTo(this, "change:dsn", function (model, dsn) {
					var oldGateway = App.salusConnector.getDevice(that._oldGatewayDsn),
							gateway = App.salusConnector.getDevice(dsn);

					that._oldGatewayDsn = dsn;

					if (oldGateway) {
						that.stopListening(oldGateway);
					}

					if (gateway) {
						that.listenTo(gateway, "change:connection_status", function () {
							that.set("connection_status", gateway.get("connection_status"));
						});
						that.set("connection_status", gateway.get("connection_status"));
					}
				});

				if (options && options.dsn) {
					this.set("dsn", options.dsn);
				}

				_.bindAll(this, "saveRuleTriggers");

				this.isDirtyFromStatus = false;

				this.set("conditions", this.get("conditions") || new Models.RuleConditionCollection());
				this.set("actions", this.get("actions") || new Models.RuleActionCollection());
				this.set("delayedActions", this.get("delayedActions") || new Models.RuleDelayedActionsCollection());
			},
            
            /**
             * 检查是否在when,action,delay action中存在无效的device
             * @returns {undefined}
             */
            checkExistsInvalidDevice:function (){
               var conditions=_.find(this.get("conditions").models,function (condition){
                   
                   if(condition.get("EUID") && !App.salusConnector.getDeviceByEUID(condition.get("EUID"))){
                       return true;
                   }
               });
               
               if(conditions){
                   return true;
               }
               
               var actions=_.find(this.get("actions").models,function (action){
                   if(action.get("EUID") && !App.salusConnector.getDeviceByEUID(action.get("EUID"))){
                       return true;
                   }
               });
               
               if(actions){
                   return true;
               }
               
               var delayedActions=_.find(this.get("delayedActions").models,function (delayedAction){
                   if(delayedAction.get("data").EUID && !App.salusConnector.getDeviceByEUID(delayedAction.get("data").EUID)){
                       return true;
                   }
               });
               
               if(delayedActions){
                   return true;
               }
               
               return false;
               
            },

			load: function () {
				var that = this,
						url = this.getUrlForEndpoint(AylaConfig.endpoints.rule.fetch);

				return App.salusConnector.makeAjaxCall(url, null, "GET", "json").then(function (data) {
					that.set(that.aParse(data));
				});
			},

			deepCopy: function () {
				var rule = this.toJSON();

				if (rule.unparsable) {
					return this;
				}

				rule.conditions = new Models.RuleConditionCollection(this.get("conditions").toJSON());
				rule.actions = new Models.RuleActionCollection(this.get("actions").toJSON());

				// todo: this should be temporary. it is initializing a collection of length 1 when empty
				if (this.get("delayedActions") && !this.get("delayedActions").isEmpty()) {
					rule.delayedActions = new Models.RuleDelayedActionsCollection(this.get("delayedActions").toJSON());
				}

				return new Models.RuleModel(rule);
			},

			isActive: function () {
				return this.get("active");
			},

			toggleActive: function () {
				var active = this.get("active");

				this.set("active", !active);

				return this.update();
			},

			includesRunNow: function () {
				return !!this.get("ruleTriggerKey");
			},

			getConditions: function () {
				return this.get("conditions");
			},

			getActions: function () {
				return this.get("actions");
			},

			hasConditions: function () {
				return this.includesRunNow() || !this.get("conditions").isEmpty();
			},

			hasActions: function () {
				return !this.get("actions").isEmpty() || !this.get("delayedActions").isEmpty();
			},

			getStatusKeys: function () {
				return App.salusConnector.getDevice(this.get("dsn")).get("ruleGroupCollection").getRuleGroupKeysForRule(this.get("key"));
			},

			_buildPayload: function () {
				var payload = {
					rule: {}
				};

				if (this.get("name")) {
					payload.rule.name = this.get("name");
				} else {
					throw new Error("A rule must have a valid name.");
				}

				// check it is parsable first
				if (!this.get("unparsable")) {
					// we assume that there is a condition, because on Build it creates ruleTriggerKey
					if (this.hasActions()) {
						var rulePayload = App.salusConnector.buildRule(this);
						payload.rule.condition = rulePayload.conditions;
						payload.rule.action = rulePayload.actions;
						this.ruleToDelete = rulePayload.ruleToDelete;
						this.ruleToSave = rulePayload.ruleToSave;

					} else {
						throw new Error("Rule did not have any actions");
					}
				} else {
					// pass thru
					payload.rule.condition = this.get("conditions");
					payload.rule.action = this.get("actions");
					payload.rule.delayedActions = this.get("delayedActions");
				}

				if (this.get("key")) {
					payload.rule.key = this.get("key");
				}

				if (!_.isNull(this.get("active"))) {
					payload.rule.active = this.get("active");
				}
                
                if(this.get("windowProtection")){
                    payload.rule.windowProtection=this.get("windowProtection");
                }

				return payload;
			},

			saveRuleTriggers: function () {
				var promiseArray = this.getTriggers().map(function (trigger) {
					return trigger.persist();
				});

				return P.all(promiseArray);
			},

			add: function () {
				var that = this,
						url = this.getUrlForEndpoint(AylaConfig.endpoints.rule.create);

				return App.salusConnector.makeAjaxCall(url, this._buildPayload(), "POST", "json").then(function (data) {
					var promise = that.ruleToSave ? that.ruleToSave.add() : P.resolve();
					return promise.then(that.saveRuleTriggers).then(function () {
						that.trigger("save:rule", that);
						that.set(that.aParse(data.rule));
						App.salusConnector.getDevice(that.get("dsn")).get("ruleCollection").push(that);
						that._completeDelayedActions();
					});
				});
			},

			update: function () {
				var that = this,
						url = this.getUrlForEndpoint(AylaConfig.endpoints.rule.update);

				return App.salusConnector.makeAjaxCall(url, this._buildPayload(), "PUT", "json").then(function (data) {
					if (!that.get("unparsable")) {
						var promise;
						if (that.ruleToDelete) {
							promise = that.ruleToDelete.unregister();
						} else if (that.ruleToSave) {
							promise = that.ruleToSave.saveOrUpdate();
						} else {
							promise = P.resolve();
						}

						return promise.then(that.saveRuleTriggers).then(function () {
							that.trigger("save:rule", that);
							that.set(that.aParse(data.rule));
							that._completeDelayedActions();
						});
					} else {
						App.warn("Skipped over saving rule trigger information as this rule is unparsable.");

						that.trigger("save:rule", that);
						that.set(that.aParse(data.rule));
					}
				});
			},

			saveOrUpdate: function () {
				if (!this.get("key")) {
					return this.add();
				} else {
					return this.update();
				}
			},

			unregister: function () {
				var that = this,
						triggers, ruleToDelete;

				if (!this.get("unparsable")) {
					// when parsable load the triggers
					triggers = this.getTriggers();
				}

				if (this.get("timerTriggerKey")) {
					ruleToDelete = App.salusConnector.getRuleByTimerKey(this.get("timerTriggerKey"));
				}

				return App.salusConnector.makeAjaxCall(this.getUrlForEndpoint(AylaConfig.endpoints.rule.delete), null, "DELETE", "text").then(function () {
					var returnPromise;

					if (_.isUndefined(triggers)) {
						App.warn("Skipped over deleting rule trigger information as this rule is unparsable.");
					}

					if (triggers && triggers.length > 0) {
						returnPromise =  P.all(triggers.map(function (trigger) {
							return trigger.remove();
						}));
					} else {
						returnPromise = P.resolve("");
					}

					return returnPromise.then(function () {
						that.trigger("sync", that);
						App.salusConnector.getSessionUser().getCurrentGatewayTileOrder().findAndRemove(that, "rule");
						that.destroy();
						if (ruleToDelete) {
							return ruleToDelete.unregister();
						}
					});
				});
			},

			resetActions: function (action) {
				this.get("actions").reset([action]);
			},

			addCondition: function (data) {
                var type = data.type;
                var device = data.device;
                var that = this, conditions = this.get("conditions").models;
                _.each(conditions, function(condition) {    
                    if(condition && condition.get("type") === type) {
                        if(type === "propVal") {
                            if(condition.get("device") && device) {
                                if(condition.get("device").get("name") === device.get("name")) {
                                    that.get("conditions").remove(condition);
                                }
                            }
                        } else {
                            that.get("conditions").remove(condition);
                        }
                    }
                });
                this.get("conditions").add(data);
			},

			addAction: function (data) {
                var that = this, actions = this.get("actions").models;
                
                _.each(actions, function(action) {
                    if(action && data.type === "propChange") {
                        if(action.get("device") && data.device) {
                            if(action.get("propName").toString() === data.propName.toString()
                                    && action.get("device").get("name") === data.device.get("name")) {
                                that.get("actions").remove(action);
                            }
                        }
                    }
                });
				this.get("actions").add(data);
			},

			addDelayedAction: function (data) {
				if (!this.get("delayedActions")) {
					this.set("delayedActions", new Models.RuleActionCollection([data]));
				} else {
					this.get("delayedActions").add(data);
				}
			},

			getRecipients: function () {
				var returnObj = {
					"email": [],
					"sms": []
				};

				if (!this.get("unparsable")) { // only run when this when parsable
					this.get("actions").each(function (action) {
						var type = action.get("type");
						if (_.contains(["email", "sms"], type)) {
							returnObj[type] = _.union(returnObj[type], action.get("recipients"));
						}
					});
				}

				return returnObj;
			},

			isDirty: function () {
				return this.isDirtyFromStatus;
			},

			markAsDirty: function () {
				this.isDirtyFromStatus = true;
			},

			markAsClean: function () {
				this.isDirtyFromStatus = false;
			},

			getUrlForEndpoint: function (endpoint) {
				var gateway = App.salusConnector.getDevice(this.get("dsn")),
						dsn = gateway.getGatewayNodeDSN() || this.get("dsn"); // fallback to gateway dsn

				return _.template(endpoint)({
					dsn: dsn,
					key: this.get("key")
				});
			},
            
            generateRuleName: function(recommendedJSON, device, num) {
                var ruleName;
                var that = this;
                ruleName = App.translate(recommendedJSON.name + "Formatted", {name: device.get("name")}) + " " + (App.salusConnector.getDisplayRuleCollection().length + num + 1);

                var ruleCollec = App.salusConnector.getDisplayRuleCollection();
                var valid = _.some(ruleCollec.models, function(model) {
                    return model.get("name") === ruleName && model.get("key") !== that.get("key");
                });
                if(valid) {
                    num++;
                    return ruleName = this.generateRuleName(recommendedJSON, device, num);
                } else {
                    return ruleName;
                }
            },

			buildRuleFromRecommended: function (recommendedJSON, device, num) {
				recommendedJSON.name = this.generateRuleName(recommendedJSON, device, num);
                recommendedJSON.dsn =  App.getCurrentGatewayDSN();

				_.each(recommendedJSON.conditions, function (condition) {
					if (condition.type === "propChange") {
						condition.EUID = device.getEUID();
						condition.propName =  device.getPropertyName(condition.propName);
					} else if (condition.type === "tod") {
						var startMoment = moment(condition.startTime, "HH:mm:ss"),
								endMoment = moment(condition.endTime, "HH:mm:ss");

						// local to utc
						condition.startTime = parseInt(startMoment.format("HHmmss"));
						condition.endTime = parseInt(endMoment.format("HHmmss"));
					}
				});

				_.each(recommendedJSON.actions, function (action) {
					if (action.type === "propChange") {
						action.EUID = device.getEUID();
                        if(_.isArray(action.propName)){
                            action.propName=_.map(action.propName,function (propName){
                                return device.getSetterPropNameIfPossible(propName);
                            })
                        }else{
                            action.propName =  device.getSetterPropNameIfPossible(action.propName);
                        }
						
					}
				});

				_.each(recommendedJSON.delayedActions, function (delayedAction) {
					if (delayedAction.subType === "propChange") {
						delayedAction.data.EUID = device.getEUID();
						delayedAction.data.propName =  device.getSetterPropNameIfPossible(delayedAction.data.propName);
					}
				});

				this.set(_.omit(recommendedJSON, ["conditions", "actions", "delayedActions", "description"]));

				this.get("conditions").reset(recommendedJSON.conditions);
				this.get("actions").reset(recommendedJSON.actions);
				this.get("delayedActions").reset(recommendedJSON.delayedActions);
			},
            
            //Dev_SBO-606 start
            buildWindowOpenRule: function (windowSensors, it600Thermostat, lock){
                
                var omitCondition=_.omit(ruleMap[constants.modelTypes.WINDOWMONITOR][0].conditions[0]);
                
                this.get("conditions").reset(ruleMap[constants.modelTypes.WINDOWMONITOR][0].conditions);
				this.get("actions").reset(ruleMap[constants.modelTypes.WINDOWMONITOR][0].actions);
                
                this.set("name", constants.hideOneTouchKey + "-" + it600Thermostat.getEUID() + "-" + it600Thermostat.get("dsn") + "-OR");
                this.set("windowProtection",ruleMap[constants.modelTypes.WINDOWMONITOR][0].windowProtection);
                
                for(var i=0; i<windowSensors.length; i++){
                    if(i!==0){
                        this.get("conditions").add(_.omit(omitCondition));
                    }
                    this.get("conditions").models[i].set("propName",windowSensors[i].getPropertyName(this.get("conditions").models[i].get("propName")));
                    this.get("conditions").models[i].set("EUID",windowSensors[i].getEUID());
                }
                
                if(!lock){
                    this.get("actions").remove(this.get("actions").models[1]);
                }
                
                _.each(this.get("actions").models,function (action){
                    action.set("EUID",it600Thermostat.getEUID());
                    action.set("propName",it600Thermostat.getSetterPropNameIfPossible(action.get("propName")));
                });
            },
            
            buildWindowCloseRule: function (windowSensors, it600Thermostat, unlock){
                
                var omitCondition=_.omit(ruleMap[constants.modelTypes.WINDOWMONITOR][1].conditions[0]);
                
                this.get("conditions").reset(ruleMap[constants.modelTypes.WINDOWMONITOR][1].conditions);
				this.get("actions").reset(ruleMap[constants.modelTypes.WINDOWMONITOR][1].actions);
                
                this.set("name", constants.hideOneTouchKey + "-" + it600Thermostat.getEUID() + "-" + it600Thermostat.get("dsn") + "-AND");
                this.set("windowProtection",ruleMap[constants.modelTypes.WINDOWMONITOR][1].windowProtection);
                
                for(var i=0; i<windowSensors.length; i++){
                    if(i!==0){
                        this.get("conditions").add(_.omit(omitCondition));
                    }
                    this.get("conditions").models[i].set("propName",windowSensors[i].getPropertyName(this.get("conditions").models[i].get("propName")));
                    this.get("conditions").models[i].set("EUID",windowSensors[i].getEUID());
                }
                
                if(!unlock){
                    this.get("actions").remove(this.get("actions").models[1]);
                }
                
                _.each(this.get("actions").models,function (action){
                    action.set("EUID",it600Thermostat.getEUID());
                    action.set("propName",it600Thermostat.getSetterPropNameIfPossible(action.get("propName")));
                });
            },
            //Dev_SBO-606 end

			aParse: function (data, options) {
				var returnObj, parsedRule = App.salusConnector.parseRule(data);

				if (!parsedRule.unparsable) {
					this._getTriggerData(parsedRule.actions, options);

					if (!this.get("conditions")) {
						this.set("conditions", new Models.RuleConditionCollection());
					}

					if (!this.get("actions")) {
						this.set("actions", new Models.RuleActionCollection());
					}

					if (!this.get("delayedActions")) {
						this.set("delayedActions", new Models.RuleDelayedActionsCollection());
					}

					this.get("conditions").set(parsedRule.conditions);
					this.get("actions").set(parsedRule.actions);
					this.get("delayedActions").set(parsedRule.delayedActions);

					this.uxhint = parsedRule.uxhint;
					this.hiddenRule = parsedRule.hiddenRule;

					returnObj = {
						name: parsedRule.name,
						key: parsedRule.key,
						conditions: this.get("conditions"),
						actions: this.get("actions"),
						delayedActions: this.get("delayedActions"),
						unparsable: false,
						timerTriggerKey: parsedRule.timerTriggerKey,
						active: parsedRule.active,
						ruleTriggerKey: parsedRule.ruleTriggerKey
					};

				} else {
					this.uxhint = parsedRule.uxhint;
					returnObj = parsedRule;
				}

				return returnObj;
			},

			getAlertTriggerKeys: function (actionData) {
				var returnArray = [];
				_.each(actionData, function (action) {
					if (action.type === "sms" || action.type === "email") {
						returnArray.push(action.triggerKey);
					}
				});

				return returnArray;
			},

			getTriggers: function (actionData) {
				if (!actionData && this.get("actions")) {
					actionData = this.get("actions").toJSON();
				}
				return App.salusConnector.getDevice(this.get("dsn")).get("ruleCollection").getTriggersForRule(this, actionData);
			},

			updateTriggers: function (triggers) {
				App.salusConnector.getDevice(this.get("dsn")).get("ruleCollection").updateTriggers(triggers);
			},

			pinToDashboard: function () {
				App.salusConnector.pinToDashboard(this, "rule");
			},

			unpinFromDashboard: function () {
				App.salusConnector.unpinFromDashboard(this, "rule");
			},

			getDisplayName: function () {
				return this.get("name");
			},

			_getTriggerData: function (actions, options) {
				var that = this;
				if (!this.get("dsn")) {
					this.set("dsn", options.dsn);
				}

				var triggers = this.getTriggers(actions);
				_.each(actions, function (action) {
					var trigger, type = action.type;

					if (type === "message" || type === "sms" || type === "email") {
						if (!triggers || triggers.length === 0) {
							that.set("unparsable", true);
						} else {
							trigger = triggers.findWhere({
								value: action.triggerKey
							});

							if (!trigger) {
								that.set("unparsable", true);
							} else {
								action.recipients = trigger.getRecipients();
								action.message = trigger.getMessage();
							}
						}
					}
				});
			},

			_completeDelayedActions: function () {
				var downStreamRule, downStreamAction, delayedAction = this.get("delayedActions");
				if (delayedAction && delayedAction instanceof B.Collection &&  delayedAction.length > 0) {
					delayedAction = delayedAction.first();
					downStreamRule = App.salusConnector.getRuleByTimerKey(delayedAction.get("timerTriggerKey"));

					if (!downStreamRule) {
						this.set("unparsable", true);
						return false;
					}

					downStreamAction = downStreamRule.get("actions").first();
					delayedAction.set("data", downStreamAction.toJSON());
					delayedAction.set("subType", downStreamAction.get("type"));
					this.get("delayedActions").trigger("reRender:collection");
				}
			}
		}).mixin([App.Models.IsInDashboardMixin, AylaBackedMixin], {
			apiWrapperObjectName: "rule"
		});
	});

	return App.Models.RuleModel;
});
