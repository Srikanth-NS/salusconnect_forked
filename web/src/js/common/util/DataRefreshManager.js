"use strict";

/**
 * The salus connector is for talking to TBD salus services. The intent is that this is a singleton for the app.
 * TODO:
 *  - should salus connector broker ayla connector calls, or just expose the ayla connector?
 */
define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/config",
	"common/util/PromiseHelper"
], function (App, P, AylaConfig, Config) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.DataRefreshManager = B.Model.extend({
			defaults: {
				isRunning: true,
				dataRefreshPromise: null
			},
			initialize: function () {
				_.bindAll(this, "refreshDevices", "initiateDevicePoll");
				this.deviceTimeout = null;
				this.refreshCount = 0;
			},

			initiateDevicePoll: function () {
				if (this.deviceTimeout) {
					this.clearTimeout();
				}
				this.deviceTimeout = setTimeout(this.refreshDevices, Config.devicePollingInformation.interval);
			},

			clearTimeout: function () {
				clearTimeout(this.deviceTimeout);
			},
			
			isRunning: function () {
				return this.get("isRunning");
			},
			
			dataRefreshPromise: function() {
				return this.get("dataRefreshPromise");
			},

			refreshDevices: function () {
				var that = this;
				var isDashboardPage = true;
				
				if (!App.onMobile() && window.location.pathname !== "/dashboard") {
					isDashboardPage = false;
				} else if (App.onMobile() && window.location.hash !== "#dashboard") {
					isDashboardPage = false;
				}
				
				if (!isDashboardPage) {
					this.initiateDevicePoll();
					return true;
				}
				
				var dataRefreshPromise = App.salusConnector.getGroupCollection().loadTileOrderGroupDatapoints().then(function () {
					App.salusConnector.getFullDeviceCollection().load();
					if (++that.refreshCount % Config.devicePollingInformation.fullRefreshCount === 0) {
						if (App.hasCurrentGateway()) {
							App.salusConnector.getSessionUser().fetchTileOrder();
							App.salusConnector.getRuleCollection().refresh(true);
							App.salusConnector.getRuleGroupCollection().refresh(true);
						}
					}
				}).finally(function() {
					that.initiateDevicePoll();
				});
				this.set("dataRefreshPromise", dataRefreshPromise);
			}
		});
	});


	return App.Models.DataRefreshManager;
});